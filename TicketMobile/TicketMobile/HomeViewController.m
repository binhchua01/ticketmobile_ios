//
//  HomeViewController.h
//
//
//  Created by THONT5 on 2/25/15.
//  Copyright (c) 2015 nguyentantho@live.com. All rights reserved.
//

#import "HomeViewController.h"
#import "LoginViewController.h"
#import "LeftMenuTableViewController.h"
#import "SiUtils.h"
#import "TicketMobile-Swift.h"

@interface HomeViewController ()

@end

@implementation HomeViewController

- (void)viewDidLoad {
    self.screenName = @"Login Screen";
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self countDownDuration];
    self.lblCoppy.text = @"Copyright \u24B8 2015 FPT Telecom";
    self.lblVersion.text=[NSString stringWithFormat:@"V %@",[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"]];

}

-(void)viewDidLayoutSubviews{
    if(IS_IPHONE4){
        //self.lblCoppy.frame = CGRectMake(self.lblCoppy.frame.origin.x, self.lblCoppy.frame.origin.y - 88, self.lblCoppy.frame.size.width, self.lblCoppy.frame.size.height);
    }
    
}

-(void)countDownDuration {
    Timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(TimerCount) userInfo:nil repeats:YES];
}

-(void)TimerCount{
    CountNumber =CountNumber + 1;
    if(CountNumber == 2){
        [Timer invalidate];
        [self GotoHome];
    }
    
}
- (void)GotoHome{    
    NSString *nib = @"LoginViewController2";
    if (IS_IPAD)
    {
        nib = @"LoginViewController_iPad";
    }

    LoginViewController2 *vc = [[LoginViewController2 alloc]initWithNibName:nib bundle:nil];
    //UINavigationController *nav = [[UINavigationController alloc]
    //                               initWithRootViewController:vc];
//    LeftMenuTableViewController *leftMenuViewController = [[LeftMenuTableViewController alloc] init];
    MFSideMenuContainerViewController *container = [MFSideMenuContainerViewController
                                                    containerWithCenterViewController: vc //nav
                                                    leftMenuViewController:nil
                                                    rightMenuViewController:nil];
    self.view.window.rootViewController = container;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
