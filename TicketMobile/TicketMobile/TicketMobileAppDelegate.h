//
//  TicketMobileAppDelegate.h
//  TicketMobile
//
//  Created by HIEUPC on 3/3/15.
//  Copyright (c) 2015 FPT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Helper/MFSideMenu/MFSideMenu.h"
#import "Helper/MFSideMenu/MFSideMenuContainerViewController.h"
#import "../GoogleAnalytics/GAI.h"



@interface TicketMobileAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property(nonatomic, strong) id<GAITracker> tracker;
@property(nonatomic, strong) NSDictionary *images;

@property (nonatomic, readonly, strong) NSString *messageKey;
@property (nonatomic, readonly, strong) NSString *deviceToken;
@property (nonatomic, assign) BOOL appActived;

@end
