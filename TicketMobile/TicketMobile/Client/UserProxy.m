//
//  UserProxy.m
//  FMap
//
//  Created by tmd on 8/28/14.
//  Copyright (c) 2014 RAD. All rights reserved.
//

#import "UserProxy.h"
#import "ShareData.h"
#import "SiUtils.h"
#import "../../TicketMobile/Modules/Users/Model/UserRecord.h"

#define methodLogin         @"login"
#define methodRegisterIMEI  @"RegisterIMEI"
#define mRegisterToken      @"register"

#define privateKey @"f23f7217E5a617294A1adBe756b180fa3F564e5fdc5e7f1fbfde6a5f5182599E2e01e16b"

@implementation UserProxy

#pragma mark LOGIN
- (void)doLogin:(NSString *)email password:(NSString *)password andOTP:(NSString *)OTP completeHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    BaseOperation * callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI, methodLogin)];
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    //create json
    
    NSString *postString = @"";
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:email    forKey:@"UserName"];
    [dict setObject:password forKey:@"Password"];
    [dict setObject:@"0"     forKey:@"Platform"];
    [dict setObject:OTP      forKey:@"OTP"];
    
    postString = [dict JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        [self endLogin:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err) {
        errHandler(err);
    };
    
    [callOp start];
}

- (void)endLogin:(NSData *)result response:(NSURLResponse *)response
completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    jsonDict = [jsonDict objectForKey:@"Result"]?:nil;
    NSString *error_code = [NSString stringWithFormat:@"%@", [jsonDict objectForKey:@"ErrorCode"]?:nil] ;
    
    if(![error_code isEqualToString:@"1"]){
        NSString *error_description = StringFormat(@"%@",[jsonDict objectForKey:@"ErrorDescription"]); ;
        handler(nil, error_code, error_description);
        return;
    }
    NSArray *data = [jsonDict objectForKey:@"Data"]?:nil;
        if(data == nil) return;
    
    /*Login success*/
    UserRecord *p = [self ParseUserRecord:[data objectAtIndex:0]];
    handler(p, error_code, @"");

}

-(UserRecord *) ParseUserRecord:(NSDictionary *)dict{
    UserRecord *rc = [[UserRecord alloc]init];
    
    rc.userName = StringFormat(@"%@",[dict objectForKey:@"Name"]);
    rc.assign = StringFormat(@"%@",[dict objectForKey:@"Assign"]);
    rc.userId = StringFormat(@"%@",[dict objectForKey:@"ID"]);
    rc.phone = StringFormat(@"%@",[dict objectForKey:@"Mobile"]);
    rc.ipPhone = StringFormat(@"%@",[dict objectForKey:@"IPPhone"]);
    rc.queueId = StringFormat(@"%@",[dict objectForKey:@"Queue"]);
    rc.parentQueueId = StringFormat(@"%@",[dict objectForKey:@"ParentQueue"]);
    rc.userEmail = StringFormat(@"%@",[dict objectForKey:@"Email"]);
    rc.create = StringFormat(@"%@",[dict objectForKey:@"Create"]);
    rc.update = StringFormat(@"%@",[dict objectForKey:@"Update"]);
    rc.view = StringFormat(@"%@",[dict objectForKey:@"View"]);
    rc.divisionname = StringFormat(@"%@",[dict objectForKey:@"DivisionName"]);
    rc.divisionid = StringFormat(@"%@",[dict objectForKey:@"DivisionID"]);
    return rc;
}

#pragma mark - Register Token to web service
/*
 "signature = md5(device_token + privateKey + email + privateKey + platform)
 VD: ta có các thông tin như sau
 device_token = ""Abc""
 email = ""daidp@fpt.com.vn""
 platform = ""ios""
 privateKey = ""123""
 => Ta sẽ có signature = md5(""Abc123daidp@fpt.com.vn123ios"")
 Tương đương với signature = ""28e009a18c18706cd2e9995de8ec0c49"""
 */

- (void)registerToken:(NSString *)deviceToken andEmail:(NSString *)email completeHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    BaseOperation * callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI, mRegisterToken)];
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    NSString *platform = @"ios";
    NSString *signature = StringFormat(@"%@%@%@%@%@", deviceToken, privateKey, email, privateKey, platform);
    //create json
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:deviceToken forKey:@"device_token"];
    [dict setObject:email forKey:@"email"];
    [dict setObject:platform forKey:@"platform"];
    [dict setObject:[signature MD5] forKey:@"signature"];
    
    NSString *postString = [dict JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [(NSMutableURLRequest*)callOp.request setValue:@"1417124836" forHTTPHeaderField:@"PublicKey"];

    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        [self endRegisterToken:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err) {
        errHandler(err);
    };
    
    [callOp start];
}

- (void)endRegisterToken:(NSData *)result response:(NSURLResponse *)response completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    jsonDict = [jsonDict objectForKey:@"Result"]?:nil;
    NSString *error_code = StringFormat(@"%@", [jsonDict objectForKey:@"ErrorCode"] ?:nil);
    NSString *error_description = StringFormat(@"%@",[jsonDict objectForKey:@"ErrorDescription"] ?:nil);
    id data = [jsonDict objectForKey:@"Data"] ?:nil;

    handler(data, error_code, error_description);
        return;
    
}


@end
