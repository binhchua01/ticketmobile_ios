//
//  ShareData.m
//  FTool
//
//  Created by tmd on 8/28/14.
//  Copyright (c) 2014 RAD. All rights reserved.
//

#import "ShareData.h"

@implementation ShareData
@synthesize userProxy;
@synthesize appProxy;
@synthesize currentUser;
@synthesize ticketProxy;
@synthesize ticketSupportProxy;
@synthesize feedbackProxy;
@synthesize branchID,branchName,locationID,locationName;

static ShareData *_instance = nil;

- (id)init
{
    self = [super init];
    if (self) {
      
        userProxy           = [[UserProxy alloc] init];
        appProxy            = [[AppProxy alloc] init];
        ticketProxy         = [[TicketProxy alloc] init];
        ticketSupportProxy  = [[TicketSupportProxy alloc] init];
        currentUser         = [[UserRecord alloc] init];
        feedbackProxy       = [[FeedbackProxy alloc]init];
}
    
    return self;
}
+ (ShareData *)instance {
    @synchronized(self)
    {
        if(_instance == nil)
        {
            _instance= [ShareData new];
            
        }
    }
    return _instance;
}

@end
