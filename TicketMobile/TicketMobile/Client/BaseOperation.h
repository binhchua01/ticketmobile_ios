
#import <Foundation/Foundation.h>

#define domainService  @"api.ticket.fpt.net"

// static NSString *const urlAPI = @"http://api.ticket.fpt.net/application/index"; // that

static NSString *const urlAPI = @"http://app.ticket.fpt.net/application/index";   // test


typedef void (^FinishBlock)(NSData *result, NSURLResponse *response);
typedef void (^ErrorBlock)(NSError *error);

@interface BaseOperation : NSObject
{
    NSString *uri;
    NSURLConnection *conn;
    NSURLRequest *request;
    NSURLResponse *response;
    NSMutableData *result;
    ErrorBlock errorHandler;
    FinishBlock completionHandler;
}

@property (nonatomic, retain) NSString *uri;
@property (nonatomic, retain) NSMutableData *result;
@property (nonatomic, retain) NSURLRequest *request;
@property (nonatomic, retain) NSURLResponse *response;
@property (nonatomic, retain) NSURLConnection *conn;
@property (nonatomic, copy) ErrorBlock errorHandler;
@property (nonatomic, copy) FinishBlock completionHandler;

- (void)start;
- (void)cancel;

@end
