//
//  AppProxy.h
//  FTool
//
//  Created by SinhNGN89 on 9/5/14.
//  Copyright (c) 2014 RAD. All rights reserved.
//

#import "BaseProxy.h"

@interface AppProxy : BaseProxy

- (void)getCheckVersion:(NSString *)version Platform:(NSString *)platform ReleaseDate:(NSString *)releaseDate completeHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;

- (void)getTicketStatusList:(NSString *)userid CurrentStatus:(NSString *)currentStatus
            completeHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;

- (void)getIssueList:(NSString *)type completeHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;

- (void)getReasonList:(NSString *)issueid completeHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;

- (void)getReasonDetailList:(NSString *)reasonID completeHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;

- (void)getEffectList:(NSString *)issueid completeHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;

- (void)getQueueList:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;

- (void)getOperateStaff:(NSString *)queue LocationID:(NSString *)locationid BranchID:(NSString *)branchid CricLev:(NSString *) criclev completeHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;

- (void)getVisorStaff:(NSString *)queue LocationID:(NSString *)locationid BranchID:(NSString *)branchid completeHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;

- (void)getProcessStaff:(NSString *)queue LocationID:(NSString *)locationid BranchID:(NSString *)branchid completeHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;

- (void)getServiceList:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;

- (void)getCusTypeList:(NSString *)servicetype completeHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;

- (void)getGetCusQty:(NSString *)type Device:(NSString *)device completeHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;

- (void)getLevel:(NSString *)issueid ReasonCode:(NSString *)reasoncode completeHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;
@end
