//
//  UserProxy.h
//  FTool
//
//  Created by tmd on 8/28/14.
//  Copyright (c) 2014 RAD. All rights reserved.
//

#import "BaseProxy.h"
#import "NSString+FPTCustom.h"

@interface UserProxy : BaseProxy

/*!Login with user name. arg1:UserName arg2:Password*/
- (void)doLogin:(NSString *)email password:(NSString *)password andOTP:(NSString *)OTP completeHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;
// Register Token to web service
- (void)registerToken:(NSString *)deviceToken andEmail:(NSString *)email completeHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;
@end
