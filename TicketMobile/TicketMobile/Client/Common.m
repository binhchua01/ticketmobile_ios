//
//  Common.m
//  MobiSales
//
//  Created by HIEUPC on 21/01/15.
//  Copyright (c) 2014 FPT.RAD.Mobisale. All rights reserved.
//

#import "Common.h"

@implementation Common


//---------------

+ (NSMutableArray *) getStatusDevice
{
    KeyValueModel *s0 = [[KeyValueModel alloc] initWithName:@"New" description:@"Chưa xử lý"];
    KeyValueModel *s1 = [[KeyValueModel alloc] initWithName:@"In" description:@"Đang xử lý"];
    KeyValueModel *s2 = [[KeyValueModel alloc] initWithName:@"OK" description:@"Đã hoàn thành"];
    KeyValueModel *s3 = [[KeyValueModel alloc] initWithName:@"NotOK" description:@"Chưa hoàn thành"];
    
    NSMutableArray *lst = [[NSMutableArray alloc] initWithObjects:s0,s1,s2,s3,nil];
    return lst;
    
}

@end
