//
//  ShareData.h
//  FMap
//
//  Created by tmd on 8/28/14.
//  Copyright (c) 2014 RAD. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserProxy.h"
#import "AppProxy.h"
#import "../Modules/Ticket/Model/TicketProxy.h"
#import "../Modules/Email/Model/FeedbackProxy.h"
#import "../../TicketMobile/Modules/Users/Model/UserRecord.h"
#import "TicketSupportProxy.h"

@interface ShareData : NSObject

@property(nonatomic,retain) NSArray *cookies;
@property(nonatomic,retain) UserProxy *userProxy;
@property(nonatomic,retain) AppProxy *appProxy;
@property(nonatomic,retain) UserRecord *currentUser;
@property(nonatomic,retain) TicketProxy *ticketProxy;
@property(nonatomic,retain) TicketSupportProxy *ticketSupportProxy;
@property(nonatomic,retain) FeedbackProxy *feedbackProxy;
@property(nonatomic,retain) NSMutableArray *arrListBonus;
@property(nonatomic,retain) NSString *branchName;
@property(nonatomic,retain) NSString *branchID;
@property(nonatomic,retain) NSString *locationName;
@property(nonatomic,retain) NSString *locationID;

+ (ShareData *)instance;

@end
