//
//  AppProxy.m
//  FTool
//
//  Created by SinhNGN89 on 9/5/14.
//  Copyright (c) 2014 RAD. All rights reserved.
//

#import "AppProxy.h"
#import "KeyValueModel.h"
#import "ShareData.h"
#import "SelectedRecord.h"

#define methodgetTicketStatusList                          @"TicketStatusList"
#define methodgetIssueList                                 @"IssueList"
#define methodReasonList                                   @"ReasonList"
#define methodReasonDetailList                             @"ReasonDetailList"
#define methodEffectList                                   @"EffectList"
#define methodGetCusQty                                    @"GetCusQty"
#define methodQueueList                                    @"QueueList"
#define methodOperateStaff                                 @"OperateStaff"
#define methodVisorStaff                                   @"VisorStaff"
#define methodProcessStaff                                 @"ProcessStaff"
#define methodServiceList                                  @"ServiceList"
#define methodCusTypeList                                  @"CusTypeList"
#define methodLevel                                        @"Level"
#define methodCheckVersion                                 @"checkVersion"
#define methodEstimatedDate                                @"EstimatedDate"

@implementation AppProxy


#pragma mark - getCheckVersion
- (void)getCheckVersion:(NSString *)version Platform:(NSString *)platform ReleaseDate:(NSString *)releaseDate completeHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    BaseOperation * callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI, methodCheckVersion)];
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    //create json
    NSString *postString = @"";
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
   
    [dict setObject:releaseDate forKey:@"ReleaseDate"];
    [dict setObject:platform forKey:@"Platform"];
    [dict setObject:version forKey:@"Version"];
    
    postString = [dict JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        [self endgetCheckVersion:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err) {
        errHandler(err);
    };
    
    [callOp start];
}

- (void)endgetCheckVersion:(NSData *)result response:(NSURLResponse *)response
         completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    jsonDict = [jsonDict objectForKey:@"Result"]?:nil;
    NSString *error_code = [NSString stringWithFormat:@"%@", [jsonDict objectForKey:@"ErrorCode"]?:nil] ;
    
    if(![error_code isEqualToString:@"0"]){
        NSString *error_description = StringFormat(@"%@",[jsonDict objectForKey:@"ErrorDescription"]); ;
        handler(nil, error_code, error_description);
        return;
    }
    NSDictionary *data = [jsonDict objectForKey:@"Data"]?:nil;
    if(data == nil) return;
    handler(data, error_code, @"");

}


#pragma mark Status ticket
- (void)getTicketStatusList:(NSString *)userid CurrentStatus:(NSString *)currentStatus
completeHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    BaseOperation * callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI, methodgetTicketStatusList)];
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    //create json
    NSString *postString = @"";
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:userid forKey:@"UserID"];
    [dict setObject:currentStatus forKey:@"CurrStatus"];
    
    postString = [dict JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        [self endgetTicketStatusList:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err) {
        errHandler(err);
    };
    
    [callOp start];
}

- (void)endgetTicketStatusList:(NSData *)result response:(NSURLResponse *)response
completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    jsonDict = [jsonDict objectForKey:@"Result"]?:nil;
    NSString *error_code = [NSString stringWithFormat:@"%@", [jsonDict objectForKey:@"ErrorCode"]?:nil] ;
    
    if(![error_code isEqualToString:@"1"]){
        NSString *error_description = StringFormat(@"%@",[jsonDict objectForKey:@"ErrorDescription"]); ;
        handler(nil, error_code, error_description);
        return;
    }
    NSDictionary *data = [jsonDict objectForKey:@"Data"]?:nil;
    if(data == nil) return;
    
    
    NSMutableArray *mArray = [NSMutableArray array];
    for(NSDictionary *d in data){
        NSString *Name = StringFormat(@"%@",[d objectForKey:@"Description"]);
        NSString *Id = StringFormat(@"%@",[d objectForKey:@"Value"]);
        KeyValueModel *s = [[KeyValueModel alloc] initWithName:Id description:Name];
        
        [mArray addObject:s];
    }
    handler(mArray, error_code, @"");
    
}

#pragma mark Issue ticket
- (void)getIssueList:(NSString *)type completeHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    BaseOperation * callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI, methodgetIssueList)];
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    //create json
    NSString *postString = @"";
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:type forKey:@"DeviceType"];
    
    postString = [dict JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        [self endgetIssueList:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err) {
        errHandler(err);
    };
    
    [callOp start];
}

- (void)endgetIssueList:(NSData *)result response:(NSURLResponse *)response
             completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    jsonDict = [jsonDict objectForKey:@"Result"]?:nil;
    NSString *error_code = [NSString stringWithFormat:@"%@", [jsonDict objectForKey:@"ErrorCode"]?:nil] ;
    
    if(![error_code isEqualToString:@"1"]){
        NSString *error_description = StringFormat(@"%@",[jsonDict objectForKey:@"ErrorDescription"]); ;
        handler(nil, error_code, error_description);
        return;
    }
    NSDictionary *data = [jsonDict objectForKey:@"Data"]?:nil;
    if(data == nil) return;
    
    
    NSMutableArray *mArray = [NSMutableArray array];
    for(NSDictionary *d in data){
        NSString *Name = StringFormat(@"%@",[d objectForKey:@"IssueName"]);
        NSString *Id = StringFormat(@"%@",[d objectForKey:@"IssueID"]);
        KeyValueModel *s = [[KeyValueModel alloc] initWithName:Id description:Name];
        
        [mArray addObject:s];
    }
    handler(mArray, error_code, @"");
    
}

#pragma mark - get reason list
- (void)getReasonList:(NSString *)issueid completeHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    BaseOperation * callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI, methodReasonList)];
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    //create json
    NSString *postString = @"";
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:issueid forKey:@"IssueID"];
    
    postString = [dict JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        [self endgetReasonList:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err) {
        errHandler(err);
    };
    
    [callOp start];
}

- (void)endgetReasonList:(NSData *)result response:(NSURLResponse *)response
      completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    jsonDict = [jsonDict objectForKey:@"Result"]?:nil;
    NSString *error_code = [NSString stringWithFormat:@"%@", [jsonDict objectForKey:@"ErrorCode"]?:nil] ;
    
    if(![error_code isEqualToString:@"1"]){
        NSString *error_description = StringFormat(@"%@",[jsonDict objectForKey:@"ErrorDescription"]); ;
        handler(nil, error_code, error_description);
        return;
    }
    NSDictionary *data = [jsonDict objectForKey:@"Data"]?:nil;
    if(data == nil) return;
    
    
    NSMutableArray *mArray = [NSMutableArray array];
    for(NSDictionary *d in data){
        NSString *Name = StringFormat(@"%@",[d objectForKey:@"ReasonName"]);
        NSString *Id = StringFormat(@"%@",[d objectForKey:@"ReasonID"]);
        KeyValueModel *s = [[KeyValueModel alloc] initWithName:Id description:Name];
        
        [mArray addObject:s];
    }
    handler(mArray, error_code, @"");
    
}

#pragma mark - get reason list
- (void)getReasonDetailList:(NSString *)reasonID completeHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    BaseOperation * callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI, methodReasonDetailList)];
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    //create json
    NSString *postString = @"";
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:reasonID forKey:@"ReasonID"];
    
    postString = [dict JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        [self endgetReasonDetailList:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err) {
        errHandler(err);
    };
    
    [callOp start];
}

- (void)endgetReasonDetailList:(NSData *)result response:(NSURLResponse *)response
       completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    jsonDict = [jsonDict objectForKey:@"Result"]?:nil;
    NSString *error_code = [NSString stringWithFormat:@"%@", [jsonDict objectForKey:@"ErrorCode"]?:nil] ;
    
    if(![error_code isEqualToString:@"1"]){
        NSString *error_description = StringFormat(@"%@",[jsonDict objectForKey:@"ErrorDescription"]); ;
        handler(nil, error_code, error_description);
        return;
    }
    NSDictionary *data = [jsonDict objectForKey:@"Data"]?:nil;
    if(data == nil) return;
    
    
    NSMutableArray *mArray = [NSMutableArray array];
    for(NSDictionary *d in data){
        NSString *ReasonDetaiName = StringFormat(@"%@",[d objectForKey:@"ReasonDetaiName"]);
        NSString *ReasonDetaiID = StringFormat(@"%@",[d objectForKey:@"ReasonDetaiID"]);
        KeyValueModel *s = [[KeyValueModel alloc] initWithName:ReasonDetaiID description:ReasonDetaiName];
        [mArray addObject:s];
    }
    handler(mArray, error_code, @"");
    
}

#pragma mark - get Effect List
- (void)getEffectList:(NSString *)issueid completeHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    BaseOperation * callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI, methodEffectList)];
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    //create json
    NSString *postString = @"";
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:issueid forKey:@"IssueID"];
    
    postString = [dict JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        [self endgetEffectList:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err) {
        errHandler(err);
    };
    
    [callOp start];
}

- (void)endgetEffectList:(NSData *)result response:(NSURLResponse *)response
       completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    jsonDict = [jsonDict objectForKey:@"Result"]?:nil;
    NSString *error_code = [NSString stringWithFormat:@"%@", [jsonDict objectForKey:@"ErrorCode"]?:nil] ;
    
    if(![error_code isEqualToString:@"1"]){
        NSString *error_description = StringFormat(@"%@",[jsonDict objectForKey:@"ErrorDescription"]); ;
        handler(nil, error_code, error_description);
        return;
    }
//    NSDictionary *data = [jsonDict objectForKey:@"Data"]?:nil;
//    if(data == nil) return;
//    
//    
//    NSMutableArray *mArray = [NSMutableArray array];
//    for(NSDictionary *d in data){
//        NSString *Name = StringFormat(@"%@",[d objectForKey:@"EffectName"]);
//        NSString *Id = StringFormat(@"%@",[d objectForKey:@"EffectID"]);
//        
//        KeyValueModel *s = [[KeyValueModel alloc] initWithName:Id description:Name];
//        
//        [mArray addObject:s];
//    }
//    handler(mArray, error_code, @"");
    NSMutableArray *data = [jsonDict objectForKey:@"Data"]?:nil;
    if(data == nil) return;
    handler(data, error_code, @"");
  
}

#pragma mark - get GetCusQty
- (void)getGetCusQty:(NSString *)type Device:(NSString *)device completeHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    BaseOperation * callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI, methodGetCusQty)];
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    //create json
    NSString *postString = @"";
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:type forKey:@"Type"];
    [dict setObject:device?:@"" forKey:@"Device"];
    postString = [dict JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        [self endgetGetCusQty:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err) {
        errHandler(err);
    };
    
    [callOp start];
}

- (void)endgetGetCusQty:(NSData *)result response:(NSURLResponse *)response
       completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    jsonDict = [jsonDict objectForKey:@"Result"]?:nil;
    NSString *error_code = [NSString stringWithFormat:@"%@", [jsonDict objectForKey:@"ErrorCode"]?:nil] ;
    
    if(![error_code isEqualToString:@"1"]){
        NSString *error_description = StringFormat(@"%@",[jsonDict objectForKey:@"ErrorDescription"]); ;
        handler(nil, error_code, error_description);
        return;
    }
    NSArray *data = [jsonDict objectForKey:@"Data"]?:nil;
    if(data == nil) return;
    
    
    NSDictionary *mArray = nil;
    NSString *CusQty = nil, *PayTVQty;
    
    CusQty = [[[data objectAtIndex:0] objectForKey:@"CusQty"]?:nil stringValue];
    PayTVQty = [[[data objectAtIndex:0] objectForKey:@"PayTVQty"]?:nil stringValue];
    
    mArray = [NSDictionary dictionaryWithObjectsAndKeys:
              CusQty, @"CusQty",
              PayTVQty, @"PayTVQty", nil];
    handler(mArray, error_code, @"");
    
}

#pragma mark - get Queue List
- (void)getQueueList:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    BaseOperation * callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI, methodQueueList)];
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    //create json
    NSString *postString = @"";
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];

    postString = [dict JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        [self endgetQueueList:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err) {
        errHandler(err);
    };
    
    [callOp start];
}

- (void)endgetQueueList:(NSData *)result response:(NSURLResponse *)response
      completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    jsonDict = [jsonDict objectForKey:@"Result"]?:nil;
    NSString *error_code = [NSString stringWithFormat:@"%@", [jsonDict objectForKey:@"ErrorCode"]?:nil] ;
    
    if(![error_code isEqualToString:@"1"]){
        NSString *error_description = StringFormat(@"%@",[jsonDict objectForKey:@"ErrorDescription"]); ;
        handler(nil, error_code, error_description);
        return;
    }
    NSDictionary *data = [jsonDict objectForKey:@"Data"]?:nil;
    if(data == nil) return;
    
    NSMutableArray *mArray = [NSMutableArray array];
    for(NSDictionary *d in data){
        NSString *Name = StringFormat(@"%@",[d objectForKey:@"Name"]);
        NSString *Id = StringFormat(@"%@",[d objectForKey:@"ID"]);
        KeyValueModel *s = [[KeyValueModel alloc] initWithName:Id description:Name];
        
        [mArray addObject:s];
    }
    handler(mArray, error_code, @"");
    
}


#pragma mark - OperateStaff
- (void)getOperateStaff:(NSString *)queue LocationID:(NSString *)locationid BranchID:(NSString *)branchid CricLev:(NSString *) criclev completeHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    BaseOperation * callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI, methodOperateStaff)];
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    //create json
    NSString *postString = @"";
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:queue forKey:@"Queue"];
    [dict setObject:locationid forKey:@"LocationID"];
    [dict setObject:branchid forKey:@"BranchID"];
    [dict setObject:criclev forKey:@"CricLev"];
    
    postString = [dict JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        [self endgetOperateStaff:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err) {
        errHandler(err);
    };
    
    [callOp start];
}

- (void)endgetOperateStaff:(NSData *)result response:(NSURLResponse *)response
             completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    jsonDict = [jsonDict objectForKey:@"Result"]?:nil;
    NSString *error_code = [NSString stringWithFormat:@"%@", [jsonDict objectForKey:@"ErrorCode"]?:nil] ;
    
    if(![error_code isEqualToString:@"1"]){
        NSString *error_description = StringFormat(@"%@",[jsonDict objectForKey:@"ErrorDescription"]); ;
        handler(nil, error_code, error_description);
        return;
    }
    NSDictionary *data = [jsonDict objectForKey:@"Data"]?:nil;
    if(data == nil) return;
    
    
    NSMutableArray *mArray = [NSMutableArray array];
    for(NSDictionary *d in data){
        NSString *Name = StringFormat(@"%@",[d objectForKey:@"Name"]);
        NSString *Id = StringFormat(@"%@",[d objectForKey:@"Email"]);
        KeyValueModel *s = [[KeyValueModel alloc] initWithName:Id description:Name];
        
        [mArray addObject:s];
    }
    handler(mArray, error_code, @"");
    
}

#pragma mark - get VisorStaff
- (void)getVisorStaff:(NSString *)queue LocationID:(NSString *)locationid BranchID:(NSString *)branchid completeHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    BaseOperation * callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI, methodVisorStaff)];
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    //create json
    NSString *postString = @"";
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:queue forKey:@"Queue"];
    [dict setObject:locationid forKey:@"LocationID"];
    [dict setObject:branchid forKey:@"BranchID"];
    
    postString = [dict JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        [self endgetVisorStaff:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err) {
        errHandler(err);
    };
    
    [callOp start];
}

- (void)endgetVisorStaff:(NSData *)result response:(NSURLResponse *)response
         completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    jsonDict = [jsonDict objectForKey:@"Result"]?:nil;
    NSString *error_code = [NSString stringWithFormat:@"%@", [jsonDict objectForKey:@"ErrorCode"]?:nil] ;
    
    if(![error_code isEqualToString:@"1"]){
        NSString *error_description = StringFormat(@"%@",[jsonDict objectForKey:@"ErrorDescription"]); ;
        handler(nil, error_code, error_description);
        return;
    }
    NSDictionary *data = [jsonDict objectForKey:@"Data"]?:nil;
    if(data == nil) return;
    
    
    NSMutableArray *mArray = [NSMutableArray array];
    for(NSDictionary *d in data){
        NSString *Name = StringFormat(@"%@",[d objectForKey:@"Name"]);
        NSString *Id = StringFormat(@"%@",[d objectForKey:@"Email"]);
        KeyValueModel *s = [[KeyValueModel alloc] initWithName:Id description:Name];
        
        [mArray addObject:s];
    }
    handler(mArray, error_code, @"");
    
}

#pragma mark - get ProcessStaff
- (void)getProcessStaff:(NSString *)queue LocationID:(NSString *)locationid BranchID:(NSString *)branchid completeHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    BaseOperation * callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI, methodProcessStaff)];
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    //create json
    NSString *postString = @"";
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:queue forKey:@"Queue"];
    [dict setObject:locationid forKey:@"LocationID"];
    [dict setObject:branchid forKey:@"BranchID"];
    
    postString = [dict JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        [self endgetProcessStaff:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err) {
        errHandler(err);
    };
    
    [callOp start];
}

- (void)endgetProcessStaff:(NSData *)result response:(NSURLResponse *)response
       completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    jsonDict = [jsonDict objectForKey:@"Result"]?:nil;
    NSString *error_code = [NSString stringWithFormat:@"%@", [jsonDict objectForKey:@"ErrorCode"]?:nil] ;
    
    if(![error_code isEqualToString:@"1"]){
        NSString *error_description = StringFormat(@"%@",[jsonDict objectForKey:@"ErrorDescription"]); ;
        handler(nil, error_code, error_description);
        return;
    }
    NSDictionary *data = [jsonDict objectForKey:@"Data"]?:nil;
    if(data == nil) return;
    
    
    NSMutableArray *mArray = [NSMutableArray array];
    for(NSDictionary *d in data){
        NSString *Name = StringFormat(@"%@",[d objectForKey:@"Name"]);
        NSString *Id = StringFormat(@"%@",[d objectForKey:@"Email"]);
        KeyValueModel *s = [[KeyValueModel alloc] initWithName:Id description:Name];
        
        [mArray addObject:s];
    }
    handler(mArray, error_code, @"");
    
}

#pragma mark - get Service list
- (void)getServiceList:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    BaseOperation * callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI, methodServiceList)];
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    //create json
    NSString *postString = @"";
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    
    postString = [dict JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        [self endgetServiceList:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err) {
        errHandler(err);
    };
    
    [callOp start];
}

- (void)endgetServiceList:(NSData *)result response:(NSURLResponse *)response
         completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    jsonDict = [jsonDict objectForKey:@"Result"]?:nil;
    NSString *error_code = [NSString stringWithFormat:@"%@", [jsonDict objectForKey:@"ErrorCode"]?:nil] ;
    
    if(![error_code isEqualToString:@"1"]){
        NSString *error_description = StringFormat(@"%@",[jsonDict objectForKey:@"ErrorDescription"]); ;
        handler(nil, error_code, error_description);
        return;
    }

    NSDictionary *data = [jsonDict objectForKey:@"Data"]?:nil;
    if(data == nil) return;
    
    NSMutableArray *mArray = [NSMutableArray array];
    for(NSDictionary *d in data){
        SelectedRecord *rc = [self ParseServiceSelectedRecord:d];
        [mArray addObject:rc];
    }
    handler(mArray, error_code, @"");
    
}

-(SelectedRecord *) ParseServiceSelectedRecord:(NSDictionary *)dict{
    SelectedRecord *rc = [[SelectedRecord alloc]init];
    
    rc.Name = StringFormat(@"%@",[dict objectForKey:@"Name"]);
    rc.Id = StringFormat(@"%@",[dict objectForKey:@"ID"]);
    rc.Check = @"0";
    return rc;
}

#pragma mark - get Cus Type
- (void)getCusTypeList:(NSString *)servicetype completeHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    BaseOperation * callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI, methodCusTypeList)];
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    //create json
    NSString *postString = @"";
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:servicetype?:@"0" forKey:@"ServiceType"];
    
    postString = [dict JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        [self endgetCusTypeList:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err) {
        errHandler(err);
    };
    
    [callOp start];
}

- (void)endgetCusTypeList:(NSData *)result response:(NSURLResponse *)response
         completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    jsonDict = [jsonDict objectForKey:@"Result"]?:nil;
    NSString *error_code = [NSString stringWithFormat:@"%@", [jsonDict objectForKey:@"ErrorCode"]?:nil] ;
    
    if(![error_code isEqualToString:@"1"]){
        NSString *error_description = StringFormat(@"%@",[jsonDict objectForKey:@"ErrorDescription"]); ;
        handler(nil, error_code, error_description);
        return;
    }

    NSDictionary *data = [jsonDict objectForKey:@"Data"]?:nil;
    if(data == nil) return;
    
    NSMutableArray *mArray = [NSMutableArray array];
    for(NSDictionary *d in data){
        SelectedRecord *rc = [self ParseCusSelectedRecord:d];
        [mArray addObject:rc];
    }
    handler(mArray, error_code, @"");    
    
}

-(SelectedRecord *) ParseCusSelectedRecord:(NSDictionary *)dict{
    SelectedRecord *rc = [[SelectedRecord alloc]init];
    
    rc.Name = StringFormat(@"%@",[dict objectForKey:@"Description"]);
    rc.Id = StringFormat(@"%@",[dict objectForKey:@"ID"]);
    rc.Check = @"0";
    return rc;
}

#pragma mark - get Cus Type
- (void)getLevel:(NSString *)issueid ReasonCode:(NSString *)reasoncode completeHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    BaseOperation * callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI, methodLevel)];
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    //create json
    NSString *postString = @"";
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:issueid forKey:@"IssueID"];
    [dict setObject:reasoncode forKey:@"ReasonCode"];
    
    postString = [dict JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        [self endgetLevel:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err) {
        errHandler(err);
    };
    
    [callOp start];
}

- (void)endgetLevel:(NSData *)result response:(NSURLResponse *)response
        completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    jsonDict = [jsonDict objectForKey:@"Result"]?:nil;
    NSString *error_code = [NSString stringWithFormat:@"%@", [jsonDict objectForKey:@"ErrorCode"]?:nil] ;
    
    if(![error_code isEqualToString:@"1"]){
        NSString *error_description = StringFormat(@"%@",[jsonDict objectForKey:@"ErrorDescription"]); ;
        handler(nil, error_code, error_description);
        return;
    }
    NSArray *data = [jsonDict objectForKey:@"Data"]?:nil;
    if(data == nil) return;
    
    
    NSDictionary *mArray = nil;
    NSString *LevelName = nil;
    
    LevelName = [[data objectAtIndex:0] objectForKey:@"LevelName"];
   
    
    mArray = [NSDictionary dictionaryWithObjectsAndKeys:
              LevelName, @"LevelName", nil];
    handler(mArray, error_code, @"");
    
}

@end
