//
//  Common.h
//  FTool
//
//  Created by MAC on 11/17/14.
//  Copyright (c) 2014 FPT.RAD.FTool. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KeyValueModel.h"

@interface Common : NSObject

+ (NSMutableArray *) getStatusDevice;

@end
