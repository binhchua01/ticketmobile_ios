//
//  SelectedCell.m
//  TicketMobile
//
//  Created by HIEUPC on 3/22/15.
//  Copyright (c) 2015 FPT. All rights reserved.
//

#import "SelectedCell.h"

@implementation SelectedCell

- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
