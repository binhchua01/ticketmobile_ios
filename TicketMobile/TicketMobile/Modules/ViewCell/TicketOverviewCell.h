//
//  TicketOverviewCell.h
//  TicketMobile
//
//  Created by HIEUPC on 3/3/15.
//  Copyright (c) 2015 FPT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TicketOverviewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *lblNumber;
@property (strong, nonatomic) IBOutlet UILabel *lblName;
@property (strong, nonatomic) IBOutlet UIImageView *imageStatus;

@end
