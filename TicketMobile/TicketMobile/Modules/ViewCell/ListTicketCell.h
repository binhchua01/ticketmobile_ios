//
//  ListTicketCell.h
//  TicketMobile
//
//  Created by THONT5 on 3/4/15.
//  Copyright (c) 2015 FPT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ListTicketCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *lblCode;
@property (strong, nonatomic) IBOutlet UILabel *lblTitle;
@property (strong, nonatomic) IBOutlet UILabel *lblPhanTu;
@property (strong, nonatomic) IBOutlet UILabel *lblTon;
@property (strong, nonatomic) IBOutlet UILabel *lblstatus;
@property (strong, nonatomic) IBOutlet UILabel *lblTitleDate;

@property (strong, nonatomic) IBOutlet UILabel *lblForCreatedTime;
@property (strong, nonatomic) IBOutlet UILabel *lblForTimeWorked;
@property (strong, nonatomic) IBOutlet UILabel *lblCreateTime;
@property (strong, nonatomic) IBOutlet UILabel *lblTimeWorked;

@property (strong, nonatomic) IBOutlet UIImageView *imageTimeWorked;

@end
