//
//  SelectedCell.h
//  TicketMobile
//
//  Created by HIEUPC on 3/22/15.
//  Copyright (c) 2015 FPT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UICheckbox.h"

@interface SelectedCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UICheckbox *btnradio;
@property (strong, nonatomic) IBOutlet UILabel *lblName;
@property (strong, nonatomic) IBOutlet UIImageView *ic_image;

@end
