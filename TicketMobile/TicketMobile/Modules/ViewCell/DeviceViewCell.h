//
//  DeviceViewCell.h
//  TicketMobile
//
//  Created by HIEUPC on 3/4/15.
//  Copyright (c) 2015 FPT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIPopoverListView.h"
#import "DetailTicketViewController.h"

//@protocol DeviceCellDelegate <NSObject>
//
//@optional
//- (void) getButtonStatus: (UIButton *)btnStatus withPopoverView: (UIPopoverListView *)popover;
//@end

@interface DeviceViewCell : UITableViewCell{
    //UIButton *btnSender;
}

//@property (nonatomic, assign) id<DeviceCellDelegate> delegate;
//@property (nonatomic, strong) DetailTicketViewController *detailTicketViewController;
//@property (nonatomic, strong) UIPopoverListView *popoverView;

@property (strong, nonatomic) IBOutlet UILabel *lblName;
@property (strong, nonatomic) IBOutlet UIButton *btnStatus;
@property (strong, nonatomic) IBOutlet UILabel *lblDate;
@property (strong, nonatomic) IBOutlet UILabel *lblPerson;
@property (strong, nonatomic) IBOutlet UILabel *lblTime;

@end
