//
//  ListTicketCell.m
//  TicketMobile
//
//  Created by HIEUPC on 3/4/15.
//  Copyright (c) 2015 FPT. All rights reserved.
//

#import "ListTicketCell.h"

@implementation ListTicketCell


- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)layoutSubviews {
    //self.lblTitle.frame = CGRectMake(0, 0, 100, 200);
}

- (void)drawRect:(CGRect)rect{
 
    [self.lblTitle setNumberOfLines:0];
    [self.lblTitle sizeToFit];
    
    if(self.lblTitle.text.length > 15){
        
        int numline = (int) self.lblTitle.text.length;
        int val = numline % 30;
        val = val==0?0:1;
        numline = numline / 30 + val;
        float height = self.lblTitle.frame.size.height * numline;
        self.lblTitle.numberOfLines = numline / 30 + numline;
        self.lblTitle.frame = CGRectMake(self.lblTitle.frame.origin.x, self.lblTitle.frame.origin.y, self.lblTitle.frame.size.width, height);
        self.contentView.frame = CGRectMake(0, 0, self.frame.size.width, 140 + height);
    }
    
}
@end
