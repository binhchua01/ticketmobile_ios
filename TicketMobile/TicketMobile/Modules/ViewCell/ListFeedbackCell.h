//
//  ListFeedbackCell.h
//  TicketMobile
//
//  Created by HIEUPC on 3/9/15.
//  Copyright (c) 2015 FPT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ListFeedbackCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *lblTitle;
@property (strong, nonatomic) IBOutlet UILabel *lblPerson;
@property (strong, nonatomic) IBOutlet UILabel *lbltitlePerson;
@property (strong, nonatomic) IBOutlet UILabel *lblTime;

@property (strong, nonatomic) IBOutlet UIImageView *imageBackgroundTitle;

@end
