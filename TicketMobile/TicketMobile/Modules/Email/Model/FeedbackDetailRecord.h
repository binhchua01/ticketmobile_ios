//
//  FeedbackDetailRecord.h
//  TicketMobile
//
//  Created by HIEUPC on 3/16/15.
//  Copyright (c) 2015 FPT. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FeedbackDetailRecord : NSObject

@property (strong, nonatomic) NSString *Cc;
@property (strong, nonatomic) NSString *CreatedDate;
@property (strong, nonatomic) NSString *FromAdr;
@property (strong, nonatomic) NSString *Subject;
@property (strong, nonatomic) NSString *ToAdr;
@property (strong, nonatomic) NSString *BodyHTML;
@property (strong, nonatomic) NSString *QueueAdr;

@end
