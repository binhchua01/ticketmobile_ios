//
//  ListFeedbackRecord.h
//  TicketMobile
//
//  Created by HIEUPC on 3/13/15.
//  Copyright (c) 2015 FPT. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ListFeedbackRecord : NSObject

@property (strong, nonatomic) NSString *CreateDate;
@property (strong, nonatomic) NSString *FromAdr;
@property (strong, nonatomic) NSString *ID;
@property (strong, nonatomic) NSString *Subject;

@end
