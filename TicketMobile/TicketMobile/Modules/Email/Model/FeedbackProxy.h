//
//  FeedbackProxy.h
//  TicketMobile
//
//  Created by HIEUPC on 3/13/15.
//  Copyright (c) 2015 FPT. All rights reserved.
//

#import "BaseProxy.h"

@interface FeedbackProxy : BaseProxy

- (void)getReponseInfo:(NSString *)ticketId completeHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;

- (void)getResponseDetail:(NSString *)ID completeHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;

- (void)getResponseSend:(NSString *)subject FromAdr:(NSString *)fromadr ToAdr:(NSString *)toadr Cc:(NSString *)cc QueueAdr:(NSString *)queueadr BodyHTML:(NSString *)bodyhtml UpdateBy:(NSString *)updateby UpdatedEmail:(NSString *)updatedemail TicketID:(NSString *)ticketid completeHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;

@end
