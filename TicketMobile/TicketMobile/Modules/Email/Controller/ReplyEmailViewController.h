//
//  ReplyEmailViewController.h
//  TicketMobile
//
//  Created by HIEUPC on 3/23/15.
//  Copyright (c) 2015 FPT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FeedbackDetailRecord.h"
#import "BaseViewController.h"



@interface ReplyEmailViewController : BaseViewController

@property (strong, nonatomic) IBOutlet UIWebView *webView;
@property (strong, nonatomic) IBOutlet UIWebView *webViewContent;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollview;
@property (strong, nonatomic) IBOutlet UIView *viewButton;

@property (strong, nonatomic) IBOutlet UILabel *lblTitle;
@property (strong, nonatomic) IBOutlet UILabel *lblfromAdr;
@property (strong, nonatomic) IBOutlet UITextField *lbltoAdr;
@property (strong, nonatomic) IBOutlet UITextField *txtCc;
@property (strong, nonatomic) IBOutlet UITextView *tvCc;

@property (strong, nonatomic) IBOutlet UIButton *btnSend;

@property (strong, nonatomic) IBOutlet UIImageView *imageBackgroundTitle;
@property (strong, nonatomic) IBOutlet UIImageView *imageBackgroundInfo;


@property (strong, nonatomic) FeedbackDetailRecord *rc;
@property (strong, nonatomic) NSString *type;
@property (strong, nonatomic) NSString *ticketId;

-(IBAction)btnSend_Click:(id)sender;
-(IBAction)btnCancel_Click:(id)sender;

@end
