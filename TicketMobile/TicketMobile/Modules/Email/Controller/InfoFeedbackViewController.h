//
//  InfoFeedbackViewController.h
//  TicketMobile
//
//  Created by HIEUPC on 3/9/15.
//  Copyright (c) 2015 FPT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "../../../BaseViewController.h"

@interface InfoFeedbackViewController : BaseViewController

@property (strong, nonatomic) IBOutlet UILabel *lblTicketNo;
@property (strong, nonatomic) IBOutlet UILabel *lblSubject;
@property (strong, nonatomic) IBOutlet UILabel *lblToAdr;
@property (strong, nonatomic) IBOutlet UILabel *lblCreateDate;
@property (strong, nonatomic) IBOutlet UILabel *lblFromAdr;
@property (strong, nonatomic) IBOutlet UIWebView *webview;

@property (strong, nonatomic) IBOutlet UIButton *btnReply;

@property (strong, nonatomic) NSString *Id;
@property (strong, nonatomic) NSString *ticketId;
@property (strong, nonatomic) IBOutlet UIView *viewCheck;

@property (strong, nonatomic) IBOutlet UIImageView *imageBackgroundTitle;
@property (strong, nonatomic) IBOutlet UIImageView *imageBackgroundInfo;

-(IBAction)btnReplyAll:(id)sender;
-(IBAction)btnReply:(id)sender;

@end
