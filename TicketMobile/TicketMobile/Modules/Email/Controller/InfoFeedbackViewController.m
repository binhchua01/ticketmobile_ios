//
//  InfoFeedbackViewController.m
//  TicketMobile
//
//  Created by HIEUPC on 3/9/15.
//  Copyright (c) 2015 FPT. All rights reserved.
//

#import "InfoFeedbackViewController.h"
#import "ShareData.h"
#import "FeedbackDetailRecord.h"
#import "ReplyEmailViewController.h"

@interface InfoFeedbackViewController (){
    NSString *bodyHTML;
    FeedbackDetailRecord *rc;
}

@end

@implementation InfoFeedbackViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"THÔNG TIN PHẢN HỒI";
    
    [self LoadData];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSString *)htmlEntityDecode:(NSString *)string
{
    string = [string stringByReplacingOccurrencesOfString:@"&quot;" withString:@"\""];
    string = [string stringByReplacingOccurrencesOfString:@"&apos;" withString:@"'"];
    string = [string stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&"];
    string = [string stringByReplacingOccurrencesOfString:@"&lt;" withString:@"<"];
    string = [string stringByReplacingOccurrencesOfString:@"&gt;" withString:@">"];
    
    return string;
}

- (void) drawView{
    float height = 0;
    self.lblToAdr.text = rc.ToAdr;
    [self.lblToAdr setNumberOfLines:0];
    [self.lblToAdr sizeToFit];
    
    self.lblFromAdr.text = rc.FromAdr;
    [self.lblFromAdr setNumberOfLines:0];
    [self.lblFromAdr sizeToFit];
    
    self.lblSubject.text = rc.Subject;
    [self.lblSubject setNumberOfLines:0];
    [self.lblSubject sizeToFit];
    int numline = (int) rc.Subject.length;
    int numline_1 = (int) rc.ToAdr.length;
    int numline_2 = (int) rc.FromAdr.length;
    
    if(rc.Subject.length > 39){
        numline = [self setNumline:numline numchar:39];
    }
    else {
        numline = 1;
    }
    
    if (rc.ToAdr.length > 34) {
        numline_1 = [self setNumline:numline_1 numchar:34];
    }
    else {
        numline_1 = 1;
    }
    if (rc.FromAdr.length > 34) {
        numline_2 = [self setNumline:numline_2 numchar:34];
    }
    else {
        numline_2 = 1;
    }
    
    UIImageView *imageBackgroundTitle = [[UIImageView alloc] initWithImage:nil];
    imageBackgroundTitle.image = [UIImage imageNamed:@"backgroundTitle_2"];
    
    UIImageView *imageBackgroundInfo = [[UIImageView alloc] initWithImage:nil];
    imageBackgroundInfo.image = [UIImage imageNamed:@"backgroundTitle_3"];
    
//    height = self.lblSubject.frame.size.height * numline;
//    self.lblSubject.frame = CGRectMake(self.lblSubject.frame.origin.x, self.lblSubject.frame.origin.y, self.lblSubject.frame.size.width, height);
//    self.lblSubject.numberOfLines = numline;
    
    height = 14*(numline - 1);
    //self.imageBackgroundTitle.frame = CGRectMake(self.imageBackgroundTitle.frame.origin.x, self.imageBackgroundTitle.frame.origin.y, self.imageBackgroundTitle.frame.size.width,self.imageBackgroundTitle.frame.size.height +height);
    imageBackgroundTitle.frame = CGRectMake(self.imageBackgroundTitle.frame.origin.x, self.imageBackgroundTitle.frame.origin.y, self.imageBackgroundTitle.frame.size.width,self.imageBackgroundTitle.frame.size.height +height);
    
    height = 14*(numline + numline_1 + numline_2 - 3);
//    self.imageBackgroundInfo.frame = CGRectMake(self.imageBackgroundInfo.frame.origin.x, self.imageBackgroundInfo.frame.origin.y, self.imageBackgroundInfo.frame.size.width, self.imageBackgroundInfo.frame.size.height + height);
    imageBackgroundInfo.frame = CGRectMake(self.imageBackgroundInfo.frame.origin.x, self.imageBackgroundInfo.frame.origin.y, self.imageBackgroundInfo.frame.size.width, self.imageBackgroundInfo.frame.size.height + height);
    
    
   // [self.view addSubview:imageBackgroundTitle];
    [self.view insertSubview:imageBackgroundTitle atIndex:1];
        
   // [self.view addSubview:imageBackgroundInfo];
    [self.view insertSubview:imageBackgroundInfo atIndex:0];

    //self.webview.frame = CGRectMake(self.webview.frame.origin.x, self.webview.frame.origin.y, self.webview.frame.size.width, 350);
    
    //self.webview.opaque = NO;
    //self.webview.backgroundColor = [UIColor colorWithRed:204.0/255.0 green:204.0/255.0 blue:204.0/255.0 alpha:1.0];
    self.webview.layer.borderColor = [UIColor colorWithRed:0.863 green:0.863 blue:0.863 alpha:1].CGColor;
    self.webview.layer.borderWidth = 1.f;
    self.webview.layer.cornerRadius = 6.0f;
    [self.webview loadHTMLString:[SiUtils DecodeBase64:rc.BodyHTML] baseURL:nil];
    bodyHTML = rc.BodyHTML;
}

- (int) setNumline: (int)numline numchar:(int)numChar{
    int val = numline % numChar;
    val = val==0?0:1;
    numline = numline / numChar + val;
    return numline;
}

#pragma mark - load Data
-(void)LoadData {
    if(![SiUtils checkNetworkAvailable]){
        [self showAlertBox:@"Thông báo" message:Mesage_Network];
    }
    
    [self showMBProcess:@"Đang tải dữ liệu..."];
    ShareData *shared = [ShareData instance];
    [shared.feedbackProxy getResponseDetail:self.Id completeHandler:^(id result, NSString *errorCode, NSString *message) {
        
        if([errorCode isEqualToString:@"1"]){
            rc = result;
            if(rc!= nil){
                [self drawView];
                
            }else {
                [self showAlertBox:@"Thông Báo" message:Mesage_DataEmpty];
            }
            
        }else {
            [self showAlertBox:@"Thông Báo" message:Mesage_DataEmpty];
        }
        [self hideMBProcess];
    } errorHandler:^(NSError *error) {
        [self hideMBProcess];
        [self showAlertBox:@"Thông Báo" message:[error localizedDescription]];
        NSLog(@"%@",[NSString stringWithFormat:@"%@",error]);
        return;
    }];
}


#pragma mark - event button
-(IBAction)btnReplyAll:(id)sender{
    NSString *nib = @"ReplyEmailViewController";
    ReplyEmailViewController *vc = [[ReplyEmailViewController alloc]initWithNibName:nib bundle:nil];
    vc.rc = rc;
    vc.type = @"2";
    vc.ticketId = self.ticketId;
    [self.navigationController pushViewController:vc animated:YES];
}

-(IBAction)btnReply:(id)sender{
    NSString *nib = @"ReplyEmailViewController";
    ReplyEmailViewController *vc = [[ReplyEmailViewController alloc]initWithNibName:nib bundle:nil];
    vc.rc = rc;
    vc.type = @"1";
    vc.ticketId = self.ticketId;
    [self.navigationController pushViewController:vc animated:YES];
}

// add by DanTT, action click button on alertView
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    
        if ([title isEqualToString:@"OK"]) {
            NSLog(@"Action OK !");
            [self.navigationController popViewControllerAnimated:YES];
        }
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
