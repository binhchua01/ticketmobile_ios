//
//  ReplyEmailViewController.m
//  TicketMobile
//
//  Created by HIEUPC on 3/23/15.
//  Copyright (c) 2015 FPT. All rights reserved.
//
// vutt11

#import "ReplyEmailViewController.h"
#import "ShareData.h"
#import "ListFeedbackViewController.h"

@interface ReplyEmailViewController ()

@end

@implementation ReplyEmailViewController
{
    UIAlertView *alertView ;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title =@"PHẢN HỒI MAIL";
    
    [self.webViewContent loadHTMLString:[SiUtils DecodeBase64:self.rc.BodyHTML] baseURL:nil];
    self.lblTitle.text = self.rc.Subject;
    //[ self drawView];
    
    self.lblfromAdr.text = [ShareData instance].currentUser.userEmail;
    self.lbltoAdr.text = [NSString stringWithFormat:@"%@,%@",self.rc.ToAdr,self.rc.FromAdr];
    if([self.type isEqualToString:@"2"]){
        
        self.tvCc.text = self.rc.Cc;
    }
    //Create ScrollView
    [self.scrollview setBackgroundColor:[UIColor clearColor]];
    [self.scrollview setCanCancelContentTouches:NO];
    self.scrollview.indicatorStyle = UIScrollViewIndicatorStyleWhite;

    [self.scrollview setScrollEnabled:YES];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnView:)];
    
    [self.view addGestureRecognizer:tap];
    [self registerForKeyboardNotifications];
    
    [self.view addGestureRecognizer:tap];
    [self loadContent];
    
    self.lbltoAdr.layer.borderColor = [UIColor colorWithRed:0.863 green:0.863 blue:0.863 alpha:1].CGColor;
    self.lbltoAdr.layer.borderWidth = 1.f;
    self.lbltoAdr.layer.cornerRadius = 6.0f;
    
    self.tvCc.layer.borderColor = [UIColor colorWithRed:0.863 green:0.863 blue:0.863 alpha:1].CGColor;
    self.tvCc.layer.borderWidth = 1.f;
    self.tvCc.layer.cornerRadius = 6.0f;
    
    self.webViewContent.layer.borderColor = [UIColor colorWithRed:0.863 green:0.863 blue:0.863 alpha:1].CGColor;
    self.webViewContent.layer.borderWidth = 1.f;
    self.webViewContent.layer.cornerRadius = 6.0f;
    
    float height = self.menuContainerViewController.view.frame.size.height;
    [self.viewButton setFrame:CGRectMake(0,height - 37,320,37)];
    [self.view addSubview:self.viewButton];
    
    [self.webView.scrollView setScrollEnabled:NO];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) drawView{
    float height = 0;
    
    [self.lblTitle setNumberOfLines:0];
    [self.lblTitle sizeToFit];
    int numline = (int) self.rc.Subject.length;
    
    if(self.rc.Subject.length > 79){
        int val = numline % 79;
        val = val==0?0:1;
        numline = numline / 79 + val;
    }
    else {
        numline = 1;
    }
    
    UIImageView *imageBackgroundTitle = [[UIImageView alloc] initWithImage:nil];
    imageBackgroundTitle.image = [UIImage imageNamed:@"backgroundTitle_2"];
    
    UIImageView *imageBackgroundInfo = [[UIImageView alloc] initWithImage:nil];
    imageBackgroundInfo.image = [UIImage imageNamed:@"backgroundTitle_3"];
    
    height = 14*(numline - 1);
    imageBackgroundTitle.frame = CGRectMake(self.imageBackgroundTitle.frame.origin.x, self.imageBackgroundTitle.frame.origin.y, self.imageBackgroundTitle.frame.size.width,self.imageBackgroundTitle.frame.size.height +height);
    
    imageBackgroundInfo.frame = CGRectMake(self.imageBackgroundInfo.frame.origin.x, self.imageBackgroundInfo.frame.origin.y, self.imageBackgroundInfo.frame.size.width, self.imageBackgroundInfo.frame.size.height + height);
    
    [self.view insertSubview:imageBackgroundTitle atIndex:4];
    
    [self.view insertSubview:imageBackgroundInfo atIndex:3];
}

-(void)viewDidLayoutSubviews{
//    float height = 400;
//    if(IS_IPHONE4){
//        height  = 520;
//    }
       // [self.scrollview setContentSize:CGSizeMake(self.view.frame.size.width, self.view.frame.size.height + height)];
}
- (void)registerForKeyboardNotifications{
    
    // Add two notifications for the keyboard. One when the keyboard is shown and
    // one when it's about to hide.
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}
- (void)deregisterFromKeyboardNotifications {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardDidHideNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
    
}
- (void)tapOnView:(UITapGestureRecognizer *)sender
{
    [self.scrollview endEditing:YES];
    
}
#pragma mark - How to make UITextView move up when keyboard is present
// Called when the UIKeyboardDidShowNotification is received
- (void)keyboardWasShown:(NSNotification *)notification {
    
    NSDictionary* info = [notification userInfo];
    
    // Get the size of the keyboard from the userInfo dictionary.
    CGSize keyboardSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    CGPoint buttonOrigin = self.webView.frame.origin;
    
    CGFloat buttonHeight = self.webView.frame.size.height;
    
    CGRect visibleRect = self.view.frame;
    
    visibleRect.size.height -= keyboardSize.height;
    
    CGPoint scrollPoint = CGPointMake(0.0, buttonOrigin.y - visibleRect.size.height + buttonHeight - 180);
    
    [self.scrollview setContentOffset:scrollPoint animated:YES];
    
}


- (void)keyboardWillBeHidden:(NSNotification *)notification {
    
    // Move the scroll view back into its original position.
    CGPoint buttonOrigin = self.webView.frame.origin;
    CGPoint scrollPoint = CGPointMake(0, buttonOrigin.y - 180);
    [self.scrollview setContentOffset:scrollPoint animated:YES];
    return;
    
}

- (void)loadContent {
    
    NSString *path =[[[NSBundle mainBundle] bundlePath] stringByAppendingString:@"/CKEditor/demo.html"];
    [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL fileURLWithPath:path]]];
    
}

-(void)webViewDidFinishLoad:(UIWebView *)_webViewFinishLoad{
    
    [_webViewFinishLoad stringByEvaluatingJavaScriptFromString:@"document.getElementById('editor1').innerHTML='';"];
    [_webViewFinishLoad stringByEvaluatingJavaScriptFromString:@"document.getElementById('editor1').focus();"];
}

-(IBAction)btnSend_Click:(id)sender{
    
    if(![SiUtils checkNetworkAvailable]){
        [self showAlertBox:@"Thông báo" message:Mesage_Network];
    }
    
    [self showMBProcess:@"Đang gửi phản hồi..."];
    NSString *content =[self.webView stringByEvaluatingJavaScriptFromString:
                                          @"CKEDITOR.instances.editor1.getData()"];;
    NSString *bodyhtml = [NSString stringWithFormat:@"%@%@",content,[SiUtils DecodeBase64:self.rc.BodyHTML]];
    ShareData *shared = [ShareData instance];
    [shared.feedbackProxy getResponseSend:self.lblTitle.text FromAdr:self.lblfromAdr.text ToAdr:self.lbltoAdr.text Cc:self.tvCc.text QueueAdr:self.rc.QueueAdr BodyHTML:bodyhtml UpdateBy:shared.currentUser.userId UpdatedEmail:shared.currentUser.userEmail TicketID:self.ticketId completeHandler:^(id result, NSString *errorCode, NSString *message) {
        
        if([errorCode isEqualToString:@"1"]){
            //[self showAlertBox:@"Thông Báo" message:@"Gửi mail thành công"];
            
            [self showAlert:@"Gửi mail thành công"];
            
            
        }else {
            [self showAlertBox:@"Thông Báo" message:@"Gửi mail thất bại"];
        }
        [self hideMBProcess];
    } errorHandler:^(NSError *error) {
        [self hideMBProcess];
        [self showAlertBox:@"Thông Báo" message:[error localizedDescription]];
        NSLog(@"%@",[NSString stringWithFormat:@"%@",error]);
        return;

    }];
}

-(IBAction)btnCancel_Click:(id)sender{
    
    NSMutableArray  *abc = [NSMutableArray arrayWithArray:self.navigationController.viewControllers];
    
    [abc removeLastObject];
    [abc removeLastObject];
    
    self.navigationController.viewControllers = abc;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
//SHOWALERT_CALLBACK


- (void)showAlert:(NSString *)message
{
    if (alertView) {
        [alertView setDelegate:nil];
        
        alertView = nil;
        
    }
    
    alertView = [[UIAlertView alloc] initWithTitle:@"Thông báo" message:message delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    alertView.tag = 13;
    [alertView show];
    
}

//DELEGATE ALERT

- (void)alertView:(UIAlertView *)alertView1 clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    switch (buttonIndex) {
        case 0:
            if (alertView1.tag == 13) {
                
//                NSMutableArray  *abcd = [NSMutableArray arrayWithArray:self.navigationController.viewControllers];
//                
//                [abcd removeLastObject];
//                [abcd removeLastObject];
//                
//                self.navigationController.viewControllers = abcd;
                
                ListFeedbackViewController * vc = [[ListFeedbackViewController alloc] initWithNibName:@"ListFeedbackViewController" bundle:nil];
                vc.ticketId = self.ticketId;
                [self.navigationController pushViewController:vc animated:YES];
                
            }
            break;
            
        default:
            break;
            
    }
    
}


@end
