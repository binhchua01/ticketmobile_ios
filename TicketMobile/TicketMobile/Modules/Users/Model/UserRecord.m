//
//  UserRecord.m
//  Ftool
//
//  Created by hieupc on 3/11/14.
//  Copyright (c) 2014 RAD. All rights reserved.
//

#import "UserRecord.h"

@implementation UserRecord

@synthesize userName;
@synthesize userPass;
@synthesize phone;
@synthesize ipPhone;
@synthesize queueId;
@synthesize parentQueueId;
@synthesize userEmail;
@synthesize userId;
@synthesize create;
@synthesize update;
@synthesize view;
@synthesize divisionid;
@synthesize divisionname;
@synthesize assign;

//vutt11
- (id)initWithCoder:(NSCoder *)aDecoder {
	if ((self = [self init])){

        self.userId = [aDecoder decodeObjectForKey:@"userId"];
        self.userName = [aDecoder decodeObjectForKey:@"userName"];
        self.userPass = [aDecoder decodeObjectForKey:@"userPass"];
        self.phone = [aDecoder decodeObjectForKey:@"phone"];
        self.ipPhone = [aDecoder decodeObjectForKey:@"ipPhone"];
        self.queueId = [aDecoder decodeObjectForKey:@"queueId"];
        self.parentQueueId = [aDecoder decodeObjectForKey:@"parentQueueId"];
        self.userEmail = [aDecoder decodeObjectForKey:@"userEmail"];
        self.create = [aDecoder decodeObjectForKey:@"create"];
        self.update = [aDecoder decodeObjectForKey:@"update"];
        self.view = [aDecoder decodeObjectForKey:@"view"];
        self.divisionid = [aDecoder decodeObjectForKey:@"divisionid"];
        self.divisionname = [aDecoder decodeObjectForKey:@"divisionname"];
        self.assign = [aDecoder decodeObjectForKey:@"assign"];
        
        
    }
    
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    
    [aCoder encodeObject:self.userName forKey:@"userId"];
    [aCoder encodeObject:self.userName forKey:@"userName"];
    [aCoder encodeObject:self.userPass forKey:@"userPass"];
    [aCoder encodeObject:self.userName forKey:@"phone"];
    [aCoder encodeObject:self.userPass forKey:@"ipPhone"];
    [aCoder encodeObject:self.userPass forKey:@"locationID"];
    [aCoder encodeObject:self.userName forKey:@"queueId"];
    [aCoder encodeObject:self.userPass forKey:@"parentQueueId"];
    [aCoder encodeObject:self.userPass forKey:@"create"];
    [aCoder encodeObject:self.userPass forKey:@"update"];
    [aCoder encodeObject:self.userPass forKey:@"view"];
    [aCoder encodeObject:self.userName forKey:@"divisionid"];
    [aCoder encodeObject:self.userPass forKey:@"divisionname"];
    [aCoder encodeObject:self.userPass forKey:@"userEmail"];
    [aCoder encodeObject:self.userPass forKey:@"assign"];
    
}

- (id) copyWithZone:(NSZone *)zone{
    
    UserRecord *object = [[UserRecord alloc] init];
    object.userId = [self.userId copyWithZone:zone];
    object.userName = [self.userName copyWithZone:zone];
    object.userPass = [self.userPass copyWithZone:zone];
    object.phone = [self.phone copyWithZone:zone];
    object.ipPhone = [self.ipPhone copyWithZone:zone];
    object.queueId = [self.queueId copyWithZone:zone];
    object.parentQueueId = [self.parentQueueId copyWithZone:zone];
    object.userEmail = [self.userEmail copyWithZone:zone];
    object.create = [self.create copyWithZone:zone];
    object.update = [self.update copyWithZone:zone];
    object.view = [self.view copyWithZone:zone];
    object.divisionid = [self.divisionid copyWithZone:zone];
    object.divisionname = [self.divisionname copyWithZone:zone];
    object.assign = [self.assign copyWithZone:zone];
    
    return object;
}


@end
