//
//  UserRecord.h
//  Ftool
//
//  Created by THONT5 on 3/11/14.
//  Copyright (c) 2014 RAD. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserRecord : NSObject

@property BOOL checkChanged;
//@property BOOL loadingIsOkay;

@property (nonatomic,retain) NSString *userId;
@property (nonatomic,retain) NSString *userName;
@property (nonatomic,retain) NSString *userPass;
@property (nonatomic,retain) NSString *phone;
@property (nonatomic,retain) NSString *ipPhone;
@property (nonatomic,retain) NSString *create;
@property (nonatomic,retain) NSString *update;
@property (nonatomic,retain) NSString *view;
@property (nonatomic,retain) NSString *role;
@property (nonatomic,retain) NSString *queueId;
@property (nonatomic,retain) NSString *parentQueueId;
@property (nonatomic,retain) NSString *userEmail;
@property (nonatomic,retain) NSString *divisionname;
@property (nonatomic,retain) NSString *divisionid;
@property (nonatomic,retain) NSString *assign;

@end
