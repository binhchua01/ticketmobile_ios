//
//  ProfileViewController.m
//  TicketMobile
//
//  Created by HIEUPC on 4/1/15.
//  Copyright (c) 2015 FPT. All rights reserved.
//

#import "ProfileViewController.h"
#import "ShareData.h"

@interface ProfileViewController ()

@end

@implementation ProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    ShareData *shared = [ShareData instance];
    self.lblFullName.text = shared.currentUser.userName;
    self.lblEmail.text = shared.currentUser.userEmail;
    self.lblQueue.text = shared.currentUser.divisionname;
    [self.btnCancel setTitle:@"\u2716" forState:UIControlStateNormal];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)btnCancel_Click:(id)sender{
    if (self.delegate ) {
        [self.delegate Cancel];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
