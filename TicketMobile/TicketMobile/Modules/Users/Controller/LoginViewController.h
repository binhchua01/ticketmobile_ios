//
//  LoginViewController.h
//  TicketMobile
//
//  Created by HIEUPC on 3/3/15.
//  Copyright (c) 2015 FPT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MFSideMenuContainerViewController.h"
#import "MFSideMenu.h"
#import "../../../Helper/MBProgress/MBProgressHUD.h"
#import "TicketMobileAppDelegate.h"

@interface LoginViewController : UIViewController

@property (strong, nonatomic) IBOutlet UITextField *txtEmail;
@property (strong, nonatomic) IBOutlet UITextField *txtPassword;
@property (weak, nonatomic) IBOutlet UITextField *txtOTP;

@property (strong, nonatomic) IBOutlet UILabel *lblCoppy;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollview;
@property (strong, nonatomic) IBOutlet UIButton *btnSignup;
@property (strong, nonatomic) IBOutlet UIButton *btnremember_touch;

-(IBAction)btnLogin:(id)sender;
-(IBAction)btnremember_touch:(id)sender;

@end
