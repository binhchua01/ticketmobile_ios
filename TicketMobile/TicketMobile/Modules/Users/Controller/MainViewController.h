//
//  MainViewController.h
//  TicketMobile
//
//  Created by HIEUPC on 3/3/15.
//  Copyright (c) 2015 FPT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MFSideMenuContainerViewController.h"
#import "MFSideMenu.h"
#import "SiUtils.h"
#import "LeftMenuTableViewController.h"
#import "../../../BaseViewController.h"

@interface MainViewController : BaseViewController
-(IBAction)btnUnAssign:(id)sender;
-(IBAction)btnAssign:(id)sender;
-(IBAction)btnMyTicket:(id)sender;
-(IBAction)btnSurvive:(id)sender;
@end
