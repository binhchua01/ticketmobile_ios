//
//  LoginViewController.m
//  TicketMobile
//
//  Created by HIEUPC on 3/3/15.
//  Copyright (c) 2015 FPT. All rights reserved.
//

#import "LoginViewController.h"
//#import "MainViewController.h"
#import "LeftMenuTableViewController.h"
#import "ListTicketNoAssignViewController.h"
#import "ShareData.h"
#import "Parse/Parse.h"
#import "HDNotificationView.h"

@interface LoginViewController (){
    MBProgressHUD *HUD;
    UIAlertView * updateVersionAlertView;
    NSString * linkUpdateVersion;
    
    TicketMobileAppDelegate *appDelegate;
    
}

@end

@implementation LoginViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    _btnSignup.layer.borderWidth = 2;
    _btnSignup.layer.borderColor = [UIColor whiteColor].CGColor;
    
    [PFUser logOut];
    
    // Do any additional setup after loading the view from its nib.
    
    //self.title = @"LOGIN";
    [self CheckRemember];
    [self.menuContainerViewController setLeftMenuWidth:0];
    
    //Create ScrollView
    [self.scrollview setBackgroundColor:[UIColor clearColor]];
    [self.scrollview setCanCancelContentTouches:NO];
    self.scrollview.indicatorStyle = UIScrollViewIndicatorStyleWhite;
    
    [self.scrollview setScrollEnabled:YES];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnView:)];
    
    [self.view addGestureRecognizer:tap];
    [self InstallIntoParsUUID];
    self.lblCoppy.text = @"Copyright \u24B8 2015 FPT Telecom";
    //[self.btnSignup setEnabled:NO];
    [self checkVersion];
    
    /*
     [[NSNotificationCenter defaultCenter] addObserver:self
     selector:@selector(keyboardDidShow:)
     name:UIKeyboardDidShowNotification
     object:nil];
     
     [[NSNotificationCenter defaultCenter] addObserver:self
     selector:@selector(keyboardDidHide:)
     name:UIKeyboardDidHideNotification
     object:nil];
     */
    [self registerForKeyboardNotifications];
    
    // Register received push notification
    appDelegate = (TicketMobileAppDelegate *)[[UIApplication sharedApplication] delegate];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receivedMessage:)
                                                 name:appDelegate.messageKey
                                               object:nil];
    
}

// Received message from push notification
- (void)receivedMessage:(NSNotification *)notification {
    [self.view endEditing:YES];
    @try {
        NSString *title = notification.userInfo[@"aps"][@"alert"][@"title"];
        NSString *message = notification.userInfo[@"aps"][@"alert"][@"body"];
        if (appDelegate.appActived) {
            NSLog(@"-------Showing notification...");
            NSString *imageName = @"NotificationIcon";
            UIImage *image = [UIImage imageNamed:imageName];
            [HDNotificationView showNotificationViewWithImage:image title:title message:message isAutoHide:NO onTouch:^{
                [HDNotificationView hideNotificationViewOnComplete:nil];
            }];
            // set badge for app icon when received notification
            UIApplication *application = [UIApplication sharedApplication];
            [application setApplicationIconBadgeNumber:application.applicationIconBadgeNumber - 1];
        }
    } @catch (NSException *exception) {
        NSLog(@"---- Error show notification: %@",exception.description);
        return;
    }
}

- (void)keyboardDidShow: (NSNotification *) notif{
    if(IS_IPHONE4){
        CGRect frm = self.view.frame;
        frm.origin.x = frm.origin.x ;
        frm.origin.y = frm.origin.y - 70;
        frm.size.width = frm.size.width;
        frm.size.height = frm.size.height;
        
        [UIView animateWithDuration:0.4
                         animations:^{
                             self.view.frame = frm;
                         }];
    }
    
}

- (void)keyboardDidHide: (NSNotification *) notif{
    if(IS_IPHONE4){
        CGRect frm = self.view.frame;
        frm.origin.x = frm.origin.x ;
        frm.origin.y = frm.origin.y + 70;
        frm.size.width = frm.size.width;
        frm.size.height = frm.size.height;
        [UIView animateWithDuration:0.3
                         animations:^{
                             self.view.frame = frm;
                         }];
    }
}

-(void)checkVersion{
    ShareData *shared = [ShareData instance];
    [shared.appProxy getCheckVersion:@"1.0.6" Platform:@"0" ReleaseDate:@"24-06-2015" completeHandler:^(id result, NSString *errorCode, NSString *message) {
        if([errorCode isEqualToString:@"0"]){
            NSString *Version = StringFormat(@"%@",[result objectForKey:@"Version"]);
            NSString *isReleaseDate = StringFormat(@"%@",[result objectForKey:@"ReleaseDate"]);
            NSString *flag = StringFormat(@"%@",[result objectForKey:@"Flag"]);
            linkUpdateVersion = StringFormat(@"%@",[result objectForKey:@"Link"]);
            [self endCheckVersion:isReleaseDate Version:Version Flag:flag];
            
        }else {
            [self showAlertBox:@"Thông báo" message:message];
        }
        [self hideMBProcess];
    } errorHandler:^(NSError *error) {
        [self hideMBProcess];
        [self showAlertBox:@"Thông Báo" message:[error localizedDescription]];
        NSLog(@"%@",[NSString stringWithFormat:@"%@",error]);
        return;
        
    }];
}

- (void)endCheckVersion:(NSString *)releaseDate Version:(NSString *)version Flag: (NSString *)flag
{
    if (![releaseDate isEqualToString:CheckReleaseDate]) {
        if ([flag isEqualToString:@"1"]) {
            NSString *alertMessage = @"Đã có phiên bản mới xin vui lòng update để tiếp tục sử dụng";
            NSString *alertTitle =@"Thông báo cập nhật";
            updateVersionAlertView = [[UIAlertView alloc] initWithTitle:alertTitle message:alertMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [updateVersionAlertView show];
            return;
        }
    }
    [self.btnSignup setEnabled:YES];
    
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (alertView == updateVersionAlertView) {
        NSLog(@"OK action");
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:linkUpdateVersion]];
        //home button press programmatically
        UIApplication *app = [UIApplication sharedApplication];
        [app performSelector:@selector(suspend)];
        
        //wait 2 seconds while app is going background
        [NSThread sleepForTimeInterval:1.0];
        
        //exit app when app is in background
        exit(0);
        
    }
}


-(void)InstallIntoParsUUID{
    NSString *UDID = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    //NSString *sUDID = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    NSString* uniqueIdentifier = nil;
    if( [UIDevice instancesRespondToSelector:@selector(identifierForVendor)] ) { // >=iOS 7
        uniqueIdentifier = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    } else { //<=iOS6, Use UDID of Device
        CFUUIDRef uuid = CFUUIDCreate(NULL);
        //uniqueIdentifier = ( NSString*)CFUUIDCreateString(NULL, uuid);- for non- ARC
        uniqueIdentifier = ( NSString*)CFBridgingRelease(CFUUIDCreateString(NULL, uuid));// for ARC
        CFRelease(uuid);
    }
    
    [PFInstallation currentInstallation][@"gender"] = UDID;
    [[PFInstallation currentInstallation] saveInBackground];
}

- (void)registerForKeyboardNotifications{
    
    // Add two notifications for the keyboard. One when the keyboard is shown and
    // one when it's about to hide.
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}
- (void)deregisterFromKeyboardNotifications {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardDidHideNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
    
}
- (void)tapOnView:(UITapGestureRecognizer *)sender
{
    [self.scrollview endEditing:YES];
    
}

#pragma mark - How to make UITextView move up when keyboard is present
// Called when the UIKeyboardDidShowNotification is received
- (void)keyboardWasShown:(NSNotification *)notification {
    
    NSDictionary* info = [notification userInfo];
    
    
    // Get the size of the keyboard from the userInfo dictionary.
    CGSize keyboardSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    CGPoint buttonOrigin = self.btnSignup.frame.origin;
    
    CGFloat buttonHeight = self.btnSignup.frame.size.height;
    
    CGRect visibleRect = self.view.frame;
    
    visibleRect.size.height -= keyboardSize.height;
    CGFloat y = 0;
    if (IS_IPHONE4) {
        y = 90;
    }
    CGPoint scrollPoint = CGPointMake(0.0, buttonOrigin.y - visibleRect.size.height + buttonHeight + y);
    
    [self.scrollview setContentOffset:scrollPoint animated:YES];
    
}


- (void)keyboardWillBeHidden:(NSNotification *)notification {
    
    // Move the scroll view back into its original position.
    CGPoint buttonOrigin = self.btnSignup.frame.origin;
    CGPoint scrollPoint = CGPointMake(0, buttonOrigin.y - 377);
    [self.scrollview setContentOffset:scrollPoint animated:YES];
    return;
    
}
-(void)viewDidLayoutSubviews{
    if(IS_IPHONE4){
        // self.lblCoppy.frame = CGRectMake(self.lblCoppy.frame.origin.x, self.lblCoppy.frame.origin.y - 88, self.lblCoppy.frame.size.width, self.lblCoppy.frame.size.height);
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}

-(IBAction)btnLogin:(id)sender{
    if(![SiUtils checkNetworkAvailable]){
        [self showAlertBox:@"Thông báo" message:Mesage_Network];
        return;
    }
    
    //Kiểm tra tên đăng nhập
    if([self.txtEmail.text isEqualToString:@""]){
        [self showAlertBox:@"Thông báo" message:@"Chưa điền tên đăng nhập"];
        return;
    }
    
    //Kiểm tra mật khẩu
    if([self.txtPassword.text isEqualToString:@""]){
        [self showAlertBox:@"Thông báo" message:@"Chưa điền mật khẩu"];
        return;
    }
    
    //Kiểm tra email
    NSArray *arrUser = [self.txtEmail.text componentsSeparatedByString: @"@"];
    if(arrUser.count > 1){
        if(![self validateEmail:self.txtEmail.text]){
            [self showAlertBox:@"Thông báo" message:@"Tên đăng nhập không hợp lệ"];
            return;
        }
    }
    
    [self showMBProcess];
    //    NSString *nib = @"MainViewController";
    //    if (IS_IPAD)
    //    {
    //        nib = @"MainViewController_iPad";
    //    }
    //
    ShareData *shared = [ShareData instance];
    [shared.userProxy doLogin:self.txtEmail.text password:self.txtPassword.text andOTP:self.txtOTP.text completeHandler:^(id result, NSString *errorCode, NSString *message) {
        if([errorCode isEqualToString:@"1"]){
            
            UserRecord *user = result;
            shared.currentUser = user;
            
            if(self.btnremember_touch.selected == YES){
                //Save userName pass
                NSData *dtpass = [SiUtils encryptAESString:self.txtPassword.text withKey:ticketdef];
                NSData *dtuser = [SiUtils encryptAESString:self.txtEmail.text withKey:ticketdef];
                NSMutableDictionary *dict = [NSMutableDictionary dictionary];
                [dict setValue:dtuser forKeyPath:@"u_a"];
                [dict setValue:dtpass forKeyPath:@"u_p"];
                [SiUtils saveUserDefaults:dict];
            }else {
                NSData *dtpass = [SiUtils encryptAESString:@"" withKey:ticketdef];
                NSData *dtuser = [SiUtils encryptAESString:@"" withKey:ticketdef];
                NSMutableDictionary *dict = [NSMutableDictionary dictionary];
                [dict setValue:dtuser forKeyPath:@"u_a"];
                [dict setValue:dtpass forKeyPath:@"u_p"];
                [SiUtils saveUserDefaults:dict];
                
            }
            
            //            MainViewController *vc = [[MainViewController alloc]initWithNibName:nib bundle:nil];
            
            UINavigationController *nav = [[UINavigationController alloc]
                                           initWithRootViewController:[self setTicketNoAssignView]];
            LeftMenuTableViewController *leftMenuViewController = [[LeftMenuTableViewController alloc] init];
            MFSideMenuContainerViewController *container = [MFSideMenuContainerViewController
                                                            containerWithCenterViewController:nav
                                                            leftMenuViewController:leftMenuViewController
                                                            rightMenuViewController:nil];
            self.view.window.rootViewController = container;
            
            shared.currentUser.userPass = self.txtPassword.text;
            
            // Register Token to web service When login success
            [self registerToken];
            
        }else {
            [self showAlertBox:@"Thông báo" message:message];
        }
        // [self updateParseUUID];
        
        [self hideMBProcess];
    } errorHandler:^(NSError *error) {
        [self hideMBProcess];
        [self showAlertBox:@"Thông Báo" message:[error localizedDescription]];
        NSLog(@"%@",[NSString stringWithFormat:@"%@",error.description]);
        return;
        
    }];
}

// Register Token to web service When login success
- (void)registerToken {
    ShareData *shared = [ShareData instance];
    [shared.userProxy registerToken:appDelegate.deviceToken ?:@"" andEmail:shared.currentUser.userEmail completeHandler:^(id result, NSString *errorCode, NSString *message) {
        NSLog(@"------ Register Token to web service: %@", message);
    } errorHandler:^(NSError *error) {
        NSLog(@"------ Register Token to web service: %@",[NSString stringWithFormat:@"%@",error.description]);
    }];
}

// Ticket chưa phân công
- (UIViewController *)setTicketNoAssignView {
    NSString *nibName = @"ListTicketNoAssignViewController";
    if (IS_IPAD) {
        nibName = @"ListTicketNoAssignViewController_iPad";
    }
    ListTicketNoAssignViewController *vc = [[ListTicketNoAssignViewController alloc]initWithNibName:nibName bundle:nil];
    
    return vc;
}

-(IBAction)btnremember_touch:(id)sender
{
    if(self.btnremember_touch.selected == YES){
        self.btnremember_touch.selected = NO;
    }else {
        self.btnremember_touch.selected = YES;
    }
}


#pragma show alert
- (void)showAlertBox:(NSString *)title
             message:(NSString *)message {
    
    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:title message:message delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK",nil];
    [alertView show];
    
}

#pragma mark process bar
- (void) showMBProcess {
    
    HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [self.view addSubview:HUD];
    
    HUD.delegate = (id<MBProgressHUDDelegate>)self;
    HUD.labelText = @"Đang xử lý...";
    
    [HUD show:YES];
}

- (void) hideMBProcess
{
    [HUD hide:YES];
}

#pragma mark - Check remember
-(void)CheckRemember
{
    NSData *userdt = [SiUtils getUserDefaults:@"u_a"];
    NSData *passdt = [SiUtils getUserDefaults:@"u_p"];
    NSString *username = [SiUtils decryptAESData:userdt withKey:ticketdef];
    NSString *password = [SiUtils decryptAESData:passdt withKey:ticketdef];
    
    username = [username stringByTrimmingCharactersInSet:
                [NSCharacterSet whitespaceCharacterSet]];
    self.txtEmail.text = username;
    if(![username isEqualToString:@""] && ![password isEqualToString:@""]){
        self.txtEmail.text = username;
        self.txtPassword.text = password;
        self.btnremember_touch.selected = YES;
    }
}

- (BOOL) validateEmail: (NSString *) candidate {
    NSString *string= candidate;
    NSError *error = NULL;
    NSRegularExpression *regex = nil;
    regex = [NSRegularExpression regularExpressionWithPattern:@"\\w+@[a-zA-Z_]+?\\.[a-zA-Z]{2,6}"
                                                      options:NSRegularExpressionCaseInsensitive
                                                        error:&error];
    NSUInteger numberOfMatches = 0;
    numberOfMatches = [regex numberOfMatchesInString:string
                                             options:0
                                               range:NSMakeRange(0, [string length])];
    //   NSLog(@"numberOfMatches is: %lu", numberOfMatches);
    if(numberOfMatches != 0){
        return TRUE;
    }
    return FALSE;
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
