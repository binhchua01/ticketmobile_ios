//
//  MainViewController.m
//  TicketMobile
//
//  Created by HIEUPC on 3/3/15.
//  Copyright (c) 2015 FPT. All rights reserved.
//

#import "MainViewController.h"
#import "TicketOverviewViewController.h"
#import "ListTicketViewController.h"
#import "ListTicketNoAssignViewController.h"
#import "ShareData.h"
#import "Parse/Parse.h"
#import <CommonCrypto/CommonDigest.h>
#import <CommonCrypto/CommonCryptor.h>


@implementation MainViewController
- (void)viewDidLoad {
    [super viewDidLoad];

    // Do any additional setup after loading the view.
    // [self setupMenuBarButtonItems];
    
    //self.title = @"TỔNG QUAN";
    
    self.screenName=self.title;
    [self installationDeviceParse];
}

- (void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    self.navigationController.navigationBarHidden = YES;
}

/* add by DanTT 21.08.2015, target: hide navigationBar when Main View show*/
- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    //self.navigationController.navigationBarHidden = YES;
}

/* add by DanTT 21.08.2015, target: show navigationBar when Main View hide*/
- (void) viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.navigationController.navigationBarHidden = NO;
    
}

-(NSString *) checkKey: (int) len {
    NSString *letters = @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    NSMutableString *SSString = [NSMutableString stringWithCapacity: len];
    for (int i=0; i<len; i++) {
        [SSString appendFormat: @"%C", [letters characterAtIndex: arc4random_uniform((uint32_t) letters.length)]];
    }
    return SSString;
}


-(void)installationDeviceParse{
    ShareData *shared = [ShareData instance];
    NSString *key =[self checkKey:4];
    NSString *UDID = [NSString stringWithFormat:@"%@%@%@", key,[[[NSString stringWithFormat:@"%@;%@", shared.currentUser.userEmail, shared.currentUser.userPass] dataUsingEncoding:NSUTF8StringEncoding] base64EncodedStringWithOptions:0],@"PA"];
    NSString *UserNameKey =[self md5WithString:shared.currentUser.userEmail];
    [PFInstallation currentInstallation][@"DeviceIMEI"] = UDID;
    [PFInstallation currentInstallation][@"UserName"] =UserNameKey;
    [[PFInstallation currentInstallation] saveInBackground];
}
-(NSString *) md5WithString:(NSString *)input
{
    const char *cStr = [input UTF8String];
    unsigned char result[16];
    CC_MD5( cStr, (CC_LONG)strlen(cStr), result ); // This is the md5 call
    return [NSString stringWithFormat:
            @"%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x",
            result[0], result[1], result[2], result[3],
            result[4], result[5], result[6], result[7],
            result[8], result[9], result[10], result[11],
            result[12], result[13], result[14], result[15]
            ];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - event button
-(IBAction)btnUnAssign:(id)sender{
    NSString *nib = @"ListTicketNoAssignViewController";
    if (IS_IPAD)
    {
        nib = @"ListTicketNoAssignViewController_iPad";
    }
    ListTicketNoAssignViewController *vc = [[ListTicketNoAssignViewController alloc]initWithNibName:nib bundle:nil];
    [self.navigationController pushViewController:vc animated:YES];
}

-(IBAction)btnAssign:(id)sender{
   
    NSString *nib = @"TicketOverviewViewController";
    if (IS_IPAD)
    {
        nib = @"TicketOverviewViewController_iPad";
    }
    TicketOverviewViewController *vc = [[TicketOverviewViewController alloc]initWithNibName:nib bundle:nil];
    [self.navigationController pushViewController:vc animated:YES];

}

-(IBAction)btnMyTicket:(id)sender{
    
    NSString *nib = @"ListTicketViewController";
    if (IS_IPAD)
    {
        nib = @"ListTicketViewController_iPad";
    }
    ListTicketViewController *vc = [[ListTicketViewController alloc]initWithNibName:nib bundle:nil];
    vc.numMenu = @"1";
    [self.navigationController pushViewController:vc animated:YES];

}

-(IBAction)btnSurvive:(id)sender{
    
    NSString *nib = @"ListTicketViewController";
    if (IS_IPAD)
    {
        nib = @"ListTicketViewController_iPad";
    }
    ListTicketViewController *vc = [[ListTicketViewController alloc]initWithNibName:nib bundle:nil];
    vc.numMenu = @"2";
    [self.navigationController pushViewController:vc animated:YES];

}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
