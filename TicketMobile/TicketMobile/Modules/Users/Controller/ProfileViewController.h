//
//  ProfileViewController.h
//  TicketMobile
//
//  Created by HIEUPC on 4/1/15.
//  Copyright (c) 2015 FPT. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ProfileDelegate <NSObject>

-(void)Cancel;

@end
@interface ProfileViewController : UIViewController


@property (strong,nonatomic) IBOutlet UIButton *btnCancel;
@property (strong,nonatomic) IBOutlet UILabel *lblFullName;
@property (strong,nonatomic) IBOutlet UILabel *lblEmail;
@property (strong,nonatomic) IBOutlet UILabel *lblQueue;

@property (retain,nonatomic) id<ProfileDelegate> delegate;

-(IBAction)btnCancel_Click:(id)sender;

@end
