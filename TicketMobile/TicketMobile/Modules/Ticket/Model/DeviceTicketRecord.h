//
//  DeviceTicketRecord.h
//  TicketMobile
//
//  Created by HIEUPC on 3/17/15.
//  Copyright (c) 2015 FPT. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DeviceTicketRecord : NSObject

@property (strong, nonatomic) NSString *DeviceName;
@property (strong, nonatomic) NSString *ErrorTimes;
@property (strong, nonatomic) NSString *Status;
@property (strong, nonatomic) NSString *UpdatedBy;
@property (strong, nonatomic) NSString *UpdatedDate;
@property (strong, nonatomic) NSString *Check;

@end
