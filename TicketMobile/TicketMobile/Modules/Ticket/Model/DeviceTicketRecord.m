//
//  DeviceTicketRecord.m
//  TicketMobile
//
//  Created by HIEUPC on 3/17/15.
//  Copyright (c) 2015 FPT. All rights reserved.
//

#import "DeviceTicketRecord.h"

@implementation DeviceTicketRecord

@synthesize DeviceName;
@synthesize ErrorTimes;
@synthesize Status;
@synthesize UpdatedBy;
@synthesize UpdatedDate;
@synthesize Check;

@end
