//
//  TicketDetailRecord.m
//  TicketMobile
//
//  Created by HIEUPC on 3/17/15.
//  Copyright (c) 2015 FPT. All rights reserved.
//

#import "TicketDetailRecord.h"

@implementation TicketDetailRecord

@synthesize Branch;
@synthesize BranchID;
@synthesize CricLev;
@synthesize CusQty;
@synthesize CusType;
@synthesize Description;
@synthesize DeviceType;
@synthesize Effect;
@synthesize FoundIPPhone;
@synthesize FoundMobile;
@synthesize FoundStaff;
@synthesize Issue;
@synthesize IssueGroup;
@synthesize Level;
@synthesize Location;
@synthesize LocationID;
@synthesize OperateStaff;
@synthesize PayTVQty;
@synthesize ProcessIPPhone;
@synthesize ProcessMobile;
@synthesize ProcessStaff;
@synthesize Queue;
@synthesize Reason;
@synthesize RequiredDate;
@synthesize ServiceType;
@synthesize TicketStatus;
@synthesize Type;
@synthesize VisorStaff;
@synthesize expectedDate;
@synthesize IssueDate;
@synthesize CreatedDate;
@synthesize ReasonDetail;
@synthesize Title;
@synthesize Priority;

@end
