//
//  QueueTicket.h
//  TicketMobile
//
//  Created by HIEUPC on 3/12/15.
//  Copyright (c) 2015 FPT. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface QueueTicket : NSObject

@property (nonatomic,retain) NSString *QueueID;
@property (nonatomic,retain) NSString *QueueName;
@property (nonatomic,retain) NSString *New;
@property (nonatomic,retain) NSString *Progress;
@property (nonatomic,retain) NSString *NotClose;
@property (nonatomic,retain) NSString *OverTime;

@property BOOL isClicked;

@end
