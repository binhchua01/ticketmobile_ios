//
//  MyTicket.h
//  TicketMobile
//
//  Created by THONT5 on 3/12/15.
//  Copyright (c) 2015 FPT. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MyTicket : NSObject

@property (nonatomic,retain) NSString *Level;
@property (nonatomic,retain) NSString *IssueName;
@property (nonatomic,retain) NSString *TicketID;
@property (nonatomic,retain) NSString *TicketStatus;
@property (nonatomic,retain) NSString *Title;
@property (nonatomic,retain) NSString *ExistTime;
@property (nonatomic,retain) NSString *Queue;

// Thêm thời gian tạo với lại thơi gian làm
@property (nonatomic,retain) NSString *CreateTime;
@property (nonatomic,retain) NSString *TimeWorked;

@end
