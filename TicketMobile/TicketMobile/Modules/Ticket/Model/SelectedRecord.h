//
//  SelectedRecord.h
//  TicketMobile
//
//  Created by HIEUPC on 3/26/15.
//  Copyright (c) 2015 FPT. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SelectedRecord : NSObject

@property (strong, nonatomic) NSString *Id;
@property (strong, nonatomic) NSString *Name;
@property (strong, nonatomic) NSString *Check;

- (id)initWithName:(NSString *)name withID:(NSString *)ID andCheck:(NSString *)check;

@end
