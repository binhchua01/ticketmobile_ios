//
//  TicketProxy.h
//  TicketMobile
//
//  Created by HIEUPC on 3/11/15.
//  Copyright (c) 2015 FPT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseProxy.h"

@interface TicketProxy : BaseProxy

- (void)getMyCreatTicket:(NSString *)email completeHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;

- (void)getMyTicket:(NSString *)email completeHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;

- (void)getqueueExistedTicketList:(NSString *)userID completeHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;

- (void)getnewTicketList:(NSString *)userID completeHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;

- (void)getTicketByStatus:(NSString *)queue Status:(NSString *)status UserID:(NSString *)userId completeHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;

-(void)getTicketInfo:(NSString *)ticketId completeHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;

- (void)getTicketDevice:(NSString *)ticketId completeHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;

- (void)getPhoneStaff:(NSString *)staffemail completeHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;

- (void)getTicketDeviceUpdate:(NSString *)devicename Status:(NSString *)status TicketId:(NSString *)ticketid UserId:(NSString *)userid Effect:(NSString *)effect CusQty:(NSString *)cusqty Keycode:(NSString *)keycode completeHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;


- (void)getTicketUpdate:(NSString *)ticketid LocationID:(NSString *)locationid BrachID:(NSString *)branchid Issue:(NSString *)issue Reason:(NSString *)reason ServiceType:(NSString *)servicetype CustType:(NSString *)custype Effect:(NSString *)effect Level:(NSString *)level Description:(NSString *)description TicketStatus:(NSString *)ticketstatus Queue:(NSString *)queue QueueName:(NSString *)queueName OperateStaff:(NSString *)operatestaff VisorStaff:(NSString *)visorstaff ProcessStaff:(NSString *)processstaff ProcessMobile:(NSString *)processmobile ProcessIPPhone:(NSString *)processipphone CricLev:(NSString *)criclev CusQty:(NSString *)cusqty PayTVQty:(NSString *)paytvqty UpdatedBy:(NSString *)updatedby UpdatedEmail:(NSString *)updatedemail IssueDate:(NSString*)issueDate RequiredDate:(NSString*)requiredDate ExpectedDate:(NSString*)expectedDate ReasonDetail:(NSString*)reasonDetail KeyCode:(NSString *)keycode  completeHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;

- (void)getUpdateExpectedDate:(NSString *)ticketId expectedDate:(NSString *)expectedDate completeHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;

- (void)getEstimatedDate:(NSString *)cricLev ReasonDetailID:(NSString*)reasonDetailID CreatedDate:(NSString*)createdDate completeHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;
- (void)getEstimateTime:(NSString*)cricLev IssueID:(NSString*)issueID completeHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;

@end
