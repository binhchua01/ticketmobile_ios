//
//  MyCreatTicket.h
//  TicketMobile
//
//  Created by THONT5 on 3/11/15.
//  Copyright (c) 2015 FPT. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MyCreatTicket : NSObject

@property (nonatomic,retain) NSString *Level;
@property (nonatomic,retain) NSString *Queue;
@property (nonatomic,retain) NSString *Staff;
@property (nonatomic,retain) NSString *TicketID;
@property (nonatomic,retain) NSString *TicketStatus;
@property (nonatomic,retain) NSString *Title;
@property (nonatomic,retain) NSString *UpdateDate;
@property (nonatomic,retain) NSString *height;

// Thêm thời gian tạo với lại thơi gian làm
@property (nonatomic,retain) NSString *CreateTime;
@property (nonatomic,retain) NSString *TimeWorked;

@end
