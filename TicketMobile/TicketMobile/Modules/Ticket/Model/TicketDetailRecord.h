//
//  TicketDetailRecord.h
//  TicketMobile
//
//  Created by HIEUPC on 3/17/15.
//  Copyright (c) 2015 FPT. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TicketDetailRecord : NSObject

@property (strong, nonatomic) NSString *Branch;
@property (strong, nonatomic) NSString *BranchID;
@property (strong, nonatomic) NSString *CricLev;
@property (strong, nonatomic) NSString *CusQty;
@property (strong, nonatomic) NSString *CusType;
@property (strong, nonatomic) NSString *Description;
@property (strong, nonatomic) NSString *DeviceType;
@property (strong, nonatomic) NSString *Effect;
@property (strong, nonatomic) NSString *FoundIPPhone;
@property (strong, nonatomic) NSString *FoundMobile;
@property (strong, nonatomic) NSString *FoundStaff;
@property (strong, nonatomic) NSString *Issue;
@property (strong, nonatomic) NSString *IssueGroup;
@property (strong, nonatomic) NSString *Level;
@property (strong, nonatomic) NSString *Location;
@property (strong, nonatomic) NSString *LocationID;
@property (strong, nonatomic) NSString *OperateStaff;
@property (strong, nonatomic) NSString *PayTVQty;
@property (strong, nonatomic) NSString *ProcessIPPhone;
@property (strong, nonatomic) NSString *ProcessMobile;
@property (strong, nonatomic) NSString *ProcessStaff;
@property (strong, nonatomic) NSString *Queue;
@property (strong, nonatomic) NSString *Reason;
@property (strong, nonatomic) NSString *RequiredDate;
@property (strong, nonatomic) NSString *ServiceType;
@property (strong, nonatomic) NSString *TicketStatus;
@property (strong, nonatomic) NSString *Type;
@property (strong, nonatomic) NSString *VisorStaff;

@property (strong, nonatomic) NSString *expectedDate;
@property (strong, nonatomic) NSString *IssueDate;
@property (strong, nonatomic) NSString *CreatedDate;
@property (strong, nonatomic) NSString *ReasonDetail;
@property (strong, nonatomic) NSString *Title;
@property (strong, nonatomic) NSString *Priority;



@end
