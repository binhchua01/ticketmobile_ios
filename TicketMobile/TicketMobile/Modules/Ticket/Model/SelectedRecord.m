//
//  SelectedRecord.m
//  TicketMobile
//
//  Created by HIEUPC on 3/26/15.
//  Copyright (c) 2015 FPT. All rights reserved.
//

#import "SelectedRecord.h"

@implementation SelectedRecord

- (id)initWithName:(NSString *)name withID:(NSString *)ID andCheck:(NSString *)check {
    self.Name  = name;
    self.Id    = ID;
    self.Check = check;
    
    return self;
}

@end
