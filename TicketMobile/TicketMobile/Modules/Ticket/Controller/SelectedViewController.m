//
//  SelectedViewController.m
//  TicketMobile
//
//  Created by HIEUPC on 3/22/15.
//  Copyright (c) 2015 FPT. All rights reserved.
//

#import "SelectedViewController.h"
#import "SelectedCell.h"
#import "ShareData.h"
#import "KeyValueModel.h"
#import "SelectedRecord.h"
#import "SiUtils.h"

@interface SelectedViewController (){
    NSMutableArray *tempArray;
}

@end

@implementation SelectedViewController



- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.tableView registerNib:[UINib nibWithNibName:@"SelectedCell" bundle:nil] forCellReuseIdentifier:@"SelectedCell"];
    tempArray = [NSMutableArray arrayWithArray:self.arrList];
    NSString *temp;
    if ([self.type isEqualToString: @"1"]) {
        temp = @"LOẠI DỊCH VỤ";
        
    }
    else if([self.type isEqualToString: @"2"]){
        temp = @"LOẠI KHÁCH HÀNG";
    }
    self.lblTitle.text = temp;
    
    //self.tableView.separatorColor = [UIColor clearColor];
    //    arrselected = [self.selected componentsSeparatedByString: @","];
    //[self LoadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//-(void)viewDidLayoutSubviews{
//    if(IS_IPHONE4){
//        self.tableView.frame = CGRectMake(0, self.tableView.frame.origin.y, self.tableView.frame.size.width, self.tableView.frame.size.height - 100);
//        self.btnDone.frame = CGRectMake(self.btnDone.frame.origin.x, self.btnDone.frame.origin.y - 100, self.btnDone.frame.size.width, self.btnDone.frame.size.height);
//    }
//
//}

#pragma mark - UITableView Delegate & Datasrouce -

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(self.arrList.count > 0){
        return self.arrList.count;
    }
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SelectedRecord *record = self.arrList[indexPath.row];
    
    CGSize constraint = CGSizeMake(210, 20000.0f);
    
    // constratins the size of the table row according to the text
    CGRect textRect = [record.Name boundingRectWithSize:constraint options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading) attributes:@{NSFontAttributeName:[UIFont fontWithName:@"Helvetica-Light" size:15]} context:nil];
    
    CGFloat height = MAX(textRect.size.height,40);
    return height;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SelectedCell *cell = (SelectedCell *)[self.tableView dequeueReusableCellWithIdentifier:@"SelectedCell"];
    if(cell != nil){
        if(self.arrList.count > 0){
            SelectedRecord *rc = [self.arrList objectAtIndex:indexPath.row];
            [cell.btnradio setTag:indexPath.row];
            cell.lblName.text = rc.Name;
            if([rc.Check isEqualToString:@"1"]){
                cell.ic_image.image = [SiUtils imageWithImageHeight:[UIImage imageNamed:@"switchCheck"] height:20];
                
            }else{
                cell.ic_image.image = [SiUtils imageWithImageHeight:[UIImage imageNamed:@"switchUncheck"] height:20];
            }
        }
        
    }
    cell.backgroundColor = [UIColor clearColor];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    SelectedCell *cellItem = (SelectedCell *)[self.tableView cellForRowAtIndexPath:indexPath];
    SelectedRecord *rc = [self.arrList objectAtIndex:indexPath.row];
    SelectedRecord *tempRC = [[SelectedRecord alloc] initWithName:rc.Name withID:rc.Id andCheck:rc.Check];
    if([tempRC.Check isEqualToString:@"1"]){
        cellItem.ic_image.image = [SiUtils imageWithImageHeight:[UIImage imageNamed:@"switchUncheck"] height:20];
        
        tempRC.Check = @"0";
        
    }else{
        cellItem.ic_image.image = [SiUtils imageWithImageHeight:[UIImage imageNamed:@"switchCheck"] height:20];
        tempRC.Check = @"1";
    }
    
    [self.arrList replaceObjectAtIndex:indexPath.row withObject:tempRC];
    
}

#pragma mark - loaddata
-(void)LoadData{
    ShareData *shared = [ShareData instance];
    if([self.type isEqualToString:@"1"]){
        [shared.appProxy getServiceList:^(id result, NSString *errorCode, NSString *message) {
            
            if([errorCode isEqualToString:@"1"]){
                self.arrList = result;
                [self.tableView reloadData];
            }
        } errorHandler:^(NSError *error) {
            [self hideMBProcess];
            [self showAlertBox:@"Thông Báo" message:[error localizedDescription]];
            NSLog(@"%@",[NSString stringWithFormat:@"%@",error]);
            return;
            
        }];
    }else {
        ShareData *shared = [ShareData instance];
        [shared.appProxy getCusTypeList:self.IdServiceType completeHandler:^(id result, NSString *errorCode, NSString *message) {
            if([errorCode isEqualToString:@"1"]){
                self.arrList = result;
                [self.tableView reloadData];
            }else {
                self.arrList = nil;
            }
        } errorHandler:^(NSError *error) {
            [self hideMBProcess];
            [self showAlertBox:@"Thông Báo" message:[error localizedDescription]];
            NSLog(@"%@",[NSString stringWithFormat:@"%@",error]);
            return;
        }];
        
    }
    
}

#pragma mark - event button
-(IBAction)btnDone_Click:(id)sender{
    if (self.deleage ) {
        
        [self.deleage CancelPopup:self.arrList Type:self.type];
    }
    
}

- (IBAction)btnCancel_Click:(id)sender {
    if (self.deleage) {
        //        [self.deleage CancelPopup];
        [self.deleage CancelPopup:tempArray Type:self.type];
        
    }
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
