//
//  SelectedViewController.h
//  TicketMobile
//
//  Created by HIEUPC on 3/22/15.
//  Copyright (c) 2015 FPT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "../../../BaseViewController.h"

@protocol SelectedDeleage <NSObject>

-(void)CancelPopup: (NSMutableArray *)output Type:(NSString *) type;
-(void) CancelPopup;

@end

@interface SelectedViewController : BaseViewController      //UIViewController
@property (strong, nonatomic) IBOutlet UILabel *lblTitle;

@property (strong, nonatomic) IBOutlet UITableView *tableView;

@property (strong, nonatomic) IBOutlet UIButton *btnDone;

@property (strong, nonatomic) NSMutableArray *arrList;
@property (strong, nonatomic) NSMutableArray *arrCheck;

@property (strong, nonatomic) NSString *IdServiceType;
@property (strong, nonatomic) NSString *selected;
@property (strong, nonatomic) NSString *type;

@property (retain ,nonatomic) id<SelectedDeleage> deleage;

-(IBAction)btnDone_Click:(id)sender;

@end
