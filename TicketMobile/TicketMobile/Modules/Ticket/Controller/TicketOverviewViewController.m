//
//  TicketOverviewViewController.m
//  TicketMobile
//
//  Created by HIEUPC on 3/3/15.
//  Copyright (c) 2015 FPT. All rights reserved.
//

#import "TicketOverviewViewController.h"
#import "TicketOverviewCell.h"
#import "ListTicketViewController.h"
#import "ShareData.h"
#import "QueueTicket.h"
#import <UIKit/UIKit.h>

@interface TicketOverviewViewController (){
    NSUInteger indexSectionCurrent;
}

@end

@implementation TicketOverviewViewController

-(void)viewDidAppear:(BOOL)animated{
    NSLog(@"-----------viewDidAppear");
    [super viewDidAppear:animated];
    ShareData *shared = [ShareData instance];
    if (shared.currentUser.checkChanged) {
        shared.currentUser.checkChanged =false;
        [self.tableView reloadData];
    }
}
@synthesize arrList;

- (void)viewDidLoad {
    NSLog(@"------------viewDidLoad");
    self.screenName= @"TỒN CHƯA HT";
    [super viewDidLoad];
    [self LoadQueueNotAssign];
    // Do any additional setup after loading the view.
    //self.tableView.separatorColor = [UIColor clearColor];
    
    self.title = @"TỒN CHƯA HT";
    [self.tableView reloadData];
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:refreshControl];
    
}
- (void)refresh:(UIRefreshControl *)refreshControl {
    NSLog(@"---------Prfess:3");
    [self LoadQueueNotAssign];
    [self.tableView reloadData];
    [refreshControl endRefreshing];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableView Delegate & Datasrouce -
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if(self.arrList.count > 0){
        return self.arrList.count;
    }
    
    return 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    QueueTicket *rc = [self.arrList objectAtIndex:section];
    if (rc.isClicked == YES) {
        return 4;
    }
    return 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *sectionHeaderView = [[UIView alloc] initWithFrame:
                                 CGRectMake(0, 100, tableView.frame.size.width, 70)];
    sectionHeaderView.tag = section;
    sectionHeaderView.layer.cornerRadius = 3.0f;
    //[sectionHeaderView setBackgroundColor:[UIColor colorWithRed:255.0/255.0 green:204.0/255.0 blue:0.0 alpha:1.0]];
    [sectionHeaderView setBackgroundColor: [UIColor colorWithPatternImage: [UIImage imageNamed:@"backgroundHeader"]]];
    //UIView *topView = [[UIView alloc] initWithFrame: CGRectMake(0, 0, tableView.frame.size.width, 1)];
//    UIView *bottomView = [[UIView alloc] initWithFrame:
//                       CGRectMake(0, 35, tableView.frame.size.width, 1)];
    UILabel *headerLabel = [[UILabel alloc] initWithFrame:
                            CGRectMake(60, 15, tableView.frame.size.width, 25.0)];

    //topView.backgroundColor = [UIColor colorWithRed:0.725 green:0.725 blue:0.725 alpha:1];
//    bottomView.backgroundColor = [UIColor colorWithRed:0.725 green:0.725 blue:0.725 alpha:1];
    headerLabel.backgroundColor = [UIColor clearColor];
    [headerLabel setFont:[UIFont fontWithName:@"" size:15.0]];
    //headerLabel.textColor = [UIColor colorWithRed:1 green:0.416 blue:0.094 alpha:1];
    headerLabel.textColor = [UIColor whiteColor];
    UIImage *myImage = [UIImage imageNamed:@"iconHT"];
    UIImageView *imageView = [[UIImageView alloc] initWithImage:myImage];
    imageView.frame = CGRectMake(20,10,30,30);
    [sectionHeaderView addSubview:headerLabel];
    [sectionHeaderView addSubview:imageView];
    //[sectionHeaderView addSubview:topView];
    
    UITapGestureRecognizer *headerTapped = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(sectionHeaderTapped:)];
    [sectionHeaderView addGestureRecognizer:headerTapped];

//    [sectionHeaderView addSubview:bottomView];
//    [sectionHeaderView addSubview:STTLabel];
    if(self.arrList.count > 0){
        QueueTicket *rc = [self.arrList objectAtIndex:section];
         headerLabel.text = rc.QueueName;
        
        //up or down arrow depending on the bool
        UIImageView *upDownArrow = [[UIImageView alloc] initWithImage:rc.isClicked ? [UIImage imageNamed:@"ic_up"] : [UIImage imageNamed:@"ic_down"]];
        upDownArrow.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
        upDownArrow.frame = CGRectMake(tableView.frame.size.width - 34, 20, 15, 10);
        
        [sectionHeaderView addSubview:upDownArrow];
    }
    return sectionHeaderView;
    
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView *footer  = [[UIView alloc] init];
    footer.backgroundColor = [UIColor colorWithRed:230.0/255.0 green:230.0/255.0 blue:230.0/255.0 alpha:1.0];
    return footer;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 50;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 8;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    QueueTicket *rc = [self.arrList objectAtIndex:indexPath.section];
    if (rc.isClicked == YES) {
        return 50;
    }

    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

    NSString *simpleTableIdentifier = @"TicketOverviewCell";
    if(IS_IPAD){
        simpleTableIdentifier = @"TicketOverviewCell_iPad";
    }
    TicketOverviewCell *cell = (TicketOverviewCell *)[self.tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:simpleTableIdentifier owner:self options:nil];
    cell = [nib objectAtIndex:0];
    
    if(self.arrList.count > 0){
        QueueTicket *rc = [self.arrList objectAtIndex:indexPath.section];
        
        switch (indexPath.row)
        {
           
            case 0:
                cell.lblName.text = @"Chưa xử lý";
                cell.lblNumber.text = rc.New;
                cell.imageStatus.image = [UIImage imageNamed:@"dot_ChuaXL"];
                break;
            case 1:
                cell.lblName.text = @"Đang xử lý";
                cell.lblNumber.text = rc.Progress;
                cell.imageStatus.image = [UIImage imageNamed:@"dot_DangXL"];
                break;
            case 2:
                cell.lblName.text = @"Chưa đóng";
                cell.lblNumber.text = rc.NotClose;
                cell.imageStatus.image = [UIImage imageNamed:@"dot_DaXL"];
                break;
            case 3:
                cell.lblName.text = @"Quá hạn";
                cell.lblNumber.text = rc.OverTime;
                cell.imageStatus.image = [UIImage imageNamed:@"dot_QH"];//
                break;
            default:
                break;
        }
        
    }
    
    cell.backgroundColor = [UIColor clearColor];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

    NSString *nib = @"ListTicketViewController";
    if (IS_IPAD)
    {
        nib = @"ListTicketViewController_iPad";
    }
    NSString *status = @"0";
    switch (indexPath.row)
    {
        case 0:
            status = @"-1";
            break;
        case 1:
            status = @"0";
            break;
        case 2:
            status = @"3";
            break;
        case 3:
            status = @"4";
            break;
        default:
            break;
    }
    QueueTicket *rc = [self.arrList objectAtIndex:indexPath.section];
    ListTicketViewController *vc = [[ListTicketViewController alloc]initWithNibName:nib bundle:nil];
    vc.Status = status;
    vc.Queue = rc.QueueID;
    vc.numMenu = @"4";
    [self.navigationController pushViewController:vc animated:YES];
    
}

#pragma mark - LoadData
-(void)LoadQueueNotAssign {
    [self showMBProcess:@"Đang tải dữ liệu..."];
    ShareData *shared = [ShareData instance];
 
    [shared.ticketProxy getqueueExistedTicketList:shared.currentUser.userId completeHandler:^(id result, NSString *errorCode, NSString *message) {
        if([errorCode isEqualToString:@"1"]){
            self.arrList = result;
            [self.tableView reloadData];
        }else {
            [self showAlertBox:@"Thông Báo" message:Mesage_DataEmpty];
        }
        [self hideMBProcess];
    } errorHandler:^(NSError *error) {
        [self hideMBProcess];
        [self showAlertBox:@"Thông Báo" message:[error localizedDescription]];
        NSLog(@"%@",[NSString stringWithFormat:@"%@",error]);
        return;

    }];
}

#pragma mark - gesture tapped
- (void)sectionHeaderTapped:(UITapGestureRecognizer *)gestureRecognizer{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:gestureRecognizer.view.tag];
    if (indexPath.row == 0) {
        if (indexPath.section != indexSectionCurrent) {
            //[[self.arrList objectAtIndex:indexSectionCurrent] setValue:@"NO" forKey:@"isClicked"];
            QueueTicket *rc = [self.arrList objectAtIndex:indexSectionCurrent];
            rc.isClicked = NO;
            [self.arrList replaceObjectAtIndex:indexSectionCurrent withObject:rc];
            indexSectionCurrent = indexPath.section;
        }
        
     //   [self.tableView endUpdates];
        QueueTicket *rc = [self.arrList objectAtIndex:indexPath.section];
        rc.isClicked = !rc.isClicked;
        [self.arrList replaceObjectAtIndex:indexPath.section withObject:rc];
        //[[self.arrList objectAtIndex:0] setValue:@"YES" forKey:@"isClicked"];

        //reload specific section animated
        //NSRange range   = NSMakeRange(indexPath.section, 1);
        NSRange range   = NSMakeRange(0, self.arrList.count);
        NSIndexSet *sectionToReload = [NSIndexSet indexSetWithIndexesInRange:range];
        [self.tableView reloadSections:sectionToReload withRowAnimation:UITableViewRowAnimationFade];
    }
}

#pragma mark - DropDown view
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
