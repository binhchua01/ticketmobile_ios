//
//  ListTicketViewController.h
//  TicketMobile
//
//  Created by HIEUPC on 3/4/15.
//  Copyright (c) 2015 FPT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"


@interface ListTicketViewController : BaseViewController<UITableViewDataSource,UITableViewDelegate>

@property (strong, nonatomic) IBOutlet UITableView *tableView;

@property (strong, nonatomic) NSString *numMenu;
@property (strong, nonatomic) NSMutableArray *arrList;


@property (strong, nonatomic) IBOutlet UILabel *txtTicketNumber;

@property (strong, nonatomic) NSString *Status;
@property (strong, nonatomic) NSString *Queue;
//@property (nonatomic,retain) UIRefreshControl *refreshControl NS_AVAILABLE_IOS(6_0);

@end
