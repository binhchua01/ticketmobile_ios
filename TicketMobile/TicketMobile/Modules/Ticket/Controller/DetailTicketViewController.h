//
//  DetailTicketViewController.h
//  TicketMobile
//
//  Created by HIEUPC on 3/4/15.
//  Copyright (c) 2015 FPT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "SelectedViewController.h"
#import "SelectedExpectedDateViewController.h"



@interface DetailTicketViewController : BaseViewController<UITableViewDelegate,UITableViewDataSource,SelectedDeleage, SelectedExpectedDateViewControllerDelegate, UITextViewDelegate >

//@property (nonatomic, strong) UIButton * btnStatusOfDeviceCell;

@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollview;
@property (strong, nonatomic) IBOutlet UIView *viewButton;
@property (strong, nonatomic) IBOutlet UIView *viewDescription;
@property (strong, nonatomic) IBOutlet UIView *viewDevice;
@property (strong, nonatomic) IBOutlet UIView *viewInfo;
@property (strong, nonatomic) IBOutlet UITextView *tvNote;

@property (strong, nonatomic) IBOutlet UIButton *btnNote;
@property (strong, nonatomic) IBOutlet UIButton *btnReason;
@property (strong, nonatomic) IBOutlet UIButton *btnReasonDetail; // nguyên nhân chi tiết

@property (strong, nonatomic) IBOutlet UIButton *btnTypeService;
@property (strong, nonatomic) IBOutlet UIButton *btnTypeCustomer;
@property (strong, nonatomic) IBOutlet UIButton *btnScope;
@property (strong, nonatomic) IBOutlet UIButton *btnStatus;
@property (strong, nonatomic) IBOutlet UIButton *btnQueue;
@property (strong, nonatomic) IBOutlet UIButton *btnManager;
@property (strong, nonatomic) IBOutlet UIButton *btnChair;
@property (strong, nonatomic) IBOutlet UIButton *btnWorking;
@property (strong, nonatomic) IBOutlet UIButton *btnSave;

@property (strong, nonatomic) IBOutlet UILabel *lblTicketNo;
@property (strong, nonatomic) IBOutlet UILabel *lblLocation;
@property (strong, nonatomic) IBOutlet UILabel *lblBrank;
@property (strong, nonatomic) IBOutlet UILabel *lblIssueGroup;
@property (strong, nonatomic) IBOutlet UILabel *lblDeviceType;
@property (strong, nonatomic) IBOutlet UILabel *lblFullName;
@property (strong, nonatomic) IBOutlet UILabel *lblPhone;
@property (strong, nonatomic) IBOutlet UILabel *lblIPPhone;
@property (strong, nonatomic) IBOutlet UILabel *lblPhoneProcess;
@property (strong, nonatomic) IBOutlet UILabel *lblIPPhoneProcess;
@property (strong, nonatomic) IBOutlet UILabel *lblCusQty;
@property (strong, nonatomic) IBOutlet UILabel *lblPayTVQty;
@property (strong, nonatomic) IBOutlet UILabel *lblCricLev;
@property (strong, nonatomic) IBOutlet UILabel *lblLevel;
@property (strong, nonatomic) IBOutlet UILabel *lblDEviceNum;

@property (strong, nonatomic) IBOutlet UILabel *lblCreateDate; // TG tạo ticket
@property (strong, nonatomic) IBOutlet UIButton *btnIssueDate; // TG xảy ra sự cố


@property (strong, nonatomic) IBOutlet UILabel *lblRequiredDate; // Thoi Gian yeu cau hoan thanh
- (IBAction)btnExpectedDate:(id)sender;// Thoi gian du kien Hoan thanh
@property (strong, nonatomic) IBOutlet UIButton *btnExpectedDate;

@property BOOL isUpdateExpectedDate;    // set YES neu expected date thay doi
@property (strong, nonatomic) NSString *expectedDateUpdate; // expected date moi
@property (strong, nonatomic) NSString *requiredDateUpdate; // required date moi
@property (strong, nonatomic) NSString *issueDateUpdate; // issue date moi

- (IBAction)btnIssueDate_clicked:(id)sender;

-(IBAction)btnNote_click:(id)sender;
-(IBAction)btnReason_click:(id)sender;
-(IBAction)btnTypeService_click:(id)sender;
-(IBAction)btnTypeCustomer_click:(id)sender;
-(IBAction)btnScope_click:(id)sender;
-(IBAction)btnStatus_click:(id)sender;
-(IBAction)btnQueue_click:(id)sender;
-(IBAction)btnManager_click:(id)sender;
-(IBAction)btnChair_click:(id)sender;
-(IBAction)btnWorking_click:(id)sender;
- (IBAction)btnReasonDetail_click:(id)sender;

-(IBAction)btnSave_click:(id)sender;
-(IBAction)btnFeedback_click:(id)sender;


@property (strong, nonatomic) NSString *ticketId;

@property (retain, nonatomic) NSMutableArray *arrDevice;
@property (strong, nonatomic) NSMutableArray *arrNote;
@property (strong, nonatomic) NSMutableArray *arrReason;
@property (strong, nonatomic) NSMutableArray *arrTypeService;
@property (strong, nonatomic) NSMutableArray *arrTypeCustomer;
@property (strong, nonatomic) NSMutableArray *arrScope;
@property (strong, nonatomic) NSMutableArray *arrStatus;
@property (strong, nonatomic) NSMutableArray *arrQueue;
@property (strong, nonatomic) NSMutableArray *arrManager;
@property (strong, nonatomic) NSMutableArray *arrChair;
@property (strong, nonatomic) NSMutableArray *arrWorking;
@property (strong, nonatomic) NSMutableArray *arrDeviceStatus;
@property (strong, nonatomic) NSMutableArray *arrReasonDetail;

@end
