//
//  ListTicketNoAssignViewController.h
//  TicketMobile
//
//  Created by HIEUPC on 3/12/15.
//  Copyright (c) 2015 FPT. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface ListTicketNoAssignViewController : BaseViewController


@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UILabel *txtTicketNumber;

//@property (strong, nonatomic) IBOutlet UILabel *txtTicketNumber;
@property (strong, nonatomic) NSString *numMenu;
@property (strong, nonatomic) NSMutableArray *arrList;

@end
