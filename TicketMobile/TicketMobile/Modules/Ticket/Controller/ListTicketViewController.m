//
//  ListTicketViewController.m
//  TicketMobile
//
//  Created by HIEUPC on 3/4/15.
//  Copyright (c) 2015 FPT. All rights reserved.
//

#import "ListTicketViewController.h"
#import "ListTicketCell.h"
#import "DetailTicketViewController.h"
#import "../../../Client/ShareData.h"
#import "../Model/MyCreatTicket.h"
#import "../Model/MyTicket.h"
#import "ShareData.h"

@interface ListTicketViewController ()

@end

@implementation ListTicketViewController

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    ShareData *shared = [ShareData instance];
    if (shared.currentUser.checkChanged) {
        shared.currentUser.checkChanged =false;
        [self LoadData];
    }
}

NSString *simpleTableIdentifier = @"ListTicketCell";
@synthesize arrList;

- (void)viewDidLoad {
    [super viewDidLoad];
    //self.screenName = @"My ticket Screen";
    // Do any additional setup after loading the view.
    switch ([self.numMenu intValue]) {
        case 1:
            self.title = @"TICKET CỦA TÔI";
            break;
        case 2:
            self.title = @"TICKET TÔI TẠO";
            break;
        case 3:
            self.title = @"TỒN CHƯA PHÂN CÔNG";
            break;
        case 4:
            self.title = @"TỒN CHƯA HOÀN THÀNH";
            break;
        default:
            break;
    }
    
    self.screenName=self.title ;
    self.tableView.separatorColor = [UIColor clearColor];
    [self.tableView registerNib:[UINib nibWithNibName:@"ListTicketCell" bundle:nil] forCellReuseIdentifier:simpleTableIdentifier];
    [self LoadData];
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc]  init];
    [refreshControl addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:refreshControl];
    
}
- (void)viewWillAppear:(BOOL)animated {
    // This method is called whenever the view is going to appear onscreen.
    // (this includes the first time it appears.)
    [super viewWillAppear:animated];
}

- (void)refresh:(UIRefreshControl *)refreshControl {
    
    NSLog(@"---------Prfess:1");
    [self LoadData];
    [self.tableView reloadData];
    [refreshControl endRefreshing];
}

-(void)viewDidLayoutSubviews{
    //    if(IS_IPHONE4){
    //        self.tableView.frame = CGRectMake(self.tableView.frame.origin.x, self.tableView.frame.origin.y, self.tableView.frame.size.width, self.tableView.frame.size.height - 88);
    //    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableView Delegate & Datasrouce -

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if(self.arrList.count > 0){
        return self.arrList.count;
    }
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 3; // you can have your own choice, of course
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *headerView = [[UIView alloc] init];
    headerView.backgroundColor = [UIColor colorWithRed:204.0/255.0 green:204.0/255.0 blue:204.0/255.0 alpha:1.0];
    return headerView;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(self.arrList.count > 0){
        //return self.arrList.count;
        return 1;
    }
    return 0;
    // return [self.arrList count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    //int i = (int)indexPath;
    float height = 0;
    int titlelenght = 0;
    if([self.numMenu isEqualToString:@"1"]) {
        // MyTicket *rc = [self.arrList objectAtIndex:indexPath.row];
        MyTicket *rc = [self.arrList objectAtIndex:indexPath.section];
        titlelenght = (int) rc.Title.length;
    }else if([self.numMenu isEqualToString:@"2"]) {
        // MyCreatTicket *rc = [self.arrList objectAtIndex:indexPath.row];
        MyCreatTicket *rc = [self.arrList objectAtIndex:indexPath.section];
        titlelenght = (int) rc.Title.length;
    }else if([self.numMenu isEqualToString:@"4"]) {
        // MyTicket *rc = [self.arrList objectAtIndex:indexPath.row];
        MyTicket *rc = [self.arrList objectAtIndex:indexPath.section];
        titlelenght = (int) rc.Title.length;
    }
    if(titlelenght > 19) {
        int numline = titlelenght;
        int val = numline % 19;
        val = val==0?0:1;
        numline = numline / 19 + val;
        height = 13 * (numline -1);
    }
    if([self.numMenu intValue] ==2) {
        return 145 + height;
    } else {
        return 170 + height;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ListTicketCell *cell = (ListTicketCell *)[self.tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    //add by DanTT, sua loi hut chieu rong cell voi ios 7.1
    if (NSFoundationVersionNumber <= NSFoundationVersionNumber_iOS_7_1) {
        cell.contentView.frame = cell.bounds;
        cell.contentView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleRightMargin |UIViewAutoresizingFlexibleTopMargin |UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleBottomMargin;
    }
    
    if(self.arrList.count > 0) {
        if([self.numMenu isEqualToString:@"1"] || [self.numMenu isEqualToString:@"4"]) {
            //MyTicket *rc = [self.arrList objectAtIndex:indexPath.row];
            MyTicket *rc = [self.arrList objectAtIndex:indexPath.section];
            
            cell.lblTitle.text = rc.Title;
            
            cell.lblCode.text = rc.TicketID;
            cell.lblTon.text = rc.ExistTime;
            cell.lblPhanTu.text = rc.IssueName;
            
            cell.lblCreateTime.text = rc.CreateTime;
            cell.lblTimeWorked.text = rc.TimeWorked;
            
        }else if([self.numMenu isEqualToString:@"2"]){
            //MyCreatTicket *rc = [self.arrList objectAtIndex:indexPath.row];
            MyCreatTicket *rc = [self.arrList objectAtIndex:indexPath.section];
            
            cell.lblTitle.text = rc.Title;
            
            cell.lblCode.text = rc.TicketID;
            
            cell.lblPhanTu.text = rc.TicketStatus;
            cell.lblCreateTime.text = rc.UpdateDate;
            cell.lblstatus.text = @"TRẠNG THÁI: ";
            cell.lblForCreatedTime.text = @"NGÀY CẬP NHẬT:";
            
            cell.lblTitleDate.text =@"THỜI GIAN TẠO:";
            cell.lblTon.text = rc.CreateTime;
            
            cell.lblForTimeWorked.hidden = YES;
            cell.lblTimeWorked.hidden = YES;
            cell.imageTimeWorked.hidden = YES;
        }
    }
    //add by DanTT
    cell.bounds = CGRectMake(0.0f, 0.0f, CGRectGetWidth(tableView.bounds), CGRectGetHeight(cell.bounds));
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *nib = @"DetailTicketViewController";
    if (IS_IPAD)
    {
        nib = @"DetailTicketViewController_iPad";
    }
    DetailTicketViewController *vc = [[DetailTicketViewController alloc]initWithNibName:nib bundle:nil];
    if([self.numMenu isEqualToString:@"1"] || [self.numMenu isEqualToString:@"4"]){
        //MyTicket *rc = [self.arrList objectAtIndex:indexPath.row];
        MyTicket *rc = [self.arrList objectAtIndex:indexPath.section];
        
        vc.ticketId = rc.TicketID;
        
    }else if([self.numMenu isEqualToString:@"2"]){
        //MyCreatTicket *rc = [self.arrList objectAtIndex:indexPath.row];
        MyCreatTicket *rc = [self.arrList objectAtIndex:indexPath.section];
        
        vc.ticketId = rc.TicketID;
    }
    [self.navigationController pushViewController:vc animated:YES];
    
}

-(void)LoadData {
    if(![SiUtils checkNetworkAvailable]){
        [self showAlertBox:@"Thông báo" message:Mesage_Network];
        return;
    }
    NSLog(@"Tichet Login--- ");
    switch ([self.numMenu intValue]) {
        case 1:
            [self loadMyTicket];
            break;
        case 2:
            [self LoadMyCreatTicket];
            break;
        case 3:
            self.title = @"TỒN CHƯA PC";
            //[self loadMyTicketUnAssign];
            break;
        case 4:
            self.title = @"TỒN CHƯA HT";
            [self loadMyTicketUnAssign];
            break;
        default:
            break;
    }
    
    if([self.numMenu isEqualToString:@"1"]){
    }
    
}

#pragma mark - Load proxy

-(void)LoadMyCreatTicket{
    [self showMBProcess:@"Đang tải dữ liệu..."];
    ShareData *shared = [ShareData instance];
    [shared.ticketProxy getMyCreatTicket:shared.currentUser.userEmail completeHandler:^(id result, NSString *errorCode, NSString *message) {
        
        if([errorCode isEqualToString:@"1"]){
            self.arrList  = result;
            
            self.txtTicketNumber.text = [NSString stringWithFormat:@"Tổng số ticket: %lu",  (unsigned long)self.arrList.count];
            if(self.arrList.count > 0){
                [self.tableView reloadData];
            }else {
                [self showAlertBox:@"Thông báo" message:Mesage_DataEmpty];
            }
            
        }else {
            [self showAlertBox:@"Thông báo" message:Mesage_DataEmpty];
        }
        
        [self hideMBProcess];
    } errorHandler:^(NSError *error) {
        [self hideMBProcess];
        [self showAlertBox:@"Thông Báo" message:[error localizedDescription]];
        NSLog(@"%@",[NSString stringWithFormat:@"%@",error]);
        return;
        
    }];
    
}

-(void)loadMyTicket {
    
    [self showMBProcess:@"Đang tải dữ liệu ..."];
    ShareData *shared = [ShareData instance];
    [shared.ticketProxy getMyTicket:shared.currentUser.userEmail completeHandler:^(id result, NSString *errorCode, NSString *message) {
        
        if([errorCode isEqualToString:@"1"]){
            self.arrList  = result;
            self.txtTicketNumber.text = [NSString stringWithFormat:@"Tổng số ticket: %lu",  (unsigned long)self.arrList.count];
            
            if(self.arrList.count > 0){
                [self.tableView reloadData];
            }else {
                [self showAlertBox:@"Thông báo" message:Mesage_DataEmpty];
            }
            
        }else {
            [self showAlertBox:@"Thông báo" message:Mesage_DataEmpty];
        }
        
        [self hideMBProcess];
    } errorHandler:^(NSError *error) {
        [self hideMBProcess];
        [self showAlertBox:@"Thông Báo" message:[error localizedDescription]];
        NSLog(@"%@",[NSString stringWithFormat:@"%@",error]);
        return;
        
    }];
}

-(void)loadMyTicketUnAssign{
    [self showMBProcess:@"Đang tải dữ liệu..."];
    ShareData *shared = [ShareData instance];
    [shared.ticketProxy getTicketByStatus:self.Queue Status:self.Status UserID:shared.currentUser.userId completeHandler:^(id result, NSString *errorCode, NSString *message) {
        
        if([errorCode isEqualToString:@"1"]){
            self.arrList  = result;
            if(self.arrList.count > 0){
                self.txtTicketNumber.text = [NSString stringWithFormat:@"Tổng số ticket: %lu",  (unsigned long)self.arrList.count];
                [self.tableView reloadData];
            }else {
                [self showAlertBox:@"Thông báo" message:Mesage_DataEmpty];
            }
            
        }else {
            [self showAlertBox:@"Thông báo" message:Mesage_DataEmpty];
        }
        
        [self hideMBProcess];
        
    } errorHandler:^(NSError *error) {
        [self hideMBProcess];
        [self showAlertBox:@"Thông Báo" message:[error localizedDescription]];
        NSLog(@"%@",[NSString stringWithFormat:@"%@",error]);
        return;
        
        
    }];
}

-(void)alertView:(UIAlertView *)alert_view didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
