//
//  DetailTicketViewController.m
//  TicketMobile
//
//  Created by HIEUPC on 3/4/15.
//  Copyright (c) 2015 FPT. All rights reserved.
//

#import "DetailTicketViewController.h"
#import "DeviceViewCell.h"
#import "KeyValueModel.h"
#import "../../../Helper/UIDropDown/UIPopoverListView.h"
#import "ListFeedbackViewController.h"
#import "ShareData.h"
#import "../Model/TicketDetailRecord.h"
#import "../Model/DeviceTicketRecord.h"
#import "../../../Client/Common.h"
#import "SelectedViewController.h"
#import "SelectedRecord.h"
#import <CommonCrypto/CommonDigest.h>
#import "Reachability.h"
#import <CommonCrypto/CommonCryptor.h>

#import "KxMenu.h"
#import "QuartzCore/QuartzCore.h"

@interface DetailTicketViewController (){
    DeviceViewCell *cellDevice;
    
    int IndexRow;
    NSString *branchId, *locationId, *criclev, *IdServiceType, *IdCusType, *ServiceTypeSelected, *CusTypeSelected, *DeviceName;
    TicketDetailRecord *rc;
    NSMutableArray *mArray;
    UIAlertView * saveAlertView, *updateSuccessAlertView;
    
    UIPopoverListView *popoverView;
    
    UIButton *btnSender;
}

@end

@implementation DetailTicketViewController{
    KeyValueModel *selectedNote, *selectedReason, *selectedTypeService, *selectedTypeCustomer, *selectedScope, *selectedStatus, *selectedQueue, *selectedManager, *selectedChair, *selectedWorking, *selectedDeviceStatus, *selectedReasonDetail;
    NSString *isEffect;
    BOOL isPressButton;
    BOOL isPressButtonIssueDate;
    BOOL isPressButtonExpectedDate;
}

enum tableSourceStype {
    Note = 1,
    Reason = 2,
    TypeService = 3,
    TypeCustomer = 4,
    Scope = 5,
    Status = 6,
    Queue = 7,
    Manager = 8,
    Chair = 9,
    Working = 10,
    DeviceStatus = 11,
    ReasonDetail = 12
};

@synthesize arrDeviceStatus;

- (void)viewDidLoad {
    //GG A
    
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.title = @"THÔNG TIN TICKET";
    isPressButton = NO;
    self.tableView.separatorColor = [UIColor clearColor];
    //Create ScrollView
    [self.scrollview setBackgroundColor:[UIColor clearColor]];
    [self.scrollview setCanCancelContentTouches:NO];
    self.scrollview.indicatorStyle = UIScrollViewIndicatorStyleWhite;
    
    [self.scrollview setScrollEnabled:YES];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnView:)];
    
    [self.view addGestureRecognizer:tap];
    
    float height = self.menuContainerViewController.view.frame.size.height;
    [self.viewButton setFrame:CGRectMake(0,height - 35,320,35)];
    [self.view addSubview:self.viewButton];
    
   
    self.tvNote.delegate = self;
    self.tvNote.layer.borderColor = [UIColor colorWithRed:0.863 green:0.863 blue:0.863 alpha:1].CGColor;
    self.tvNote.layer.borderWidth = 1.f;
    self.tvNote.layer.cornerRadius = 6.0f;
    
    self.arrDeviceStatus = [Common getStatusDevice];
    self.lblTicketNo.text = [NSString stringWithFormat:@"Ticket No: %@",self.ticketId];
    
    mArray = [[NSMutableArray alloc] init];
    //vutt11
    ShareData *shared = [ShareData instance];
   
        if([shared.currentUser.update isEqualToString:@"0"]){
            self.btnSave.enabled = FALSE;
        }
        else{
            self.btnSave.enabled = TRUE;
        }
    
    self.screenName= @"THÔNG TIN TICKET";
    UITapGestureRecognizer *tapScreen = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnScreen:)];
    [self.view addGestureRecognizer:tapScreen];
    
    [self LoadData];
    
}

- (void)viewDidLayoutSubviews {
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.scrollview endEditing:YES];
    [self.view endEditing:YES];
    [self.tvNote endEditing:YES];
    [self.viewButton endEditing:YES];
    //[self.viewDescription endEditing:YES];
    //[self.viewDevice endEditing:YES];
    //[self.viewInfo endEditing:YES];
}
- (void)tapOnView:(UITapGestureRecognizer *)sender {
    [self.scrollview endEditing:YES];
    
}

- (void)tapOnScreen:(UITapGestureRecognizer *)sender {
    if (popoverView != nil) {
        [popoverView hideDropDown:btnSender];
        [self rel];
    }
    
    // add by DanTT 24.08.2015
    [self.tvNote resignFirstResponder];
}
#pragma mark - UITextView delegate
- (void)textViewDidBeginEditing:(UITextView *)textView{
    if (popoverView != nil) {
        [popoverView hideDropDown:btnSender];
        [self rel];
    }
}

#pragma mark - UITableView Delegate & Datasrouce -

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(self.arrDevice.count > 0){
        return self.arrDevice.count;
    }
    return 0;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    float height = 0;
    int deviceNameLenght = 0;
    if (self.arrDevice.count > 0) {
        DeviceTicketRecord *devicerecord = [self.arrDevice objectAtIndex:indexPath.row];
        deviceNameLenght = (int) devicerecord.DeviceName.length;
        if (deviceNameLenght > 21) {
            int numline = deviceNameLenght;
            int val = numline %21;
            val = val ==0?0:1;
            numline = numline/21 + val;
            height = 10 *(numline-1);
        }
    }
    return 150 + height;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *simpleTableIdentifier = @"DeviceViewCell";
    DeviceViewCell *cell = (DeviceViewCell *)[self.tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"DeviceViewCell" owner:self options:nil];
    cell = [nib objectAtIndex:0];
    
    if(cell != nil){
        if(self.arrDevice.count > 0){
            DeviceTicketRecord *devicerecord = [self.arrDevice objectAtIndex:indexPath.row];
            cell.lblDate.text = devicerecord.UpdatedDate;
            cell.lblName.text = devicerecord.DeviceName;
            cell.lblPerson.text = devicerecord.UpdatedBy;
            NSString *StatusName = devicerecord.Status;
            if([[StatusName uppercaseString] isEqualToString:@"NEW"]){
                StatusName = @"Chưa xử lý";
            }else if([[StatusName uppercaseString]  isEqualToString:@"IN"]){
                StatusName = @"Đang xử lý";
            }
            else if([[StatusName uppercaseString]  isEqualToString:@"OK"]){
                StatusName = @"Đã hoàn thành";
            }
            else if([[StatusName uppercaseString]  isEqualToString:@"NOTOK"]){
                StatusName = @"Chưa hoàn thành";
            }
            [cell.btnStatus  setTitle:StatusName forState:UIControlStateNormal];
            [cell.btnStatus setTag:indexPath.row];
            cell.lblTime.text = devicerecord.ErrorTimes;
            [cell.btnStatus addTarget:self action:@selector(btnStatus_touch:) forControlEvents:UIControlEventTouchUpInside];
            
            // add by DanTT 20.08.2015
            //cell.delegate = (id)self;
            //cell.detailTicketViewController = self;
            //cell.popoverView = popoverView;
        }
        
    }
    cell.backgroundColor = [UIColor clearColor];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}

#pragma mark - DropDown view

- (void)setupDropDownView:(NSInteger)tag {
    // set down
    [self.view endEditing:YES];
    
    CGFloat xWidth = self.view.bounds.size.width - 20.0f;
    CGFloat yHeight = 272.0f;
    CGFloat yOffset = (self.view.bounds.size.height - yHeight)/2.0f;
    
    UIPopoverListView *poplistview = [[UIPopoverListView alloc] initWithFrame:CGRectMake(10, yOffset, xWidth, yHeight)];
    poplistview.delegate = (id)self;
    poplistview.datasource = (id)self;
    poplistview.tag = tag;
    poplistview.listView.scrollEnabled = TRUE;
    
    switch (tag) {
        case Note:
            [poplistview setTitle:NSLocalizedString(@"Chọn mô tả", @"")];
            break;
        case Reason:
            [poplistview setTitle:NSLocalizedString(@"Chọn nguyên nhân", @"")];
            break;
        case ReasonDetail:
            [poplistview setTitle:NSLocalizedString(@"Chọn nguyên nhân chi tiết", @"")];
            break;
        case TypeService:
            [poplistview setTitle:NSLocalizedString(@"Chọn loại dịch vụ", @"")];
            break;
        case TypeCustomer:
            [poplistview setTitle:NSLocalizedString(@"Chọn loại khách hàng", @"")];
            break;
        case Scope:
            [poplistview setTitle:NSLocalizedString(@"Chọn phạm vi ảnh hưởng", @"")];
            break;
        case Status:
            [poplistview setTitle:NSLocalizedString(@"Chọn trạng thái", @"")];
            break;
        case Queue:
            [poplistview setTitle:NSLocalizedString(@"Chọn Queue", @"")];
            break;
        case Manager:
            [poplistview setTitle:NSLocalizedString(@"Chọn nhân viên giám sát", @"")];
            break;
        case Chair:
            [poplistview setTitle:NSLocalizedString(@"Chọn nhân viên chủ trì", @"")];
            break;
        case Working:
            [poplistview setTitle:NSLocalizedString(@"Chọn nhân viên xử lý", @"")];
            break;
        case DeviceStatus:
            [poplistview setTitle:NSLocalizedString(@"Chọn trạng thái", @"")];
            break;
        default:
            break;
    }
    [poplistview show];
    
}
- (CGFloat)popoverListView:(UIPopoverListView *)popoverListView
   heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    // add by DanTT 20.08.21015
    switch (popoverListView.tag) {
        case DeviceStatus:
        case Status:
        case Queue:
            return 25.0f;
            break;
        case Reason:
        case ReasonDetail:
            return 50.0f;
            break;
        default:
            break;
    }
    
    return 40.0f;
}

#pragma mark - UIPopoverListViewDataSource

- (void) setUpDropDownCell:(NSInteger)row cell:(UITableViewCell*)cell tag:(NSInteger)tag {
    KeyValueModel *model;
    [self.view endEditing:YES];
    cell.textLabel.font=[UIFont fontWithName:@"Arial" size:13];
    cell.textLabel.textColor = [UIColor whiteColor];
    //cell.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
    cell.textLabel.numberOfLines = 0;
    [cell.textLabel sizeToFit];
    
    cell.backgroundColor = [UIColor blackColor];
    switch (tag) {
        case Note:
            model = [self.arrNote objectAtIndex:row];
            cell.textLabel.text = model.Values;
            break;
        case Reason:
            model = [self.arrReason objectAtIndex:row];
            cell.textLabel.text = model.Values;
            break;
        case ReasonDetail:
            model = [self.arrReasonDetail objectAtIndex:row];
            cell.textLabel.text = model.Values;
            break;
        case TypeService:
            model = [self.arrTypeService objectAtIndex:row];
            cell.textLabel.text = model.Values;
            break;
        case TypeCustomer:
            model = [self.arrTypeCustomer objectAtIndex:row];
            cell.textLabel.text = model.Values;
            break;
        case Scope: {
            NSDictionary *d= [self.arrScope objectAtIndex:row];
            NSString *Name = StringFormat(@"%@",[d objectForKey:@"EffectName"]);
            NSString *Id = StringFormat(@"%@",[d objectForKey:@"EffectID"]);
            model = [[KeyValueModel alloc] initWithName:Id description:Name];
            cell.textLabel.text = model.Values;
            break;
        }
        case Status:
            model = [self.arrStatus objectAtIndex:row];
            cell.textLabel.text = model.Values;
            break;
        case Queue:
            model = [self.arrQueue objectAtIndex:row];
            cell.textLabel.text = model.Values;
            break;
        case Manager:
            model = [self.arrManager objectAtIndex:row];
            cell.textLabel.text = model.Values;
            break;
        case Chair:
            model = [self.arrChair objectAtIndex:row];
            cell.textLabel.text = model.Values;
            break;
        case Working:
            model = [self.arrWorking objectAtIndex:row];
            cell.textLabel.text = model.Values;
            break;
        case DeviceStatus:
            model = [self.arrDeviceStatus objectAtIndex:row];
            cell.textLabel.text = model.Values;
            break;
        default:
            break;
    }
}

#pragma mark - UIPopoverListViewDataSource

- (UITableViewCell *)popoverListView:(UIPopoverListView *)popoverListView
                    cellForIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifier = @"cell";
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                                   reuseIdentifier:identifier] ;
    [self setUpDropDownCell:indexPath.row cell:cell tag:popoverListView.tag];
    //[self setUpDropDownCell:indexPath.row cell:cell tag:popoverView.tag];
    
    return cell;
}

- (NSInteger)popoverListView:(UIPopoverListView *)popoverListView
       numberOfRowsInSection:(NSInteger)section {
    NSInteger count;
    switch (popoverListView.tag){ //popoverView.tag)
        case Note:
            count = self.arrNote.count;
            break;
        case Reason:
            count = self.arrReason.count;
            break;
        case ReasonDetail:
            count = self.arrReasonDetail.count;
            break;
        case TypeService:
            count = self.arrTypeService.count;
            break;
        case TypeCustomer:
            count = self.arrTypeCustomer.count;
            break;
        case Scope:
            count = self.arrScope.count;
            break;
        case Status:
            count = self.arrStatus.count;
            break;
        case Queue:
            count = self.arrQueue.count;
            break;
        case Manager:
            count = self.arrManager.count;
            break;
        case Chair:
            count = self.arrChair.count;
            break;
        case Working:
            count = self.arrWorking.count;
            break;
        case DeviceStatus:
            count = self.arrDeviceStatus.count;
            break;
        default:
            break;
    }
    return count;
}

#pragma mark - UIPopoverListViewDelegate

- (void) didDropDownSelected:(NSInteger)row tag:(NSInteger)tag {
    NSIndexPath *path = [NSIndexPath indexPathForRow:IndexRow inSection:0];
    cellDevice = (DeviceViewCell *)[self.tableView cellForRowAtIndexPath:path];
    

    switch (tag) {
        case Note:
            isPressButton = YES;
            selectedNote = [self.arrNote objectAtIndex:row];
            [self.btnNote setTitle:selectedNote.Values forState:UIControlStateNormal];
            [self showMBProcess:@"Loading..."];
            [self LoadReasonList:@"0" IssueId:selectedNote.Key];
            [self LoadEffectList:@"" IssueId:selectedNote.Key];
            break;
        case Reason:
            isPressButton = YES;
            selectedReason = [self.arrReason objectAtIndex:row];
            [self.btnReason setTitle:selectedReason.Values forState:UIControlStateNormal];
            [self showMBProcess:@"Loading..."];
            [self LoadReasonDetailList:selectedReason.Key key:@""];
            //[self LoadLevel:selectedNote.Key ReasonCode:selectedReason.Key];
            break;
        case ReasonDetail:
            isPressButton = YES;
            selectedReasonDetail = [self.arrReasonDetail objectAtIndex:row];
            [self.btnReasonDetail setTitle:selectedReasonDetail.Values forState:UIControlStateNormal];
            [self showMBProcess:@"Loading..."];
            [self LoadEstimatedDate:criclev ReasonDetailID:selectedReasonDetail.Key];
            break;
        case TypeService:
            selectedTypeService = [self.arrTypeService objectAtIndex:row];
            [self.btnTypeService setTitle:selectedTypeService.Values forState:UIControlStateNormal];
            break;
        case TypeCustomer:
            selectedTypeCustomer = [self.arrTypeCustomer objectAtIndex:row];
            [self.btnTypeCustomer setTitle:selectedTypeCustomer.Values forState:UIControlStateNormal];
            break;
        case Scope:{
            isPressButton = YES;
            NSDictionary *d= [self.arrScope objectAtIndex:row];
            NSString *Name = StringFormat(@"%@",[d objectForKey:@"EffectName"]);
            NSString *Id = StringFormat(@"%@",[d objectForKey:@"EffectID"]);
            selectedScope = [[KeyValueModel alloc] initWithName:Id description:Name];
            [self.btnScope setTitle:selectedScope.Values forState:UIControlStateNormal];
            isEffect = StringFormat(@"%@",[d objectForKey:@"IsEffect"]);
            if(![selectedScope.Key isEqualToString:@"1502"]){
                if ([isEffect isEqualToString:@"0"]) {
                    self.lblCusQty.text = @"0";
                    self.lblPayTVQty.text = @"0";
                    break;
                }
                [self showMBProcess:@"Loading..."];
                [self LoadCusQty:rc.Type];
            }
        }
            break;
        case Status:
            selectedStatus = [self.arrStatus objectAtIndex:row];
            [self.btnStatus setTitle:selectedStatus.Values forState:UIControlStateNormal];
            [self CheckQueue];
            break;
        case Queue:
            selectedQueue = [self.arrQueue objectAtIndex:row];
            [self.btnQueue setTitle:selectedQueue.Values forState:UIControlStateNormal];
            [self LoadOperateStaff:selectedQueue.Key LocationID:locationId BranchID:branchId CricLev:criclev ];
            [self LoadVisorStaff:selectedQueue.Key LocationID:locationId BranchID:branchId Key:@"0"];
            [self LoadProcessStaff:selectedQueue.Key LocationID:locationId BranchID:branchId Key:@"0" Status:selectedQueue.Key ];
            [self CheckProcessStaff];
            //vutt11
            self.lblPhoneProcess.text = @"";
            self.lblIPPhoneProcess.text = @"";

           
            
            
            break;
        case Manager:
            selectedManager = [self.arrManager objectAtIndex:row];
            [self.btnManager setTitle:selectedManager.Values forState:UIControlStateNormal];
            break;
        case Chair:
            selectedChair = [self.arrChair objectAtIndex:row];
            [self.btnChair setTitle:selectedChair.Values forState:UIControlStateNormal];
            break;
        case Working:
            selectedWorking = [self.arrWorking objectAtIndex:row];
            [self.btnWorking setTitle:selectedWorking.Values forState:UIControlStateNormal];
            [self LoadPhoneStaff:selectedWorking.Key];
            break;
        case DeviceStatus:
            selectedDeviceStatus = [self.arrDeviceStatus objectAtIndex:row];
            [cellDevice.btnStatus setTitle:selectedDeviceStatus.Values forState:UIControlStateNormal];
            NSString *devicename = nil, *status;
            
            devicename = DeviceName;
            status = selectedDeviceStatus.Key;
            KeyValueModel *s = [[KeyValueModel alloc] initWithName:devicename description:status];
            int x = 0;
            if(mArray.count == 0){
                [mArray addObject:s];
            }else {
                for (int i =0; i < mArray.count; i++) {
                    KeyValueModel *model = [mArray objectAtIndex:i];
                    if([[DeviceName lowercaseString] isEqualToString:[model.Key lowercaseString]]){
                        x = x+1;
                        [mArray removeObjectAtIndex:i];
                    }
                }
                if(x > 0){
                    [mArray addObject:s];
                }
                
            }
            DeviceTicketRecord *record =[self.arrDevice objectAtIndex:IndexRow];
            record.Status = selectedDeviceStatus.Key;
            break;
            
    }
    [self.view endEditing:NO];
}

// add by Dan TT 20.08.2015: create drop down list
- (void) setDropDownView: (NSInteger)tag withSender:(id)sender {
    NSString *type;
    CGFloat f = 150;
    switch (tag) {
        case Status:
            type = @"up";
            f = 105;
            break;
        case DeviceStatus:
            type = @"down";
            f = 105;
            break;
        case Queue:
        case Manager:
        case Chair:
        case Working:
            type = @"up";
            break;
        case Note:
        case Reason:
        case ReasonDetail:
        case Scope:
            type = @"down";
            break;
        default:
            break;
    }
    
    if (btnSender == sender) {
        if (popoverView != nil) {
            [popoverView hideDropDown:sender];
            [self rel];
            return;
        }
    }
    btnSender = sender;
    
    if (popoverView != nil) {
        [popoverView hideDropDown:sender];
        [self rel];
    }
    if (popoverView == nil) {
        popoverView = [[UIPopoverListView alloc] showDropDown:sender withHeight:&f withType:type];
        popoverView.delegate = (id) self;
        popoverView.datasource = (id) self;
        popoverView.listView.scrollEnabled = TRUE;
        popoverView.tag = tag;
        
    }
}

- (void) niDropDownDelegateMethod: (UIPopoverListView *) sender {
    [self rel];
}

-(void)rel {
    popoverView = nil;
}

#pragma mark - UIPopoverListViewDelegate

- (void)popoverListView:(UIPopoverListView *)popoverListView
     didSelectIndexPath:(NSIndexPath *)indexPath {
    
    [self didDropDownSelected:indexPath.row tag:popoverListView.tag];
    //[self didDropDownSelected:indexPath.row tag:popoverView.tag];
    
}

#pragma mark - event button
- (IBAction)btnExpectedDate:(id)sender {
    [self.tvNote resignFirstResponder];
    isPressButtonExpectedDate = YES;
    SelectedExpectedDateViewController *selectedExpectedDateViewController = [[SelectedExpectedDateViewController alloc] initWithNibName:@"SelectedExpectedDateViewController" bundle:nil];
    selectedExpectedDateViewController.delegate = self;
    selectedExpectedDateViewController.ticketId = self.ticketId;
    selectedExpectedDateViewController.type = @"2";
    // selectedExpectedDateViewController.expectedDate = self.btnExpectedDate.titleLabel.text;
    [self presentPopupViewController:selectedExpectedDateViewController animationType:MJPopupViewAnimationSlideBottomTop];
    
    // add by DanTT 21.08.2015
    [self tapOnScreen:nil];
}

- (IBAction)btnIssueDate_clicked:(id)sender {
    [self.tvNote resignFirstResponder];
    isPressButtonIssueDate = YES;
    SelectedExpectedDateViewController *selectedExpectedDateViewController = [[SelectedExpectedDateViewController alloc] initWithNibName:@"SelectedExpectedDateViewController" bundle:nil];
    selectedExpectedDateViewController.delegate = self;
    selectedExpectedDateViewController.ticketId = self.ticketId;
    selectedExpectedDateViewController.type = @"1";
    selectedExpectedDateViewController.createDate = [self convertStringToDate:rc.CreatedDate];
    [self presentPopupViewController:selectedExpectedDateViewController animationType:MJPopupViewAnimationSlideBottomTop];
    
    [self tapOnScreen:nil];
}

- (IBAction)btnNote_click:(id)sender {
    //[self setupDropDownView:Note];
    [self setDropDownView:Note withSender:sender];
    
}

- (IBAction)btnReason_click:(id)sender {
    //[self setupDropDownView:Reason];
    [self setDropDownView:Reason withSender:sender];
}

- (IBAction)btnTypeService_click:(id)sender {
    [self.tvNote resignFirstResponder];
    SelectedViewController *vc = [[SelectedViewController alloc]initWithNibName:@"SelectedViewController" bundle:nil];
    vc.deleage = (id)self;
    vc.selected = IdServiceType;
    vc.type = @"1";
    vc.lblTitle.text = @"Loại dịch vụ";
    vc.arrList = self.arrTypeService;
    [self presentPopupViewController:vc animationType:MJPopupViewAnimationSlideBottomTop];
    
    // add by DanTT 21.08.2015
    [self tapOnScreen:nil];
}

- (IBAction)btnTypeCustomer_click:(id)sender {
    [self.tvNote resignFirstResponder];
    SelectedViewController *vc = [[SelectedViewController alloc]initWithNibName:@"SelectedViewController" bundle:nil];
    vc.deleage = (id)self;
    vc.IdServiceType = IdServiceType;
    vc.selected = IdCusType;
    vc.arrList = self.arrTypeCustomer;
    vc.type = @"2";
    vc.lblTitle.text = @"Loại khách hàng";
    [self presentPopupViewController:vc animationType:MJPopupViewAnimationSlideBottomTop];
    
    // add by DanTT 21.08.2015
    [self tapOnScreen:nil];
}

- (IBAction)btnScope_click:(id)sender {
    //[self setupDropDownView:Scope];
    [self setDropDownView:Scope withSender:sender];
    
}

- (IBAction)btnStatus_click:(id)sender {
    //[self setupDropDownView:Status];
    [self setDropDownView:Status withSender:sender];
    
}

- (IBAction)btnQueue_click:(id)sender {
    //[self setupDropDownView:Queue];
    [self setDropDownView:Queue withSender:sender];
    
}

- (IBAction)btnManager_click:(id)sender {
    //[self setupDropDownView:Manager];
    [self setDropDownView:Manager withSender:sender];
    
}

- (IBAction)btnChair_click:(id)sender {
    //[self setupDropDownView:Chair];
    [self setDropDownView:Chair withSender:sender];
    
}

- (IBAction)btnWorking_click:(id)sender {
    //[self setupDropDownView:Working];
    [self setDropDownView:Working withSender:sender];
    
}

- (IBAction)btnReasonDetail_click:(id)sender {
    [self setDropDownView:ReasonDetail withSender:sender];
}

- (IBAction)btnStatus_touch:(id)sender {
    UIButton *a= (UIButton *)sender;
    IndexRow = (int)a.tag;
    NSIndexPath *path = [NSIndexPath indexPathForRow:IndexRow inSection:0];
    DeviceViewCell *cellItem = (DeviceViewCell *)[self.tableView cellForRowAtIndexPath:path];
    DeviceName = cellItem.lblName.text;
    //[self setupDropDownView:DeviceStatus];
    [self setDropDownView:DeviceStatus withSender:a];
    
}

// add by DanTT 20.08.2015
/*
 - (void) getButtonStatus:(UIButton *)btnStatus withPopoverView:(UIPopoverListView *)popover{
 UIButton *a= btnStatus;
 //UIButton *a = [self.delegate getButtonStatus]; // lay vi tri Button Status o DeviceViewCell
 //UIButton *a = self.btnStatusOfDeviceCell;
 IndexRow = (int)a.tag;
 NSIndexPath *path = [NSIndexPath indexPathForRow:IndexRow inSection:0];
 DeviceViewCell *cellItem = (DeviceViewCell *)[self.tableView cellForRowAtIndexPath:path];
 DeviceName = cellItem.lblName.text;
 popoverView = popover;
 
 }
 */

// abb by DanTT
- (IBAction)btnSave_click:(id)sender {
    saveAlertView = [[UIAlertView alloc]initWithTitle:@"Thông báo" message:@"Bạn có muốn lưu thông tin ticket? " delegate:self cancelButtonTitle:@"Không" otherButtonTitles:@"Có",nil];
    
    updateSuccessAlertView = [[UIAlertView alloc] initWithTitle:@"Thông báo" message:@"Cập nhật thành công" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    
    [saveAlertView show];
}
// abb by DanTT
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    
    if (alertView == saveAlertView) {
        if ([title isEqualToString:@"Có"]) {
            NSLog(@"Action Có !");
            
            if(![SiUtils checkNetworkAvailable]){
                [self showAlertBox:@"Thông báo" message:Mesage_Network];
                return;
            }
            if([selectedStatus.Key isEqualToString:@"0"]){
                if([self.btnWorking.currentTitle isEqualToString:@"Chọn nhân viên xử lý"]){
                    [self showAlertBox:@"Thông Báo" message:@"Chưa chọn nhân viên xử lý"];
                    return;
                }
            }
            if ([selectedReason.Key isEqualToString:@""]) {
                [self showAlertBox:@"Thông Báo" message:@"Chưa chọn nguyên nhân"];
                return;
            }
            if ([selectedReasonDetail.Key isEqualToString:@""]) {
                [self showAlertBox:@"Thông Báo" message:@"Chưa chọn nguyên nhân chi tiết"];
                return;
            }
            [self showMBProcess:@"Đang lưu ticket..."];
            ShareData *shared = [ShareData instance];
            // cap nhat thoi gian du kien hoan thanh: DaTT2: 04/08/2015
            //[self updateExpectedDate:shared]; // API này không còn dùng
            
            /* luu thong tin ticket */
            if([selectedStatus.Key isEqualToString:@"1"]) {
                if([selectedReason.Key isEqualToString:@"162"]) {
                    [self showAlertBox:@"Thông Báo" message:@"Không thể lưu Ticket khi nguyên nhân chưa xác định"];
                    [self hideMBProcess];
                    return;
                    
                }else {
                    for (int i = 0; i < self.arrDevice.count; i++) {
                        DeviceTicketRecord *record =[self.arrDevice objectAtIndex:i];
                        NSString *statusdevice  = record.Status;
                        if(![[statusdevice uppercaseString] isEqualToString:@"OK"]){
                            [self showAlertBox:@"Thông Báo" message:@"Trạng thái thiết bị chưa chọn hoàn thành"];
                            [self hideMBProcess];
                            return;
                        }
                    }
                    
                }
            }
            //Update ticket
            NSString *ticketid = self.ticketId;
            NSString *issue = selectedNote.Key?:@"";
            NSString *reason = selectedReason.Key?:@"";
            NSString *servicetype = IdServiceType?:@"";
            NSString *custype = IdCusType?:@"";
            NSString *effect = selectedScope.Key?:@"";
            NSString *level = self.lblLevel.text?:@"";
            NSString *description = self.tvNote.text?:@"";
            NSString *ticketstatus = selectedStatus.Key?:@"";
            NSString *queue = selectedQueue.Key?:@"";
            NSString *operatestaff = selectedChair.Key?:@"";
            NSString *visorstaff = selectedManager.Key?:@"";
            NSString *processstaff = selectedWorking.Key?:@"";
            NSString *processmobile = self.lblPhoneProcess.text?:@"";
            NSString *processipphone = self.lblIPPhoneProcess.text?:@"";
            NSString *cusqty = self.lblCusQty.text?:@"";
            NSString *paytvqty = self.lblPayTVQty.text?:@"";
            NSString *userid = shared.currentUser.userId?:@"";
            
            
            NSString *Keycode = [NSString stringWithFormat:@"%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@",KeyApi,ticketid,issue,reason,servicetype,custype,effect, level, description, ticketstatus, queue, operatestaff, visorstaff, processstaff, processmobile, processipphone, cusqty, paytvqty, userid];
            Keycode = [self base64forData:[self md5WithString:Keycode]];
            Keycode = [Keycode lowercaseString];
            
            //Update Device
            if(mArray.count > 0){
                for (int i = 0; i < mArray.count; i++) {
                    KeyValueModel *model = [mArray objectAtIndex:i];
                    NSString *StatusName = model.Values;
                    NSString *devicename = model.Key;
                    NSString *checksum = [NSString stringWithFormat:@"%@%@%@%@%@",KeyApi,devicename,StatusName, shared.currentUser.userId , self.ticketId];
                    checksum = [self base64forData:[self md5WithString:checksum]];
                    checksum = [checksum lowercaseString];
                    [shared.ticketProxy getTicketDeviceUpdate:devicename Status:StatusName TicketId:self.ticketId UserId:shared.currentUser.userId Effect:selectedScope.Key CusQty:self.lblCusQty.text Keycode:checksum completeHandler:^(id result, NSString *errorCode, NSString *message) {
                        
                        if([errorCode isEqualToString:@"1"]){
                            [shared.ticketProxy getTicketUpdate:self.ticketId LocationID:rc.LocationID BrachID:rc.BranchID Issue:selectedNote.Key Reason:selectedReason.Key ServiceType:IdServiceType CustType:IdCusType Effect:selectedScope.Key Level:self.lblLevel.text Description:self.tvNote.text TicketStatus:selectedStatus.Key Queue:selectedQueue.Key QueueName:selectedQueue.Values OperateStaff:selectedChair.Key VisorStaff:selectedManager.Key ProcessStaff:selectedWorking.Key ProcessMobile:self.lblPhoneProcess.text ProcessIPPhone:self.lblIPPhoneProcess.text CricLev:self.lblCricLev.text CusQty:self.lblCusQty.text PayTVQty:self.lblPayTVQty.text UpdatedBy:shared.currentUser.userId UpdatedEmail:shared.currentUser.userEmail IssueDate:self.issueDateUpdate RequiredDate:self.requiredDateUpdate ExpectedDate:self.expectedDateUpdate ReasonDetail:selectedReasonDetail.Key KeyCode:Keycode completeHandler:^(id result, NSString *errorCode, NSString *message) {
                                
                                if([errorCode isEqualToString:@"1"]){
                                    [updateSuccessAlertView show];
                                }else {
                                    [self showAlertBox:@"Thông Báo" message:message];
                                }
                                [self hideMBProcess];
                                return;
                            } errorHandler:^(NSError *error) {
                                [self showAlertBox:@"Thông Báo" message:[error localizedDescription]];
                                NSLog(@"%@",[NSString stringWithFormat:@"%@",error]);
                                [self hideMBProcess];
                                return;
                            }];
                            
                        }else {
                            [self showAlertBox:@"Thông Báo" message:message];
                        }
                    } errorHandler:^(NSError *error) {
                        [self showAlertBox:@"Thông Báo" message:[error localizedDescription]];
                        NSLog(@"%@",[NSString stringWithFormat:@"%@",error]);
                        [self hideMBProcess];
                        return;
                    }];
                }
            }else {
                [shared.ticketProxy getTicketUpdate:self.ticketId LocationID:rc.LocationID BrachID:rc.BranchID Issue:selectedNote.Key Reason:selectedReason.Key ServiceType:IdServiceType CustType:IdCusType Effect:selectedScope.Key Level:self.lblLevel.text Description:self.tvNote.text TicketStatus:selectedStatus.Key Queue:selectedQueue.Key QueueName:selectedQueue.Values OperateStaff:selectedChair.Key VisorStaff:selectedManager.Key ProcessStaff:selectedWorking.Key ProcessMobile:self.lblPhoneProcess.text ProcessIPPhone:self.lblIPPhoneProcess.text CricLev:self.lblCricLev.text CusQty:self.lblCusQty.text PayTVQty:self.lblPayTVQty.text UpdatedBy:shared.currentUser.userId UpdatedEmail:shared.currentUser.userEmail IssueDate:self.issueDateUpdate RequiredDate:self.requiredDateUpdate ExpectedDate:self.expectedDateUpdate ReasonDetail:selectedReasonDetail.Key KeyCode:Keycode completeHandler:^(id result, NSString *errorCode, NSString *message) {
                    
                    if([errorCode isEqualToString:@"1"]){
                        //[self showAlertBox:@"Thông Báo" message:@"Cập nhật thành công"];
                        [updateSuccessAlertView show];
                    }else {
                        [self showAlertBox:@"Thông Báo" message:message];
                    }
                    [self hideMBProcess];
                    return;
                } errorHandler:^(NSError *error) {
                    [self showAlertBox:@"Thông Báo" message:[error localizedDescription]];
                    NSLog(@"%@",[NSString stringWithFormat:@"%@",error]);
                    [self hideMBProcess];
                    return;
                }];
            }
            shared.currentUser.checkChanged=true;
        }
        return;
    }
    if (alertView == updateSuccessAlertView){
        if ([title isEqualToString: @"OK"]) {
            [self.navigationController popViewControllerAnimated:YES];
            return;
        }
    }
}

// cap nhat thoi gian du kien hoan thanh
- (void) updateExpectedDate: (ShareData *) shared{
    if (self.isUpdateExpectedDate == YES) {
        
        [shared.ticketProxy getUpdateExpectedDate:self.ticketId expectedDate:self.expectedDateUpdate completeHandler:^(id result, NSString *errorCode, NSString *message) {
            if (![errorCode isEqualToString:@"1"]) {
                [self showAlertBox:@"Thông báo" message:message];
                [self hideMBProcess];
                return;
            }
        } errorHandler:^(NSError *error) {
            [self showAlertBox:@"Thông Báo" message:[error localizedDescription]];
            NSLog(@"%@",[NSString stringWithFormat:@"%@",error]);
            [self hideMBProcess];
            return;
        }];
        self.isUpdateExpectedDate = NO;
    }
}

-(IBAction)btnFeedback_click:(id)sender{
    ListFeedbackViewController *vc = [[ListFeedbackViewController alloc]initWithNibName:@"ListFeedbackViewController" bundle:nil];
    vc.ticketId  = self.ticketId;
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - LoadData
-(void)LoadData {
    if(![SiUtils checkNetworkAvailable]){
        [self showAlertBox:@"Thông báo" message:Mesage_Network];
        return;
    }
    [self showMBProcess:@"Đang tải dữ liệu..."];
    ShareData *shared = [ShareData instance];
    [shared.ticketProxy getTicketInfo:self.ticketId completeHandler:^(id result, NSString *errorCode, NSString *message) {
        if([SiUtils checkNetworkAvailable]){
            if([errorCode isEqualToString:@"1"]){
                rc = result;
                self.lblBrank.text = rc.Branch;
                self.lblLocation.text = rc.Location;
                self.lblDeviceType.text = rc.DeviceType;
                self.lblIssueGroup.text = rc.IssueGroup;
                self.lblFullName.text = rc.FoundStaff;
                self.lblPhoneProcess.text = rc.ProcessMobile;
                self.lblIPPhoneProcess.text = rc.ProcessIPPhone;
                self.tvNote.text = rc.Description;
                self.lblPayTVQty.text = rc.PayTVQty;
                self.lblCusQty.text = rc.CusQty;
                self.lblLevel.text = rc.Level;
                self.lblCricLev.text = rc.CricLev;
                self.lblPhone.text = rc.FoundMobile;
                self.lblIPPhone.text = rc.FoundIPPhone;
                // thoi gian yeu cau hoan thanh
                self.lblRequiredDate.text =rc.RequiredDate;
                // add by dantt 8.1.2016
                [self.btnExpectedDate setTitle:rc.expectedDate forState:UIControlStateNormal];
                self.lblCreateDate.text = rc.CreatedDate;
                [self.btnIssueDate setTitle:rc.IssueDate forState:UIControlStateNormal];
                // convert type date from dd-MM-yyyy HH:mm:ss to yyyy-MM-dd HH:mm:ss
                self.requiredDateUpdate= [self convertDateToString:[self convertStringToDate:rc.RequiredDate]];
                self.expectedDateUpdate= [self convertDateToString:[self convertStringToDate:rc.expectedDate]];
                self.issueDateUpdate= [self convertDateToString:[self convertStringToDate:rc.IssueDate]];
                
                
                locationId = rc.LocationID;
                branchId = rc.BranchID;
                criclev = rc.CricLev;
                CusTypeSelected = rc.CusType;
                ServiceTypeSelected = rc.ServiceType;
                IdServiceType = rc.ServiceType;
                IdCusType = rc.CusType;
                
                [self LoadDevice];
                
                [self LoadIssueList:rc.Issue Type:rc.Type];

                [self LoadReasonList:rc.Reason IssueId:rc.Issue?:@""];
                
                [self LoadTypeService];
                
                [self LoadTypeCus:rc.ServiceType];
                
//                [self LoadEffectList:rc.Effect IssueId:rc.Issue?:@""];

                [self LoadStatusTicket:rc.TicketStatus CurrentStatus:rc.TicketStatus];
                [self LoadQueueList:rc.Queue];
                
                [self loadNhanVien:rc.Queue];
                
                [self LoadEffectList:rc.Effect IssueId:rc.Issue?:@""];

                return;
            }
            [self showAlertBox:@"Thông Báo" message:Mesage_DataEmpty];
            return;
        }
        [self showAlertBox:@"Thông Báo" message:Mesage_Network];
        [self hideMBProcess];
        return;
    } errorHandler:^(NSError *error) {
        [self hideMBProcess];
        [self showAlertBox:@"Thông Báo" message:[error localizedDescription]];
        NSLog(@"%@",[NSString stringWithFormat:@"%@",error]);
        return;
    }];
}

-(void)loadNhanVien:(NSString *)Queue {
    [self LoadOperateStaff:Queue LocationID:locationId BranchID:branchId CricLev:criclev];
    [self LoadVisorStaff:Queue LocationID:locationId BranchID:branchId Key:rc.VisorStaff];
    [self LoadProcessStaff:Queue LocationID:locationId BranchID:branchId Key:rc.ProcessStaff Status:rc.TicketStatus];
    [self LoadPhoneStaff:rc.ProcessStaff];
//    [self LoadIssueList:rc.Issue Type:rc.Type];
}
-(void)LoadDevice {
    if(![SiUtils checkNetworkAvailable]){
        [self showAlertBox:@"Thông báo" message:Mesage_Network];
        [self hideMBProcess];
        return;
    }
    ShareData *shared = [ShareData instance];
    [shared.ticketProxy getTicketDevice:self.ticketId completeHandler:^(id result, NSString *errorCode, NSString *message) {
        
        if([errorCode isEqualToString:@"1"]){
            self.arrDevice = result;
            if(self.arrDevice.count > 0){
                [self.tableView reloadData];
                self.lblDEviceNum.text = [NSString stringWithFormat:@"%d",(int)self.arrDevice.count];
            }else {
                self.arrDevice = nil;
                self.lblDEviceNum.text = @"0";
            }
//            [self LoadEffectList:rc.Effect IssueId:rc.Issue?:@""];
            return;
        }
        [self showAlertBox:@"Thông Báo" message:Mesage_DataEmpty];
        [self hideMBProcess];
        return;
    } errorHandler:^(NSError *error) {
        [self showAlertBox:@"Thông Báo" message:[error localizedDescription]];
        NSLog(@"%@",[NSString stringWithFormat:@"%@",error]);
        [self hideMBProcess];
        return ;
    }];
    
}

-(void)LoadStatusTicket:(NSString *)Key CurrentStatus:(NSString *)currentstatus {
    if(![SiUtils checkNetworkAvailable]){
        [self showAlertBox:@"Thông báo" message:Mesage_Network];
        [self hideMBProcess];
        return;
    }
    
    ShareData *shared = [ShareData instance];
    [shared.appProxy getTicketStatusList:shared.currentUser.userId CurrentStatus:currentstatus completeHandler:^(id result, NSString *errorCode, NSString *message) {
        if([errorCode isEqualToString:@"1"]){
            self.arrStatus = result;
            if(self.arrStatus.count > 0) {
                int x = -1;
                for (int i = 0; i < self.arrStatus.count; i ++) {
                    selectedStatus = [self.arrStatus objectAtIndex:i];
                    if([selectedStatus.Key isEqualToString:Key]){
                        x =  i;
                        break;
                    }
                }
                
                if(x == -1){
                    [self.btnStatus setTitle:@"Chọn trạng thái" forState:UIControlStateNormal];
                }else {
                    selectedStatus = [self.arrStatus objectAtIndex:x];
                    [self.btnStatus setTitle:selectedStatus.Values forState:UIControlStateNormal];
                }
                
                [self CheckQueue];
            }else {
                self.arrDeviceStatus = nil;
            }
        }
        
    } errorHandler:^(NSError *error) {
        [self showAlertBox:@"Thông Báo" message:[error localizedDescription]];
        NSLog(@"%@",[NSString stringWithFormat:@"%@",error]);
        [self hideMBProcess];
        return;
        
    }];
}

-(void)LoadIssueList:(NSString *)Key Type:(NSString *)type {
    if(![SiUtils checkNetworkAvailable]){
        [self showAlertBox:@"Thông báo" message:Mesage_Network];
        [self hideMBProcess];
        return;
    }

    ShareData *shared = [ShareData instance];
    [shared.appProxy getIssueList:type completeHandler:^(id result, NSString *errorCode, NSString *message) {
        if([errorCode isEqualToString:@"1"]){
            self.arrNote = result;
            if(self.arrNote.count > 0){
                int x = -1;
                for (int i = 0; i < self.arrNote.count; i ++) {
                    selectedNote = [self.arrNote objectAtIndex:i];
                    if([selectedNote.Key isEqualToString:Key]){
                        x =  i;
                        break;
                    }
                }
                if(x == -1){
                    [self.btnNote setTitle:@"Chọn mô tả" forState:UIControlStateNormal];
                    
                }else {
                    selectedNote = [self.arrNote objectAtIndex:x];
                    [self.btnNote setTitle:selectedNote.Values forState:UIControlStateNormal];
                }
            }
            
        }
        
    } errorHandler:^(NSError *error) {
        [self showAlertBox:@"Thông Báo" message:[error localizedDescription]];
        NSLog(@"%@",[NSString stringWithFormat:@"%@",error]);
        [self hideMBProcess];
        return;
        
    }];
}

-(void)LoadReasonList: (NSString *)Key IssueId:(NSString *)issueid {
    if(![SiUtils checkNetworkAvailable]){
        [self showAlertBox:@"Thông báo" message:Mesage_Network];
        [self hideMBProcess];
        return;
    }

    ShareData *shared = [ShareData instance];
    [shared.appProxy getReasonList:issueid completeHandler:^(id result, NSString *errorCode, NSString *message) {
        if([errorCode isEqualToString:@"1"]){
            self.arrReason = result;
            if(self.arrReason.count > 0){
                int x = -1;
                for (int i = 0; i < self.arrReason.count; i ++) {
                    selectedReason = [self.arrReason objectAtIndex:i];
                    if([selectedReason.Key isEqualToString:Key]){
                        x =  i;
                        break;
                    }
                }
                
                if(x == -1){
                    [self.btnReason setTitle:@"[Chọn nguyên nhân]" forState:UIControlStateNormal];
                    selectedReason = [[KeyValueModel alloc] initWithName:@"" description:@""];
                }else {
                    selectedReason = [self.arrReason objectAtIndex:x];
                    [self.btnReason setTitle:selectedReason.Values forState:UIControlStateNormal];
                }
                if (isPressButton == YES) {
                    [self LoadReasonDetailList:selectedReason.Key?:@"" key:@""];
                    return;
                }
                [self LoadReasonDetailList:selectedReason.Key?:@"" key:rc.ReasonDetail?:@""];
               // [self LoadLevel:selectedNote.Key?:@"" ReasonCode:selectedReason.Key?:@""]; // load level không còn sử dụng
            }else {
                self.arrReason = nil;
                [self.btnReason setTitle:@"[Chọn nguyên nhân]" forState:UIControlStateNormal];
            }
            
        }
        
    } errorHandler:^(NSError *error) {
        [self showAlertBox:@"Thông Báo" message:[error localizedDescription]];
        NSLog(@"%@",[NSString stringWithFormat:@"%@",error]);
        [self hideMBProcess];
        return;
    }];
}

-(void)LoadReasonDetailList:(NSString *)reasonID key:(NSString*)Key {
    if(![SiUtils checkNetworkAvailable]){
        [self showAlertBox:@"Thông báo" message:Mesage_Network];
        [self hideMBProcess];
        return;
    }

    ShareData *shared = [ShareData instance];
    [shared.appProxy getReasonDetailList:reasonID completeHandler:^(id result, NSString *errorCode, NSString *message) {
        self.arrReasonDetail = result;
        if (self.arrReasonDetail.count <= 0) {
            [self.btnReasonDetail setTitle:@"[Chọn nguyên nhân chi tiết]" forState:UIControlStateNormal];
            if (isPressButton == YES) {
                isPressButton = NO;
                [self hideMBProcess];
            }
            return;
        }
        if([errorCode isEqualToString:@"1"]){
            if(self.arrReasonDetail.count > 0){
                if ([Key isEqualToString:@""]) {
                    [self.btnReasonDetail setTitle:@"[Chọn nguyên nhân chi tiết]" forState:UIControlStateNormal];
                    selectedReasonDetail = [[KeyValueModel alloc] initWithName:@"" description:@""];
                    if (isPressButton == YES) {
                        isPressButton = NO;
                        [self hideMBProcess];
                    }
                    return;
                }
                for (int i = 0; i < self.arrReasonDetail.count; i ++) {
                    selectedReasonDetail = [self.arrReasonDetail objectAtIndex:i];
                    if([selectedReasonDetail.Key isEqualToString:Key]){
                        [self.btnReasonDetail setTitle:selectedReasonDetail.Values forState:UIControlStateNormal];
                        break;
                    }
                }
                if (isPressButton == YES) {
                    [self LoadEstimatedDate:criclev?:@"" ReasonDetailID:selectedReasonDetail.Key?:@""];
                }
            }
        }
    } errorHandler:^(NSError *error) {
        [self showAlertBox:@"Thông Báo" message:[error localizedDescription]];
        NSLog(@"%@",[NSString stringWithFormat:@"%@",error]);
        [self hideMBProcess];
        return;
    }];
}

- (void)LoadEstimatedDate:(NSString *)cricLev ReasonDetailID:(NSString *)reasonDetailID {
    if(![SiUtils checkNetworkAvailable]){
        [self showAlertBox:@"Thông báo" message:Mesage_Network];
        [self hideMBProcess];
        return;
    }
    NSString *createDate = [self convertDateToString:[self convertStringToDate:rc.CreatedDate]];
    NSLog(@"create date: %@",createDate);
    ShareData *shared = [ShareData instance];
    [shared.ticketProxy getEstimatedDate:cricLev ReasonDetailID:reasonDetailID CreatedDate:createDate completeHandler:^(id result, NSString *errorCode, NSString *message) {
        if([errorCode isEqualToString:@"1"]){
            if (result == nil) {
                if (isPressButton == YES) {
                    [self hideMBProcess];
                    isPressButton = NO;
                }
                return;
            }
            @try {
                NSString *EstimatedDateValue = [[result objectAtIndex:0] objectForKey:@"EstimatedDateValue"];
                [self.btnExpectedDate setTitle:EstimatedDateValue forState:UIControlStateNormal];
                self.lblRequiredDate.text = EstimatedDateValue;
                self.requiredDateUpdate= [self convertDateToString:[self convertStringToDate:EstimatedDateValue]];
                self.expectedDateUpdate= [self convertDateToString:[self convertStringToDate:EstimatedDateValue]];
            }
            @catch (NSException *exception) {
                NSLog(@"%@",exception.description);
            }
        }
        if (isPressButton == YES) {
            [self hideMBProcess];
            isPressButton = NO;
        }
    } errorHandler:^(NSError *error) {
        [self showAlertBox:@"Thông Báo" message:[error localizedDescription]];
        NSLog(@"%@",[NSString stringWithFormat:@"%@",error]);
        [self hideMBProcess];
        return;
    }];
}

-(void)LoadEffectList:(NSString *)Key IssueId:(NSString *)issueid {
    if(![SiUtils checkNetworkAvailable]){
        [self showAlertBox:@"Thông báo" message:Mesage_Network];
        [self hideMBProcess];
        return;
    }

    ShareData *shared = [ShareData instance];
    [shared.appProxy getEffectList:issueid completeHandler:^(id result, NSString *errorCode, NSString *message) {
        self.arrScope = result;
        if (self.arrScope.count <=0) {
            [self hideMBProcess];
            return;
        }
        if([errorCode isEqualToString:@"1"]){
            for (int i = 0; i < self.arrScope.count; i ++) {
                NSDictionary *d= [self.arrScope objectAtIndex:i];
                NSString *Name = StringFormat(@"%@",[d objectForKey:@"EffectName"]);
                NSString *Id = StringFormat(@"%@",[d objectForKey:@"EffectID"]);
                selectedScope = [[KeyValueModel alloc] initWithName:Id description:Name];
                if ([Key isEqualToString:@""]) {
                    [self setScope:d selectedScope:selectedScope];
                    return;
                }
                if ([selectedScope.Key isEqualToString:Key]) {
                    [self setScope:d selectedScope:selectedScope];
                    return;
                }
            }
        }
    } errorHandler:^(NSError *error) {
        [self showAlertBox:@"Thông Báo" message:[error localizedDescription]];
        NSLog(@"%@",[NSString stringWithFormat:@"%@",error]);
        [self hideMBProcess];
        return;
    }];
}

- (void) setScope:(NSDictionary*)d selectedScope:(KeyValueModel*)scope {
    [self.btnScope setTitle:scope.Values forState:UIControlStateNormal];
    isEffect = StringFormat(@"%@",[d objectForKey:@"IsEffect"]);
    if ([isEffect isEqualToString:@"0"]) {
        self.lblCusQty.text = @"0";
        self.lblPayTVQty.text = @"0";
        [self hideMBProcess];
        return;
    }
    [self LoadCusQty:rc.Type];
}

-(void)LoadQueueList:(NSString *)Key {
    if(![SiUtils checkNetworkAvailable]){
        [self showAlertBox:@"Thông báo" message:Mesage_Network];
        [self hideMBProcess];
        return;
    }

    ShareData *shared = [ShareData instance];
    [shared.appProxy getQueueList:^(id result, NSString *errorCode, NSString *message) {
        if([errorCode isEqualToString:@"1"]){
            self.arrQueue = result;
            if(self.arrQueue.count > 0){
                int x = -1;
                for (int i = 0; i < self.arrQueue.count; i ++) {
                    selectedQueue = [self.arrQueue objectAtIndex:i];
                    if([selectedQueue.Key isEqualToString:Key]){
                        x =  i;
                        [self loadNhanVien:selectedQueue.Key];
                        break;
                    }
                }
                if(x == -1){
                    [self.btnQueue setTitle:@"Chọn Queue" forState:UIControlStateNormal];
                }else {
                    selectedQueue = [self.arrQueue objectAtIndex:x];
                    [self.btnQueue setTitle:selectedQueue.Values forState:UIControlStateNormal];
                }
            }else {
                self.arrQueue = nil;
                [self.btnQueue setTitle:@"Chọn Queue" forState:UIControlStateNormal];
            }
            
        }
        
    } errorHandler:^(NSError *error) {
        [self showAlertBox:@"Thông Báo" message:[error localizedDescription]];
        NSLog(@"%@",[NSString stringWithFormat:@"%@",error]);
        [self hideMBProcess];
        return;
    }];
}

-(void)LoadOperateStaff:(NSString *)queue LocationID:(NSString *)locationid BranchID:(NSString *)branchid CricLev:(NSString *)criclevLoadO {
    if(![SiUtils checkNetworkAvailable]){
        [self showAlertBox:@"Thông báo" message:Mesage_Network];
        [self hideMBProcess];
        return;
    }

    ShareData *shared = [ShareData instance];
    [shared.appProxy getOperateStaff:queue LocationID:locationid BranchID:branchid CricLev:criclevLoadO completeHandler:^(id result, NSString *errorCode, NSString *message) {
        
        if([errorCode isEqualToString:@"1"]){
            self.arrChair = result;
            if(self.arrChair.count > 0){
                // [self.btnChair setTitle:@"Vui lòng chọn" forState:UIControlStateNormal];
                int x = -1;
                NSString *arrChairNV =rc.OperateStaff;
                for (int i = 0; i < self.arrChair.count; i ++) {
                    selectedChair = [self.arrChair objectAtIndex:i];
                    if([selectedChair.Key isEqualToString:arrChairNV]){
                        x =  i;
                        break;
                    }
                }
                if (x != -1){
                    selectedChair = [self.arrChair objectAtIndex:x];
                    [self.btnChair setTitle:selectedChair.Values forState:UIControlStateNormal];
                }
                else{
                    selectedChair = [self.arrChair objectAtIndex:0];
                    [self.btnChair setTitle:selectedChair.Values forState:UIControlStateNormal];
                }
                
                
            }else {
                self.arrChair = nil;
                [self.btnChair setTitle:@"Vui lòng chọn" forState:UIControlStateNormal];
            }
            
        }
        
    } errorHandler:^(NSError *error) {
        [self showAlertBox:@"Thông Báo" message:[error localizedDescription]];
        NSLog(@"%@",[NSString stringWithFormat:@"%@",error]);
        [self hideMBProcess];
        return;
    }];
}

-(void)LoadVisorStaff:(NSString *)queue LocationID:(NSString *)locationid BranchID:(NSString *)branchid Key:(NSString *)key {
    if(![SiUtils checkNetworkAvailable]){
        [self showAlertBox:@"Thông báo" message:Mesage_Network];
        [self hideMBProcess];
        return;
    }

    ShareData *shared = [ShareData instance];
    [shared.appProxy getVisorStaff:queue LocationID:locationId BranchID:branchId completeHandler:^(id result, NSString *errorCode, NSString *message) {
        
        if([errorCode isEqualToString:@"1"]){
            self.arrManager = result;
            if(self.arrManager.count > 0){
                int x = -1;
                for (int i = 0; i < self.arrManager.count; i ++) {
                    
                    selectedManager = [self.arrManager objectAtIndex:i];
                    if([selectedManager.Key isEqualToString:key]){
                        x =  i;
                        break;
                    }
                }
                if(x == -1){
                    selectedManager = [self.arrManager objectAtIndex:0];
                    [self.btnManager setTitle:selectedManager.Values forState:UIControlStateNormal];
                }else {
                    selectedManager = [self.arrManager objectAtIndex:x];
                    [self.btnManager setTitle:selectedManager.Values forState:UIControlStateNormal];
                }
                
            }else {
                self.arrManager = nil;
                [self.btnManager setTitle:@"Chọn nhân viên giám sát" forState:UIControlStateNormal];
            }
        }
    } errorHandler:^(NSError *error) {
        [self showAlertBox:@"Thông Báo" message:[error localizedDescription]];
        NSLog(@"%@",[NSString stringWithFormat:@"%@",error]);
        [self hideMBProcess];
        return;
    }];
}

-(void)LoadProcessStaff:(NSString *)queue LocationID:(NSString *)locationid BranchID:(NSString *)branchid Key:(NSString *)key Status:(NSString *)status {
    if(![SiUtils checkNetworkAvailable]){
        [self showAlertBox:@"Thông báo" message:Mesage_Network];
        [self hideMBProcess];
        return;
    }

    ShareData *shared = [ShareData instance];
    if(![status isEqualToString:selectedStatus.Key]){
        [shared.appProxy getProcessStaff:queue LocationID:locationid BranchID:branchid completeHandler:^(id result, NSString *errorCode, NSString *message) {
            
            if([errorCode isEqualToString:@"1"]){
                self.arrWorking = result;
                if(self.arrWorking.count > 0){
                    int x = -1;
                    for (int i = 0; i < self.arrWorking.count; i ++) {
                        selectedWorking = [self.arrWorking objectAtIndex:i];
                        if([selectedWorking.Key isEqualToString:key]){
                            x =  i;
                            break;
                        }
                    }
                    if(x == -1){
                        [self.btnWorking setTitle:@"[Chọn nhân viên xử lý]" forState:UIControlStateNormal];
                        selectedWorking = [[KeyValueModel alloc] initWithName:@"" description:@""];
                    }else {
                        selectedWorking = [self.arrWorking objectAtIndex:x];
                        [self.btnWorking setTitle:selectedWorking.Values forState:UIControlStateNormal];
                        [self LoadPhoneStaff:selectedWorking.Key];
                    }
                    
                }else {
                    self.arrWorking = nil;
                    [self.btnWorking setTitle:@"[Chọn nhân viên xử lý]" forState:UIControlStateNormal];
                }
            }
        } errorHandler:^(NSError *error) {
            [self showAlertBox:@"Thông Báo" message:[error localizedDescription]];
            NSLog(@"%@",[NSString stringWithFormat:@"%@",error]);
            [self hideMBProcess];
            return;
        }];
    }
}

-(void)LoadPhoneStaff:(NSString *)staffemail {
    if(![SiUtils checkNetworkAvailable]){
        [self showAlertBox:@"Thông báo" message:Mesage_Network];
        [self hideMBProcess];
        return;
    }

    ShareData *shared = [ShareData instance];
    [shared.ticketProxy getPhoneStaff:staffemail completeHandler:^(id result, NSString *errorCode, NSString *message) {
        if (result == nil) {
            return;
        }
        if([errorCode isEqualToString:@"1"]){
            self.lblPhoneProcess.text = [result objectForKey:@"Phone"];
            self.lblIPPhoneProcess.text = [result objectForKey:@"IPPhone"];
        }else {
            self.lblPhoneProcess.text = @"";
            self.lblIPPhoneProcess.text = @"";
        }
    } errorHandler:^(NSError *error) {
        self.lblPhoneProcess.text = @"";
        self.lblIPPhoneProcess.text = @"";
        [self showAlertBox:@"Thông Báo" message:[error localizedDescription]];
        NSLog(@"%@",[NSString stringWithFormat:@"%@",error]);
        [self hideMBProcess];
        return;
    }];
}

-(void)LoadCusQty:(NSString *)type {
    if(![SiUtils checkNetworkAvailable]){
        [self showAlertBox:@"Thông báo" message:Mesage_Network];
        [self hideMBProcess];
        return;
    }

    NSString *device;
    for (int i = 0; i < self.arrDevice.count; i++) {
        DeviceTicketRecord *record = [self.arrDevice objectAtIndex:i];
        if(i == 0){
            device = record.DeviceName;
        }else {
            device = [NSString stringWithFormat:@"%@,%@",device,record.DeviceName];
        }
    }
    ShareData *shared = [ShareData instance];
    [shared.appProxy getGetCusQty:type Device:device completeHandler:^(id result, NSString *errorCode, NSString *message) {
        if (result == nil) {
            [self hideMBProcess];
            return;
        }
        if([errorCode isEqualToString:@"1"]){
            self.lblCusQty.text =[result objectForKey:@"CusQty"];
            self.lblPayTVQty.text = [result objectForKey:@"PayTVQty"];
        }
        if (isPressButton == YES) {
            isPressButton = NO;
        }
        [self hideMBProcess];
    } errorHandler:^(NSError *error) {
        [self showAlertBox:@"Thông Báo" message:[error localizedDescription]];
        NSLog(@"%@",[NSString stringWithFormat:@"%@",error]);
        [self hideMBProcess];
        return;
    }];
}

-(void)LoadLevel:(NSString *)issueid ReasonCode:(NSString *)reasoncode {
    if(![SiUtils checkNetworkAvailable]){
        [self showAlertBox:@"Thông báo" message:Mesage_Network];
        [self hideMBProcess];
        return;
    }

    ShareData *shared = [ShareData instance];
    [shared.appProxy getLevel:issueid ReasonCode:reasoncode completeHandler:^(id result, NSString *errorCode, NSString *message) {
        
        if([errorCode isEqualToString:@"1"]){
            self.lblLevel.text = [result objectForKey:@"LevelName"];
        }
        if (isPressButton == YES) {
            [self hideMBProcess];
            isPressButton = NO;
        }
    } errorHandler:^(NSError *error) {
        [self showAlertBox:@"Thông Báo" message:[error localizedDescription]];
        NSLog(@"%@",[NSString stringWithFormat:@"%@",error]);
        [self hideMBProcess];
        return;
    }];
}

-(void)LoadTypeService{
    if(![SiUtils checkNetworkAvailable]){
        [self showAlertBox:@"Thông báo" message:Mesage_Network];
        [self hideMBProcess];
        return;
    }

    ShareData *shared = [ShareData instance];
    [shared.appProxy getServiceList:^(id result, NSString *errorCode, NSString *message) {
        
        if([errorCode isEqualToString:@"1"]){
            NSString *nameServiceType;
            self.arrTypeService = result;
            [self.tableView reloadData];
            NSArray *arrselected = [IdServiceType componentsSeparatedByString: @","];
            for (int j = 0; j < self.arrTypeService.count; j++) {
                SelectedRecord *record = [self.arrTypeService objectAtIndex:j];
                for (int i = 0; i < arrselected.count; i++) {
                    NSString *a = [arrselected objectAtIndex:i];
                    if([a isEqualToString:record.Id] ){
                        record.Check = @"1";
                        if(nameServiceType == nil){
                            nameServiceType = record.Name;
                            
                        }else {
                            nameServiceType  = [NSString stringWithFormat:@"%@,%@",nameServiceType,record.Name];
                        }
                        
                    }
                }
            }
            [self.btnTypeService setTitle:nameServiceType forState:UIControlStateNormal];
        }
    } errorHandler:^(NSError *error) {
        [self showAlertBox:@"Thông Báo" message:[error localizedDescription]];
        NSLog(@"%@",[NSString stringWithFormat:@"%@",error]);
        [self hideMBProcess];
        return;
    }];
}

-(void)LoadTypeCus:(NSString *)idservicetype {
    if(![SiUtils checkNetworkAvailable]){
        [self showAlertBox:@"Thông báo" message:Mesage_Network];
        [self hideMBProcess];
        return;
    }

    ShareData *shared = [ShareData instance];
    [shared.appProxy getCusTypeList:idservicetype completeHandler:^(id result, NSString *errorCode, NSString *message) {
        if([errorCode isEqualToString:@"1"]){
            NSString *nameCus;
            self.arrTypeCustomer = result;
            [self.tableView reloadData];
            NSArray *arrselected = [IdCusType componentsSeparatedByString: @","];
            for (int j = 0; j < self.arrTypeCustomer.count; j++) {
                SelectedRecord *record = [self.arrTypeCustomer objectAtIndex:j];
                for (int i = 0; i < arrselected.count; i++) {
                    NSString *a = [arrselected objectAtIndex:i];
                    if([a isEqualToString:record.Id] ){
                        record.Check = @"1";
                        if(nameCus == nil){
                            nameCus = record.Name;
                        }else {
                            nameCus  = [NSString stringWithFormat:@"%@,%@",nameCus,record.Name];
                        }
                        
                    }
                }
            }
            [self.btnTypeCustomer setTitle:nameCus forState:UIControlStateNormal];
            
        }else {
            self.arrTypeCustomer = nil;
        }
    } errorHandler:^(NSError *error) {
        [self showAlertBox:@"Thông Báo" message:[error localizedDescription]];
        NSLog(@"%@",[NSString stringWithFormat:@"%@",error]);
        [self hideMBProcess];
        return;
    }];
    
}

#pragma mark - Check queue for status
-(void)CheckQueue {
    ShareData *shared = [ShareData instance];
    self.btnWorking.enabled = FALSE;
    self.btnQueue.enabled = FALSE;
    //Status  = New
    
    KeyValueModel *model = [[KeyValueModel alloc]init];
    if([selectedStatus.Key isEqualToString:@"-1"]){
        self.btnQueue.enabled = FALSE;
        if([shared.currentUser.assign isEqualToString:@"1"]){
            
            self.btnWorking.enabled = TRUE;
            
        }
        else {
            self.btnWorking.enabled = FALSE;
            if([shared.currentUser.parentQueueId isEqualToString:selectedQueue.Key]){
                model.Key = shared.currentUser.userEmail;
                model.Values = shared.currentUser.userName;
                [self LoadPhoneStaff:shared.currentUser.userEmail];
                
            }
            else {
                model.Key = @"0";
                model.Values = @"";
            }
            selectedWorking = model;
            [self.btnWorking setTitle:selectedWorking.Values forState:UIControlStateNormal];
            
            
}
        
}
    //Status  = Inprogress
    else if([selectedStatus.Key isEqualToString:@"0"]){
        self.btnQueue.enabled = FALSE;
        //vutt11
        if([shared.currentUser.assign isEqualToString:@"1"]){
            
            self.btnWorking.enabled = TRUE;
            
        }
        if([shared.currentUser.parentQueueId isEqualToString:selectedQueue.Key]){
            model.Key = shared.currentUser.userEmail;
            model.Values = shared.currentUser.userName;
            [self LoadPhoneStaff:shared.currentUser.userEmail];
            selectedWorking = model;
            [self.btnWorking setTitle:selectedWorking.Values forState:UIControlStateNormal];
            
        }
    }
    //Status  = Forward
    else if([selectedStatus.Key isEqualToString:@"3"]){
        self.btnWorking.enabled = TRUE;
    }
    //Status  = Changed
    else if([selectedStatus.Key isEqualToString:@"2"]){
        self.btnQueue.enabled = TRUE;
        self.btnWorking.enabled = TRUE;
    }
    
}

-(void)CheckProcessStaff {
    ShareData *shared = [ShareData instance];
    KeyValueModel *model = [[KeyValueModel alloc]init];
    //If user have role assign
    if([shared.currentUser.assign isEqualToString:@"1"]){
        self.btnWorking.enabled = TRUE;
    }else {
        self.btnWorking.enabled = FALSE;
        //if user have queue same queue select
        if([shared.currentUser.parentQueueId isEqualToString:selectedQueue.Key]){
            model.Key = shared.currentUser.userEmail;
            model.Values = shared.currentUser.userName;
            [self LoadPhoneStaff:shared.currentUser.userEmail];
        }
        else {
            model.Key = @"0";
            model.Values = @"";
            self.lblPhoneProcess.text = @"";
            self.lblIPPhoneProcess.text = @"";
        }
        selectedWorking = model;
        
    }
    [self.btnWorking setTitle:selectedWorking.Values forState:UIControlStateNormal];
}

#pragma mark - Deleage
- (void) CancelPopup{
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationSlideBottomBottom];
}

-(void)CancelPopup:(NSMutableArray *)output Type: type{
    if ([type isEqual:@"1"]) {
        self.arrTypeService = output;
        
    } else {
        self.arrTypeCustomer = output;
    }
    
    NSString *nametypeservice, *namecustype;
    int flagservice = 0, flagcus=0;
    if(output.count > 0){
        for (int i = 0; i < output.count; i++) {
            SelectedRecord *vc = [output objectAtIndex: i];
            if([vc.Check isEqualToString:@"1"]){
                if([type isEqualToString: @"1"]){
                    flagservice = flagservice  + 1;
                    if(nametypeservice == nil){
                        IdServiceType = [NSString stringWithFormat:@"%@",vc.Id];
                        nametypeservice  = [NSString stringWithFormat:@"%@",vc.Name];
                    }else {
                        IdServiceType = [NSString stringWithFormat:@"%@,%@",IdServiceType,vc.Id];
                        nametypeservice  = [NSString stringWithFormat:@"%@,%@",nametypeservice,vc.Name];
                    }
                }else {
                    flagcus = flagcus + 1;
                    if(namecustype == nil){
                        IdCusType = [NSString stringWithFormat:@"%@",vc.Id];
                        namecustype  = [NSString stringWithFormat:@"%@",vc.Name];
                    }else {
                        IdCusType = [NSString stringWithFormat:@"%@,%@",IdCusType,vc.Id];
                        namecustype  = [NSString stringWithFormat:@"%@,%@",namecustype,vc.Name];
                    }
                    
                }
            }
            if([type isEqualToString: @"1"]){
                [self.btnTypeService setTitle:nametypeservice forState:UIControlStateNormal];
            }else {
                [self.btnTypeCustomer setTitle:namecustype forState:UIControlStateNormal];
            }
            
        }
        
    }
    if([type isEqualToString: @"1"]){
        if(flagservice == 0){
            IdServiceType = @"";
            [self.btnTypeService setTitle:@"" forState:UIControlStateNormal];
        }
    }else {
        if(flagcus == 0){
            IdCusType = @"";
            [self.btnTypeCustomer setTitle:@"" forState:UIControlStateNormal];
        }
    }
    
    if([type isEqualToString: @"1"]){
        ShareData *shared = [ShareData instance];
        [shared.appProxy getCusTypeList:IdServiceType completeHandler:^(id result, NSString *errorCode, NSString *message) {
            NSString *t;
            if([errorCode isEqualToString:@"1"]){
                self.arrTypeCustomer = result;
                if(self.arrTypeCustomer.count > 0){
                    NSArray *arrselected = [IdCusType componentsSeparatedByString: @","];
                    for (int j = 0; j < self.arrTypeCustomer.count; j++) {
                        SelectedRecord *record = [self.arrTypeCustomer objectAtIndex:j];
                        for (int i = 0; i < arrselected.count; i++) {
                            NSString *a = [arrselected objectAtIndex:i];
                            if([a isEqualToString:record.Id] ){
                                record.Check = @"1";
                                if(t == nil){
                                    IdCusType = [NSString stringWithFormat:@"%@",record.Id];
                                    t = record.Name;
                                }else {
                                    IdCusType = [NSString stringWithFormat:@"%@,%@",IdCusType,record.Id];
                                    t  = [NSString stringWithFormat:@"%@,%@",t,record.Name];
                                }
                            }
                        }
                    }
                    [self.btnTypeCustomer setTitle:t forState:UIControlStateNormal];
                }else {
                    [self.btnTypeCustomer setTitle:@"" forState:UIControlStateNormal];
                }
            }
            
        } errorHandler:^(NSError *error) {
            [self showAlertBox:@"Thông Báo" message:[error localizedDescription]];
            NSLog(@"%@",[NSString stringWithFormat:@"%@",error]);
            [self hideMBProcess];
            return;
        }];
    }
    
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationSlideBottomTop];
}

// su kien dong cap nhat thoi gian du kien hoan thanh: Datt2_03/08/2015

- (void) CancelPopup:(NSString *)stringExpectedDate expectedDateUpdate:(NSString *)expectedDateUpdate isUpdateExpectedDate:(BOOL)isUpdateExpectedDate{
    self.isUpdateExpectedDate = isUpdateExpectedDate;
    if (isUpdateExpectedDate == YES) {
        if (isPressButtonExpectedDate == YES) {
            isPressButtonExpectedDate = NO;
            [self.btnExpectedDate setTitle:stringExpectedDate forState:UIControlStateNormal];
            self.expectedDateUpdate = expectedDateUpdate;
        }
        if (isPressButtonIssueDate == YES) {
            isPressButtonIssueDate = NO;
            [self.btnIssueDate setTitle:stringExpectedDate forState:UIControlStateNormal];
            self.issueDateUpdate = expectedDateUpdate;
        }
    }
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationSlideTopBottom];
}

#pragma mark - MD5
-(NSData *) md5WithString:(NSString *)input {
    const char *cStr = [input UTF8String];
    unsigned char digest[16];
    CC_MD5( cStr, (CC_LONG)strlen(cStr), digest );
    
    return [[NSData alloc] initWithBytes:digest length:CC_MD5_DIGEST_LENGTH];
}
- (NSString*)base64forData:(NSData*)theData {
    
    const uint8_t* input = (const uint8_t*)[theData bytes];
    NSInteger length = [theData length];
    
    static char table[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
    
    NSMutableData* data = [NSMutableData dataWithLength:((length + 2) / 3) * 4];
    uint8_t* output = (uint8_t*)data.mutableBytes;
    
    NSInteger i;
    for (i=0; i < length; i += 3) {
        NSInteger value = 0;
        NSInteger j;
        for (j = i; j < (i + 3); j++) {
            value <<= 8;
            
            if (j < length) {
                value |= (0xFF & input[j]);
            }
        }
        
        NSInteger theIndex = (i / 3) * 4;
        output[theIndex + 0] =                    table[(value >> 18) & 0x3F];
        output[theIndex + 1] =                    table[(value >> 12) & 0x3F];
        output[theIndex + 2] = (i + 1) < length ? table[(value >> 6)  & 0x3F] : '=';
        output[theIndex + 3] = (i + 2) < length ? table[(value >> 0)  & 0x3F] : '=';
    }
    
    return [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
}

- (NSDate *)convertStringToDate:(NSString *)dateStringInput {
    @try {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"dd/MM/yyyy HH:mm:ss"];
        NSDate *date = [dateFormatter dateFromString:dateStringInput];
        NSLog(@"result convertStringToDate: %@",date.description);
        // date will always contain value in GMT +0:00
        NSLog(@"New NSDate (NSDate): %@", [dateFormatter stringFromDate:date]);
        
        // converts date into string
        NSLog(@"New NSDate (NSString): %@", [dateFormatter stringFromDate:date]);
        return date;
        
    }
    @catch (NSException *exception) {
        
    }
    
}

- (NSString *)convertDateToString:(NSDate*)dateInput {
    @try {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSString *stringDate = [dateFormatter stringFromDate:dateInput];
        NSLog(@"result convertDateToString: %@",stringDate);
        return stringDate;
    }
    @catch (NSException *exception) {
        
    }
    
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
