//
//  ListTicketNoAssignViewController.m
//  TicketMobile
//
//  Created by HIEUPC on 3/12/15.
//  Copyright (c) 2015 FPT. All rights reserved.
//

#import "ListTicketNoAssignViewController.h"
#import "ListTicketCell.h"
#import "MyTicket.h"
#import "ShareData.h"
#import "DetailTicketViewController.h"

@interface ListTicketNoAssignViewController ()
@end

@implementation ListTicketNoAssignViewController

@synthesize arrList;
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    ShareData *shared = [ShareData instance];
    if (shared.currentUser.checkChanged) {
        shared.currentUser.checkChanged =false;
        [self LoadData];
    }
}
- (void)viewDidLoad {
    self.screenName=@"TỒN CHƯA PC";
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.tableView.separatorColor = [UIColor clearColor];
    self.title = @"TỒN CHƯA PC";
    self.screenName=self.title;
    [self LoadData];
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:refreshControl];
  
}
- (void)refresh:(UIRefreshControl *)refreshControl {
    NSLog(@"---------Prfess :2");
    [self LoadData];
    [self.tableView reloadData];
    [refreshControl endRefreshing];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidLayoutSubviews{
//    if(IS_IPHONE4){
//        self.tableView.frame = CGRectMake(0, self.tableView.frame.origin.y, self.tableView.frame.size.width, self.tableView.frame.size.height - 88);
//    }
}

#pragma mark - UITableView Delegate & Datasrouce -

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if(self.arrList.count > 0){
        return self.arrList.count;
    }
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 3; // you can have your own choice, of course
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *headerView = [[UIView alloc] init];
    headerView.backgroundColor = [UIColor colorWithRed:204.0/255.0 green:204.0/255.0 blue:204.0/255.0 alpha:1.0];
    return headerView;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(self.arrList.count > 0){
        //return self.arrList.count;
        return 1;
    }
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    //int i = (int)indexPath;
    float height = 0;
    int titlelenght = 0;
    //MyTicket *rc = [self.arrList objectAtIndex:indexPath.row];
    MyTicket *rc = [self.arrList objectAtIndex:indexPath.section];
    titlelenght = (int) rc.Title.length;
    if(titlelenght > 19){
        int numline = titlelenght;
        int val = numline % 19;
        val = val==0?0:1;
        numline = numline / 19 + val;
        height = 13 * (numline-1);
    }
    return 159 + height;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    

    NSString *simpleTableIdentifier = @"ListTicketCell";
    if(IS_IPAD){
        simpleTableIdentifier = @"ListTicketCell_iPad";
    }
    ListTicketCell *cell = (ListTicketCell *)[self.tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:simpleTableIdentifier owner:self options:nil];
    cell = [nib objectAtIndex:0];
    
    //add by DanTT, sua loi hut chieu rong cell voi ios 7.1
    if (NSFoundationVersionNumber <= NSFoundationVersionNumber_iOS_7_1){
        cell.contentView.frame = cell.bounds;
        cell.contentView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleRightMargin |UIViewAutoresizingFlexibleTopMargin |UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleBottomMargin;
    }
    
    if(self.arrList.count > 0){
        
        //MyTicket *rc = [self.arrList objectAtIndex:indexPath.row];
        MyTicket *rc = [self.arrList objectAtIndex:indexPath.section];

        cell.lblTitle.text = rc.Title;
        
        cell.lblCode.text = rc.TicketID;
      
        cell.lblPhanTu.text = rc.IssueName;
        
        cell.lblCreateTime.text = rc.ExistTime;
        cell.lblForCreatedTime.text =@"THỜI GIAN TỒN:";
        
        
        cell.lblTitleDate.text =@"THỜI GIAN TẠO:";
        cell.lblTon.text = rc.CreateTime;
        
        cell.lblForTimeWorked.hidden = YES;
        cell.lblTimeWorked.hidden = YES;
        cell.imageTimeWorked.hidden = YES;
    }
    //add by DanTT
    cell.bounds = CGRectMake(0.0f, 0.0f, CGRectGetWidth(tableView.bounds), CGRectGetHeight(cell.bounds));
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *nib = @"DetailTicketViewController";
    if (IS_IPAD)
    {
        nib = @"DetailTicketViewController_iPad";
    }
    DetailTicketViewController *vc = [[DetailTicketViewController alloc]initWithNibName:nib bundle:nil];
    //MyTicket *rc = [self.arrList objectAtIndex:indexPath.row];
    MyTicket *rc = [self.arrList objectAtIndex:indexPath.section];
    vc.ticketId = rc.TicketID;
    [self.navigationController pushViewController:vc animated:YES];
    
}

-(void)LoadData {
    if(![SiUtils checkNetworkAvailable]){
        [self showAlertBox:@"Thông báo" message:Mesage_Network];
    }
    
    [self showMBProcess:@"Đang tải dữ liệu..."];
    ShareData *shared = [ShareData instance];
    [shared.ticketProxy getnewTicketList:shared.currentUser.userId completeHandler:^(id result, NSString *errorCode, NSString *message) {
        
        if([errorCode isEqualToString:@"1"]){
            self.arrList = result;
            if(self.arrList.count > 0){
                self.txtTicketNumber.text = [NSString stringWithFormat:@"  Tổng số ticket: %lu",  (unsigned long)self.arrList.count];
                [self.tableView reloadData];
            }else {
                [self showAlertBox:@"Thông Báo" message:Mesage_DataEmpty];
            }
            
        }else {
            [self showAlertBox:@"Thông Báo" message:Mesage_DataEmpty];
        }
        [self hideMBProcess];
    } errorHandler:^(NSError *error) {
        [self hideMBProcess];
        [self showAlertBox:@"Thông Báo" message:[error localizedDescription]];
        NSLog(@"%@",[NSString stringWithFormat:@"%@",error]);
        return;

    }];
}

-(void)alertView:(UIAlertView *)alert_view didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    [self.navigationController popViewControllerAnimated:YES];
}
@end
