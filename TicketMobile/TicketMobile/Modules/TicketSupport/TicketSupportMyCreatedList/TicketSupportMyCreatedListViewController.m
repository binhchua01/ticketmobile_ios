//
//  TicketSupportMyCreatedViewController.m
//  TicketMobile
//
//  Created by ISC-DanTT on 7/11/16.
//  Copyright (c) 2016 FPT. All rights reserved.
//

#import "TicketSupportMyCreatedListViewController.h"
#import "TicketSupportMyCreatedListViewCell.h"
#import "TicketSupportMyCreatedListModel.h"
#import "CreateTicketSupportViewController.h"
#import "KeyValueModel.h"
#import "ShareData.h"

@interface TicketSupportMyCreatedListViewController () <UITableViewDelegate, UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *listTableView;

@end

@implementation TicketSupportMyCreatedListViewController {
    TicketSupportMyCreatedListModel *cellModel;
    NSArray *dataArray;
    NSMutableArray *sectionDataArray;
    NSArray *arrSectionTitle;
    BOOL checkLoadData;
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"DS TICKET HỖ TRỢ TÔI TẠO";
    [self getTicketSupportMyCreatedList];
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.tintColor = [UIColor orangeColor];
    [refreshControl addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
    [self.listTableView addSubview:refreshControl];
    checkLoadData = TRUE;
}

- (void)viewDidAppear:(BOOL)animated {
    if (checkLoadData == TRUE) {
        checkLoadData = FALSE;
        return;
    }else {
        
        [self getTicketSupportMyCreatedList];
    }
    
}
- (void)refresh:(UIRefreshControl *)refreshControl {
    NSLog(@"---------Refreshing...");
    [self getTicketSupportMyCreatedList];
    [refreshControl endRefreshing];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}
- (void)getTicketSupportMyCreatedList {
    if(![SiUtils checkNetworkAvailable]){
        [self showAlertBox:@"Thông báo" message:Mesage_Network];
        return;
    }
    [self showMBProcess:@"Đang tải dữ liệu..."];
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    ShareData *shared = [ShareData instance];
    [dict setObject:shared.currentUser.userEmail forKey:@"UserEmail"];
    [shared.ticketSupportProxy getMyCreateSupport:dict completeHandler:^(id result, NSString *errorCode, NSString *message) {
        
        if([errorCode isEqualToString:@"1"]){
            dataArray  = result;
            
            if(dataArray.count > 0){
                arrSectionTitle = [self getTitleSectionArray];
                sectionDataArray = [NSMutableArray array];
                NSString *titleSection;
                for (titleSection in arrSectionTitle) {
                    [sectionDataArray addObject:[self getDataForSection:titleSection]];
                    
                }
                
                [self.listTableView reloadData];
            }else {
                [self showAlertBox:@"Thông báo" message:Mesage_DataEmpty];
            }
            
        }else {
            [self showAlertBox:@"Thông báo" message:Mesage_DataEmpty];
        }
        
        [self hideMBProcess];
        
    } errorHandler:^(NSError *error) {
        [self hideMBProcess];
        [self showAlertBox:@"Thông Báo" message:[error localizedDescription]];
        NSLog(@"%@",[NSString stringWithFormat:@"%@",error]);
        return;
        
    }];
}
/*
 * Get array title of section in table view from data recived web service
 */

- (NSMutableArray *)getTitleSectionArray {
    NSMutableArray *titleSectionArray = [NSMutableArray array];
    for (cellModel in dataArray) {
        NSString *titleSection = cellModel.queue;
        if (![titleSectionArray containsObject:titleSection]) {
            [titleSectionArray addObject:titleSection];
        }
    }
    return titleSectionArray;
}

- (KeyValueModel *)getDataForSection:(NSString*)titleSection {
    NSMutableArray *arr = [[NSMutableArray alloc] init];
    for (cellModel in dataArray) {
        NSString *newTitleSection = cellModel.queue;
        if ([newTitleSection isEqualToString:titleSection]) {
            [arr addObject:cellModel];
        }
    }
    KeyValueModel *item = [[KeyValueModel alloc] initWithName:titleSection description:arr];
    return item;
}

#pragma mark - UITableViewDataSource methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return arrSectionTitle.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    KeyValueModel *item = sectionDataArray[section];
    NSArray *array = item.Values;
    return array.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 50;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *sectionHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 100, tableView.frame.size.width, 70)];
    sectionHeaderView.tag = section;
    sectionHeaderView.layer.cornerRadius = 1.0f;
    [sectionHeaderView setBackgroundColor: [UIColor colorWithPatternImage: [UIImage imageNamed:@"backgroundHeader"]]];
    
    UILabel *headerLabel = [[UILabel alloc] initWithFrame:CGRectMake(60, 15, tableView.frame.size.width, 25.0)];
    headerLabel.backgroundColor = [UIColor clearColor];
    [headerLabel setFont:[UIFont fontWithName:@"" size:15.0]];
    headerLabel.textColor = [UIColor whiteColor];
    headerLabel.text = arrSectionTitle[section];
    
    UIImage *myImage = [UIImage imageNamed:@"iconHT"];
    UIImageView *imageView = [[UIImageView alloc] initWithImage:myImage];
    imageView.frame = CGRectMake(20,10,30,30);
    
    [sectionHeaderView addSubview:headerLabel];
    [sectionHeaderView addSubview:imageView];
    
    return sectionHeaderView;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView *footer  = [[UIView alloc] init];
    footer.backgroundColor = [UIColor colorWithRed:230.0/255.0 green:230.0/255.0 blue:230.0/255.0 alpha:1.0];
    return footer;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 8;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    KeyValueModel *item = sectionDataArray[indexPath.section];
    NSArray *array = item.Values;
    cellModel = array[indexPath.row];
    CGFloat height = [self getHeightLabelWithString:cellModel.title withHeight:31.0f andWidth:113.0f];
    return 145 +height;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    TicketSupportMyCreatedListViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TicketSupportMyCreatedCellID"];
    
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"TicketSupportMyCreatedListViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    // Configure the cell...
    KeyValueModel *item = sectionDataArray[indexPath.section];
    NSArray *array = item.Values;
    cellModel = array[indexPath.row];
    cell.cellModel = cellModel;
    return cell;
    
}

#pragma mark - UITableViewDelegate methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    CreateTicketSupportViewController *vc = [[CreateTicketSupportViewController alloc] init];
    KeyValueModel *item = sectionDataArray[indexPath.section];
    NSArray *array = item.Values;
    cellModel = array[indexPath.row];
    vc.ticketID = cellModel.ticketID;
    vc.viewType = updateTicketSupport;
    vc.request = cellModel.title;
    
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - Custom height along string lenght

- (CGFloat)getHeightLabelWithString:(NSString *)string withHeight:(CGFloat)height andWidth:(CGFloat)width {
    CGSize constraint = CGSizeMake(width, 20000.0f);
    // constratins the size of the table row according to the text
    CGRect textRect = [string boundingRectWithSize:constraint options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading) attributes:@{NSFontAttributeName:[UIFont fontWithName:@"Helvetica Neue" size:13]} context:nil];
    
    return MAX(textRect.size.height,height) - height;
}


@end
