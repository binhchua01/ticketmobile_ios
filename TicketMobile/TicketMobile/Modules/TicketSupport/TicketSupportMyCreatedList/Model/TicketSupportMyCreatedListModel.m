//
//  TicketSupportMyCreatedListModel.m
//  TicketMobile
//
//  Created by ISC-DanTT on 7/11/16.
//  Copyright (c) 2016 FPT. All rights reserved.
//

#import "TicketSupportMyCreatedListModel.h"

@implementation TicketSupportMyCreatedListModel

- (id)initWithData:(NSDictionary *)dict {
    
    self.ticketID = dict[@"TicketID"];
    self.title = dict[@"Title"];
    self.queue = dict[@"Queue"];
    self.ticketStatus = dict[@"TicketStatus"];
    self.priority = dict[@"Priority"];
    self.updateDate = dict[@"UpdateDate"];
    self.staff = dict[@"Staff"];
    self.createdDate = dict[@"CreatedDate"];
    self.estimatedTime = dict[@"EstimatedTime"];
    self.timeWorker = dict[@"TimeWorked"];
    
    if (self.ticketID == nil) {
        return nil;
    }
    
    return self;
    
}

@end
