//
//  TicketSupportMyCreatedListModel.h
//  TicketMobile
//
//  Created by ISC-DanTT on 7/11/16.
//  Copyright (c) 2016 FPT. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TicketSupportMyCreatedListModel : NSObject

@property (nonatomic, copy) NSString *ticketID;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *queue;
@property (nonatomic, copy) NSString *ticketStatus;
@property (nonatomic, copy) NSString *priority;
@property (nonatomic, copy) NSString *updateDate;
@property (nonatomic, copy) NSString *staff;
@property (nonatomic, copy) NSString *createdDate;
@property (nonatomic, copy) NSString *estimatedTime;
@property (nonatomic, copy) NSString *timeWorker;

- (id)initWithData:(NSDictionary *)dict;

@end
