//
//  TicketSupportMyCreatedViewCell.h
//  TicketMobile
//
//  Created by ISC-DanTT on 7/11/16.
//  Copyright (c) 2016 FPT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TicketSupportMyCreatedListModel.h"

@interface TicketSupportMyCreatedListViewCell : UITableViewCell

@property (copy, nonatomic) TicketSupportMyCreatedListModel *cellModel;

@property (weak, nonatomic) IBOutlet UILabel *ticketIDLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *ticketStatusLabel;
@property (weak, nonatomic) IBOutlet UILabel *updateDateLabe;

@property (weak, nonatomic) IBOutlet UILabel *dateUpdate;

@end
