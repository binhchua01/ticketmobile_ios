//
//  TicketSupportMyCreatedViewCell.m
//  TicketMobile
//
//  Created by ISC-DanTT on 7/11/16.
//  Copyright (c) 2016 FPT. All rights reserved.
//

#import "TicketSupportMyCreatedListViewCell.h"

@implementation TicketSupportMyCreatedListViewCell

- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
    self.ticketIDLabel.text = self.ticketStatusLabel.text = self.titleLabel.text = self.updateDateLabe.text = @"";
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)drawRect:(CGRect)rect {
    self.titleLabel.numberOfLines = 0;
    [self.titleLabel sizeToFit];
}

- (void)setCellModel:(TicketSupportMyCreatedListModel *)cellModel {
    _cellModel = cellModel;
    
    self.ticketIDLabel.text      = cellModel.ticketID;
    self.titleLabel.text         = cellModel.title;
    self.ticketStatusLabel.text  = cellModel.ticketStatus;
    self.updateDateLabe.text    = cellModel.createdDate;
    self.dateUpdate.text = cellModel.updateDate;
    
}

@end
