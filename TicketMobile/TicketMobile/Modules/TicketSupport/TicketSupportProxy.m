//
//  TicketSupportProxy.m
//  TicketMobile
//
//  Created by ISC-DanTT on 7/14/16.
//  Copyright (c) 2016 FPT. All rights reserved.
//

#import "TicketSupportProxy.h"
#import "KeyValueModel.h"
#import "SelectedRecord.h"
#import "StaffModel.h"
#import "ProcessStaffModel.h"
#import "TicketSupportMyCreatedListModel.h"
#import "ContractModel.h"

#define mCreateTicketSupport @"CreateSupport"
#define mMyCreateSupport    @"MyCreateSupport"
#define mGetBusinessArea    @"GetBusinessArea"
#define mGetBranchLocation  @"GetBranchLocation"
#define mGetServiceType     @"GetServiceType"
#define mGetCustomerType    @"GetCustomerType"
#define mGetInput           @"GetInput"
#define mGetReason          @"GetRFO"
#define mGetPriority        @"GetPriority"
#define mOperateStaff       @"OperateStaff"
#define mVisorStaff         @"VisorStaff"
#define mProcessStaff       @"ProcessStaff"
#define mHTEstimatedTime    @"HTEstimatedTime"
#define mCustomerList       @"CustomerList"
#define mTicketSupportUpdate @"TicketSupportUpdate"

@implementation TicketSupportProxy

// Lấy dữ liệu vùng miền
- (void)getBusinessArea:(NSDictionary *)dict completeHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    [self requestWithPath:mGetBusinessArea withParameters:dict completeHandler:^(id result, NSString *errorCode, NSString *message) {
        if(![errorCode isEqualToString:@"1"]){
            handler(nil, errorCode, message);
            return;
        }
        NSArray *data = result;
        if(data.count <= 0){
            errorCode = @"0";
            handler(nil, errorCode, @"Không có dữ liệu Vùng miền trả về!");
            return;
        }
        
        NSMutableArray *mArray = [NSMutableArray array];
        
        for (NSDictionary *item in data) {
            SelectedRecord *record = [[SelectedRecord alloc] init];
            record.Id = item[@"LocationID"];
            record.Name = item[@"Description"];
            record.Check = @"0";
            [mArray addObject:record];        }
        
        handler(mArray, errorCode, @"Tải dữ liệu Vùng miền thành công!");
        
    } errorHandler:^(NSError *error) {
        errHandler(error);
        
    }];
}

// Lấy dữ liệu chi nhánh ảnh hưởng
- (void)getBranchLocation:(NSDictionary *)dict completeHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    [self requestWithPath:mGetBranchLocation withParameters:dict completeHandler:^(id result, NSString *errorCode, NSString *message) {
        if(![errorCode isEqualToString:@"1"]){
            handler(nil, errorCode, message);
            return;
        }
        NSArray *data = result;
        if(data.count <= 0){
            errorCode = @"0";
            handler(nil, errorCode, @"Không có dữ liệu chi nhánh trả về!");
            return;
        }
        
        NSMutableArray *mArray = [NSMutableArray array];
        
        for (NSDictionary *item in data) {
            SelectedRecord *record = [[SelectedRecord alloc] init];
            record.Id = item[@"BranchID"];
            record.Name = item[@"BranchName"];
            record.Check = @"0";
            [mArray addObject:record];
        }
        
        handler(mArray, errorCode, @"Tải dữ liệu chi nhánh thành công!");
        
    } errorHandler:^(NSError *error) {
        errHandler(error);
        
    }];

}

// Lấy dữ liệu loại dịch vụ
- (void)getServiceType:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    NSDictionary *dict = [NSDictionary dictionary];
    [self requestWithPath:mGetServiceType withParameters:dict completeHandler:^(id result, NSString *errorCode, NSString *message) {
        if(![errorCode isEqualToString:@"1"]){
            handler(nil, errorCode, message);
            return;
        }
        NSArray *data = result;
        if(data.count <= 0){
            errorCode = @"0";
            handler(nil, errorCode, @"Không có dữ liệu loại dịch vụ trả về!");
            return;
        }
        
        NSMutableArray *mArray = [NSMutableArray array];
        
        for (NSDictionary *item in data) {
            KeyValueModel *model = [[KeyValueModel alloc] initWithName:item[@"ID"] description:item[@"Name"]];
            
            [mArray addObject:model];
        }
        
        handler(mArray, errorCode, @"Tải dữ liệu loại dịch vụ thành công!");
        
    } errorHandler:^(NSError *error) {
        errHandler(error);
        
    }];
}

// Lấy dữ liệu loại khách hàng
- (void)getCustomerType:(NSDictionary *)dict completeHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {

    [self requestWithPath:mGetCustomerType withParameters:dict completeHandler:^(id result, NSString *errorCode, NSString *message) {
        if(![errorCode isEqualToString:@"1"]){
            handler(nil, errorCode, message);
            return;
        }
        NSArray *data = result;
        if(data.count <= 0){
            errorCode = @"0";
            handler(nil, errorCode, @"Không có dữ liệu loại khách hàng trả về!");
            return;
        }
        
        NSMutableArray *mArray = [NSMutableArray array];
        
        for (NSDictionary *item in data) {
            KeyValueModel *model = [[KeyValueModel alloc] initWithName:item[@"ID"] description:item[@"Name"]];
            
            [mArray addObject:model];
        }
        
        handler(mArray, errorCode, @"Tải dữ liệu loại khách hàng thành công!");
        
    } errorHandler:^(NSError *error) {
        errHandler(error);

    }];
}

// Lấy dữ liệu mô tả
- (void)getInput:(NSDictionary *)dict completeHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    [self requestWithPath:mGetInput withParameters:dict completeHandler:^(id result, NSString *errorCode, NSString *message) {
        if(![errorCode isEqualToString:@"1"]){
            handler(nil, errorCode, message);
            return;
        }
        NSArray *data = result;
        if(data.count <= 0){
            errorCode = @"0";
            handler(nil, errorCode, @"Không có dữ liệu mô tả trả về!");
            return;
        }
        
        NSMutableArray *mArray = [NSMutableArray array];
        
        for (NSDictionary *item in data) {
            KeyValueModel *model = [[KeyValueModel alloc] initWithName:item[@"ID"] description:item[@"Description"]];
            
            [mArray addObject:model];
        }
        
        handler(mArray, errorCode, @"Tải dữ liệu mô tả thành công!");
        
    } errorHandler:^(NSError *error) {
        errHandler(error);
        
    }];
}

// Lấy dữ liệu nguyên nhân
- (void)getRFO:(NSDictionary *)dict completeHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    [self requestWithPath:mGetReason withParameters:dict completeHandler:^(id result, NSString *errorCode, NSString *message) {
        if(![errorCode isEqualToString:@"1"]){
            handler(nil, errorCode, message);
            return;
        }
        NSArray *data = result;
        if(data.count <= 0){
            errorCode = @"0";
            handler(nil, errorCode, @"Không có dữ liệu nguyên nhân trả về!");
            return;
        }
        
        NSMutableArray *mArray = [NSMutableArray array];
        
        for (NSDictionary *item in data) {
            KeyValueModel *model = [[KeyValueModel alloc] initWithName:item[@"ID"] description:item[@"Description"]];
            
            [mArray addObject:model];
        }
        
        handler(mArray, errorCode, @"Tải dữ liệu nguyên nhân thành công!");
        
    } errorHandler:^(NSError *error) {
        errHandler(error);
        
    }];
}

// Lấy dữ liệu mức priority
- (void)getPriority:(NSDictionary *)dict completeHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    [self requestWithPath:mGetPriority withParameters:dict completeHandler:^(id result, NSString *errorCode, NSString *message) {
        if(![errorCode isEqualToString:@"1"]){
            handler(nil, errorCode, message);
            return;
        }
        NSArray *data = result;
        if(data.count <= 0){
            errorCode = @"0";
            handler(nil, errorCode, @"Không có dữ liệu nguyên nhân trả về!");
            return;
        }
        
        id priorityValue;
        for (NSDictionary *item in data) {
            priorityValue = item[@"PriorityValue"];
            
        }
        
        handler(priorityValue, errorCode, @"Tải dữ liệu nguyên nhân thành công!");
        
    } errorHandler:^(NSError *error) {
        errHandler(error);
        
    }];
}

// Lấy dữ liệu thời gian qui định hoàn thành
- (void)getHTEstimatedTime:(NSDictionary *)dict completeHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    [self requestWithPath:mHTEstimatedTime withParameters:dict completeHandler:^(id result, NSString *errorCode, NSString *message) {
        if(![errorCode isEqualToString:@"1"]){
            handler(nil, errorCode, message);
            return;
        }
        NSArray *data = result;
        if(data.count <= 0){
            errorCode = @"0";
            handler(nil, errorCode, @"Không có dữ liệu HT estimate time trả về!");
            return;
        }
        
        id priorityValue;
        for (NSDictionary *item in data) {
            priorityValue = item[@"EstimatedTimeValue"];
            
        }
        
        handler(priorityValue, errorCode, @"Tải dữ liệu HT estimate time thành công!");
        
    } errorHandler:^(NSError *error) {
        errHandler(error);
        
    }];
}

// Lấy dữ liệu nhân viên chủ trì
- (void)getOperateStaff:(NSDictionary *)dict completeHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    [self requestWithPath:mOperateStaff withParameters:dict completeHandler:^(id result, NSString *errorCode, NSString *message) {
        if(![errorCode isEqualToString:@"1"]){
            handler(nil, errorCode, message);
            return;
        }
        NSArray *data = result;
        if(data.count <= 0){
            errorCode = @"0";
            handler(nil, errorCode, @"Không có dữ liệu nhân viên chủ trì trả về!");
            return;
        }
        
        NSMutableArray *mArray = [NSMutableArray array];
        
        for (NSDictionary *item in data) {
            StaffModel *model = [[StaffModel alloc] initWithName:item[@"Name"] withID:item[@"ID"] andEmail:item[@"Email"]];
            
            [mArray addObject:model];
        }
        handler(mArray, errorCode, @"Tải dữ liệu nhân viên chủ trì thành công!");
        
    } errorHandler:^(NSError *error) {
        errHandler(error);
        
    }];
}

// Lấy dữ liệu nhân viên giám sát
- (void)getVisorStaff:(NSDictionary *)dict completeHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    [self requestWithPath:mVisorStaff withParameters:dict completeHandler:^(id result, NSString *errorCode, NSString *message) {
        if(![errorCode isEqualToString:@"1"]){
            handler(nil, errorCode, message);
            return;
        }
        NSArray *data = result;
        if(data.count <= 0){
            errorCode = @"0";
            handler(nil, errorCode, @"Không có dữ liệu nhân viên giám sát trả về!");
            return;
        }
        
        NSMutableArray *mArray = [NSMutableArray array];
        
        for (NSDictionary *item in data) {
            StaffModel *model = [[StaffModel alloc] initWithName:item[@"Name"] withID:item[@"ID"] andEmail:item[@"Email"]];
            
            [mArray addObject:model];
        }
        handler(mArray, errorCode, @"Tải dữ liệu nhân viên giám sát thành công!");
        
    } errorHandler:^(NSError *error) {
        errHandler(error);
        
    }];
}

// Lấy dữ liệu nhân viên xử lý
- (void)getProcessStaff:(NSDictionary *)dict completeHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    [self requestWithPath:mProcessStaff withParameters:dict completeHandler:^(id result, NSString *errorCode, NSString *message) {
        if(![errorCode isEqualToString:@"1"]){
            handler(nil, errorCode, message);
            return;
        }
        NSArray *data = result;
        if(data.count <= 0){
            errorCode = @"0";
            handler(nil, errorCode, @"Không có dữ liệu nhân viên xử lý trả về!");
            return;
        }
        
        NSMutableArray *mArray = [NSMutableArray array];
        
        for (NSDictionary *item in data) {
            ProcessStaffModel *model = [[ProcessStaffModel alloc] initWithData:item];
            
            [mArray addObject:model];
        }
        
        handler(mArray, errorCode, @"Tải dữ liệu nhân viên xử lý thành công!");
        
    } errorHandler:^(NSError *error) {
        errHandler(error);
        
    }];
}

// Lấy dữ liệu danh sách hợp đồng
- (void)getContractList:(NSDictionary *)dict completeHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    [self requestWithPath:mCustomerList withParameters:dict completeHandler:^(id result, NSString *errorCode, NSString *message) {
        if(![errorCode isEqualToString:@"1"]){
            handler(nil, errorCode, message);
            return;
        }
        NSArray *data = result;
        if(data.count <= 0){
            errorCode = @"0";
            handler(nil, errorCode, @"Không có dữ liệu hợp đồng trả về!");
            return;
        }
        
        NSMutableArray *mArray = [NSMutableArray array];
        
        for (NSDictionary *item in data) {
            ContractModel *model = [[ContractModel alloc] initWithData:item];
            [mArray addObject:model];
        }
        
        handler(mArray, errorCode, @"Tải dữ liệu hợp đồng thành công!");
        
    } errorHandler:^(NSError *error) {
        errHandler(error);
        
    }];
}

// Tạo ticket hỗ trợ
- (void)createSupport:(NSDictionary *)dict completeHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    [self requestWithPath:mCreateTicketSupport withParameters:dict completeHandler:^(id result, NSString *errorCode, NSString *message) {
        
        handler(result, errorCode, message);
        
    } errorHandler:^(NSError *error) {
        errHandler(error);
        
    }];
}

// update ticket support
- (void)updateTicketSupport:(NSDictionary *)dict completeHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    [self requestWithPath:mTicketSupportUpdate withParameters:dict completeHandler:^(id result, NSString *errorCode,NSString *message){
        handler(result,errorCode,message);
    } errorHandler:^(NSError *error){
        
        errHandler(error);
    }];
    
}

// Lấy dữ liệu danh sách ticket hỗ trợ tôi tạo
- (void)getMyCreateSupport:(NSDictionary *)dict completeHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    [self requestWithPath:mMyCreateSupport withParameters:dict completeHandler:^(id result, NSString *errorCode, NSString *message) {
        if(![errorCode isEqualToString:@"1"]){
            handler(nil, errorCode, message);
            return;
        }
        NSArray *data = result;
        if(data.count <= 0){
            errorCode = @"0";
            handler(nil, errorCode, @"Không có dữ liệu danh sách ticket trả về!");
            return;
        }
        
        NSMutableArray *mArray = [NSMutableArray array];
        
        for (NSDictionary *item in data) {
            TicketSupportMyCreatedListModel *model = [[TicketSupportMyCreatedListModel alloc] initWithData:item];
            
            [mArray addObject:model];
        }
        
        handler(mArray, errorCode, @"Tải dữ liệu  danh sách ticket thành công!");
        
    } errorHandler:^(NSError *error) {
        errHandler(error);
        
    }];
}

/* 
 * Create request to Web service
 */
- (void)requestWithPath:(NSString *)path withParameters:(NSDictionary *)parameters completeHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    BaseOperation * callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI, path)];
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    //create json
    NSString *postString = [parameters JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        [self endRequest:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err) {
        errHandler(err);
    };
    
    [callOp start];
}

- (void)endRequest:(NSData *)result response:(NSURLResponse *)response
         completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    /*Parse json response*/
    @try {
        NSDictionary *jsonDict = [self.parser objectWithData:result];
        jsonDict = [jsonDict objectForKey:@"Result"]?:nil;
        NSString *error_code = [NSString stringWithFormat:@"%@", [jsonDict objectForKey:@"ErrorCode"]?:nil] ;
        NSString *error_description = StringFormat(@"%@",[jsonDict objectForKey:@"ErrorDescription"]); ;
        NSArray *data = [jsonDict objectForKey:@"Data"]?:nil;
        
        handler(data, error_code, error_description);
    }
    
    @catch (NSException *exception) {
        NSLog(@"%@", exception.description);
        handler(nil,nil,@"Error!");
    }
}

@end
