//
//  TicketSupportProxy.h
//  TicketMobile
//
//  Created by ISC-DanTT on 7/14/16.
//  Copyright (c) 2016 FPT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseProxy.h"

@interface TicketSupportProxy : BaseProxy

// Lấy dữ liệu vùng miền
- (void)getBusinessArea:(NSDictionary *)dict completeHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;
// Lấy dữ liệu chi nhánh ảnh hưởng
- (void)getBranchLocation:(NSDictionary *)dict completeHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;
// Lấy dữ liệu loại dịch vụ
- (void)getServiceType:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;
// Lấy dữ liệu loại khách hàng
- (void)getCustomerType:(NSDictionary *)dict completeHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;
// Lấy dữ liệu mô tả
- (void)getInput:(NSDictionary *)dict completeHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;
// Lấy dữ liệu nguyên nhân
- (void)getRFO:(NSDictionary *)dict completeHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;
// Lấy dữ liệu mức priority
- (void)getPriority:(NSDictionary *)dict completeHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;
// Lấy dữ liệu thời gian qui định hoàn thành
- (void)getHTEstimatedTime:(NSDictionary *)dict completeHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;
// Lấy dữ liệu nhân viên chủ trì
- (void)getOperateStaff:(NSDictionary *)dict completeHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;
// Lấy dữ liệu nhân viên giám sát
- (void)getVisorStaff:(NSDictionary *)dict completeHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;
// Lấy dữ liệu nhân viên xử lý
- (void)getProcessStaff:(NSDictionary *)dict completeHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;
// Lấy dữ liệu danh sách hợp đồng
- (void)getContractList:(NSDictionary *)dict completeHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;
// Tạo ticket hỗ trợ
- (void)createSupport:(NSDictionary *)dict completeHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;
// Lấy dữ liệu danh sách ticket hỗ trợ tôi tạo
- (void)getMyCreateSupport:(NSDictionary *)dict completeHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;
// update Ticketsupport
- (void)updateTicketSupport:(NSDictionary *)dict completeHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;


@end
