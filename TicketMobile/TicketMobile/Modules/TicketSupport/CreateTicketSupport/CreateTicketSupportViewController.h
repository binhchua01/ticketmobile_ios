//
//  CreateTicketSupportViewController.h
//  TicketMobile
//
//  Created by ISC-DanTT on 7/13/16.
//  Copyright (c) 2016 FPT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "UIView+FPTCustom.h"

typedef enum : NSUInteger {
    updateTicketSupport = 1,
    createTicketSupport,
} TicketSupportViewType;

@protocol CreateTicketSupportViewDelegate <NSObject>

@optional
- (void)pushToTicketSupportListView;

@end

@interface CreateTicketSupportViewController : BaseViewController

@property (strong, nonatomic) id<CreateTicketSupportViewDelegate> delegate;

@property (assign, nonatomic) TicketSupportViewType viewType;
@property (strong, nonatomic) NSMutableArray *contractArray;
@property (strong, nonatomic) NSString  *ticketID;
@property (strong, nonatomic) NSString  *request;

@property (weak, nonatomic) IBOutlet UIButton *locationButton;  // Vùng miền
@property (weak, nonatomic) IBOutlet UIButton *branchButton;    // Chi nhánh
@property (weak, nonatomic) IBOutlet UIButton *serviceTypeButton;   // Loại dịch vụ
@property (weak, nonatomic) IBOutlet UIButton *customerTypeButton;  // Loại khách hàng
@property (weak, nonatomic) IBOutlet UIButton *descriptionButton;   // Mô tả
@property (weak, nonatomic) IBOutlet UIButton *enterContractButton; // Nhập hợp đồng
@property (weak, nonatomic) IBOutlet UIButton *reasonButton;    // Nguyên nhân
@property (weak, nonatomic) IBOutlet UIButton *notifyStaffButton;   // Nhân viên thông báo
@property (weak, nonatomic) IBOutlet UIButton *departmentNotifyButton; // Bộ phận thông báo
@property (weak, nonatomic) IBOutlet UIButton *troubleStatusButton; // Tình trạng sự cố
@property (weak, nonatomic) IBOutlet UIButton *queueButton;         // Queue
@property (weak, nonatomic) IBOutlet UIButton *operateStaffButton;  // Nhân viên chủ trì
@property (weak, nonatomic) IBOutlet UIButton *supervisoryStaffButton;  // Nhân viên giám sát
@property (weak, nonatomic) IBOutlet UIButton *processStaffButton;  // Nhân viên xử lý

@property (weak, nonatomic) IBOutlet UIButton *updateButton;  // Cập nhật thông tin Ticket
@property (weak, nonatomic) IBOutlet UIButton *feedbackButton;

@property (weak, nonatomic) IBOutlet UITextView *requestTextView;
@property (weak, nonatomic) IBOutlet UITextView *noteTextView;

@property (weak, nonatomic) IBOutlet UILabel *priorityLabel;
@property (weak, nonatomic) IBOutlet UILabel *notifyStaffPhone;
@property (weak, nonatomic) IBOutlet UILabel *notifyStaffIPPhone;
@property (weak, nonatomic) IBOutlet UILabel *processStaffPhone;
@property (weak, nonatomic) IBOutlet UILabel *processStaffIPPhone;
@property (weak, nonatomic) IBOutlet UILabel *createTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *estimatedTimeLabel;


- (IBAction)buttonPressed:(id)sender;

- (id)initWithViewType:(TicketSupportViewType)viewType;

@end
