//
//  ContractViewController.m
//  TicketMobile
//
//  Created by ISC-DanTT on 7/20/16.
//  Copyright (c) 2016 FPT. All rights reserved.

#import "ContractViewController.h"
#import "ContractModel.h"
#import "ContractTableViewCell.h"
#import "UIView+FPTCustom.h"

@interface ContractViewController ()<UITableViewDataSource, UITableViewDelegate ,UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UIView *enterInforContractView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *enterInforContractViewHeightLayoutConstraint;
@property (weak, nonatomic) IBOutlet UITableView *listContractTableView;

@end

@implementation ContractViewController {
    NSMutableArray *tempContractArray;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    tempContractArray = [NSMutableArray arrayWithArray:self.contractArray];
    [self.enterInforContractView styleBorderMap];
    if (self.ticketSupportViewType == updateTicketSupport) {
       // [self showEnterContractInfoView:YES];
       // [self.enterContractSwitch setEnabled:NO];
        self.doneButton.userInteractionEnabled = YES;
    }
    UITapGestureRecognizer *tapGestureRecohnizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
    [self.view addGestureRecognizer:tapGestureRecohnizer];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)handleSingleTap:(UITapGestureRecognizer *)sender {
    [self.view endEditing:YES];
}

- (void)viewDidAppear:(BOOL)animated {
    
    if (tempContractArray.count >0) {
        [self.enterContractSwitch setEnabled:NO];
        [self showEnterContractInfoView:NO];
    }
    else {
        [self.enterContractSwitch setEnabled:YES];
        [self showEnterContractInfoView:YES];
    }
    
}
// Cancel Contract View
- (IBAction)cancelButtonPressed:(id)sender {
    if (self.delegate) {
        [self.delegate cancelContractViewWithDataReturn:tempContractArray];
    }
}
// Switch on/off for show/hide enter contract info view
- (IBAction)switchPressed:(id)sender {
    UISwitch *switchTemp = sender;
    if (switchTemp.isOn) {
        [self showEnterContractInfoView:YES];
        
        return;
    }
    
    [self showEnterContractInfoView:NO];

}

- (IBAction)doneButtonPressed:(id)sender {
    if (self.delegate) {
        [self.delegate cancelContractViewWithDataReturn:self.contractArray];
    }
}

#pragma mark - Add one contract to list
- (IBAction)addButtonPressed:(id)sender {
    //vutt11
    
    NSString *textSpace = self.contractTextField.text;
    NSCharacterSet *whitespace = [NSCharacterSet whitespaceCharacterSet];
 
    NSRange range = [textSpace rangeOfCharacterFromSet:whitespace];
    
    if (range.location != NSNotFound && (self.contractTextField.text.length>0) ) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Thông Báo" message:@"Bạn đã gõ hợp đồng sai!!!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alertView show];
        [self.contractTextField becomeFirstResponder];
        return;
    }
//    if ([self.contractTextField.text isEqualToString:@""]) {
//        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Thông báo" message:@"Bạn cần nhập thông tin Hợp đồng" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//        [alertView show];
//        [self.contractTextField becomeFirstResponder];
//        return;
//    }
    
    if (self.domainTextField.text.length>0 ) {
        if ([self validateUrl:self.domainTextField.text]) {
            [self addContract];
            return;
        }
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Thông báo" message:@"IP/Domain không hợp lệ!(ví dụ(172.30.27.24)) \nVui lòng nhập lại!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alertView show];
        [self.contractTextField becomeFirstResponder];
    }
    
    if ((self.contractTextField.text.length > 0 && !([self.contractTextField.text isEqualToString:@" "])) ||self.nameTextField.text.length >0) {
        
        self.contractTextField.text = [self.contractTextField.text stringByReplacingOccurrencesOfString:@" " withString:@""];
        [self addContract];
        
        return;
        
    }
    
}
//vutt11
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if ( textField == self.nameTextField && [self.nameTextField.text length]>=35 && (range.length == 0)) {
       
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Thông báo" message:@"Bạn đã nhập tên quá dài!!!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alertView show];
            [self.nameTextField becomeFirstResponder];
            return NO;
}
   else if (textField == self.contractTextField && [self.contractTextField.text length]>=35 && (range.length == 0)) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Thông báo" message:@"Bạn đã nhập tên hợp đồng quá dài!!!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alertView show];
        [self.nameTextField becomeFirstResponder];
        return NO;
    }
    
    else if (textField == self.domainTextField && [self.domainTextField.text length ]>= 35 && (range.length == 0)) {
        
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Thông báo" message:@"Bạn đã nhập domain quá dài!!!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alertView show];
            [self.nameTextField becomeFirstResponder];
            return NO;
    }
    else
    {
        return YES;
    }
    return YES;
}

- (void)showEnterContractInfoView:(BOOL)status {
    [self.view endEditing:YES];
    
    if (status) {
        self.enterInforContractViewHeightLayoutConstraint.constant = 207;
        
    } else {
        self.enterInforContractViewHeightLayoutConstraint.constant = 0;
    }
    
    [UIView animateWithDuration:0.25
                     animations:^{
                         [self.view layoutIfNeeded];
                     }];
}

- (BOOL)validateUrl:(NSString *)candidate {
    NSString *urlRegEx =
    @"(http[s]?\\:\\/\\/)?((\\w)*|([0-9]*)|([-|_])*)+([\\.|/]((\\w)*|([0-9]*)|([-|_])*))+";
    NSPredicate *urlTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", urlRegEx];
    return [urlTest evaluateWithObject:candidate];
    
}

- (void)addContract {
    ContractModel *contractModel = [[ContractModel alloc] initWithContract:self.contractTextField.text withDomain:self.domainTextField.text withAddress:self.addressTextField.text andName:self.nameTextField.text];
    [self.contractArray insertObject:contractModel atIndex:0];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    [self tableView:self.listContractTableView commitEditingStyle:UITableViewCellEditingStyleInsert forRowAtIndexPath:indexPath];
    [self clearTextField];
    
}

- (void)clearTextField {
    [self.view endEditing:YES];

    self.contractTextField.text = self.nameTextField.text = self.domainTextField.text = self.addressTextField.text = @"";
}

#pragma mark - UITableViewDataSource methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.contractArray.count > 0) {
        return self.contractArray.count;
    }
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
   // return 130;
    
    CGSize labelContraints              = CGSizeMake(219, 10000.0f);
    NSString *note = [self.contractArray[indexPath.row] valueForKey:@"address"];
    
    NSStringDrawingContext *context     = [[NSStringDrawingContext alloc] init];
    
    CGRect labelRect                    = [note boundingRectWithSize:labelContraints
                                                             options:NSStringDrawingUsesLineFragmentOrigin
                                                          attributes:nil
                                                             context:context];
    CGFloat height = MAX(labelRect.size.height, 21);
    return 130+height;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellID = @"ContractViewCellID";
    ContractTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ContractTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    if (self.contractArray.count > 0) {
        cell.cellModel = self.contractArray[indexPath.row];
    }
    
    return cell;
}

#pragma mark - UITableViewDelegate methods

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.ticketSupportViewType == updateTicketSupport) {
        return NO;
    }
    return YES;
}

- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath{
    return @"Xoá";
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [self.contractArray removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
        return;
    }
    
    if (editingStyle == UITableViewCellEditingStyleInsert) {
        [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationTop];
        [tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
        return;
    }
}

@end
