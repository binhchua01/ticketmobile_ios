//
//  ContractViewController.h
//  TicketMobile
//
//  Created by ISC-DanTT on 7/20/16.
//  Copyright (c) 2016 FPT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CreateTicketSupportViewController.h"

@protocol ContractViewDelegate <NSObject>

@optional
- (void)cancelContractViewWithDataReturn:(NSMutableArray *)dataReturn;

@end

@interface ContractViewController : UIViewController

@property (nonatomic, strong) id<ContractViewDelegate> delegate;
@property (nonatomic, strong) NSMutableArray *contractArray;
@property (nonatomic, assign) TicketSupportViewType ticketSupportViewType;

@property (weak, nonatomic) IBOutlet UITextField *contractTextField;
@property (weak, nonatomic) IBOutlet UITextField *nameTextField;
@property (weak, nonatomic) IBOutlet UITextField *domainTextField;
@property (weak, nonatomic) IBOutlet UITextField *addressTextField;

@property (weak, nonatomic) IBOutlet UIButton *doneButton;
@property (weak, nonatomic) IBOutlet UISwitch *enterContractSwitch;
@end
