//
//  ContractTableViewCell.h
//  TicketMobile
//
//  Created by ISC-DanTT on 7/20/16.
//  Copyright (c) 2016 FPT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ContractModel.h"

@interface ContractTableViewCell : UITableViewCell

@property (nonatomic, strong) ContractModel *cellModel;

@property (weak, nonatomic) IBOutlet UILabel *contractLabel;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *domainLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;

@end
