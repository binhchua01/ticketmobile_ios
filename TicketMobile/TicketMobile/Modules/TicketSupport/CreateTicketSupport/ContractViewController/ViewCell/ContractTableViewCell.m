//
//  ContractTableViewCell.m
//  TicketMobile
//
//  Created by ISC-DanTT on 7/20/16.
//  Copyright (c) 2016 FPT. All rights reserved.
//

#import "ContractTableViewCell.h"
#import "UIView+FPTCustom.h"
@implementation ContractTableViewCell

- (void)awakeFromNib {
    // Initialization code
    [self.contentView styleBorderMap];
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setCellModel:(ContractModel *)cellModel {
    _cellModel = cellModel;
    self.contractLabel.text = cellModel.contract;
    self.nameLabel.text     = cellModel.name;
    self.domainLabel.text   = cellModel.domain;
    self.addressLabel.text  = cellModel.address;
}

@end
