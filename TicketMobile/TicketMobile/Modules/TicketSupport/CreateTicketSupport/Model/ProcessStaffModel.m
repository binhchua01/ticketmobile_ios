//
//  ProcessStaffModel.m
//  TicketMobile
//
//  Created by ISC on 7/22/16.
//  Copyright © 2016 FPT. All rights reserved.
//

#import "ProcessStaffModel.h"

@implementation ProcessStaffModel

- (id)initWithData:(NSDictionary *)dict {
    self.name       = dict[@"Name"];
    self.ID         = dict[@"ID"];
    self.email      = dict[@"Email"];;
    self.ipPhone    = dict[@"ProcessIP"];
    self.cellPhone  = dict[@"ProcessPhone"];
    
    return self;
}

@end
