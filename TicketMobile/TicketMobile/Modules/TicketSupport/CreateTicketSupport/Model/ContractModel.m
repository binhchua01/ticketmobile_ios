//
//  ContractModel.m
//  TicketMobile
//
//  Created by ISC-DanTT on 7/20/16.
//  Copyright (c) 2016 FPT. All rights reserved.
//

#import "ContractModel.h"

@implementation ContractModel

- (id)initWithContract:(NSString *)contract withDomain:(NSString *)domain withAddress:(NSString *)address andName:(NSString *)name {
    self.contract = contract;
    self.domain   = domain;
    self.address  = address;
    self.name     = name;
    
    return self;
    
}

- (id)initWithData:(NSDictionary *)dict {
    self.contract = dict[@"ContractID"];
    self.domain   = dict[@"IPDomain"];
    self.address  = dict[@"Address"];
    self.name     = dict[@"Name"];
    
    return self;
}

@end
