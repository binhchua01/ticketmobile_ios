//
//  StaffModel.m
//  TicketMobile
//
//  Created by ISC-DanTT on 7/19/16.
//  Copyright (c) 2016 FPT. All rights reserved.
//

#import "StaffModel.h"

@implementation StaffModel

- (id)initWithName:(NSString *)name withID:(NSString *)ID andEmail:(NSString *)email {
    self.name = name;
    self.ID = ID;
    self.email = email;
    
    return self;
}

@end
