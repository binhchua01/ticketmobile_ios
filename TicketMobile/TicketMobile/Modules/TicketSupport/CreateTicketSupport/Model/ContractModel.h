//
//  ContractModel.h
//  TicketMobile
//
//  Created by ISC-DanTT on 7/20/16.
//  Copyright (c) 2016 FPT. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ContractModel : NSObject
// Contract|Domain|Address|Name
@property (nonatomic, strong) NSString *contract;
@property (nonatomic, strong) NSString *domain;
@property (nonatomic, strong) NSString *address;
@property (nonatomic, strong) NSString *name;

- (id)initWithContract:(NSString *)contract withDomain:(NSString *)domain withAddress:(NSString *)address andName:(NSString *)name;
- (id)initWithData:(NSDictionary *)dict;

@end
