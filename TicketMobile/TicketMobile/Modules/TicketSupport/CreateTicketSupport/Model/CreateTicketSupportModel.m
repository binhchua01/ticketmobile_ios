//
//  CreateTicketSupportModel.m
//  TicketMobile
//
//  Created by ISC-DanTT on 7/14/16.
//  Copyright (c) 2016 FPT. All rights reserved.
//

#import "CreateTicketSupportModel.h"

@implementation CreateTicketSupportModel

-(id)setData {
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    
    [dict setObject:self.kind forKey:@"Kind"];
    
    [dict setObject:self.ticketStatus forKey:@"TicketStatus"];
    
    [dict setObject:self.title forKey:@"Title"];
    
    [dict setObject:self.desc forKey:@"Description"];
    
    [dict setObject:self.priority forKey:@"Priority"];
    
    [dict setObject:self.cricLev forKey:@"CricLev"];
    
    [dict setObject:self.estimatedTime forKey:@"EstimatedTime"];
    
    [dict setObject:self.foundStaff forKey:@"FoundStaff"];
    [dict setObject:self.foundDivision forKey:@"FoundDivision"];
    [dict setObject:self.foundPhone forKey:@"FoundPhone"];
    [dict setObject:self.foundIP forKey:@"FoundIP"];
    [dict setObject:self.foundDivName forKey:@"FoundDivName"];
    [dict setObject:self.foundName forKey:@"FoundName"];
    
    [dict setObject:self.remindStatus forKey:@"RemindStatus"];
    
    [dict setObject:self.hasWarning forKey:@"HasWarning"];
    
    [dict setObject:self.operateStaff forKey:@"OperateStaff"];
    [dict setObject:self.visorStaff forKey:@"VisorStaff"];
    [dict setObject:self.processStaff forKey:@"ProcessStaff"];
    [dict setObject:self.processPhone forKey:@"ProcessPhone"];
    [dict setObject:self.processIP forKey:@"ProcessIP"];
    
    [dict setObject:self.queue forKey:@"Queue"];
    
    [dict setObject:self.parentID forKey:@"ParentID"];
    
    [dict setObject:self.locationID forKey:@"LocationID"];
    [dict setObject:self.locationName forKey:@"LocationName"];

    [dict setObject:self.effBranch forKey:@"EffBranch"];
    [dict setObject:self.branchName forKey:@"BranchName"];

    [dict setObject:self.serviceType forKey:@"ServiceType"];
    [dict setObject:self.serviceName forKey:@"ServiceName"];

    [dict setObject:self.cusType forKey:@"CusType"];
    [dict setObject:self.cusName forKey:@"CusName"];

    [dict setObject:self.issueID forKey:@"IssueID"];
    [dict setObject:self.issueName forKey:@"IssueName"];

    [dict setObject:self.reasonGroup forKey:@"ReasonGroup"];
    [dict setObject:self.processStep forKey:@"ProcessStep"];
    [dict setObject:self.processName forKey:@"ProcessName"];
    
    [dict setObject:self.options forKey:@"Options"];
    
    return dict;
    
}

@end
