//
//  StaffModel.h
//  TicketMobile
//
//  Created by ISC-DanTT on 7/19/16.
//  Copyright (c) 2016 FPT. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface StaffModel : NSObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) NSString *ID;

- (id)initWithName:(NSString *)name withID:(NSString *)ID andEmail:(NSString *)email;


@end
