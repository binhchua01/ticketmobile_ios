//
//  CreateTicketSupportModel.h
//  TicketMobile
//
//  Created by ISC-DanTT on 7/14/16.
//  Copyright (c) 2016 FPT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KeyValueModel.h"

@interface CreateTicketSupportModel : NSObject

@property (copy, nonatomic) NSString *kind;
@property (copy, nonatomic) NSString *ticketStatus;
@property (copy, nonatomic) NSString *priority;
@property (copy, nonatomic) NSString *hasWarning;
@property (copy, nonatomic) NSString *title;
@property (copy, nonatomic) NSString *desc;
@property (copy, nonatomic) NSString *cricLev;
@property (copy, nonatomic) NSString *descrip;

@property (copy, nonatomic) NSString *estimatedTime;
@property (copy, nonatomic) NSString *foundStaff;
@property (copy, nonatomic) NSString *foundDivision;
@property (copy, nonatomic) NSString *foundPhone;
@property (copy, nonatomic) NSString *foundIP;
@property (copy, nonatomic) NSString *remindStatus;
@property (copy, nonatomic) NSString *operateStaff;

@property (copy, nonatomic) NSString *visorStaff;
@property (copy, nonatomic) NSString *processStaff;
@property (copy, nonatomic) NSString *processPhone;
@property (copy, nonatomic) NSString *processIP;
@property (copy, nonatomic) NSString *queue;
@property (copy, nonatomic) NSString *parentID;
@property (copy, nonatomic) NSString *locationID;

@property (copy, nonatomic) NSString *effBranch;
@property (copy, nonatomic) NSString *serviceType;
@property (copy, nonatomic) NSString *issueID;
@property (copy, nonatomic) NSString *reasonGroup;
@property (copy, nonatomic) NSString *processStep;
@property (copy, nonatomic) NSString *processName;
@property (copy, nonatomic) NSString *issueName;

@property (copy, nonatomic) NSString *locationName;
@property (copy, nonatomic) NSString *branchName;
@property (copy, nonatomic) NSString *serviceName;
@property (copy, nonatomic) NSString *cusName;
@property (copy, nonatomic) NSString *cusType;
@property (copy, nonatomic) NSString *foundDivName;
@property (copy, nonatomic) NSString *foundName;
@property (copy, nonatomic) NSString *options;

-(id)setData;

@end
