//
//  ProcessStaffModel.h
//  TicketMobile
//
//  Created by ISC on 7/22/16.
//  Copyright © 2016 FPT. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ProcessStaffModel : NSObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) NSString *ID;
@property (nonatomic, strong) NSString *ipPhone;
@property (nonatomic, strong) NSString *cellPhone;

- (id)initWithData:(NSDictionary *)dict;

@end
