//
//  CreateTicketSupportViewController.m
//  TicketMobile
//
//  Created by ISC-DanTT on 7/13/16.
//  Copyright (c) 2016 FPT. All rights reserved.
//

/*
 Khi load lên:
 -	Load vùng miền (api GetBusinessArea). ---- cho phép check chọn nhiều.
 -	Load loại dịch vụ (dùng api GetServiceType cho ticket HT). --- cho phép check chọn nhiều.
 -	Load queue (api QueueList).
 
 Khi chọn vùng miền:
 -	Load CN ảnh hưởng (api GetBranchLocation).
 -	Load lại mức priority (api GetPriority).
 -	Load thông tin NV xử lý, NV chủ trì, NV giám sát (như ticket SC).
 
 Khi chọn chi nhánh ảnh hưởng:
 -	Load lại mức priority (api GetPriority).
 -	Load thông tin NV xử lý, NV chủ trì, NV giám sát (như ticket SC).
 
 Khi chọn loại dịch, Khi chọn loại khách hàng:
 -	Load mô tả (api GetInput).
 -	Load nguyên nhân (api GetRFO).
 
 Khi chọn mô tả:
 -	Load mức priority (api GetPriority).
 -	Load TG quy định hoàn thành (api EstimatedDate). ---- trả về 1 số nguyên.
 
 Khi chọn queue:
 -	Load thông tin NV xử lý, NV chủ trì, NV giám sát (như ticket SC).
 
 Chức năng nhập hợp đồng:
 -	Gồm các thông tin như trong hình.
 -	Thông tin hợp đồng có thể không bắt buộc.
 -	Nếu nhập thông tin có thể nhập 1 hoặc nhiều trong số 4 trường.
 -	Cho phép nhập nhiều lần.
 -	Thông tin hợp đồng, IP/Domain không được có các ký tự đặc biệt (chỉ gồm chữ, số, dấu “.”, dấu “:”, dấu “/”.
 -	Toàn bộ thông tin được lưu định dạng “Contract|Domain|Address|Name”, nếu nhiều thông tin thì cách nhau bằng dấu “;”, ví dụ: “Contract|Domain|Address|Name;Contract|Domain|Address|Name;Contract|Domain|Address|Name”.
 -	Thông tin này là trường Options của api CreateSupport.
 */

#import "CreateTicketSupportViewController.h"
#import "TicketSupportMyCreatedListViewController.h"
#import "PopupCustomViewController.h"
#import "ContractViewController.h"
#import "ListFeedbackViewController.h"
#import "ContractModel.h"
#import "ShareData.h"
#import "KeyValueModel.h"
#import "SelectedRecord.h"
#import "StaffModel.h"
#import "ProcessStaffModel.h"
#import "NIDropDown.h"
#import "TicketDetailRecord.h"
#import "IQKeyboardManager.h"

typedef enum : NSUInteger {
    Success = 1,
    Failed,
} AlertViewTag;

@interface CreateTicketSupportViewController () <PopupCustomViewDelegate, NIDropDownDelegate, ContractViewDelegate, UIActionSheetDelegate, UIAlertViewDelegate, UIScrollViewDelegate,UITextViewDelegate>

@property (strong, nonatomic) IBOutlet UIView *menuCreateTicketView;
@property (strong, nonatomic) IBOutlet UIView *menuUpdateTicketView;
@property (weak, nonatomic) IBOutlet UIView *timeView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *timeViewHeightLC;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *timeViewTopLC;
@property (weak, nonatomic) IBOutlet UIView *troubleStatusView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *troubleStatusViewHeightLC;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewHeightLC;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;

@property (weak, nonatomic) IBOutlet UIButton *btnUpdateTicket;
@property (strong, nonatomic) NSString *date;
@property (strong, nonatomic) NSString *finishTime;
@property (strong, nonatomic) dispatch_group_t group;


@end

@implementation CreateTicketSupportViewController {
    
    
    
    TicketDetailRecord *rc;
    
    ShareData       *shareData;
    NSMutableArray  *locationArray; // Danh sách vùng miền
    NSMutableArray  *branchLocationArray;    // Danh sách chi nhánh, tải theo vùng miền
    NSMutableArray  *serviceTypeArray;  // Danh sách loại dịch vụ
    NSMutableArray  *queueArray;    // Danh sách queue
    NSMutableArray  *customerTypeArray; // Danh sách loại khách hàng
    NSMutableArray  *inputArray; // Danh sách mô tả
    NSMutableArray  *reasonArray; // Danh sách nguyên nhân
    NSMutableArray  *operateStaffArray; // Danh sách nhân viên chủ trì
    NSMutableArray  *visorStaffArray;    // Danh sách nhân viên giám sát
    NSMutableArray  *processStaffArray;    // Danh sách nhân viên xử lý
    NSMutableArray  *ticketStatusArray;    // Danh sách trạng thái ticket
    
    NSMutableString *idLocationString, *nameLocationString, *idBranchString, *nameBranchString, *contractString;
    
    NSString *HTEstimatedTimeValue;   // Thời gian quy định hoàn thành
    
    KeyValueModel   *serviceTypeSelected, *customerTypeSelected, *inputSelected, *reasonSelected, *queueSelected, *ticketStatusSelected;
    
    StaffModel *operateStaffSelected, *visorStaffSelected;
    
    ProcessStaffModel *processStaffSelected;
    
    PopupCustomViewController *popupCustomView;
    
    NIDropDown       *dropDownView;
    
    UIButton         *buttonPressed;
    
    BOOL             _isSelectedAll, _isRefreshing;
    
    UIRefreshControl *_refreshControl;
    NSDate *date1;
    NSDate *date2;
    
    BOOL checkLocation,checkBranch;
    
    

}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.requestTextView.delegate = self;
    [self setDefaultView];
    // Get data from web service
    checkBranch = NO;
    checkLocation = NO;
    self.group = dispatch_group_create();
    
    switch (self.viewType) {
        case createTicketSupport:
            [self setCreateTicketView];
            break;
            
        case updateTicketSupport:
            [self getLocationData];
            [self setUpdateTicketView];
            break;
            
        default:
            break;
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (id)initWithViewType:(TicketSupportViewType)viewType {
    CreateTicketSupportViewController *vc = [[CreateTicketSupportViewController alloc] init];
    vc.viewType = viewType;
    return vc;
}

- (void)handleSingleTap:(UITapGestureRecognizer *)sender {
    [self.view endEditing:YES];
    if (dropDownView !=nil) {
        [dropDownView hideDropDown:buttonPressed];
        dropDownView = nil;
    }
}

- (void)setDefaultView {
    // Init data
    _isSelectedAll = NO;
    self.contractArray  = [NSMutableArray array];
    idLocationString    = [[NSMutableString alloc] init];
    idBranchString      = [[NSMutableString alloc] init];
    nameLocationString  = [[NSMutableString alloc] init];
    nameBranchString    = [[NSMutableString alloc] init];
    contractString      = [[NSMutableString alloc] init];
    shareData = [ShareData instance];
    // Set info for notify staff
    [self.notifyStaffButton setTitle:shareData.currentUser.userName forState:UIControlStateNormal];
    [self.notifyStaffButton setEnabled:NO];
    self.notifyStaffPhone.text = shareData.currentUser.phone;
    self.notifyStaffIPPhone.text = shareData.currentUser.ipPhone;
    [self.departmentNotifyButton setTitle:shareData.currentUser.divisionname forState:UIControlStateNormal];
    [self.departmentNotifyButton setEnabled:NO];
    [self.requestTextView styleBorder];
    [self.noteTextView styleBorder];
    // Set handle tap on screen
    UITapGestureRecognizer *tapGestureRecohnizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
    [self.view addGestureRecognizer:tapGestureRecohnizer];
    
    self.scrollView.delegate = self;
    
    // Add refresh control when scroll
    _refreshControl = [[UIRefreshControl alloc] init];
    _refreshControl.tintColor = [UIColor orangeColor];
    NSDictionary *attrsDictionary = [NSDictionary dictionaryWithObject:[UIColor orangeColor] forKey:NSForegroundColorAttributeName];
    NSAttributedString *attributedTitle = [[NSAttributedString alloc] initWithString:@"Thả để tải lại dữ liệu..." attributes:attrsDictionary];
    _refreshControl.attributedTitle = attributedTitle;
    [self.scrollView insertSubview:_refreshControl atIndex:0];
}

#pragma refresh scroll view

- (void)refreshData:(UIRefreshControl *)refreshControll {
    
    NSLog(@"----- Refreshing data...");
    dispatch_async(dispatch_get_main_queue(), ^{
        // Get data from web service
        switch (self.viewType) {
            case createTicketSupport:
                [self clearData];
                // Get data from wed service
                [self showMBProcess:@"Đang tải dữ liệu..."];
                NSLog(@"Getting loaction...");
                [self getLocationData];
                
                NSLog(@"Getting service type...");
                [self getServiceTypeDataWithID:@"0"];
                
                NSLog(@"Getting queue...");
                [self getQueueDataWithQueueID:@"0"];
                
                break;
                
            case updateTicketSupport:
                [self getContractListWithTicketID:self.ticketID];
                [self getTicketSupportDetail];
                
                break;
                
            default:
                break;
        }
    });
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (scrollView.contentOffset.y <= -150 && ![_refreshControl isRefreshing]) {
        [_refreshControl beginRefreshing];
        _isRefreshing = YES;
        return;
    }
    
    if (scrollView.contentOffset.y > -150 && _isRefreshing) {
        _isRefreshing = NO;
        // Refreshing...
        [self refreshData:_refreshControl];
        // End refresh
        [_refreshControl endRefreshing];
        return;
    }
}

- (void)clearData {
    [self setTitleDefultForButton:self.locationButton];
    [self setTitleDefultForButton:self.branchButton];
    [self setTitleDefultForButton:self.serviceTypeButton];
    [self setTitleDefultForButton:self.customerTypeButton];
    [self setTitleDefultForButton:self.descriptionButton];
    [self setTitleDefultForButton:self.reasonButton];
    [self setTitleDefultForButton:self.queueButton];
    [self setTitleDefultForButton:self.operateStaffButton];
    [self setTitleDefultForButton:self.supervisoryStaffButton];
    [self setTitleDefultForButton:self.processStaffButton];
    [self setTitleDefultForButton:self.troubleStatusButton];
    [locationArray removeAllObjects];
    [branchLocationArray removeAllObjects];
    [serviceTypeArray removeAllObjects];
    [customerTypeArray removeAllObjects];
    [inputArray removeAllObjects];
    [reasonArray removeAllObjects];
    [_contractArray removeAllObjects];
    [queueArray removeAllObjects];
    [operateStaffArray removeAllObjects];
    [visorStaffArray removeAllObjects];
    [processStaffArray removeAllObjects];
    
    [idLocationString setString:@""];
    [nameLocationString setString:@""];
    [idBranchString setString:@""];
    [nameBranchString setString:@""];
    
    serviceTypeSelected = customerTypeSelected = inputSelected = reasonSelected = nil;
    
    queueSelected = nil;
    operateStaffSelected = visorStaffSelected = nil;
    processStaffSelected = nil;
    
    self.requestTextView.text = self.noteTextView.text = @"";
    self.priorityLabel.text = @"0";
    self.processStaffPhone.text = self.processStaffIPPhone.text = @"";
    
    [self.scrollView layoutIfNeeded];
}

#pragma mark - Settup data default when load view the first times
- (void)setCreateTicketView {
    // Set title View
    self.title = @"TẠO TICKET HỖ TRỢ";
    // Set menu View
    float height = self.menuContainerViewController.view.frame.size.height;
    [self.menuCreateTicketView setFrame:CGRectMake(0,height - 35,self.menuCreateTicketView.frame.size.width,self.menuCreateTicketView.frame.size.height)];
    [self.view addSubview:self.menuCreateTicketView];
    // Remove time View and trouble status view
    //    self.contentViewHeightLC.constant = self.scrollView.frame.size.height - self.timeViewHeightLC.constant - self.troubleStatusViewHeightLC.constant;
    
    self.contentViewHeightLC.constant = 1340.0f - self.timeViewHeightLC.constant - self.troubleStatusViewHeightLC.constant;
    self.timeViewHeightLC.constant = 0;
    self.timeViewTopLC.constant = 0;
    self.troubleStatusViewHeightLC.constant = 0;
    // Get data from wed service
    [self showMBProcess:@"Đang tải dữ liệu..."];
    NSLog(@"Getting loaction...");
    [self getLocationData];
    
    NSLog(@"Getting service type...");
    [self getServiceTypeDataWithID:@"0"];
    
    NSLog(@"Getting queue...");
    [self getQueueDataWithQueueID:@"0"];
    
}
//vutt11

- (void)setUpdateTicketView {
    // Set title View
    self.title = @"CHI TIẾT TICKET HỖ TRỢ";
    // Set menu View
    float height = self.menuContainerViewController.view.frame.size.height;
    [self.menuUpdateTicketView setFrame:CGRectMake(0,height - 35,self.menuUpdateTicketView.frame.size.width,self.menuUpdateTicketView.frame.size.height)];
    [self.view addSubview:self.menuUpdateTicketView];
    // Get data from web service
    [self getContractListWithTicketID:self.ticketID];
    [self getTicketSupportDetail];
    
}
//vutt11
- (BOOL) textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if ([textView.text length]>= 400  && (range.length == 0)) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Thông báo" message:@"Bạn đã nhập quá số text của yêu cầu" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alertView show];
            [textView becomeFirstResponder];
            return NO;
    }
        else{
            return YES;
        } 
    
    return YES;
}

-(void)setTicketSupportDetailData {
    shareData.branchName = rc.Branch;
    shareData.branchID = rc.BranchID;
    shareData.locationName = rc.Location;
    shareData.locationID = rc.LocationID;
    
    [self.locationButton setTitle:shareData.locationName forState:UIControlStateNormal];
    [self.locationButton setEnabled:YES];
    [self.branchButton setTitle: shareData.branchName forState:UIControlStateNormal];
    [self.branchButton setEnabled:YES];
     self.finishTime = rc.RequiredDate;
    self.requestTextView.text = self.request;
    self.requestTextView.editable = NO;
    [self.requestTextView endEditing:NO];
    self.noteTextView.text = rc.Description;
    self.noteTextView.editable = YES;
    self.createTimeLabel.text = rc.CreatedDate;
    self.estimatedTimeLabel.text = rc.RequiredDate;
    [self.enterContractButton setTitle:@"[Xem hợp đồng]" forState:UIControlStateNormal];
    [self.processStaffButton setEnabled:YES];
    self.processStaffPhone.text = rc.ProcessMobile;
    self.processStaffIPPhone.text = rc.ProcessIPPhone;
    
     [self getQueueDataWithQueueID:rc.Queue];
    [self getServiceTypeDataWithID:rc.ServiceType];
    [self getPriorityDataWithInputID:rc.Issue locationID:shareData.locationID branchID:shareData.branchID];
    [self getStatusTicket:rc.TicketStatus currentStatus:rc.TicketStatus];
    

    
}

#pragma mark - Load data from Web service
// Get  Ticket Support Detail
-(void)getTicketSupportDetail {
    if(![SiUtils checkNetworkAvailable]){
        [self showAlertBox:@"Thông báo" message:Mesage_Network];
        return;
    }
    [self showMBProcess:@"Đang tải dữ liệu..."];
    ShareData *shared = [ShareData instance];
    [shared.ticketProxy getTicketInfo:self.ticketID completeHandler:^(id result, NSString *errorCode, NSString *message) {
        if([errorCode isEqualToString:@"1"]){
            rc = result;
            
            [self getEstimateTime:rc.CricLev IssueID:inputSelected.Key];
            
            [self setTicketSupportDetailData];
            
            return;
        }
        [self showAlertBox:@"Thông Báo" message:Mesage_Network];
        [self hideMBProcess];
        
    } errorHandler:^(NSError *error) {
        
        [self hideMBProcess];
        [self showAlertBox:@"Thông Báo" message:[error localizedDescription]];
        return;
    }];
}

// lấy thời gian Hoàn Thành
- (void) getEstimateTime :(NSString *)cricLev IssueID:(NSString*)issueID
{
    
    if(![SiUtils checkNetworkAvailable]){
        [self showAlertBox:@"Thông báo" message:Mesage_Network];
        return;
    }
    
    //[self showMBProcess:@"Đang tải dữ liệu..."];
    ShareData *shared = [ShareData instance];
    [shared.ticketProxy getEstimateTime:cricLev IssueID:issueID completeHandler:^(id result, NSString *errorCode, NSString *message){
        if([errorCode isEqualToString:@"1"]){
    
            NSString *estimatedTimeValue = [NSString stringWithFormat:@"%@",[[result objectAtIndex:0]objectForKey:@"EstimatedTimeValue"]];
            
            
            date1 = [self convertStringToDate:rc.RequiredDate];
           
            
            //conver minute add estimatedTime
            NSTimeInterval secondsInOneMinute = [estimatedTimeValue integerValue]*60;
            
            date2 = [date1 dateByAddingTimeInterval:secondsInOneMinute];
            
            
            //lay thoi gian hoan thanh updateticket ho tro
            self.finishTime = [self convertDateToString:date2];
            self.estimatedTimeLabel.text = self.finishTime;
            
        }
        
        
    } errorHandler: ^(NSError *error){
        //[self hideMBProcess];
        [self showAlertBox:@"Thông Báo" message:[error localizedDescription]];
        return;
    }];

}

/*
 * Lấy dữ liệu Vùng miền
 */
- (void)getLocationData {
    
    if(![SiUtils checkNetworkAvailable]){
        [self showAlertBox:@"Thông báo" message:Mesage_Network];
        return;
    }
    NSDictionary *dict = [NSDictionary dictionaryWithObject:shareData.currentUser.userId ?:@"0" forKey:@"UserID"];
    
    [shareData.ticketSupportProxy getBusinessArea:dict completeHandler:^(id result, NSString *errorCode, NSString *message) {
        
        if([errorCode isEqualToString:@"1"]){
            locationArray  = result;
            
            if(locationArray.count > 0){
                NSLog(@"Getted location!");
                return;
            }
        }
        
        [self showAlertBox:@"Thông báo" message:message];
        
    } errorHandler:^(NSError *error) {
        [self hideMBProcess];
        [self showAlertBox:@"Thông Báo" message:[error localizedDescription]];
    }];
}

/*
 * Lấy dữ liệu chi nhánh ảnh hưởng theo vùng miền
 */
- (void)getBranchLocationDataWithLocationID:(NSString *)locationID {
    // Set default value
    [branchLocationArray removeAllObjects];
    
    [self setTitleDefultForButton:self.branchButton];
    
    if (locationID.length <= 0) {
        return;
    }
    
    if(![SiUtils checkNetworkAvailable]){
        [self showAlertBox:@"Thông báo" message:Mesage_Network];
        return;
    }
    NSDictionary *dict = [NSDictionary dictionaryWithObject:locationID ?:@"0" forKey:@"LocationID"];
    //    [self showMBProcess:@"Đang tải dữ liệu chi nhánh..."];
    [shareData.ticketSupportProxy getBranchLocation:dict completeHandler:^(id result, NSString *errorCode, NSString *message) {
        
        if([errorCode isEqualToString:@"1"]){
            branchLocationArray  = result;
            
            if(branchLocationArray.count > 0){
                NSLog(@"Getted branch location!");
                [self hideMBProcess];
                return;
            }
        }
        [self hideMBProcess];
        [self showAlertBox:@"Thông báo" message:message];
        
    } errorHandler:^(NSError *error) {
        [self hideMBProcess];
        [self showAlertBox:@"Thông Báo" message:[error localizedDescription]];
    }];
}
/*
 * Lấy dữ liệu Loại dịch vụ
 */
- (void)getServiceTypeDataWithID:(NSString *)serviceTypeID {
    if(![SiUtils checkNetworkAvailable]){
        [self showAlertBox:@"Thông báo" message:Mesage_Network];
        [self hideMBProcess];
        return;
    }
    //dispatch_group_enter(self.group);
    ShareData *shared = [ShareData instance];
    [shared.ticketSupportProxy getServiceType:^(id result, NSString *errorCode, NSString *message) {
        
        if([errorCode isEqualToString:@"1"]){
            serviceTypeArray = result;
            if (serviceTypeArray.count > 0) {
                NSLog(@"Getted service type!");
                
                if (![serviceTypeID isEqualToString:@"0"]) {
                    for (serviceTypeSelected in serviceTypeArray) {
                        if ([serviceTypeSelected.Key isEqual:serviceTypeID]) {
                            [self.serviceTypeButton setTitle:serviceTypeSelected.Values forState:UIControlStateNormal];
                            [self.serviceTypeButton setEnabled:YES];
                            
                            [self getCustomerTypeDataWithServiceTypeID:rc.ServiceType andCustomerTypeID:rc.CusType];
                            
                            return;
                        }
                    }
                }
                
                return;
            }
        }
        
        [self showAlertBox:@"Thông báo" message:message];
        
        
    } errorHandler:^(NSError *error) {
        [self showAlertBox:@"Thông Báo" message:[error localizedDescription]];
        [self hideMBProcess];
       // dispatch_group_leave(self.group);
        return;
    }];
}

/*
 * Lấy dữ liệu Loại khách hàng theo Loại dịch vụ
 */
- (void)getCustomerTypeDataWithServiceTypeID:(NSString *)serviceTypeID andCustomerTypeID:(NSString *)customerTypeID {
    if (serviceTypeArray.count <= 0) {
        return;
    }
    
    if(![SiUtils checkNetworkAvailable]){
        [self showAlertBox:@"Thông báo" message:Mesage_Network];
        return;
    }
    
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:serviceTypeID ?:@"0" forKey:@"ServiceType"];
    
    if ([customerTypeID isEqual:@"0"]) {
        [self showMBProcess:@"Đang tải loại khách hàng..."];
    }
    
    ShareData *shared = [ShareData instance];
    [shared.ticketSupportProxy getCustomerType:dict completeHandler:^(id result, NSString *errorCode, NSString *message) {
        
        if([errorCode isEqualToString:@"1"]){
            customerTypeArray = result;
            if (customerTypeArray.count > 0) {
                NSLog(@"Getted customer type!");
                if (![customerTypeID isEqual:@"0"]) {
                    for (customerTypeSelected in customerTypeArray) {
                        if ([customerTypeSelected.Key isEqual:customerTypeID]) {
                            [self.customerTypeButton setTitle:customerTypeSelected.Values forState:UIControlStateNormal];
                            [self.customerTypeButton setEnabled:YES];
                            
                            [self getInputDataWithCustomerTypeID:rc.CusType andInputID:rc.Issue];
                            
                            [self getReasonDataWithCustomerTypeID:rc.CusType andReasonID:rc.Reason];
                            
                            return;
                        }
                    }
                }
                [self hideMBProcess];
                return;
            }
        }
        [self hideMBProcess];
        [self showAlertBox:@"Thông báo" message:message];
        
    } errorHandler:^(NSError *error) {
        [self showAlertBox:@"Thông Báo" message:[error localizedDescription]];
        [self hideMBProcess];
        return;
    }];
}

/*
 * Lấy dữ liệu Mô tả theo Loại khách hàng
 */
- (void)getInputDataWithCustomerTypeID:(NSString *)customerTypeID andInputID:(NSString *)inputID {
    if (customerTypeArray.count <= 0) {
        return;
    }
    
    if(![SiUtils checkNetworkAvailable]){
        [self showAlertBox:@"Thông báo" message:Mesage_Network];
        return;
    }
    
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:customerTypeID ?:@"0" forKey:@"CusType"];
    
    if ([inputID isEqual:@"0"]) {
        [self showMBProcess:@"Đang tải mô tả..."];
        
    }
    
    ShareData *shared = [ShareData instance];
    [shared.ticketSupportProxy getInput:dict completeHandler:^(id result, NSString *errorCode, NSString *message) {
        
        if([errorCode isEqualToString:@"1"]){
            inputArray = result;
            if (inputArray.count > 0) {
                NSLog(@"Getted input!");
                if (![inputID isEqual:@"0"]) {
                    for (inputSelected in inputArray) {
                        if ([inputSelected.Key isEqual:inputID]) {
                            [self.descriptionButton setTitle:inputSelected.Values forState:UIControlStateNormal];
                            [self.descriptionButton setEnabled:YES];
                            [self hideMBProcess];
                            return;
                        }
                    }
                }
                [self hideMBProcess];
                return;
            }
        }
        [self hideMBProcess];
        [self showAlertBox:@"Thông báo" message:message];
    } errorHandler:^(NSError *error) {
        [self showAlertBox:@"Thông Báo" message:[error localizedDescription]];
        [self hideMBProcess];
        return;
    }];
}
/*
 * Lấy dữ liệu Độ ưu tiên theo mô tả, vùng miền và chi nhánh
 */
- (void)getPriorityDataWithInputID:(NSString *)inputID locationID:(NSString *)locationID branchID:(NSString *)branchID {
    
    if(![SiUtils checkNetworkAvailable]){
        [self showAlertBox:@"Thông báo" message:Mesage_Network];
        return;
    }
    
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:inputID ?:@"0" forKey:@"IssueID"];
    [dict setObject:locationID ?:@"0" forKey:@"Location"];
    [dict setObject:branchID ?:@"0" forKey:@"Branch"];
    
   // dispatch_group_enter(self.group);
    ShareData *shared = [ShareData instance];
    [shared.ticketSupportProxy getPriority:dict completeHandler:^(id result, NSString *errorCode, NSString *message) {
        
        if([errorCode isEqualToString:@"1"]){
            self.priorityLabel.text = StringFormat(@"%@",result);
            
            [self getEstimateTime:self.priorityLabel.text IssueID:inputSelected.Key];
            
            NSLog(@"Getted priority!");
            [self hideMBProcess];
            return;
        }
        
        [self hideMBProcess];
        [self showAlertBox:@"Thông báo" message:message];
        
        
    } errorHandler:^(NSError *error) {
        [self showAlertBox:@"Thông Báo" message:[error localizedDescription]];
        [self hideMBProcess];
       // dispatch_group_leave(self.group);
        return;
    }];
}

/*
 * Lấy dữ liệu thời gian quy định hoàn thành theo mô tả
 */
- (void)getHTEstimatedTime:(NSString *)cricLev inputID:(NSString *)inputID {
    if(![SiUtils checkNetworkAvailable]){
        [self showAlertBox:@"Thông báo" message:Mesage_Network];
        [self hideMBProcess];
        return;
    }
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:inputID ?:@"0" forKey:@"IssueID"];
    [dict setObject:cricLev ?:@"1" forKey:@"CricLev"];
    
    ShareData *shared = [ShareData instance];
    [shared.ticketSupportProxy getHTEstimatedTime:dict completeHandler:^(id result, NSString *errorCode, NSString *message) {
        
        if([errorCode isEqualToString:@"1"]){
            HTEstimatedTimeValue = StringFormat(@"%@",result);
            NSLog(@"Getted HT Estimated Time!");
            return;
        }
        
        NSLog(@"Error Get HT Estimated Time: %@",message);
        
    } errorHandler:^(NSError *error) {
        [self showAlertBox:@"Thông Báo" message:[error localizedDescription]];
        [self hideMBProcess];
        return;
    }];
}
/*
 * Lấy dữ liệu hợp đồng khi tải chi tiết Ticket
 */
- (void)getContractListWithTicketID:(NSString *)ticketID {
    if(![SiUtils checkNetworkAvailable]){
        [self showAlertBox:@"Thông báo" message:Mesage_Network];
        [self hideMBProcess];
        return;
    }
    
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:ticketID ?:@"0" forKey:@"TicketID"];
    
    ShareData *shared = [ShareData instance];
    [shared.ticketSupportProxy getContractList:dict completeHandler:^(id result, NSString *errorCode, NSString *message) {
        
        if([errorCode isEqualToString:@"1"]){
            self.contractArray = result;
            NSLog(@"Getted Contract List!");
            return;
        }
        
        NSLog(@"Error Get HT Estimated Time: %@",message);
        
    } errorHandler:^(NSError *error) {
        [self showAlertBox:@"Thông Báo" message:[error localizedDescription]];
        [self hideMBProcess];
        return;
    }];
}

/*
 * Lấy dữ liệu Nguyên nhân theo Loại khách hàng
 */
- (void)getReasonDataWithCustomerTypeID:(NSString *)customerTypeID andReasonID:(NSString *)reasonID {
    if (customerTypeArray.count <= 0) {
        return;
    }
    
    if(![SiUtils checkNetworkAvailable]){
        [self showAlertBox:@"Thông báo" message:Mesage_Network];
        return;
    }
    
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:customerTypeID ?:@"0" forKey:@"CusType"];
    
    ShareData *shared = [ShareData instance];
    [shared.ticketSupportProxy getRFO:dict completeHandler:^(id result, NSString *errorCode, NSString *message) {
        
        if([errorCode isEqualToString:@"1"]){
            reasonArray = result;
            if (reasonArray.count > 0) {
                NSLog(@"Getted reason!");
                if (![reasonID isEqual:@"0"]) {
                    for (reasonSelected in reasonArray) {
                        if ([reasonSelected.Key isEqual:reasonID]) {
                            [self.reasonButton setTitle:reasonSelected.Values forState:UIControlStateNormal];
                            [self.reasonButton setEnabled:YES];
                            [self hideMBProcess];
                            return;
                        }
                    }
                }
                [self hideMBProcess];
                return;
            }
        }
        
        [self hideMBProcess];
        [self showAlertBox:@"Thông báo" message:message];
        
    } errorHandler:^(NSError *error) {
        [self showAlertBox:@"Thông Báo" message:[error localizedDescription]];
        [self hideMBProcess];
        return;
    }];
}

/*
 * Trạng thái Ticket
 */

-(void)getStatusTicket:(NSString *)key currentStatus:(NSString *)currentstatus {
    if(![SiUtils checkNetworkAvailable]){
        [self showAlertBox:@"Thông báo" message:Mesage_Network];
        [self hideMBProcess];
        return;
    }
    //dispatch_group_enter(self.group);
    ShareData *shared = [ShareData instance];
    [shared.appProxy getTicketStatusList:shared.currentUser.userId CurrentStatus:currentstatus completeHandler:^(id result, NSString *errorCode, NSString *message) {
        if([errorCode isEqualToString:@"1"]){
            ticketStatusArray = result;
            if(ticketStatusArray.count > 0){
                for (ticketStatusSelected in ticketStatusArray) {
                    if([ticketStatusSelected.Key isEqualToString:key]){
                        [self.troubleStatusButton setTitle:ticketStatusSelected.Values forState:UIControlStateNormal];
                        [self.troubleStatusButton setEnabled:YES];
                        // [self CheckQueue];
                        if ([ticketStatusSelected.Key isEqualToString:@"0"]) {
                            [self.queueButton setEnabled:FALSE];
                        }
                        
                        [self hideMBProcess];
                        return;
                    }
                }
                [self.troubleStatusButton setTitle:@"" forState:UIControlStateNormal];
                [self.troubleStatusButton setEnabled:YES];
                [self hideMBProcess];
                
            }
        }
        
        
        
    } errorHandler:^(NSError *error) {
        [self showAlertBox:@"Thông Báo" message:[error localizedDescription]];
        [self hideMBProcess];
       // dispatch_group_leave(self.group);
        return;
        
    }];
}

/*
 * Lấy dữ liệu Queue
 */

-(void)getQueueDataWithQueueID:(NSString *)queueID {
    if(![SiUtils checkNetworkAvailable]){
        [self showAlertBox:@"Thông báo" message:Mesage_Network];
        [self hideMBProcess];
        return;
    }
    
    ShareData *shared = [ShareData instance];
    //vutt11
    //dispatch_group_enter(self.group);
    [shared.appProxy getQueueList:^(id result, NSString *errorCode, NSString *message) {
        if([errorCode isEqualToString:@"1"]){
            queueArray = result;
            [self.operateStaffButton setEnabled:YES];
            [self.supervisoryStaffButton setEnabled:YES];
            [self.processStaffButton setEnabled:YES];
            
            if(queueArray.count > 0){
                NSLog(@"Getted queue!");
                
               
                
                if (![queueID isEqual:@"0"]) {
                    for (queueSelected in queueArray) {
                        if ([queueSelected.Key isEqual:queueID]) {
                            [self.queueButton setTitle:queueSelected.Values forState:UIControlStateNormal];
                            //NO
                            if (![queueSelected.Key isEqualToString:@"0"]) {
                                [self.queueButton setEnabled:FALSE];
                            }else {
                                [self.queueButton setEnabled:TRUE];
                            }
                            
                            if ([shared.currentUser.parentQueueId isEqualToString:queueSelected.Key ])
                            {
                                [self.btnUpdateTicket setHidden:NO];
                                
                            }else
                                
                            {
                                [self.btnUpdateTicket setHidden:YES];
                                
                            }
                            
                            [self getOperateStaffWithQueueID:rc.Queue withCricLev:rc.CricLev withLocationID:rc.LocationID withBranchID:rc.BranchID andOperateStaffEmail:rc.OperateStaff];
                            
                            [self getVisorStaffWithQueueID:rc.Queue withLocationID:rc.LocationID withBranchID:rc.BranchID andVisorStaffEmail:rc.VisorStaff];
                            
                            [self getProcessStaffWithQueueID:rc.Queue withLocationID:rc.LocationID andBranchID:rc.BranchID];
                            
                            self.processStaffButton.enabled = TRUE;
                            
                           
                            return;
                        }
                    }
                }
                
                [self hideMBProcess];
                return;
            }
        }
        [self hideMBProcess];
        [self showAlertBox:@"Thông báo" message:message];
         //dispatch_group_wait(self.group, DISPATCH_TIME_FOREVER);
        
        
    } errorHandler:^(NSError *error) {
        [self showAlertBox:@"Thông Báo" message:[error localizedDescription]];
        [self hideMBProcess];
        
       // dispatch_group_leave(self.group);

        return;
    }];
    
   
}

/*
 * Lấy dữ liệu nhân viên chủ trì theo queue, vùng miền, chi nhánh và mức cricle
 */
- (void)getOperateStaffWithQueueID:(NSString *)queueID withCricLev:(NSString *)cricLev withLocationID:(NSString *)locationID withBranchID:(NSString *)branchID andOperateStaffEmail:(NSString *)operateStaffEmail {
    if (queueArray.count <= 0) {
        return;
    }
    
    if(![SiUtils checkNetworkAvailable]){
        [self showAlertBox:@"Thông báo" message:Mesage_Network];
        return;
    }
    
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:queueID ?:@"0" forKey:@"Queue"];
    [dict setObject:cricLev ?:@"1" forKey:@"CricLev"];
    [dict setObject:locationID ?:@"0" forKey:@"LocationID"];
    [dict setObject:branchID ?:@"0" forKey:@"BranchID"];
    
    if ([operateStaffEmail isEqual:@""]) {
        [self showMBProcess:@"Đang tải dữ liệu..."];
    }
   // dispatch_group_enter(self.group);
    ShareData *shared = [ShareData instance];
    [shared.ticketSupportProxy getOperateStaff:dict completeHandler:^(id result, NSString *errorCode, NSString *message) {
        
        if([errorCode isEqualToString:@"1"]){
           
            operateStaffArray = result;
            operateStaffSelected = operateStaffArray[0];
            [self.operateStaffButton setTitle:operateStaffSelected.name forState:UIControlStateNormal];
           //  [self.operateStaffButton setEnabled:NO];
            
            if (operateStaffArray.count > 0) {
                NSLog(@"Getted operate staff!");
                if (![operateStaffEmail isEqual:@""]) {
                    for (operateStaffSelected in operateStaffArray) {
                        if ([operateStaffSelected.email isEqual:operateStaffEmail]) {
                            //vutt11
                            //processStaffSelected = processStaffArray[indexPath.row];
                            //operateStaffSelected = operateStaffArray[0];
                            [self.operateStaffButton setTitle:operateStaffSelected.name forState:UIControlStateNormal];
                            //NO
                            [self.operateStaffButton setEnabled:YES];
                            [self hideMBProcess];
                            return;
                        }
                    }
                }
                return;
            }
        }
        //vutt11
        [self hideMBProcess];
        [self showAlertBox:@"Thông báo" message:@"Không có thông tin NV chủ trì, NV giám sát, NV xử lý"];
        [self.operateStaffButton setEnabled:NO];
        [self.supervisoryStaffButton setEnabled:NO];
        [self.processStaffButton setEnabled:NO];
        
       // dispatch_group_leave(self.group);
        
    } errorHandler:^(NSError *error) {
        [self showAlertBox:@"Thông Báo" message:[error localizedDescription]];
        [self hideMBProcess];
        return;
    }];
}

/*
 * Lấy dữ liệu nhân viên giám sát theo queue, vùng miền, chi nhánh và mức cricle
 */
- (void)getVisorStaffWithQueueID:(NSString *)queueID withLocationID:(NSString *)locationID withBranchID:(NSString *)branchID andVisorStaffEmail:(NSString *)visorStaffEmail {
    if (queueArray.count <= 0) {
        return;
    }
    
    if(![SiUtils checkNetworkAvailable]){
        [self showAlertBox:@"Thông báo" message:Mesage_Network];
        return;
    }
    
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:queueID ?:@"0" forKey:@"Queue"];
    [dict setObject:locationID ?:@"0" forKey:@"LocationID"];
    [dict setObject:branchID ?:@"0" forKey:@"BranchID"];
    
    
    //dispatch_group_enter(self.group);
    ShareData *shared = [ShareData instance];
    [shared.ticketSupportProxy getVisorStaff:dict completeHandler:^(id result, NSString *errorCode, NSString *message) {
        
        if([errorCode isEqualToString:@"1"]){
            visorStaffArray = result;
            if (visorStaffArray.count > 0) {
                NSLog(@"Getted visor staff!");
                
                //vutt11
                visorStaffSelected = visorStaffArray[0];
                [self.supervisoryStaffButton setTitle:visorStaffSelected.name forState:UIControlStateNormal];
                if (![visorStaffEmail isEqual:@""]) {
                    for (visorStaffSelected in visorStaffArray) {
                        if ([visorStaffSelected.email isEqual:visorStaffEmail]) {
                           // visorStaffSelected.name = visorStaffArray[0];
                            [self.supervisoryStaffButton setTitle:visorStaffSelected.name forState:UIControlStateNormal];
                            //NO
                            [self.supervisoryStaffButton setEnabled:YES];
                            [self hideMBProcess];
                            return;
                        }
                    }
                }
                [self hideMBProcess];
                return;
            }
        }
        [self hideMBProcess];
        
          //dispatch_group_leave(self.group);
        
    } errorHandler:^(NSError *error) {
        [self showAlertBox:@"Thông Báo" message:[error localizedDescription]];
        [self hideMBProcess];
        return;
    }];
  
    
}

/*
 * Lấy dữ liệu nhân viên giám sát theo queue, vùng miền, chi nhánh và mức cricle
 */
- (void)getProcessStaffWithQueueID:(NSString *)queueID withLocationID:(NSString *)locationID andBranchID:(NSString *)branchID {

    
    if(![SiUtils checkNetworkAvailable]){
        [self showAlertBox:@"Thông báo" message:Mesage_Network];
        return;
    }
    
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:queueID ?:@"0" forKey:@"Queue"];
    [dict setObject:locationID ?:@"0" forKey:@"LocationID"];
    [dict setObject:branchID ?:@"0" forKey:@"BranchID"];
    
    //dispatch_group_enter(self.group);
    ShareData *shared = [ShareData instance];
    [shared.ticketSupportProxy getProcessStaff:dict completeHandler:^(id result, NSString *errorCode, NSString *message) {
        
        if([errorCode isEqualToString:@"1"]){
            processStaffArray = result;

            if (processStaffArray.count > 0) {
                
                if (![rc.ProcessStaff isEqualToString:@""]) {
                    
                    for (int i= 0; i<processStaffArray.count;i++) {
                         processStaffSelected = processStaffArray[i];
                        if ([processStaffSelected.email isEqual:rc.ProcessStaff]) {
                            
                           
                            [self.processStaffButton setTitle:processStaffSelected.name forState:UIControlStateNormal];
                            
                            processStaffSelected = processStaffArray[i];
                            
                            return ;
                        
                        }
                    }
                }
                
                [self CheckQueue];
            
                NSLog(@"Getted process staff!");
                [self hideMBProcess];
                return;
            }
        }
        [self hideMBProcess];
        // dispatch_group_leave(self.group);
        
    } errorHandler:^(NSError *error) {
        [self showAlertBox:@"Thông Báo" message:[error localizedDescription]];
        [self hideMBProcess];
        return;
    }];
   
}

/*
 * Tạo Ticket Hỗ trợ
 */
- (void)createTicketSupport {
    if(![SiUtils checkNetworkAvailable]){
        [self showAlertBox:@"Thông báo" message:Mesage_Network];
        return;
    }
    
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:[self settupDataCreateTicketSupport]];
    [self showMBProcess:@"Đang tạo Ticket..."];
    ShareData *shared = [ShareData instance];
    [shared.ticketSupportProxy createSupport:dict completeHandler:^(id result, NSString *errorCode, NSString *message) {
        
        if([errorCode isEqualToString:@"1"]){
            [self showAlerViewWithTitle:@"Thông báo" withMessage:message andTag:Success];
            [self hideMBProcess];
            
            return;
        }
        [self hideMBProcess];
        [self showAlerViewWithTitle:@"Lỗi" withMessage:message andTag:Failed];
        
    } errorHandler:^(NSError *error) {
        [self showAlertBox:@"Thông Báo" message:[error localizedDescription]];
        [self hideMBProcess];
        return;
    }];
}
// UPDATE TICKET
- (void)updateTicketSupport {
    
    if(![SiUtils checkNetworkAvailable]){
        [self showAlertBox:@"Thông báo" message:Mesage_Network];
        return;
    }
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:[self setupDataUpdateTicketSupport]];
    [self showMBProcess:@"Đang update Ticket..."];
    ShareData *shared = [ShareData instance];
    [shared.ticketSupportProxy updateTicketSupport:dict completeHandler:^(id result, NSString *errorCode, NSString *message){
        
        if ([errorCode isEqualToString:@"1"]) {
            [self showAlerViewWithTitle:@"Thông báo" withMessage:message andTag:Success];
            [self hideMBProcess];
            return ;
        }
        [self hideMBProcess];
        [self showAlerViewWithTitle:@"Lỗi" withMessage:message andTag:Failed];
        
    } errorHandler:^(NSError *error){
        
        [self showAlertBox:@"Thông Báo" message:[error localizedDescription]];
        [self hideMBProcess];
        return;
    }];

}

- (IBAction)buttonPressed:(id)sender {
    buttonPressed = sender;
    NSInteger index = 0;
    
    if (sender == self.locationButton && locationArray.count > 0) {
        // event when location button pressed ...
        [self showPopupViewWithTitle:@"VÙNG MIỀN" andData:locationArray];
        return;
    }
    
    if (sender == self.branchButton && branchLocationArray.count > 0) {
        // event when branch button pressed ...
        [self showPopupViewWithTitle:@"CHI NHÁNH ẢNH HƯỞNG" andData:branchLocationArray];
        return;
    }
    
    if (sender == self.serviceTypeButton && serviceTypeArray.count > 0) {
        // event when service type button pressed ...
        index = [self getIndexRow:serviceTypeArray andDataSelected:serviceTypeSelected];
        [self showDropDownViewAtPosition:sender withIndexRow:index andData:[self getDataForDropDownView:serviceTypeArray]];
        
        return;
    }
    
    if (sender == self.customerTypeButton && customerTypeArray.count > 0) {
        // event when customer type button pressed ...
        index = [self getIndexRow:customerTypeArray andDataSelected:customerTypeSelected];
        
        [self showDropDownViewAtPosition:sender withIndexRow:index andData:[self getDataForDropDownView:customerTypeArray]];
        
        return;
        
    }
    
    if (sender == self.descriptionButton && inputArray.count > 0) {
        // event when description button pressed ...
        index = [self getIndexRow:inputArray andDataSelected:inputSelected];
        
        [self showDropDownViewAtPosition:sender withIndexRow:index andData:[self getDataForDropDownView:inputArray]];
        
        return;
    }
    
    if (sender == self.enterContractButton) {
        // event when enter contract button pressed ...
        [self showContractViewWithData:self.contractArray];
        return;
    }
    
    if (sender == self.reasonButton && reasonArray.count >0) {
        // event when reason button pressed ...
        index = [self getIndexRow:reasonArray andDataSelected:reasonSelected];
        [self showDropDownViewAtPosition:sender withIndexRow:index andData:[self getDataForDropDownView:reasonArray]];
        
        return;
    }
    if (sender == self.notifyStaffButton) {
        // event when notify staff button pressed ...
        
        return;
    }
    if (sender == self.departmentNotifyButton) {
        // event when department notify button pressed ...
        
        return;
    }
    if (sender == self.troubleStatusButton && ticketStatusArray.count > 0) {
        // event when trouble status button pressed ...
        
        index = [self getIndexRow:ticketStatusArray andDataSelected:ticketStatusSelected];
        [self showDropDownViewAtPosition:sender withIndexRow:index andData:[self getDataForDropDownView:ticketStatusArray]];
        

        
        return;
    }
    //vutt11
    if (sender == self.queueButton && queueArray.count > 0) {
        // event when queue button pressed ...
        index = [self getIndexRow:queueArray andDataSelected:queueSelected];
        [self showDropDownViewAtPosition:sender withIndexRow:index andData:[self getDataForDropDownView:queueArray]];
        [self.operateStaffButton setEnabled:YES];
        [self.supervisoryStaffButton setEnabled:YES];
        [self.processStaffButton setEnabled:YES];
        return;
    }
    
    if (sender == self.operateStaffButton && operateStaffArray.count > 0) {
        // event when operate staff button pressed ...
        index = [self getIndexRow:operateStaffArray andDataSelected:operateStaffSelected];
        [self showDropDownViewAtPosition:sender withIndexRow:index andData:[self getDataForDropDownView:operateStaffArray]];
        return;
    }
    
    if (sender == self.supervisoryStaffButton && visorStaffArray.count > 0) {
        // event when supervisory staff button pressed ...
        index = [self getIndexRow:visorStaffArray andDataSelected:visorStaffSelected];
        [self showDropDownViewAtPosition:sender withIndexRow:index andData:[self getDataForDropDownView:visorStaffArray]];
        return;
    }
    
    if (sender == self.processStaffButton && processStaffArray.count > 0) {
        // event when process staff button pressed ...
        // IF User has assign is 1 and parent queue equal queue selected, enable selected process staff
        
        index = [self getIndexRow:processStaffArray andDataSelected:processStaffSelected];
        [self showDropDownViewAtPosition:sender withIndexRow:index andData:[self getDataForDropDownView:processStaffArray]];
        
//        if ([shareData.currentUser.assign isEqualToString:@"1"] && [queueSelected.Key isEqualToString:shareData.currentUser.parentQueueId]) {
//            index = [self getIndexRow:processStaffArray andDataSelected:processStaffSelected];
//            [self showDropDownViewAtPosition:sender withIndexRow:index andData:[self getDataForDropDownView:processStaffArray]];
//        }
        
        return;
    }
    
    if (sender == self.updateButton) {
        // event when update button pressed ...
        NSString *message = [self checkUpdateInfo];
        if (message.length > 0) {
            [self showAlertBox:@"Thông báo" message:message];
            
        } else {
            [self showActionSheetWithTitle:@"Bạn chắc chắn muốn tạo Ticket này?" andTag:1];
        }
        return;
    }
    
    if (sender == self.feedbackButton) {
        ListFeedbackViewController *vc = [[ListFeedbackViewController alloc]initWithNibName:@"ListFeedbackViewController" bundle:nil];
        vc.ticketId  = self.ticketID;
        [self.navigationController pushViewController:vc animated:YES];
        return;
    }
    
    if (sender == self.btnUpdateTicket) {
        [self updateTicketSupport];
    }
    
    
}

#pragma mark - Checking info before create Ticket
// Settup data for API Create Ticket Support
- (NSMutableDictionary *)settupDataCreateTicketSupport {
    
    contractString = [self settupContractStringWithData:self.contractArray];
    
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:@"2" forKey:@"Kind"];
    
    [dict setObject:@"-1" forKey:@"TicketStatus"];
    [dict setObject:@"1" forKey:@"CricLev"];
    [dict setObject:@"Email" forKey:@"RemindStatus"];
    [dict setObject:@"0" forKey:@"HasWarning"];
    [dict setObject:@"0" forKey:@"ParentID"];
    [dict setObject:@"" forKey:@"ProcessStep"];
    [dict setObject:self.requestTextView.text forKey:@"Title"];
    [dict setObject:self.noteTextView.text forKey:@"Description"];
    [dict setObject:self.priorityLabel.text forKey:@"Priority"];
    [dict setObject:HTEstimatedTimeValue ?:@"0" forKey:@"EstimatedTime"];
    
    [dict setObject:shareData.currentUser.userId forKey:@"FoundStaff"];
    [dict setObject:shareData.currentUser.userName forKey:@"FoundName"];
    [dict setObject:shareData.currentUser.divisionid forKey:@"FoundDivision"];
    [dict setObject:shareData.currentUser.divisionname forKey:@"FoundDivName"];
    [dict setObject:shareData.currentUser.phone forKey:@"FoundPhone"];
    [dict setObject:shareData.currentUser.ipPhone forKey:@"FoundIP"];
    
    [dict setObject:operateStaffSelected.ID ?:@"0" forKey:@"OperateStaff"];
    [dict setObject:visorStaffSelected.ID ?:@"0" forKey:@"VisorStaff"];
    
    [dict setObject:processStaffSelected.ID ?:@"0" forKey:@"ProcessStaff"];
    [dict setObject:processStaffSelected.name ?:@"" forKey:@"ProcessName"];
    [dict setObject:processStaffSelected.cellPhone ?:@"" forKey:@"ProcessPhone"];
    [dict setObject:processStaffSelected.ipPhone ?:@"" forKey:@"ProcessIP"];
    
    [dict setObject:queueSelected.Key ?:@"0" forKey:@"Queue"];
    
    [dict setObject:idLocationString forKey:@"LocationID"];
    [dict setObject:nameLocationString forKey:@"LocationName"];
    
    [dict setObject:idBranchString forKey:@"EffBranch"];
    [dict setObject:nameBranchString forKey:@"BranchName"];
    
    [dict setObject:serviceTypeSelected.Key ?:@"0" forKey:@"ServiceType"];
    [dict setObject:serviceTypeSelected.Values ?:@"" forKey:@"ServiceName"];
    
    [dict setObject:customerTypeSelected.Key ?:@"0" forKey:@"CusType"];
    [dict setObject:customerTypeSelected.Values ?:@"" forKey:@"CusName"];
    
    [dict setObject:inputSelected.Key ?:@"0" forKey:@"IssueID"];
    [dict setObject:inputSelected.Values ?:@"" forKey:@"IssueName"];
    
    [dict setObject:reasonSelected.Key ?:@"0" forKey:@"ReasonGroup"];
    
    [dict setObject:contractString forKey:@"Options"];
    
    return dict;
    
}

- (NSMutableString *)settupContractStringWithData:(NSMutableArray *)data {
    NSMutableString *returnString = [[NSMutableString alloc] init];
    for (ContractModel *item in data) {
        if (returnString.length <= 0) {
            [returnString appendString:StringFormat(@"%@|%@|%@|%@", item.contract, item.domain, item.address, item.name)];
        } else {
            [returnString appendString:StringFormat(@";%@|%@|%@|%@",item.contract, item.domain, item.address, item.name)];
        }
    }
    return returnString;
    
}

//vutt11
#pragma mark - Checking info before update Ticket
- (NSMutableDictionary *)setupDataUpdateTicketSupport {
    
   contractString = [self settupContractStringWithData:self.contractArray];
    
    NSMutableDictionary *dict = [NSMutableDictionary dictionary] ;
    
    [dict setObject:self.ticketID forKey:@"TicketID"];
    [dict setObject:ticketStatusSelected.Key forKey:@"TicketStatus"];
    [dict setObject:self.requestTextView.text forKey:@"Title"];
    [dict setObject:self.noteTextView.text forKey:@"Description"];
    [dict setObject:self.priorityLabel.text forKey:@"Priority"];
    [dict setObject:rc.CricLev forKey:@"CricLev"];
    [dict setObject:self.finishTime forKey:@"EstimatedTime"];
    [dict setObject:operateStaffSelected.ID?:@"0" forKey:@"OperateStaff"];
    [dict setObject:visorStaffSelected.ID?:@"0" forKey:@"VisorStaff"];
    [dict setObject:processStaffSelected.ID?:@"0" forKey:@"ProcessStaff"];
   [dict setObject:processStaffSelected.cellPhone ?:@"" forKey:@"ProcessPhone"];
    [dict setObject:processStaffSelected.ipPhone ?:@"" forKey:@"ProcessIP"];
    [dict setObject:queueSelected.Key?:@"0" forKey:@"Queue"];
    [dict setObject:shareData.currentUser.userId forKey:@"UpdateBy"];
   
    
    [dict setObject:serviceTypeSelected.Key forKey:@"ServiceType"];
    [dict setObject:customerTypeSelected.Key ?:@"0" forKey:@"CusType"];
    [dict setObject:inputSelected.Key ?:@"0" forKey:@"IssueID"];
    [dict setObject:reasonSelected.Key ?:@"0" forKey:@"ReasonGroup"];
    [dict setObject:shareData.currentUser.userEmail forKey:@"UpdatedEmail"];
     [dict setObject:inputSelected.Values ?:@"" forKey:@"IssueName"];
    
    
    [dict setObject:serviceTypeSelected.Values ?:@"" forKey:@"ServiceName"];
    [dict setObject:customerTypeSelected.Values ?:@"" forKey:@"CusName"];
    [dict setObject:reasonSelected.Values forKey:@"ReasonName"];
    [dict setObject:contractString forKey:@"Options"];
    
    NSString *keyCode;
    
  
    
    if (checkLocation == YES) {
        shareData.locationName = nameLocationString;
        shareData.locationID = idLocationString;
        
        [dict setObject:nameLocationString forKey:@"LocationName"];
        [dict setObject:idLocationString forKey:@"LocationID"];
        
    } else {
        
        [dict setObject:shareData.locationName forKey:@"LocationName"];
        [dict setObject:shareData.locationID forKey:@"LocationID"];
    }
    
    if (checkBranch == YES) {
        shareData.branchName = nameBranchString;
        shareData.branchID   = idBranchString;
        [dict setObject:nameBranchString forKey:@"BranchName"];
        [dict setObject:idBranchString forKey:@"EffBranch"];
        
    } else {
        
        [dict setObject:shareData.branchName forKey:@"BranchName"];
        [dict setObject: shareData.branchID forKey:@"EffBranch"];
        
    }
    if (checkLocation == YES) {
        
       keyCode  = [NSString stringWithFormat:@"%@%@%@%@%@",KeyApi,idLocationString,idBranchString,inputSelected.Key,queueSelected.Key];
        
    } else {
        
      keyCode  = [NSString stringWithFormat:@"%@%@%@%@%@",KeyApi,shareData.locationID,shareData.branchID,inputSelected.Key,queueSelected.Key];
        
    }
    
    keyCode = [self base64forData:[self md5WithString:keyCode]];
    keyCode = [keyCode lowercaseString];
    [dict setObject:keyCode?:@"" forKey:@"KeyCode"];
    
    return dict;
    
}


// Checking Info before create ticket
- (NSString *)checkUpdateInfo {
    // Kiểm tra vùng miền
    if (idLocationString.length <= 0) {
        return @"Vui lòng chọn vùng miền!";
    }
    // Kiểm tra chi nhánh
    if (idBranchString.length <= 0) {
        return @"Vui lòng chọn chi nhánh!";
    }
    // Kiểm tra nhập yêu cầu
    if (self.requestTextView.text.length <= 0) {
        return @"Vui lòng nhập yêu cầu!";
        self.requestTextView.delegate = self;
        [self.requestTextView becomeFirstResponder];
    }
    // Kiểm tra loại dịch vụ
    if (serviceTypeSelected == nil) {
        return @"Vui lòng chọn loại dịch vụ!";
    }
    // Kiểm tra loại khách hàng
    if (customerTypeSelected == nil) {
        return @"Vui lòng chọn loại khách hàng!";
    }
    // Kiểm tra mô tả
    if (inputSelected == nil) {
        return @"Vui lòng chọn mô tả!";
    }
    // Kiểm tra queue
    if (queueSelected == nil) {
        return @"Vui lòng chọn queue!";
    }
    // Kiểm tra nhân viên chủ trì
    if (operateStaffSelected == nil) {
        return @"Vui lòng chọn nhân viên chủ trì!";
    }
    // Kiểm tra nhân viên giám sát
    if (visorStaffSelected == nil) {
        return @"Vui lòng chọn nhân viên giám sát!";
    }
    // Kiểm tra nhân viên xử lý
    // IF User has assign is 1 and parent queue equal queue selected, enable selected process staff
    
//    if ([shareData.currentUser.assign isEqualToString:@"1"] && [queueSelected.Key isEqualToString:shareData.currentUser.parentQueueId] && processStaffSelected == nil) {
//        return @"Vui lòng chọn nhân viên xử lý!";
//        
//    }
    
    
    return @"";
}

// Show action sheet
- (void)showActionSheetWithTitle:(NSString *)title andTag:(NSInteger)tag {
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:title delegate:self cancelButtonTitle:@"Không" destructiveButtonTitle:@"Có" otherButtonTitles:nil, nil];
    actionSheet.tag = tag;
    [actionSheet showInView:[UIApplication sharedApplication].keyWindow];
}

// Show Alert View
- (void)showAlerViewWithTitle:(NSString *)title withMessage:(NSString *)message andTag:(AlertViewTag)tag {
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title message:message delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    alertView.tag = tag;
    [alertView show];
    
}

// When Create Ticket Support Success
- (void)pushToTicketSupportMyCreateListView {
    [self.navigationController popViewControllerAnimated:NO];
    if (self.delegate) {
        [self.delegate pushToTicketSupportListView];
    }
}

#pragma mark - UIActionSheetDelegate methods
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (actionSheet.tag == 1 && buttonIndex == 0 ) {
        [self createTicketSupport];
        return;
    }
    
}

#pragma mark - UIAlertViewDelegate methods
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (alertView.tag == Success && buttonIndex == 0) {
        [self pushToTicketSupportMyCreateListView];
        return;
    }
}

#pragma mark - ContractViewDelegate methods
// Show Contract View
- (void)showContractViewWithData:(NSMutableArray *)data {
    ContractViewController *vc = [[ContractViewController alloc] init];
    vc.delegate = self;
    vc.contractArray = data;
    vc.ticketSupportViewType = self.viewType;
    [self presentViewController:vc animated:YES completion:nil];
}

- (void)cancelContractViewWithDataReturn:(NSMutableArray *)dataReturn {
    self.contractArray = dataReturn;
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Set Data when selected row in Popup View
// Show popup view with title and data
- (void)showPopupViewWithTitle:(NSString *)title andData:(NSMutableArray *)data {
    /// Init popup view controll
    popupCustomView = [[PopupCustomViewController alloc] init];
    popupCustomView.delegate = self;
    popupCustomView.viewTitle = title;
    popupCustomView.dataArray = data;
    popupCustomView.isSelectedAll = _isSelectedAll;
    
    [self presentPopupViewController:popupCustomView animationType:MJPopupViewAnimationSlideBottomTop];
}

- (void)getIDNameFromData:(NSArray *)data idString:(NSMutableString *)idString andNameString:(NSMutableString *)nameString {
    [idString setString:@""];
    [nameString setString:@""];
    
    for (SelectedRecord *record in data) {
        if ([record.Check isEqualToString:@"1"]) {
            if (idString.length <= 0) {
                [idString appendString:StringFormat(@"%@",record.Id)];
                [nameString appendString:StringFormat(@"%@",record.Name)];
                
            } else {
                [idString appendString:StringFormat(@", %@",record.Id)];
                [nameString appendString:StringFormat(@", %@",record.Name)];
                
            }
        }
    }
    if (nameString.length >0) {
        [buttonPressed setTitle:nameString forState:UIControlStateNormal];
        
    } else {
        
        [self setTitleDefultForButton:buttonPressed];
        
    }
    
    [buttonPressed setNeedsLayout];
    
}

- (void)setTitleDefultForButton:(UIButton *)sender {
    if (sender == self.locationButton) {
        [sender setTitle:@"[Chọn vùng miền]" forState:UIControlStateNormal];
        return;
        
    }
    
    if (sender == self.branchButton) {
        [sender setTitle:@"[Chọn chi nhánh]" forState:UIControlStateNormal];
        return;
    }
    
    if (sender == self.serviceTypeButton) {
        [sender setTitle:@"[Chọn dịch vụ]" forState:UIControlStateNormal];
        return;
    }
    
    if (sender == self.customerTypeButton) {
        [sender setTitle:@"[Chọn khách hàng]" forState:UIControlStateNormal];
        return;
    }
    
    if (sender == self.descriptionButton) {
        [sender setTitle:@"[Chọn mô tả]" forState:UIControlStateNormal];
        return;
    }
    
    if (sender == self.reasonButton) {
        [sender setTitle:@"[Chọn nguyên nhân]" forState:UIControlStateNormal];
        return;
    }
    
    if (sender == self.queueButton) {
        [sender setTitle:@"[Chọn queue]" forState:UIControlStateNormal];
        return;
    }
    
    if (sender == self.operateStaffButton || sender == self.supervisoryStaffButton || sender == self.processStaffButton) {
        [sender setTitle:@"[Chọn nhân viên]" forState:UIControlStateNormal];
        return;
    }
    if (sender == self.troubleStatusButton) {
        [sender setTitle:@"[Chọn tình trạng hỗ trợ]" forState:UIControlStateNormal];
        
    }
    
}

#pragma mark - PopupCustomViewDelegate methods

- (void)cancelPopupCustomViewWithDataReturn:(NSMutableArray *)dataArrayReturn isSelectedAll:(BOOL)isSelectedAll withType:(ButtonType)type {
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationSlideBottomBottom];
    _isSelectedAll = isSelectedAll;
    if (buttonPressed == self.locationButton) {
        locationArray = dataArrayReturn;
        checkLocation = YES;
        if (type == Cancel) {
            return;
        }
        
        [self getIDNameFromData:locationArray idString:idLocationString andNameString:nameLocationString];
        
        [self getBranchLocationDataWithLocationID:idLocationString];
        
    }
    if (buttonPressed == self.branchButton) {
        branchLocationArray = dataArrayReturn;
        checkBranch = YES;
        if (type == Cancel) {
            return;
        }
        
        if (isSelectedAll) {
            [idBranchString setString: @"0"];
            [nameBranchString setString:@"Tất cả chi nhánh"];
            [buttonPressed setTitle:nameBranchString forState:UIControlStateNormal];
            [buttonPressed setNeedsLayout];
            
        } else {
            [self getIDNameFromData:branchLocationArray idString:idBranchString andNameString:nameBranchString];
        }
        
        if (queueSelected != nil) {
            NSLog(@"--------- Reloading Staff data ---------");
            [self resetStaffDataWithQueueID:queueSelected.Key withLocationIDs:idLocationString andBranchIDs:idBranchString];
        }
        if (inputSelected != nil) {
            NSLog(@"--------- Reloading priority data ---------");
            [self getPriorityDataWithInputID:inputSelected.Key locationID:idLocationString branchID:idBranchString];
        }
    }
}

#pragma mark - Get data for show in DropDown View
- (NSMutableArray *)getDataForDropDownView:(NSArray *)dataInput {
    NSMutableArray *returnData = [NSMutableArray array];
    if (buttonPressed == self.operateStaffButton || buttonPressed == self.supervisoryStaffButton) {
        for (StaffModel *item in dataInput) {
            [returnData addObject:item.name];
        }
        return returnData;
    }
    
    if (buttonPressed == self.processStaffButton) {
        for (ProcessStaffModel *item in dataInput) {
            [returnData addObject:item.name];
        }
        return returnData;
    }
    
    for (KeyValueModel *item in dataInput) {
        [returnData addObject:item.Values];
    }
    
    return returnData;
}

- (NSInteger)getIndexRow:(NSArray *)dataInput andDataSelected:(id)dataSelected {
    if (dataSelected == nil) {
        return 0;
    }
    if (buttonPressed == self.operateStaffButton || buttonPressed == self.supervisoryStaffButton) {
        for (int i = 0; i <= dataInput.count; i++) {
            StaffModel *temp = dataInput[i];
            if (dataSelected == temp) {
                return i;
            }
        }
    }
    
    if (buttonPressed == self.processStaffButton) {
        for (int i = 0; i <= dataInput.count; i++) {
            ProcessStaffModel *temp = dataInput[i];
            if (dataSelected == temp) {
                return i;
            }
        }
    }
    
    for (int i = 0; i <= dataInput.count; i++) {
        KeyValueModel *temp = dataInput[i];
        if (dataSelected == temp) {
            return i;
        }
    }
    
    return 0;
}

#pragma mark - NIDropDown methods

// Show Dropdown view
- (void)showDropDownViewAtPosition:(UIButton *)sender withIndexRow:(NSInteger)index andData:(NSArray *)data {
    [self.view endEditing:YES];
    if (dropDownView != nil && buttonPressed != sender) {
        [dropDownView hideDropDown:buttonPressed];
        dropDownView = nil;
    }
    
    NSString *animationType = @"up";
    if (sender == self.serviceTypeButton || sender == self.queueButton || sender == self.operateStaffButton || sender == self.supervisoryStaffButton || sender == self.processStaffButton) {
        animationType = @"down";
    }
    if (sender == self.troubleStatusButton) {
        animationType = @"up";
        
    }
    if (dropDownView == nil) {
        
        CGFloat height = 140;
        CGFloat width = 125;
        
        if (data.count <= 4) {
            height = 40*data.count;
            
        }
        
        dropDownView = [[NIDropDown alloc] showDropDownAtButton:sender index:index height:&height width:&width data:data images:nil animation:animationType];
        dropDownView.delegate = self;
        return;
    }
    
    [dropDownView hideDropDown:sender];
    dropDownView = nil;
    
}

#pragma mark - NIDropDownDelegate methods
/*
 * Set nil for DropDown View when pressed row in it
 */
- (void)niDropDownDelegateMethod:(NIDropDown *)sender {
    
    dropDownView = nil;
}

/*
 * Get data when selected row in DropDown View
 */
- (void)dropDownListView:(NIDropDown *)dropDownListView didSelectIndexPath:(NSIndexPath *)indexPath atButton:(UIButton *)button {
    if (button == self.serviceTypeButton) {
        // Set title defult for buttons when selected service type again
        [self setTitleDefultForButton:self.customerTypeButton];
        [self setTitleDefultForButton:self.descriptionButton];
        [self setTitleDefultForButton:self.reasonButton];
       
        // Set empty data
        customerTypeSelected = inputSelected = reasonSelected = nil;
        [inputArray removeAllObjects];
        [reasonArray removeAllObjects];
        self.priorityLabel.text = @"0";
        
        serviceTypeSelected = serviceTypeArray[indexPath.row];
        
        [self getCustomerTypeDataWithServiceTypeID:serviceTypeSelected.Key andCustomerTypeID:@"0"];
        
        return;
    }
    
    if (button == self.customerTypeButton) {
        // Set title defult for buttons when selected service type again
        [self setTitleDefultForButton:self.descriptionButton];
        [self setTitleDefultForButton:self.reasonButton];
        // Set empty data
        inputSelected = reasonSelected = nil;
        [inputArray removeAllObjects];
        [reasonArray removeAllObjects];
        self.priorityLabel.text = @"0";
        
        customerTypeSelected = customerTypeArray[indexPath.row];
        
        [self getInputDataWithCustomerTypeID:customerTypeSelected.Key andInputID:@"0"];
        [self getReasonDataWithCustomerTypeID:customerTypeSelected.Key andReasonID:@"0"];
        
        return;
    }
    
    if (button == self.descriptionButton) {
        inputSelected = inputArray[indexPath.row];
        [self getHTEstimatedTime:@"1" inputID:inputSelected.Key];
       // [self getEstimateTime:rc.CricLev IssueID:inputSelected.Key];
        [self getPriorityDataWithInputID:inputSelected.Key locationID:idLocationString branchID:idBranchString];
        
        return;
        
    }

    if (button == self.reasonButton) {
        reasonSelected = reasonArray[indexPath.row];
        return;
    }
    //vutt11
    if (button == self.queueButton) {
        queueSelected = queueArray[indexPath.row];
         [self CheckProcessStaff];
        [self resetStaffDataWithQueueID:queueSelected.Key withLocationIDs:idLocationString andBranchIDs:idBranchString];
        
        if ([shareData.currentUser.assign isEqualToString:@"1"] && [queueSelected.Key isEqualToString:shareData.currentUser.parentQueueId]){
            [self.processStaffButton setEnabled:YES];
            [self.processStaffButton setTitle:@"[Chọn nhân viên]" forState:UIControlStateNormal];
            return;
        }
        

        else {
            [self.processStaffButton setTitle:@"" forState:UIControlStateNormal];
            [self.processStaffButton setEnabled:NO];
        }
        return;
    }
    
    if (button == self.operateStaffButton) {
        operateStaffSelected = operateStaffArray[0];
        operateStaffSelected = operateStaffArray[indexPath.row];
        return;
    }
    
    if (button == self.supervisoryStaffButton) {
        visorStaffSelected = visorStaffArray[indexPath.row];
        return;
    }
    
    if (button == self.processStaffButton) {
        
       
        processStaffSelected = processStaffArray[indexPath.row];
        [self LoadPhoneStaff:processStaffSelected.email];
//        self.processStaffPhone.text = processStaffSelected.cellPhone;
//        self.processStaffIPPhone.text = processStaffSelected.ipPhone;
        self.processStaffButton.enabled = TRUE;
        return;
    }
    
    // get typeStatus
    if (button == self.troubleStatusButton) {
        
        [self setTitleDefultForButton:self.troubleStatusButton];
        ticketStatusSelected = ticketStatusArray[indexPath.row];
      
        [self.troubleStatusButton setTitle:ticketStatusSelected.Values forState:UIControlStateNormal];
        
        [self CheckQueue];
        
        return;
        
    }
}

- (void)resetStaffDataWithQueueID:(NSString *)queueID withLocationIDs:(NSString *)locationIDs andBranchIDs:(NSString *)branchIDs {
    // Set title defult for buttons when selected service type again
    //vutt11
    [self setTitleDefultForButton:self.operateStaffButton];
    [self setTitleDefultForButton:self.supervisoryStaffButton];
    [self setTitleDefultForButton:self.processStaffButton];
    self.processStaffPhone.text = self.processStaffIPPhone.text = @"";
    // Set empty data
    operateStaffSelected = visorStaffSelected = nil;
    processStaffSelected = nil;
    [operateStaffArray removeAllObjects];
    [visorStaffArray removeAllObjects];
    [processStaffArray removeAllObjects];
    
    [self getOperateStaffWithQueueID:queueID withCricLev:@"1" withLocationID:locationIDs withBranchID:branchIDs andOperateStaffEmail:@"" ];
    
    [self getVisorStaffWithQueueID:queueID withLocationID:locationIDs withBranchID:branchIDs andVisorStaffEmail:@""];
    
    [self getProcessStaffWithQueueID:queueID withLocationID:locationIDs andBranchID:branchIDs];
}

#pragma mark - Convert date to string and string to date
- (NSString *)convertDateToString:(NSDate*)dateInput {
    @try {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"dd/MM/yyyy HH:mm:ss"];
        NSString *stringDate = [dateFormatter stringFromDate:dateInput];
        NSLog(@"result convertDateToString: %@",stringDate);
        return stringDate;
    }
    @catch (NSException *exception) {
        
    }
}

- (NSDate *)convertStringToDate:(NSString *)dateStringInput {
    @try {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"dd/MM/yyyy HH:mm:ss"];
        NSDate *date = [dateFormatter dateFromString:dateStringInput];
        NSLog(@"result convertStringToDate: %@",date.description);
        // date will always contain value in GMT +0:00
        NSLog(@"New NSDate (NSDate): %@", [dateFormatter stringFromDate:date]);
        
        // converts date into string
        NSLog(@"New NSDate (NSString): %@", [dateFormatter stringFromDate:date]);
        return date;
        
    }
    @catch (NSException *exception) {
        
    }
    
}

#pragma mark - MD5
-(NSData *) md5WithString:(NSString *)input {
    const char *cStr = [input UTF8String];
    unsigned char digest[16];
    CC_MD5( cStr, (CC_LONG)strlen(cStr), digest );
    
    return [[NSData alloc] initWithBytes:digest length:CC_MD5_DIGEST_LENGTH];
}
- (NSString*)base64forData:(NSData*)theData {
    
    const uint8_t* input = (const uint8_t*)[theData bytes];
    NSInteger length = [theData length];
    
    static char table[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
    
    NSMutableData* data = [NSMutableData dataWithLength:((length + 2) / 3) * 4];
    uint8_t* output = (uint8_t*)data.mutableBytes;
    
    NSInteger i;
    for (i=0; i < length; i += 3) {
        NSInteger value = 0;
        NSInteger j;
        for (j = i; j < (i + 3); j++) {
            value <<= 8;
            
            if (j < length) {
                value |= (0xFF & input[j]);
            }
        }
        
        NSInteger theIndex = (i / 3) * 4;
        output[theIndex + 0] =                    table[(value >> 18) & 0x3F];
        output[theIndex + 1] =                    table[(value >> 12) & 0x3F];
        output[theIndex + 2] = (i + 1) < length ? table[(value >> 6)  & 0x3F] : '=';
        output[theIndex + 3] = (i + 2) < length ? table[(value >> 0)  & 0x3F] : '=';
    }
    
    return [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
}

- (void)CheckQueue {
    
    ShareData *shared = [ShareData instance];
    self.processStaffButton.enabled = TRUE;
    self.queueButton.enabled = FALSE;
    KeyValueModel *model = [[KeyValueModel alloc]init];
   
    
    if ([ ticketStatusSelected.Key isEqualToString:@"-1"]) {
        self.queueButton.enabled = FALSE;
        self.processStaffButton.enabled = TRUE;
        if ([shared.currentUser.assign isEqualToString:@"1"]) {
            self.processStaffButton.enabled = TRUE;
            if ([shared.currentUser.parentQueueId isEqualToString:queueSelected.Key ]) {
                model.Key = shared.currentUser.userEmail;
                model.Values = shared.currentUser.userName;
                if (![processStaffSelected.name isEqualToString:@""]) {
                    
                    [self.processStaffButton setTitle:processStaffSelected.name forState:UIControlStateNormal];
                }
            }
        }else {
            self.processStaffButton.enabled = FALSE;
                model.Key = @"0";
                model.Values = @"";
        }
    }
    //Status = Improgress
    else if ([ticketStatusSelected.Key isEqualToString:@"0"]){
       
        self.queueButton.enabled = FALSE;
        self.processStaffButton.enabled = FALSE;
      
        [self LoadPhoneStaff:shared.currentUser.userEmail];
        if (processStaffSelected.name.length > 0) {
            
            [self.processStaffButton setTitle:processStaffSelected.name forState:UIControlStateNormal];
        }
        else {
            [self.processStaffButton setTitle:shareData.currentUser.userName forState:UIControlStateNormal];
        }
        
        if ([shared.currentUser.assign isEqualToString:@"1"]) {
            self.processStaffButton.enabled = FALSE;
            
            if ([shared.currentUser.parentQueueId isEqualToString:queueSelected.Key]) {
                self.processStaffButton.enabled = TRUE;
                model.Key = shared.currentUser.userEmail;
                model.Values = shared.currentUser.userName;
                
                if (![processStaffSelected.name isEqualToString:@""]) {
                    
                    [self.processStaffButton setTitle:processStaffSelected.name forState:UIControlStateNormal];
                }
                else {
                    [self.processStaffButton setTitle:shareData.currentUser.userName forState:UIControlStateNormal];
                }
            }
        }
        
    }
    // Status = Forward
    else if ([ticketStatusSelected.Key isEqualToString:@"3"])
    {
         self.queueButton.enabled = FALSE;
        self.processStaffButton.enabled = TRUE;
        
            if ( [shareData.currentUser.assign isEqualToString:@"1"] && [queueSelected.Key isEqualToString:shareData.currentUser.parentQueueId])
            {
        
                self.processStaffButton.enabled = TRUE;
        
            }
    }
    // Status = Changed
    else if ([ticketStatusSelected.Key isEqualToString:@"2"])
    {
        self.queueButton.enabled = TRUE;
        self.queueButton.enabled = TRUE;
    }
}

- (void)CheckProcessStaff {
    ShareData *shared = [ShareData instance];
    KeyValueModel *model = [[KeyValueModel alloc]init];
    //[self.processStaffButton setTitle:shared.currentUser.userName forState:UIControlStateNormal];
    //If user have role assign
    if ([shared.currentUser.assign isEqualToString:@"1"]) {
        self.processStaffButton.enabled = TRUE;
        
        if ([shared.currentUser.parentQueueId isEqualToString:queueSelected.Key]) {
            model.Key = shared.currentUser.userEmail;
            model.Values = shared.currentUser.userName;
           
            if (![processStaffSelected.name isEqualToString:@""]) {
                
                [self.processStaffButton setTitle:processStaffSelected.name forState:UIControlStateNormal];
            }
        
    }
        else {
            
            model.Key = @"";
            model.Values = @"";
            self.processStaffPhone.text = @"";
            self.processStaffIPPhone.text = @"";
            [self.processStaffButton setTitle:shared.currentUser.userName forState:UIControlStateNormal];
            
        }
        
    }
    
}

- (void)LoadPhoneStaff:(NSString*)staffemail {
    
    if(![SiUtils checkNetworkAvailable]){
        [self showAlertBox:@"Thông báo" message:Mesage_Network];
        [self hideMBProcess];
        return;
    }
     ShareData *shared = [ShareData instance];
    [shared.ticketProxy getPhoneStaff:staffemail completeHandler:^(id result, NSString *errorCode, NSString *message){
        
        if (result == nil) {
            return ;
        }
        if ([errorCode isEqualToString:@"1"]) {
            self.processStaffPhone.text = [result objectForKey:@"Phone"];
            self.processStaffIPPhone.text = [result objectForKey:@"IPPhone"];
        } else {
            
            self.processStaffIPPhone.text = @"";
            self.processStaffPhone.text = @"";
        }
    
    } errorHandler:^(NSError *error){
        self.processStaffIPPhone.text = @"";
        self.processStaffPhone.text = @"";
        [self showAlertBox:@"Thông Báo" message:[error localizedDescription]];
        NSLog(@"%@",[NSString stringWithFormat:@"%@",error]);
        [self hideMBProcess];
        return;
        
    }];

    
}


@end
