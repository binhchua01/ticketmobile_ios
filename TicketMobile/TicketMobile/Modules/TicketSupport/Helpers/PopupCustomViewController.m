//
//  PopupCustomViewController.m
//  TicketMobile
//
//  Created by ISC-DanTT on 7/15/16.
//  Copyright (c) 2016 FPT. All rights reserved.
//

#import "PopupCustomViewController.h"
#import "SelectedRecord.h"

@interface PopupCustomViewController ()<UITableViewDataSource, UITableViewDelegate>

@end

@implementation PopupCustomViewController
{
    NSMutableArray *tempDataArray;
}

@synthesize dataArray, viewTitle;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    // Set title for view
    self.viewTitleLabel.text = self.viewTitle;
    tempDataArray = [NSMutableArray arrayWithArray:dataArray];
    
    if (self.dataArray.count <= 0) {
        [self.listTableView setHidden:YES];
        [self.selectedAllButton setHidden:YES];
        
    }
    
    if ([self.viewTitle isEqualToString:@"VÙNG MIỀN"]) {
        [self.selectedAllButton setHidden:YES];
        //vutt11
        // self.selectedAllButton.selected = self.isSelectedAll = [self selectedAll];
        
    } else {
        self.selectedAllButton.selected = self.isSelectedAll = [self selectedAll];
    }

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)selectedAll {
    for (SelectedRecord *rc in self.dataArray) {
        if ([rc.Check isEqual:@"0"]) {
            return NO;
        }
    }
    return YES;
}

#pragma mark - UITableViewDataSource Methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return dataArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    SelectedRecord *record = dataArray[indexPath.row];
    
    CGSize constraint = CGSizeMake(210, 20000.0f);
    
    // constratins the size of the table row according to the text
    CGRect textRect = [record.Name boundingRectWithSize:constraint options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading) attributes:@{NSFontAttributeName:[UIFont fontWithName:@"Helvetica-Light" size:15]} context:nil];
    
    CGFloat height = MAX(textRect.size.height,39);
    
    return height;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    // If the indexPath was found among the selected ones, set the checkmark on the cell
    SelectedRecord *record = dataArray[indexPath.row];
    if ([record.Check isEqualToString:@"0"]) {
        cell.accessoryType = UITableViewCellAccessoryNone;
        
    } else {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
        
    }
    // Set title for cell
    cell.textLabel.font = [UIFont fontWithName:@"Helvetica-Light" size:15];
    cell.textLabel.numberOfLines = 0;
    [cell.textLabel sizeToFit];
    cell.textLabel.text = record.Name;
    
    return cell;
    
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.listTableView deselectRowAtIndexPath:indexPath animated:YES];
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];

    SelectedRecord *record = dataArray[indexPath.row];
    SelectedRecord *tempRecord = [[SelectedRecord alloc] initWithName:record.Name withID:record.Id andCheck:record.Check];
    
    if ([tempRecord.Check isEqualToString:@"0"]) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
        tempRecord.Check = @"1";
        
    } else {
        cell.accessoryType = UITableViewCellAccessoryNone;
        tempRecord.Check = @"0";
        if (self.isSelectedAll) {
            self.selectedAllButton.selected = NO;
            self.isSelectedAll = NO;
        }
    }
    
    [dataArray replaceObjectAtIndex:indexPath.row withObject:tempRecord];
}

- (IBAction)selectedAllButtonPressed:(id)sender {
    
    self.isSelectedAll = !self.isSelectedAll;
    self.selectedAllButton.selected = !self.selectedAllButton.selected;
    
    // If isSelectedAll is YES, set check all record else not check.
    for (int i=0; i < dataArray.count; i++) {
        SelectedRecord *record = dataArray[i];
        SelectedRecord *tempRecord = [[SelectedRecord alloc] initWithName:record.Name withID:record.Id andCheck:record.Check];

        if ([tempRecord.Check isEqualToString:@"0"] && self.isSelectedAll) {
            tempRecord.Check = @"1";
            
        } else if ([tempRecord.Check isEqualToString:@"1"] && !self.isSelectedAll) {
            tempRecord.Check = @"0";
        }
        
        [dataArray replaceObjectAtIndex:i withObject:tempRecord];

    }
    
    [self.listTableView reloadData];

}

- (IBAction)cancelButtonPressed:(id)sender {
    if (self.delegate) {
        [self.delegate cancelPopupCustomViewWithDataReturn:tempDataArray isSelectedAll:self.isSelectedAll withType:Cancel];

    }
}

- (IBAction)chooseButtonPressed:(id)sender {
    if (self.delegate) {
        [self.delegate cancelPopupCustomViewWithDataReturn:dataArray isSelectedAll:self.isSelectedAll withType:Done];
    }
}

@end
