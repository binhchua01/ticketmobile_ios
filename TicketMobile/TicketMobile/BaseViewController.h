//
//  BaseViewController.h
//  FTool
//
//  Created by Pham Duy Khang on 11/6/14.
//  Copyright (c) 2014 FPT.RAD.FTool. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MFSideMenu.h"
#import "SiUtils.h"
#import "Helper/MBProgress/MBProgressHUD.h"
#import "UIViewController+MJPopupViewController.h"
#import "MFSideMenuContainerViewController.h"
#import "MFSideMenu.h"
#import "AppInfoViewController.h"
#import "ProfileViewController.h"
#import "../GoogleAnalytics/GAITrackedViewController.h"

//extern BOOL *checkChanged;

@interface BaseViewController : GAITrackedViewController<AppinfoDelegate,ProfileDelegate>

@property (nonatomic,retain) IBOutlet UILabel *lblUserName;

@property (nonatomic,retain) NSString *regcode;


/*!CreatetableView*/
//- (void)createTableView:(CGRect)rect;
/*!show/hiden*/
//- (void)hiddenTableObjectName:(BOOL)_bool;
/* return special type*/

- (UIBarButtonItem *)rightMenuBarButtonItem;
- (BOOL) returnSpecialType:(NSString *)type;

- (void)showAlertBox:(NSString *)title
             message:(NSString *)message;
- (void) showMBProcess:(NSString *)title;
- (void) hideMBProcess;
-(NSString *)getIPAddress;

@end
