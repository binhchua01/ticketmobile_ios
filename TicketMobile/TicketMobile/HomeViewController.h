//
//  HomeViewController.h
//
//
//  Created by THONT5 on 2/25/15.
//  Copyright (c) 2015 nguyentantho@live.com. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "Helper/MFSideMenu/MFSideMenuContainerViewController.h"
#import "Helper/MFSideMenu/MFSideMenu.h"
#import "../GoogleAnalytics/GAITrackedViewController.h"

int CountNumber;
@interface HomeViewController : GAITrackedViewController{
    NSTimer *Timer;
}
-(void)TimerCount;


@property (strong,nonatomic) IBOutlet UILabel *lblCoppy;

@property (strong, nonatomic) IBOutlet UILabel *lblVersion;

@end
