//
//  UIButton+FPTCustom.m
//  MobiPayCam
//
//  Created by FPT on 9/10/15.
//  Copyright (c) 2015 FPT. All rights reserved.
//

#import "UIButton+FPTCustom.h"
#import "UIColor+FPTCustom.h"

@implementation UIButton (FPTCustom)

- (void)styleButtonUpdate
{
    [self.layer setCornerRadius:6.0f];
    [self.layer setBorderColor:[UIColor colorMain].CGColor];
    [self.layer setBorderWidth:0.6f];
}

@end
