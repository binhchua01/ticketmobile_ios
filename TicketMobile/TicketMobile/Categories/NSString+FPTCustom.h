//
//  NSString+FPTCustom.h
//  TicketMobile
//
//  Created by ISC on 7/29/16.
//  Copyright © 2016 FPT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CommonCrypto/CommonDigest.h>

@interface NSString (FPTCustom)

- (NSString *)MD5;

@end
