//
//  UIViewController+FPTCustom.m
//  MobiPayCam
//
//  Created by FPT on 8/25/15.
//  Copyright (c) 2015 FPT. All rights reserved.
//

#import "UIViewController+FPTCustom.h"
#import "UIFont+FPTCustom.h"
#import "MBProgressHUD.h"

@implementation UIViewController (FPTCustom)

- (void)showHUDWithMessage:(NSString *)message
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = message;
    hud.labelFont = [UIFont fontWithName:mainFontName size:15];
}

- (void)hideHUD
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}

- (void)removeAllNavigationBarButtons
{
    self.title = @"";
    for (UIView *view in self.navigationController.navigationBar.subviews) {
        if ([view isKindOfClass:[UIButton class]]) {
            [view removeFromSuperview];
        }
    }
}

@end
