//
//  UIViewController+FPTCustom.h
//  MobiPayCam
//
//  Created by FPT on 8/25/15.
//  Copyright (c) 2015 FPT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (FPTCustom)

- (void)showHUDWithMessage:(NSString *)message;
- (void)hideHUD;
- (void)removeAllNavigationBarButtons;

@end
