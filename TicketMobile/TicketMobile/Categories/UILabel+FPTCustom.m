//
//  UILabel+FPTCustom.m
//  MobiPay
//
//  Created by ISC-HieuDT20 on 4/6/16.
//  Copyright © 2016 Hieu Do. All rights reserved.
//

#import "UILabel+FPTCustom.h"

@implementation UILabel (FPTCustom)

- (NSInteger)lineCount
{
    // Calculate height text according to font
    NSInteger lineCount = 0;
    CGSize labelSize = (CGSize){self.frame.size.width, FLT_MAX};
    CGRect requiredSize = [self.text boundingRectWithSize:labelSize  options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: self.font} context:nil];
    
    // Calculate number of lines
    int charSize = self.font.leading;
    int rHeight = requiredSize.size.height;
    lineCount = rHeight/charSize;
    
    return lineCount;
}


@end
