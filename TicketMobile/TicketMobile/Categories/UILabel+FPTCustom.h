//
//  UILabel+FPTCustom.h
//  MobiPay
//
//  Created by ISC-HieuDT20 on 4/6/16.
//  Copyright © 2016 Hieu Do. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (FPTCustom)

- (NSInteger)lineCount;

@end
