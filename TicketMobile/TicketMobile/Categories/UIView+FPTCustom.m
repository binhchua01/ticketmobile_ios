//
//  UIView+FPTCustom.m
//  MobiPayCam
//
//  Created by FPT on 9/1/15.
//  Copyright (c) 2015 FPT. All rights reserved.
//

#import "UIView+FPTCustom.h"
#import "UIColor+FPTCustom.h"

@implementation UIView (FPTCustom)

- (void)styleBorderViewWithCGFloat:(CGFloat )CGFloat
{
    self.layer.borderColor = [UIColor colorMain].CGColor;
    self.layer.borderWidth = CGFloat;
}

- (void)styleBorder
{
    [self.layer setCornerRadius:3.0f];
}

- (void)styleBorderMap
{
    [self.layer setCornerRadius:3.0f];
    [self.layer setBorderColor:[UIColor colorMain].CGColor];
    [self.layer setBorderWidth:1.0f];
}

@end
