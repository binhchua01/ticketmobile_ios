//
//  BaseViewController.m
//  FTool
//
//  Created by Pham Duy Khang on 11/6/14.
//  Copyright (c) 2014 FPT.RAD.FTool. All rights reserved.
//

#import "BaseViewController.h"
#import <netinet/in.h>
#include <ifaddrs.h>
#include <arpa/inet.h>
#import <sys/socket.h>
#import "KxMenu.h"
#import "AppInfoViewController.h"
#import "ProfileViewController.h"
#import "../TicketMobile/Client/ShareData.h"
#import "TicketMobileAppDelegate.h"
#import "HDNotificationView.h"


//BOOL *checkChanged=FALSE;

@interface BaseViewController ()
{
    MBProgressHUD *HUD;
    TicketMobileAppDelegate *appDelegate;
}
@end

@implementation BaseViewController{
    bool statemenu;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    //self.title = @"TICKET SYSTEM";
    statemenu = YES;
    [self setupMenuBarButtonItems];
    [self.menuContainerViewController setRightMenuWidth:0];
    
    // Register received push notification
    appDelegate = (TicketMobileAppDelegate *)[[UIApplication sharedApplication] delegate];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receivedMessage:)
                                                 name:appDelegate.messageKey
                                               object:nil];
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    statemenu = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// Received message from push notification
- (void)receivedMessage:(NSNotification *)notification {
    [self.view endEditing:YES];
    @try {
        NSString *title = notification.userInfo[@"aps"][@"alert"][@"title"];
        NSString *message = notification.userInfo[@"aps"][@"alert"][@"body"];
        if (appDelegate.appActived) {
            NSLog(@"-------Showing notification...");
            NSString *imageName = @"NotificationIcon";
            UIImage *image = [UIImage imageNamed:imageName];
            [HDNotificationView showNotificationViewWithImage:image title:title message:message isAutoHide:NO onTouch:^{
                [HDNotificationView hideNotificationViewOnComplete:nil];
            }];
            // set badge for app icon when received notification
            UIApplication *application = [UIApplication sharedApplication];
            [application setApplicationIconBadgeNumber:application.applicationIconBadgeNumber - 1];
        }
    } @catch (NSException *exception) {
        NSLog(@"---- Error show notification: %@",exception.description);
        return;
    }
    
}

#pragma mark - UIBarButtonItems

- (void)setupMenuBarButtonItems {
    NSMutableArray *controllers = [self.navigationController.viewControllers mutableCopy];
    
    if(self.menuContainerViewController.menuState == MFSideMenuStateClosed &&
       ![controllers[0] isEqual:self]) {
        self.navigationItem.leftBarButtonItem = [self backBarButtonItem];
        
    } else {
        self.navigationItem.leftBarButtonItem = [self leftMenuBarButtonItem];
    }

//    self.navigationItem.rightBarButtonItem = [self rightMenuBarButtonItem];
    
    // add by DanTT
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:51.0/255.0 green:153.0/255.0 blue:255.0/255.0 alpha:1.0]];
}

- (UIBarButtonItem *)leftMenuBarButtonItem {
    UIImage *image = [SiUtils imageWithImageHeight:[UIImage imageNamed:@"ic_menu"] height:30];
    return [[UIBarButtonItem alloc]
            initWithImage:[image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStyleBordered
            target:self
            action:@selector(leftSideMenuButtonPressed:)];
}

- (UIBarButtonItem *)rightMenuBarButtonItem {

    UIImage *image = [SiUtils imageWithImageHeight:[[UIImage imageNamed:@"icon_more"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] height:30];
    return [[UIBarButtonItem alloc]
            initWithImage:[image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStyleBordered
            target:self
            action:@selector(rightSideMenuButtonPressed:)];
}

#pragma mark - UIBarButtonItem Callbacks

- (void)backButtonPressed:(id)sender {
        NSLog(@"---------------BACK MENU-------------------");
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)leftSideMenuButtonPressed:(id)sender {
    
    [self.menuContainerViewController toggleLeftSideMenuCompletion:^{
    }];
}

- (void)rightSideMenuButtonPressed:(id)sender {

    if(statemenu){
        statemenu = NO;
        [self showMenu];
    } else {
        [KxMenu dismissMenu];
        statemenu = YES;
    }

}

- (UIBarButtonItem *)backBarButtonItem {
    
    UIImage *image = [SiUtils imageWithImageHeight:[[UIImage imageNamed:@"icon_back"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] height:30];
    return [[UIBarButtonItem alloc]
            initWithImage:[image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStyleBordered
            target:self
            action:@selector(backButtonPressed:)];
}

#pragma mark - menu
- (void)showMenu {
    ShareData *shared = [ShareData instance];
    NSString *username = shared.currentUser.userEmail;
    NSArray *menuItems =
    @[
      
      [KxMenuItem menuItem:username
                     image:[SiUtils imageWithImageHeight:[UIImage imageNamed:@"ic_header_user"] height:30]
                    target:self
                    action:@selector(pushMenuUserInfo:)],
      
      [KxMenuItem menuItem:@"Thông tin"
                     image:[SiUtils imageWithImageHeight:[UIImage imageNamed:@"ic_menu_info"] height:30]
                    target:self
                    action:@selector(pushMenuInfo:)],
      
      [KxMenuItem menuItem:@"Thoát"
                     image:[SiUtils imageWithImageHeight:[UIImage imageNamed:@"ic_menu_logout"] height:30]
                    target:self
                    action:@selector(pushMenuLogout:)],
      ];
    
    KxMenuItem *first = menuItems[0];
    first.foreColor = [UIColor colorWithRed:47/255.0f green:112/255.0f blue:225/255.0f alpha:1.0];
//    [KxMenu setTintColor:[UIColor colorWithRed:1 green:1 blue:1 alpha:1]];
    float x = 250;
    if(IS_IPAD){
        x = 944;
    }
    [KxMenu showMenuInView:self.view
                  fromRect:CGRectMake(x, 15, 100, 50)
                 menuItems:menuItems];
}

- (void) pushMenuInfo:(id)sender {
    statemenu = YES;
    AppInfoViewController *vc = [[AppInfoViewController alloc]initWithNibName:@"AppInfoViewController" bundle:nil];
    vc.delegate = (id)self;
   [self presentPopupViewController:vc animationType:MJPopupViewAnimationSlideBottomBottom];
}

- (void) pushMenuUserInfo:(id)sender {
    statemenu = YES;
    ProfileViewController *vc = [[ProfileViewController alloc]initWithNibName:@"ProfileViewController" bundle:nil];
    vc.delegate = (id)self;
    [self presentPopupViewController:vc animationType:MJPopupViewAnimationSlideBottomBottom];
}

-(void)Cancel {
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationSlideBottomTop];
}

-(void)test {
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationSlideBottomTop];
}

- (void) pushMenuLogout:(id)sender {
    statemenu = YES;
    exit(0);
}

#pragma show alert
- (void)showAlertBox:(NSString *)title
             message:(NSString *)message {
    
    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:title message:message delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK",nil];
    [alertView show];
    
    
}

#pragma mark process bar
- (void) showMBProcess:(NSString *)title
{
    
    HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [self.view addSubview:HUD];
    
    HUD.delegate = (id<MBProgressHUDDelegate>)self;
    HUD.labelText = title;
    
	[HUD show:YES];
}

- (void) hideMBProcess
{
	[HUD hide:YES];
}


#pragma retun special type
- (BOOL) returnSpecialType:(NSString *)type {
    if([type rangeOfString:@"?"].location != NSNotFound)
        return 1;
    else if([type rangeOfString:@"˜"].location != NSNotFound)
        return 1;
    else if([type rangeOfString:@"`"].location != NSNotFound)
        return 1;
    else if([type rangeOfString:@"!"].location != NSNotFound)
        return 1;
    else if([type rangeOfString:@"#"].location != NSNotFound)
        return 1;
    else if([type rangeOfString:@"ˆ"].location != NSNotFound)
        return 1;
    else if([type rangeOfString:@"*"].location != NSNotFound)
        return 1;
    else if([type rangeOfString:@"("].location != NSNotFound)
        return 1;
    else if([type rangeOfString:@")"].location != NSNotFound)
        return 1;
    else if([type rangeOfString:@"+"].location != NSNotFound)
        return 1;
    else if([type rangeOfString:@"{"].location != NSNotFound)
        return 1;
    else if([type rangeOfString:@"}"].location != NSNotFound)
        return 1;
    else if([type rangeOfString:@"["].location != NSNotFound)
        return 1;
    else if([type rangeOfString:@"]"].location != NSNotFound)
        return 1;
    else if([type rangeOfString:@"|"].location != NSNotFound)
        return 1;
    else if([type rangeOfString:@"&"].location != NSNotFound)
        return 1;
    else if([type rangeOfString:@"$"].location != NSNotFound)
        return 1;
    else if([type rangeOfString:@"@"].location != NSNotFound)
        return 1;
    else if([type rangeOfString:@":"].location != NSNotFound)
        return 1;
    else if([type isEqualToString:@""])
        return 1;
    return 0;

}

#pragma getIPadress
-(NSString *)getIPAddress {
    struct ifaddrs *interfaces = NULL;
    struct ifaddrs *temp_addr = NULL;
    NSString *wifiAddress = nil;
    NSString *cellAddress = nil;
    NSString *addr;
    if(!getifaddrs(&interfaces)) {
        temp_addr = interfaces;
        while(temp_addr != NULL) {
            sa_family_t sa_type = temp_addr->ifa_addr->sa_family;
            if(sa_type == AF_INET || sa_type == AF_INET6) {
                NSString *name = [NSString stringWithUTF8String:temp_addr->ifa_name];
                addr = [NSString stringWithUTF8String:inet_ntoa(((struct sockaddr_in *)temp_addr->ifa_addr)->sin_addr)];
                if([name isEqualToString:@"en0"]) {
                    wifiAddress = addr;
                } else
                    if([name isEqualToString:@"pdp_ip0"]) {
                        cellAddress = addr;
                    }
            }
            temp_addr = temp_addr->ifa_next;
        }
        // Free memory
        freeifaddrs(interfaces);
    }
    return addr;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


@end
