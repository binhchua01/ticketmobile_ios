/*
     File: IconDownloader.h 
*/


@interface ImageDownloader : NSObject

@property (nonatomic, strong) NSString *urlImage;
@property (nonatomic, copy) void (^completionHandler)(UIImage *image);

- (void)startDownload;
- (void)cancelDownload;

@end
