//
//  HomeViewController.h
//
//
//  Created by THONT5 on 2/25/15.
//  Copyright (c) 2015 nguyentantho@live.com. All rights reserved.
//
#import <Foundation/Foundation.h>

@interface SiUtils : NSObject

#pragma mark - Network
+ (BOOL)checkNetworkAvailable;

#pragma mark - frame
+ (CGSize)getScreenFrameSize;

#pragma mark - Crypto
+ (NSString *)getSHA1:(NSString*)input;
+ (NSString*)getSHA512:(NSString*)input;
+ (NSString *)getSHA1WithData:(NSData*)data;
+ (NSData *)encryptAESString:(NSString*)plaintext withKey:(NSString*)key;
+ (NSString *)decryptAESData:(NSData*)ciphertext withKey:(NSString*)key;
+ (NSString *) DecodeBase64:(NSString *)input;

#pragma mark - Image
+ (UIImage *)imageWithImage:(UIImage *)image convertToSize:(CGSize)size;
+ (UIImage *)imageWithImage:(UIImage *)image convertToSize:(CGSize)size point:(CGPoint)xy;
+ (UIImage *)imageWithImageHeight:(UIImage *)image height:(float)height;
+ (UIImage *)imageByOverwriteColor:(UIColor *)color alpha:(float)alpha image:(UIImage*)image;

#pragma mark - Color
+ (UIColor *)colorFromHexString:(NSString *)hexString;

#pragma mark - UserDefaults
+ (void)saveUserDefaults:(NSDictionary *)dictionary;
+ (id)getUserDefaults:(NSString *)key;

#pragma mark - Handle string
+ (NSString *)CheckNULLString:(NSString*)input;
@end