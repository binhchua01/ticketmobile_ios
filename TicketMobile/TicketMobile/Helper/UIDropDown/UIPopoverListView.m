//
//  UIPopoverListView.m
//  UIPopoverListViewDemo
//
//  Created by su xinde on 13-3-13.
//  Copyright (c) 2013年 su xinde. All rights reserved.
//

#import "UIPopoverListView.h"
#import <QuartzCore/QuartzCore.h>
#import "TicketMobileAppDelegate.h"
//#define FRAME_X_INSET 20.0f
//#define FRAME_Y_INSET 40.0f

@interface UIPopoverListView ()

@property(nonatomic, strong) UITableView *table;
@property(nonatomic, strong) UIButton *btnSender;

//- (void)defalutInit;
- (void)fadeIn;
- (void)fadeOut;

@end

@implementation UIPopoverListView

@synthesize datasource = _datasource;
@synthesize delegate = _delegate;

@synthesize listView = _listView;

@synthesize table;
@synthesize btnSender;
@synthesize animationDirection;

/*Version mới không dùng đến các method này, nhưng k được xoá*/
/*
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self defalutInit];
    }
    return self;
}

- (void)defalutInit
{
    self.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    self.layer.borderWidth = 1.0f;
    self.layer.cornerRadius = 0.0f;
    self.clipsToBounds = TRUE;

    _titleView = [[UILabel alloc] initWithFrame:CGRectZero];
    _titleView.font = [UIFont systemFontOfSize:17.0f];
    //_titleView.backgroundColor = MAIN_COLOR_LIGHT;
    _titleView.backgroundColor = [UIColor colorWithRed:1 green:0.416 blue:0.094 alpha:1];

    _titleView.textAlignment = UITextAlignmentCenter;
    CGFloat xWidth = self.bounds.size.width;
    _titleView.lineBreakMode = UILineBreakModeTailTruncation;
    _titleView.frame = CGRectMake(0, 0, xWidth, 32.0f);
    //[self addSubview:_titleView];

    //CGRect tableFrame = CGRectMake(0, 32.0f, xWidth, self.bounds.size.height-32.0f);
    CGRect tableFrame = CGRectMake(0, 0, xWidth, 0);
    _listView = [[UITableView alloc] initWithFrame:tableFrame style:UITableViewStylePlain];
    _listView.dataSource = self;
    _listView.delegate = self;
    [self addSubview:_listView];

    _overlayView = [[UIControl alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    _overlayView.backgroundColor = [UIColor colorWithRed:.16 green:.17 blue:.21 alpha:.5];
    [_overlayView addTarget:self
                     action:@selector(dismiss)
           forControlEvents:UIControlEventTouchUpInside];

}
*/

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(self.datasource &&
       [self.datasource respondsToSelector:@selector(popoverListView:numberOfRowsInSection:)])
    {
        return [self.datasource popoverListView:self numberOfRowsInSection:section];
    }
    
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(self.datasource &&
       [self.datasource respondsToSelector:@selector(popoverListView:cellForIndexPath:)])
    {
        return [self.datasource popoverListView:self cellForIndexPath:indexPath];
    }
    return nil;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(self.delegate &&
       [self.delegate respondsToSelector:@selector(popoverListView:heightForRowAtIndexPath:)])
    {
        return [self.delegate popoverListView:self heightForRowAtIndexPath:indexPath];
    }
    
    return 0.0f;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(self.delegate &&
       [self.delegate respondsToSelector:@selector(popoverListView:didSelectIndexPath:)])
    {
        [self.delegate popoverListView:self didSelectIndexPath:indexPath];
    }
    [self hideDropDown:btnSender];
    
    //[self dismiss];
    [self myDelegate];
}

// add by DanTT
- (void) myDelegate {
    [self.delegate niDropDownDelegateMethod:self];
}

#pragma mark - animations

- (void)fadeIn
{
    self.transform = CGAffineTransformMakeScale(1.3, 1.3);
    self.alpha = 0;
    [UIView animateWithDuration:.35 animations:^{
        self.alpha = 1;
        self.transform = CGAffineTransformMakeScale(1, 1);
    }];
    
}
- (void)fadeOut
{
    [UIView animateWithDuration:.35 animations:^{
        self.transform = CGAffineTransformMakeScale(1.3, 1.3);
        self.alpha = 0.0;
    } completion:^(BOOL finished) {
        if (finished) {
            [_overlayView removeFromSuperview];
            [self removeFromSuperview];
        }
    }];
}

- (void)setTitle:(NSString *)title
{
    _titleView.text = title;
}

- (void)show
{
    UIWindow *keywindow = [[UIApplication sharedApplication] keyWindow];
    [keywindow addSubview:_overlayView];
    [keywindow addSubview:self];
    
    self.center = CGPointMake(keywindow.bounds.size.width/2.0f,
                             keywindow.bounds.size.height/2.0f);
    [self fadeIn];
}

- (void)dismiss
{
    [self fadeOut];
   // [self myDelegate];

}

#define mark - UITouch
- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    // tell the delegate the cancellation
    if (self.delegate && [self.delegate respondsToSelector:@selector(popoverListViewCancel:)]) {
        [self.delegate popoverListViewCancel:self];
    }
    
    // dismiss self
    [self dismiss];
}

/* add by DanTT, create drop down list, copy from lib: NIDropDown (https://codeload.github.com/BijeshNair/NIDropDown/zip/master)
*/

- (id)showDropDown:(UIButton *)b withHeight:(CGFloat *)height withType:(NSString *)direction{
    btnSender = b;
    animationDirection = direction;
    self.listView = (UITableView *)[super init];
    if (self) {
        // Initialization code
        CGRect btn = b.frame;
        if ([direction isEqualToString:@"up"]) {
            self.frame = CGRectMake(btn.origin.x, btn.origin.y, btn.size.width, 0);
            self.layer.shadowOffset = CGSizeMake(-5, -5);
        }else if ([direction isEqualToString:@"down"]) {
            self.frame = CGRectMake(btn.origin.x, btn.origin.y+btn.size.height, btn.size.width, 0);
            self.layer.shadowOffset = CGSizeMake(-5, 5);
        }

//        self.layer.masksToBounds = NO;
//        self.layer.cornerRadius = 8;
//        self.layer.shadowRadius = 5;
//        self.layer.shadowOpacity = 0.5;
        
        _listView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, btn.size.width, 0)];
        _listView.dataSource = self;
        _listView.delegate = self;
        _listView.layer.cornerRadius = 2;
        _listView.backgroundColor = [UIColor blackColor];
        _listView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        _listView.separatorColor = [UIColor grayColor];
        //[self addSubview:_listView];
        
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.5];
        if ([direction isEqualToString:@"up"]) {
            self.frame = CGRectMake(btn.origin.x, btn.origin.y-*height, btn.size.width, *height);
        } else if([direction isEqualToString:@"down"]) {
            self.frame = CGRectMake(btn.origin.x, btn.origin.y+btn.size.height, btn.size.width, *height);
        }
        _listView.frame = CGRectMake(0, 0, btn.size.width, *height);
        [UIView commitAnimations];
        
        [self addSubview:_listView];
        
        //UITapGestureRecognizer *gestureRecognizer;
        //gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismiss)];
        //[self addGestureRecognizer:gestureRecognizer];
        
        UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:_listView action:nil];
        [self.listView addGestureRecognizer:gestureRecognizer];
        gestureRecognizer.cancelsTouchesInView = NO;
        
        [b.superview addSubview:self];
        //[b.superview insertSubview:self atIndex:100 ];

    }
    return self;
}

// add by DanTT
-(void)hideDropDown:(UIButton *)b {
    CGRect btn = b.frame;
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.2];
    if ([animationDirection isEqualToString:@"up"]) {
        self.frame = CGRectMake(btn.origin.x, btn.origin.y, btn.size.width, 0);
    }else if ([animationDirection isEqualToString:@"down"]) {
        self.frame = CGRectMake(btn.origin.x, btn.origin.y+ btn.size.height, btn.size.width, 0);
    }
    _listView.frame = CGRectMake(0, 0, btn.size.width, 0);
    [UIView commitAnimations];
}


// draw round rect corner

/*
 - (void)drawRect:(CGRect)rect
 {
 CGContextRef c = UIGraphicsGetCurrentContext();
 CGContextSetFillColorWithColor(c, [_fillColor CGColor]);
 CGContextSetStrokeColorWithColor(c, [_borderColor CGColor]);
 
 CGContextBeginPath(c);
 addRoundedRectToPath(c, rect, 10.0f, 10.0f);
 CGContextFillPath(c);
 
 CGContextSetLineWidth(c, 1.0f);
 CGContextBeginPath(c);
 addRoundedRectToPath(c, rect, 10.0f, 10.0f);
 CGContextStrokePath(c);
 }
 
 
 static void addRoundedRectToPath(CGContextRef context, CGRect rect,
 float ovalWidth,float ovalHeight)
 
 {
 float fw, fh;
 
 if (ovalWidth == 0 || ovalHeight == 0) {// 1
 CGContextAddRect(context, rect);
 return;
 }
 
 CGContextSaveGState(context);// 2
 
 CGContextTranslateCTM (context, CGRectGetMinX(rect),// 3
 CGRectGetMinY(rect));
 CGContextScaleCTM (context, ovalWidth, ovalHeight);// 4
 fw = CGRectGetWidth (rect) / ovalWidth;// 5
 fh = CGRectGetHeight (rect) / ovalHeight;// 6
 
 CGContextMoveToPoint(context, fw, fh/2); // 7
 CGContextAddArcToPoint(context, fw, fh, fw/2, fh, 1);// 8
 CGContextAddArcToPoint(context, 0, fh, 0, fh/2, 1);// 9
 CGContextAddArcToPoint(context, 0, 0, fw/2, 0, 1);// 10
 CGContextAddArcToPoint(context, fw, 0, fw, fh/2, 1); // 11
 CGContextClosePath(context);// 12
 
 CGContextRestoreGState(context);// 13
 }
 */

@end
