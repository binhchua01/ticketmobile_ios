//
//  LoginViewController.swift
//  TicketMobile
//
//  Created by Tran Tan Duc on 4/29/20.
//  Copyright © 2020 FPT. All rights reserved.


import UIKit

class LoginViewController2: UIViewController {

    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassWord: UITextField!
    @IBOutlet weak var txtOTP: UITextField!
    @IBOutlet weak var scrollview: UIScrollView!
    @IBOutlet weak var btnSignUp: UIButton!
    @IBOutlet weak var btnremember_touch: UIButton!
    @IBOutlet weak var ViewButton: UIView!
    

    var HUD: MBProgressHUD? = nil
    var updateVersionAlertView: UIAlertController? = nil
    var linkUpdateVersion: String? = nil

    var appDelegate: TicketMobileAppDelegate? = nil

    override func viewDidLoad() {
        super.viewDidLoad()
        ViewButton.layer.cornerRadius = 15
        ViewButton.layer.borderWidth = 2
        ViewButton.layer.borderColor = UIColor.white.cgColor
        CheckRemember()
        menuContainerViewController.leftMenuWidth = 0
        scrollview.backgroundColor = UIColor.clear
        scrollview.canCancelContentTouches = false
        scrollview.indicatorStyle = .white
        scrollview.isScrollEnabled = true

        let tap = UITapGestureRecognizer(target: self, action: #selector(tap(onView:)))
        view.addGestureRecognizer(tap)

        installIntoParsUUID()
        checkVersion()
        registerForKeyboardNotifications()

        appDelegate = UIApplication.shared.delegate as? TicketMobileAppDelegate
        NotificationCenter.default.addObserver(self, selector: #selector(receivedMessage(_:)), name: (appDelegate?.messageKey).map { NSNotification.Name(rawValue: $0) }, object: nil)


    }

    @objc override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    @IBAction func btnRemember_action(_ sender: Any) {
    if (btnremember_touch.isSelected == true) {
        self.btnremember_touch.setImage(UIImage(named: "uicheckbox_checked.png"), for: .normal)
        btnremember_touch.isSelected = false
    } else {
        btnremember_touch.isSelected = true
        self.btnremember_touch.setImage(UIImage(named: "uicheckbox_unchecked.png"), for: .normal)
    }
}
    @IBAction func btnSignUp_action(_ sender: Any) {
        if(!(SiUtils.checkNetworkAvailable())){
            let alert = UIAlertController(title: "Thông báo", message: "Kết nối thất bại, Vui lòng kiểm tra lại đường truyền", preferredStyle: .alert)
                       let cancelButton = UIAlertAction(title: "OK", style: .default, handler: nil)
                       alert.addAction(cancelButton)
                       self.present(alert, animated: true)
        }

        if (txtEmail.text == "") {
            let alert = UIAlertController(title: "Thông báo", message: "Chưa điền tên đăng nhập", preferredStyle: .alert)
            let cancelButton = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(cancelButton)
            self.present(alert, animated: true)
            return
        }

        //Kiểm tra mật khẩu
        if (txtPassWord.text == "") {
            let alert = UIAlertController(title: "Thông báo", message: "Chưa điền mật khẩu", preferredStyle: .alert)
            let cancelButton = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(cancelButton)
            self.present(alert, animated: true)
            return
        }

        //Kiểm tra email
        let arrUser = txtEmail.text?.components(separatedBy: "@")
        if arrUser!.count > 1 {
            if !validateEmail(txtEmail.text) {
                let alert = UIAlertController(title: "Thông báo", message: "Tên đăng nhập không hợp lệ", preferredStyle: .alert)
                let cancelButton = UIAlertAction(title: "OK", style: .default, handler: nil)
                alert.addAction(cancelButton)
                self.present(alert, animated: true)
                return
            }
        }
        self.showMBProcess()

        let shared = ShareData.instance()
        shared?.userProxy.doLogin(self.txtEmail.text, password: txtPassWord.text, andOTP: txtOTP.text, completeHandler: { result, errorCode, message in
            if (errorCode == "1") {
                let user = result as! UserRecord
                 shared?.currentUser = user
                if(self.btnremember_touch.isSelected == true) {
                    let dtuser : Data = SiUtils.encryptAESString(self.txtEmail.text, withKey: "ticketfpttelecomrad0411")
                    let dtpass : Data = SiUtils.encryptAESString(self.txtPassWord.text, withKey: "ticketfpttelecomrad0411")
                    let dict : NSMutableDictionary = NSMutableDictionary()
                    dict.setValue(dtuser, forKeyPath: "u_a")
                    dict.setValue(dtpass, forKeyPath: "u_p")
                    SiUtils.saveUserDefaults(dict as? [AnyHashable : Any])
                }
                else {
                    let dtuser : Data = SiUtils.encryptAESString("" , withKey: "ticketfpttelecomrad0411")
                    let dtpass : Data = SiUtils.encryptAESString("" , withKey: "ticketfpttelecomrad0411")
                    let dict : NSMutableDictionary = NSMutableDictionary()
                    dict.setValue(dtuser, forKeyPath: "u_a")
                    dict.setValue(dtpass, forKeyPath: "u_p")
                    SiUtils.saveUserDefaults(dict as? [AnyHashable : Any])
                }

                let nav = UINavigationController(rootViewController: self.setTicketNoAssignView()!)
                let leftMenuViewController = LeftMenuTableViewController()
                let container = MFSideMenuContainerViewController.container(withCenter: nav, leftMenuViewController: leftMenuViewController, rightMenuViewController: nil)
                self.view.window!.rootViewController = container

                shared?.currentUser.userPass = self.txtPassWord.text
                self.registerToken()
            }
            else {
                let alert = UIAlertController(title: "Thông báo", message: message, preferredStyle: .alert)
                let cancelButton = UIAlertAction(title: "OK", style: .default, handler: nil)
                alert.addAction(cancelButton)
                self.present(alert, animated: true)
            }
            self.hideMBProcess()
        }, errorHandler: { error in
                self.hideMBProcess()
                let alert = UIAlertController(title: "Thông báo", message: error?.localizedDescription, preferredStyle: .alert)
                let cancelButton = UIAlertAction(title: "OK", style: .default, handler: nil)
                alert.addAction(cancelButton)
                self.present(alert, animated: true)

                if let error = error {
                    print("\("\(error)")")
                    return

                }
                return
        })
}

    @objc func registerToken() {
        let shared = ShareData.instance()
            shared?.userProxy.registerToken(appDelegate?.deviceToken ?? "", andEmail: shared?.currentUser.userEmail, completeHandler: { result, errorCode, message in
            print("------ Register Token to web service: \(message ?? "")")
        }, errorHandler: { error in
                if let error = error {
                print( "------ Register Token to web service: \("\(error)")" )
                    return
            }
            return
            })
    }

    @objc func CheckRemember() {
        let userdt = SiUtils.getUserDefaults("u_a")
        let passdt = SiUtils.getUserDefaults("u_p")
        let username : String = SiUtils.decryptAESData(userdt as? Data, withKey: "ticketfpttelecomrad0411")
        let password : String = SiUtils.decryptAESData(passdt as? Data, withKey: "ticketfpttelecomrad0411")
        let Username = username.trimmingCharacters(in: CharacterSet.whitespaces)
        self.txtEmail.text = Username
        if !(Username == "") && !(password == "") {
            txtEmail.text = username
            txtPassWord.text = password
            btnremember_touch.isSelected = true
        }
    }

    @objc func tap(onView sender: UITapGestureRecognizer?) {
        scrollview.endEditing(true)
    }
    @objc func installIntoParsUUID() {
        let UDID = UIDevice.current.identifierForVendor?.uuidString
        UIDevice.instancesRespond(to: #selector(getter: UIDevice.identifierForVendor))
        var uniqueIdentifier : String? = UIDevice.current.identifierForVendor?.uuidString
// ios > 7
        PFInstallation.current()["gender"] = UDID ?? ""
        PFInstallation.current().saveInBackground()
    }

    func checkVersion() {
        let shared = ShareData.instance()
        shared?.appProxy.getCheckVersion("1.0.6", platform: "0", releaseDate: "24-06-2015", completeHandler: { result, errorCode, message in
            if (errorCode == "0") {
                if let dicResult =  result as? Dictionary<String, Any> {
                    let version = dicResult["Version"] as? String ?? ""
                    let isReleaseDate = dicResult["isReleaseDate"] as? String ?? ""
                    let flag = dicResult["flag"] as? String ?? ""
                    self.linkUpdateVersion = dicResult["Link"] as? String ?? ""
                    self.endCheckVersion(isReleaseDate, Version: version, Flag: flag)
                }
            } else {
                let alert = UIAlertController(title: "Thông báo", message: message, preferredStyle: .alert)
                let cancelButton = UIAlertAction(title: "OK", style: .default, handler: nil)
                alert.addAction(cancelButton)
                self.present(alert, animated: true)
            }
            self.hideMBProcess()
        } , errorHandler: { error in
                self.hideMBProcess()
                let alert = UIAlertController(title: "Thông báo", message: error?.localizedDescription, preferredStyle: .alert)
                let cancelButton = UIAlertAction(title: "OK", style: .default, handler: nil)
                alert.addAction(cancelButton)
                self.present(alert, animated: true)

                if let error = error {
                    print("\("\(error)")")
                }
                return
        })
    }

    @objc func showMBProcess() {

        HUD = MBProgressHUD.showAdded(to: view, animated: true)
        view.addSubview(HUD!)

        HUD!.delegate = self as? MBProgressHUDDelegate ?? nil
        HUD!.labelText = "Đang xử lý..."

        HUD!.show(true)
    }

    @objc func hideMBProcess() {
        HUD?.hide(true)
    }

    @objc func endCheckVersion(_ releaseDate: String?, Version: String?, Flag: String?) {
        if !(releaseDate == "15-03-2017") {
            if (Flag == "1") {
                let alertMessage = "Đã có phiên bản mới xin vui lòng update để tiếp tục sử dụng"
                let alertTitle = "Thông báo cập nhật"
                updateVersionAlertView = UIAlertController(title: alertTitle, message: alertMessage, preferredStyle: .alert)
                let cancelButton = UIAlertAction(title: "OK", style: .default, handler: { action in })
                updateVersionAlertView!.addAction(cancelButton)
                present(updateVersionAlertView!, animated: true)
                return
            }
        }
        btnSignUp.isEnabled = true
    }

     @objc func alertView(_ alertView: UIAlertController, clickedButtonAt buttonIndex: Int) {
        if alertView == updateVersionAlertView {
            print("OK action")
            if let url = URL(string: self.linkUpdateVersion!) {
                UIApplication.shared.openURL(url)
            }
            //home button press programmatically
            let app = UIApplication.shared
            app.perform(#selector(NSXPCListener.suspend))

            //wait 2 seconds while app is going background
            Thread.sleep(forTimeInterval: 1.0)

            //exit app when app is in background
            exit(0)
        }
    }

    @objc func registerForKeyboardNotifications() {

        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWasShown(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillBeHidden(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }

    @objc func keyboardWasShown(_ notification: Notification?) {
////        guard let keyboardFrame = notification?.userInfo![UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else { return }
////               scrollview.contentInset.bottom = view.convert(keyboardFrame.cgRectValue, from: nil).size.height
        let info = notification?.userInfo


        // Get the size of the keyboard from the userInfo dictionary.
        let keyboardSize = (info?[UIResponder.keyboardFrameBeginUserInfoKey] as AnyObject).cgRectValue.size

        let ViewOrigin = ViewButton.frame.origin

        let ViewHeight = ViewButton.frame.size.height

        var visibleRect = view.frame

        visibleRect.size.height -= keyboardSize.height

        let scrollPoint = CGPoint(x: 0.0, y: ViewOrigin.y - visibleRect.size.height + 2*ViewHeight )

        scrollview.setContentOffset(scrollPoint, animated: true)

    }

    @objc func keyboardWillBeHidden(_ notification: Notification?) {
        scrollview.contentInset.bottom = 0
        return
    }
    @objc func receivedMessage(_ notification: Notification?) {
        view.endEditing(true)
        if let dicUserInfo =  notification?.userInfo as? Dictionary<String, Any> {
            if let aps = dicUserInfo["aps"] as? Dictionary<String, Any> {
                if let alert = aps["alert"] as? Dictionary<String, Any> {
                    let title = alert["title"] as? String ?? ""
                    let message = alert["body"] as? String ?? ""
                    if (appDelegate?.appActived)! {
                        print("-------Showing notification...")
                        let imageName = "NotificationIcon"
                        let image = UIImage(named: imageName)
                        HDNotificationView.show(with: image, title: title, message: message, isAutoHide: false, onTouch: {
                        HDNotificationView.hide(onComplete: nil)
                        })
                        let application = UIApplication.shared
                        application.applicationIconBadgeNumber = application.applicationIconBadgeNumber - 1
                    }
                }
            }
        }
    }

    @objc func deregisterFromKeyboardNotifications() {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardDidHideNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }

    @objc func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent) {
        view.endEditing(true)
    }

    func validateEmail(_ candidate: String?) -> Bool {
        let string = candidate
        let regex = try! NSRegularExpression(pattern: "\\w+@[a-zA-Z_]+?\\.[a-zA-Z]{2,6}", options: .caseInsensitive)

        var numberOfMatches = 0
        numberOfMatches = regex.numberOfMatches(in: string ?? "", options: [], range: NSRange(location: 0, length: string?.count ?? 0))
        //   NSLog(@"numberOfMatches is: %lu", numberOfMatches);
        if numberOfMatches != 0 {
            return true
        }
        return false
    }

    @objc func setTicketNoAssignView() -> UIViewController? {
        let nibName = "ListTicketNoAssignViewController"
        let vc = ListTicketNoAssignViewController(nibName: nibName, bundle: nil)
        return vc
    }
}
