//
//  LeftMenuCell.h
//  MobiSales
//
//  Created by Nguyen Tan Tho on 1/21/15.
//  Copyright (c) 2015 Aryan Ghassemi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LeftMenuCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *lbl_Label;
@property (strong, nonatomic) IBOutlet UIImageView *ic_image;


@end
