//
//  HomeViewController.h
//
//
//  Created by THONT5 on 2/25/15.
//  Copyright (c) 2015 nguyentantho@live.com. All rights reserved.
//

#import <UIKit/UIKit.h>

static NSString *TicketChuaPhanCong = @"TICKET CHƯA PHÂN CÔNG";
static NSString *TicketChuaHoanThanh = @"TICKET CHƯA HOÀN THÀNH";
static NSString *TicketCuaToi = @"TICKET CỦA TÔI";
static NSString *TicketToiTao = @"TICKET TÔI TẠO";
static NSString *DanhSachTicketToiTao = @"DANH SÁCH TICKET TÔI TẠO";
static NSString *TaoTicketHoTro = @"TẠO TICKET HỖ TRỢ";
static NSString *ThongTinUngDung = @"THÔNG TIN ỨNG DỤNG";
static NSString *DangXuat = @"ĐĂNG XUẤT";
static NSString *ThoatUngDung = @"THOÁT ỨNG DỤNG";

@interface LeftMenuTableViewController : UIViewController

@end
