//
//  HomeViewController.h
//  Created by THONT5 on 2/25/15.
//  Copyright (c) 2015 nguyentantho@live.com. All rights reserved.

#import "LeftMenuTableViewController.h"
#import "../Helper/SiUtils.h"
#import "ShareData.h"
#import "LeftMenuCell.h"
#import "KeyValueModel.h"


#import "TicketOverviewViewController.h"
#import "ListTicketViewController.h"
#import "ListTicketNoAssignViewController.h"
#import "TicketSupportMyCreatedListViewController.h"
#import "CreateTicketSupportViewController.h"
#import "TicketMobile-Swift.h"

@interface LeftMenuTableViewController () <AppinfoDelegate, CreateTicketSupportViewDelegate ,UIActionSheetDelegate>
@property (weak, nonatomic) IBOutlet UITableView *listTableView;

@end

@implementation LeftMenuTableViewController {
    NSArray *menu;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    [self setMenu];
    
}

-(void)viewDidAppear:(BOOL)animated{
    self.listTableView.frame = CGRectMake(0, 0, self.view.frame.size.width, [SiUtils getScreenFrameSize].height);
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setMenu {
    ShareData *shared = [ShareData instance];
    NSString *userName = [shared.currentUser.userName uppercaseString];
    
    // UserName
    KeyValueModel *userItem = [[KeyValueModel alloc] initWithName:userName description:@[]];
    
    // Menu Ticket Trouble
    KeyValueModel *ttItem1 = [[KeyValueModel alloc] initWithName:TicketChuaPhanCong description:@"tonChuaPC"];
    KeyValueModel *ttItem2 = [[KeyValueModel alloc] initWithName:TicketChuaHoanThanh description:@"tonChuaHT"];
    KeyValueModel *ttItem3 = [[KeyValueModel alloc] initWithName:TicketCuaToi description:@"myTicket"];
    KeyValueModel *ttItem4 = [[KeyValueModel alloc] initWithName:TicketToiTao description:@"myTicketCreate"];
    NSArray *ticketTroubleMenu = [NSArray arrayWithObjects:ttItem1, ttItem2, ttItem3, ttItem4, nil];
    KeyValueModel *menu1 = [[KeyValueModel alloc] initWithName:@"TICKET SỰ CỐ" description:ticketTroubleMenu];
    
    // Menu Ticket Support
    KeyValueModel *tsItem1 = [[KeyValueModel alloc] initWithName:DanhSachTicketToiTao description:@"myTicketCreate"];
    KeyValueModel *tsItem2 = [[KeyValueModel alloc] initWithName:TaoTicketHoTro description:@"myTicketCreate"];
    NSArray *ticketSupportMenu = [NSArray arrayWithObjects:tsItem1, tsItem2, nil];
    KeyValueModel *menu2 = [[KeyValueModel alloc] initWithName:@"TICKET HỖ TRỢ" description:ticketSupportMenu];

    // Menu System App
    KeyValueModel *sItem1 = [[KeyValueModel alloc] initWithName:ThongTinUngDung description:@"ic_menu_info"];
    KeyValueModel *sItem2 = [[KeyValueModel alloc] initWithName:DangXuat description:@"ic_menu_logout"];
    KeyValueModel *sItem3 = [[KeyValueModel alloc] initWithName:ThoatUngDung description:@"ic_menu_logout"];
    NSArray *systemMenu = [NSArray arrayWithObjects:sItem1, sItem2, sItem3, nil];
    KeyValueModel *menu3 = [[KeyValueModel alloc] initWithName:@"HỆ THỐNG" description:systemMenu];
    
    // Menu
    menu = [NSArray arrayWithObjects:userItem, menu1, menu2, menu3, nil];

}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return menu.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    KeyValueModel *sectionItem = menu[section];
    NSArray *rowItems = sectionItem.Values;
    if (rowItems.count > 0) {
        return rowItems.count;
    }
    
    return 1;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        return 40;
    }
    return 30;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *sectionHeaderView = [[UIView alloc] initWithFrame:
                                 CGRectMake(0, 0, tableView.frame.size.width, 30)];
    sectionHeaderView.backgroundColor = [UIColor clearColor];

    UILabel *headerLabel = [[UILabel alloc] initWithFrame:
                            CGRectMake(sectionHeaderView.frame.origin.x + 15, sectionHeaderView.frame.origin.y, tableView.frame.size.width, sectionHeaderView.frame.size.height)];
    
    headerLabel.backgroundColor = [UIColor clearColor];
    [headerLabel setFont:[UIFont fontWithName:@"" size:15.0]];
    headerLabel.textColor = [UIColor colorWithRed:45.0/255.0 green:2.0/255.0 blue:12.0/255.0 alpha:1];

    UIImageView *iconImage = [[UIImageView alloc] initWithFrame:CGRectMake(headerLabel.frame.origin.x, headerLabel.frame.origin.y - 5, 35, 35)];
    iconImage.image = [UIImage imageNamed:@"ic_header_user"];
    
    UIImageView *line = [[UIImageView alloc] initWithFrame:CGRectMake(0, sectionHeaderView.frame.size.height, tableView.frame.size.width, 2)];
    line.backgroundColor = [UIColor orangeColor];
    
    [sectionHeaderView addSubview:headerLabel];

    KeyValueModel *sectionItem = menu[section];
    headerLabel.text = sectionItem.Key;

    if (section == 0) {
        headerLabel.frame = CGRectMake(headerLabel.frame.origin.x + iconImage.frame.size.width, headerLabel.frame.origin.y, headerLabel.frame.size.width, headerLabel.frame.size.height);
        [sectionHeaderView addSubview:iconImage];
        return sectionHeaderView;
    }
    
    [sectionHeaderView addSubview:line];
    
    return sectionHeaderView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        return 10;
    }
    return 40;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *simpleTableIdentifier = @"LeftMenuCell";
    LeftMenuCell *cell = (LeftMenuCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"LeftMenuCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
//    UIImageView *line = [[UIImageView alloc] initWithFrame:CGRectMake(17, 40, tableView.frame.size.width, 1)];
//    line.backgroundColor = [UIColor lightGrayColor];
//    [cell addSubview:line];

    // Configure the cell...
    NSString *title;
    NSString *imageName;
    KeyValueModel *cellModel;
    KeyValueModel *sectionItem = menu[indexPath.section];
    
    NSArray *rowItems = sectionItem.Values;
    if (rowItems.count < 1) {
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
        
    }
    
    cellModel = rowItems[indexPath.row];

    title = cellModel.Key;
    imageName = cellModel.Values;
    cell.lbl_Label.text = title;
    cell.ic_image.image = [UIImage imageNamed:imageName];

    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [self.listTableView deselectRowAtIndexPath:indexPath animated:YES];
    
    KeyValueModel *sectionItem = menu[indexPath.section];
    NSArray *rowItems = sectionItem.Values;
    if (rowItems.count < 1) {
        return;
        
    }
    
    KeyValueModel *cellModel;
    cellModel = rowItems[indexPath.row];
    UIViewController *vc = [self setViewControllerWithKey:cellModel.Key];
    if (vc == nil) {
        return;
    }
    
    UINavigationController *nav = [((UINavigationController *)self.menuContainerViewController.centerViewController) initWithRootViewController:vc ];
    
    self.menuContainerViewController.centerViewController = nav;
    [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
    
}

- (UIViewController *)setViewControllerWithKey:(NSString *)key {
    if ([key isEqual:TicketChuaPhanCong]) {
        return [self setTicketNoAssignView];
    }
    
    if ([key isEqual:TicketChuaHoanThanh]) {
        return [self setTicketAssignView];
    }
    
    if ([key isEqual:TicketCuaToi]) {
        return [self setTicketViewWithType:@"1"];
    }
    
    if ([key isEqual:TicketToiTao]) {
        return [self setTicketViewWithType:@"2"];
    }
    
    if ([key isEqual:DanhSachTicketToiTao]) {
        return [[TicketSupportMyCreatedListViewController alloc] init];
    }
    
    if ([key isEqual:TaoTicketHoTro]) {
        CreateTicketSupportViewController * createTicketSupportView = [[CreateTicketSupportViewController alloc] initWithViewType:createTicketSupport];
        createTicketSupportView.delegate = self;
        return createTicketSupportView;
    }
    
    if ([key isEqual:ThongTinUngDung]) {
        [self pushToMenuInfoView];
        return nil;
    }
    
    if ([key isEqual:DangXuat]) {
        [self logOut];
        return nil;
    }
    
    if ([key isEqual:ThoatUngDung]) {
        [self showActionSheetWithTitle:@"Bạn chắc chắn muốn thoát ứng dụng?" andTag:2];
        return nil;

    }
    return nil;
}

// Ticket chưa phân công
- (UIViewController *)setTicketNoAssignView {
    NSString *nibName = @"ListTicketNoAssignViewController";
    if (IS_IPAD) {
        nibName = @"ListTicketNoAssignViewController_iPad";
    }
    ListTicketNoAssignViewController *vc = [[ListTicketNoAssignViewController alloc]initWithNibName:nibName bundle:nil];
    return vc;
}

// Ticket chưa hoàn thành
- (UIViewController *)setTicketAssignView {
    NSString *nibName = @"TicketOverviewViewController";
    if (IS_IPAD)
    {
        nibName = @"TicketOverviewViewController_iPad";
    }
    TicketOverviewViewController *vc = [[TicketOverviewViewController alloc]initWithNibName:nibName bundle:nil];
    return vc;
}
// Ticket của tôi nếu type = 1
// Ticket tôi tạo nếu type = 2
- (UIViewController *)setTicketViewWithType:(NSString *)type {
    NSString *nibName = @"ListTicketViewController";
    if (IS_IPAD)
    {
        nibName = @"ListTicketViewController_iPad";
    }
    ListTicketViewController *vc = [[ListTicketViewController alloc]initWithNibName:nibName bundle:nil];
    vc.numMenu = type;
    
    return vc;
}
- (void)pushToMenuInfoView {
    AppInfoViewController *vc = [[AppInfoViewController alloc]initWithNibName:@"AppInfoViewController" bundle:nil];
    vc.delegate = self;
    [self presentPopupViewController:vc animationType:MJPopupViewAnimationSlideBottomBottom];
}

-(void)logOut{
    self.menuContainerViewController.menuState = MFSideMenuStateClosed;

    MFSideMenuContainerViewController *container = self.menuContainerViewController;
    
    LoginViewController2 *vc = [[LoginViewController2 alloc] initWithNibName:@"LoginViewController2" bundle:nil];
    [container setCenterViewController:vc];
    
}

- (void)exitApp {
    exit(0);
}

#pragma mark - CreateTicketSupportViewDelegate methods
- (void)pushToTicketSupportListView {
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:2];
    [self tableView:self.listTableView didSelectRowAtIndexPath:indexPath];
}

// AppInfo delegate method
-(void)test {
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationSlideBottomTop];
}

// Show thông báo thoát ứng dụng
- (void)showActionSheetWithTitle:(NSString *)title andTag:(NSInteger)tag {
    UIActionSheet *actionsheet = [[UIActionSheet alloc] initWithTitle:title delegate:self cancelButtonTitle:@"Không" destructiveButtonTitle:@"Có" otherButtonTitles:nil, nil];
    actionsheet.tag = tag;
    [actionsheet showInView:[UIApplication sharedApplication].keyWindow];
}

#pragma mark - UIActionSheet Delegate methods
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0 && actionSheet.tag == 2) {
        [self exitApp];
    }
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
