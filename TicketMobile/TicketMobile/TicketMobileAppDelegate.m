//
//  TicketMobileAppDelegate.m
//  TicketMobile
//
//  Created by HIEUPC on 3/3/15.
//  Copyright (c) 2015 FPT. All rights reserved.
//

#import "TicketMobileAppDelegate.h"
#import <AudioToolbox/AudioToolbox.h>
#import <Parse/Parse.h>
#import "Client/ShareData.h"
#import "IQKeyboardManager.h"

//add by DanTT
// method để back về view trước bằng cách vuốt màn hình sang phải
#import "UIViewController+MLTransition.h"

@implementation TicketMobileAppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    //    custom keyboard
    [[IQKeyboardManager sharedManager] setToolbarManageBehaviour:IQAutoToolbarByPosition];
    //add by DanTT
    // method để back về view trước bằng cách vuốt màn hình sang phải
//    if (IS_OS_7_OR_LATER) {
//        [UIViewController validatePanPackWithMLTransitionGestureRecognizerType:MLTransitionGestureRecognizerTypePan];
//    }
    
    [self initSideMenuControl];
    GAI *ga = [GAI sharedInstance];
    [ga trackerWithTrackingId:@"UA-62880680-2"];
    ga.trackUncaughtExceptions =YES;
    ga.dispatchInterval =30;
    
    
    [Parse setApplicationId:@"9Voh5OI0mEpE2Hky3vHjI467vZ1DI4bNvQgb0RgF"
                  clientKey:@"MNNt2ebxixkYmvnBrifrWB8EDPKEJeNlww8TCGyN"];
    
    _messageKey = @"onMessageReceived";
   
    UIUserNotificationType userNotificationTypes = (UIUserNotificationTypeAlert |
                                                    UIUserNotificationTypeBadge |
                                                    UIUserNotificationTypeSound);
    UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:userNotificationTypes
                                                                             categories:nil];
    
    
    if(IS_OS_8_OR_LATER){
       //  NSLog(@"---IOS 8");
        [application registerUserNotificationSettings:settings];
        [application registerForRemoteNotifications];
    }  // IS_OS_8_OR_LATER
    //Right, that is the point
    else{
       // NSLog(@"---IOS 7");
        UIRemoteNotificationType myTypes = UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeSound;
        [application registerForRemoteNotificationTypes:myTypes];

    }
    //register to receive notifications
   
    ShareData *shared = [ShareData instance];
    shared.currentUser.checkChanged=false;
   
     return YES;
}

- (void)application:(UIApplication *)app didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {

    NSString *token = [[deviceToken description] stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    
    
    token = [token stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    NSLog(@"device token: %@", token);
    _deviceToken = token;
    
    PFInstallation *currentInstallation = [PFInstallation currentInstallation];
    [currentInstallation setDeviceTokenFromData:deviceToken];
    currentInstallation.channels = @[@"global"];
    [currentInstallation saveInBackground];

}

- (void)application:(UIApplication *)app didFailToRegisterForRemoteNotificationsWithError:(NSError *)err {
    NSLog(@"------- Loi 02:%@",err.description);
}


- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
//    [PFPush handlePush:userInfo];
    [[NSNotificationCenter defaultCenter] postNotificationName:_messageKey object:nil userInfo:userInfo];
    
    // set vibrate when received notification while device set silent
    AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
    // set badge for app icon when received notification
    if (!_appActived) {
        [application setApplicationIconBadgeNumber:application.applicationIconBadgeNumber + 1];
    }
}

//- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
//    [[NSNotificationCenter defaultCenter] postNotificationName:_messageKey object:nil userInfo:userInfo];
//}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    _appActived = NO;
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    _appActived = YES;
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [application setApplicationIconBadgeNumber:0];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

#pragma mark SideMenuControl
- (void)initSideMenuControl {
    
    UIStoryboard *storyboard ;
    if(IS_IPHONE)
        storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:[NSBundle mainBundle]];
    else
        storyboard = [UIStoryboard storyboardWithName:@"Main_iPad" bundle:[NSBundle mainBundle]];
    
    MFSideMenuContainerViewController *container = (MFSideMenuContainerViewController *)self.window.rootViewController;
    
    NSString *firstViewControllerName = @"HomeViewController";
    
    UINavigationController *navigationController = [storyboard instantiateViewControllerWithIdentifier:firstViewControllerName];
//    UIViewController *leftSideMenuViewController = [storyboard instantiateViewControllerWithIdentifier:@"LeftMenuTableViewController"];
    
    [container setCenterViewController:navigationController];
//    [container setRightMenuViewController:leftMenu];
    [container setLeftMenuViewController:nil];
    [container setRightMenuWidth:0];
    
    //layout sideMenuControl
    container.shadow.opacity = 0.1f;
    container.shadow.radius = 2.0f;
    container.shadow.color = [UIColor blackColor];
    container.enableMenu = NO;
    
    
}


@end
