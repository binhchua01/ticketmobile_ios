//
//  main.m
//  TicketMobile
//
//  Created by HIEUPC on 3/3/15.
//  Copyright (c) 2015 FPT. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TicketMobileAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([TicketMobileAppDelegate class]));
    }
    
}
