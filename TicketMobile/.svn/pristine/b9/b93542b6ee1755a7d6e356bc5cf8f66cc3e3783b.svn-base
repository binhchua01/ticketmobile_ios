//
//  TicketProxy.m
//  TicketMobile
//
//  Created by HIEUPC on 3/11/15.
//  Copyright (c) 2015 FPT. All rights reserved.
//

#import "TicketProxy.h"
#import "SiUtils.h"
#import "MyCreatTicket.h"
#import "MyTicket.h"
#import "QueueTicket.h"
#import "TicketDetailRecord.h"
#import "DeviceTicketRecord.h"

#define methodmyCreatedTicket                       @"MyCreate"
#define methodmyTicket                              @"myTicket"
#define methodqueueExistedTicketList                @"QueueExistedTicket"
#define methodnewTicketList                         @"NewTicket"
#define methodticketByStatus                        @"ticketByStatus"
#define methodTicketInfo                            @"TicketInfo"
#define methodTicketDevice                          @"TicketDevice"
#define methodTicketDeviceUpdate                    @"ticketDeviceUpdate"
#define methodTicketUpdate                          @"ticketUpdate"
#define methodPhoneStaff                            @"PhoneStaff"
#define methodEstimatedDate                         @"EstimatedDate"
#define methodUpdateExpectedDate                    @"updateExpectedDate"

@implementation TicketProxy

#pragma mark - Get My Ticket
- (void)getMyCreatTicket:(NSString *)email completeHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    BaseOperation * callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI, methodmyCreatedTicket)];
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    //create json
    NSString *postString = @"";
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:email forKey:@"UserEmail"];
    
    postString = [dict JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        [self endgetMyCreatTicket:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err) {
        errHandler(err);
    };
    
    [callOp start];
}

- (void)endgetMyCreatTicket:(NSData *)result response:(NSURLResponse *)response
completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    jsonDict = [jsonDict objectForKey:@"Result"]?:nil;
    NSString *error_code = [NSString stringWithFormat:@"%@", [jsonDict objectForKey:@"ErrorCode"]?:nil] ;
    
    if(![error_code isEqualToString:@"1"]){
        NSString *error_description = StringFormat(@"%@",[jsonDict objectForKey:@"ErrorDescription"]); ;
        handler(nil, error_code, error_description);
        return;
    }
    NSDictionary *data = [jsonDict objectForKey:@"Data"]?:nil;
    if(data == nil) return;
    
    NSMutableArray *mArray = [NSMutableArray array];
    for(NSDictionary *d in data){
        MyCreatTicket *rc = [self ParseMyCreatTicketRecord:d];
        [mArray addObject:rc];
    }
    handler(mArray, error_code, @"");
    
}

-(MyCreatTicket *) ParseMyCreatTicketRecord:(NSDictionary *)dict{
    MyCreatTicket *rc = [[MyCreatTicket alloc]init];
    
    rc.Level = StringFormat(@"%@",[dict objectForKey:@"Level"]);
    rc.Queue = StringFormat(@"%@",[dict objectForKey:@"Queue"]);
    rc.Staff = StringFormat(@"%@",[dict objectForKey:@"Staff"]);
    rc.TicketID = StringFormat(@"%@",[dict objectForKey:@"TicketID"]);
    rc.TicketStatus = StringFormat(@"%@",[dict objectForKey:@"TicketStatus"]);
    rc.Title = StringFormat(@"%@",[dict objectForKey:@"Title"]);
    rc.UpdateDate = StringFormat(@"%@",[dict objectForKey:@"UpdateDate"]);
    
    rc.CreateTime = StringFormat(@"%@",[dict objectForKey:@"CreatedDate"]);
    rc.TimeWorked = StringFormat(@"%@", [dict objectForKey:@"TimeWorked"]);

    return rc;
}

#pragma mark - get MyTicket

- (void)getMyTicket:(NSString *)email completeHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    BaseOperation * callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI, methodmyTicket)];
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    //create json
    NSString *postString = @"";
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:email forKey:@"UserEmail"];
    
    postString = [dict JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        [self endgetMyTicket:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err) {
        errHandler(err);
    };
    
    [callOp start];
}

- (void)endgetMyTicket:(NSData *)result response:(NSURLResponse *)response
          completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    jsonDict = [jsonDict objectForKey:@"Result"]?:nil;
    NSString *error_code = [NSString stringWithFormat:@"%@", [jsonDict objectForKey:@"ErrorCode"]?:nil] ;
    
    if(![error_code isEqualToString:@"1"]){
        NSString *error_description = StringFormat(@"%@",[jsonDict objectForKey:@"ErrorDescription"]); ;
        handler(nil, error_code, error_description);
        return;
    }
    NSDictionary *data = [jsonDict objectForKey:@"Data"]?:nil;
    if(data == nil) return;
    
    NSMutableArray *mArray = [NSMutableArray array];
    for(NSDictionary *d in data){
        MyTicket *rc = [self ParseMyTicketRecord:d];
        [mArray addObject:rc];
    }
    handler(mArray, error_code, @"");
    
}

-(MyTicket *) ParseMyTicketRecord:(NSDictionary *)dict{
    MyTicket *rc = [[MyTicket alloc]init];
    
    rc.Level = StringFormat(@"%@",[dict objectForKey:@"Level"]);
    rc.ExistTime = StringFormat(@"%@",[dict objectForKey:@"ExistTime"]);
    rc.IssueName = StringFormat(@"%@",[dict objectForKey:@"IssueName"]);
    rc.TicketID = StringFormat(@"%@",[dict objectForKey:@"TicketID"]);
    rc.TicketStatus = StringFormat(@"%@",[dict objectForKey:@"TicketStatus"]);
    rc.Title = StringFormat(@"%@",[dict objectForKey:@"Title"]);
    
    rc.CreateTime = StringFormat(@"%@",[dict objectForKey:@"CreatedDate"]);
    rc.TimeWorked = StringFormat(@"%@",[dict objectForKey:@"EstimatedTime"]);
    return rc;
}

#pragma mark - get Queue Ticket not assign

- (void)getqueueExistedTicketList:(NSString *)userID completeHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    BaseOperation * callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI, methodqueueExistedTicketList)];
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    //create json
    NSString *postString = @"";
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:userID forKey:@"UserID"];
    
    postString = [dict JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        [self endqueueExistedTicketList:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err) {
        errHandler(err);
    };
    
    [callOp start];
}

- (void)endqueueExistedTicketList:(NSData *)result response:(NSURLResponse *)response
     completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    jsonDict = [jsonDict objectForKey:@"Result"]?:nil;
    NSString *error_code = [NSString stringWithFormat:@"%@", [jsonDict objectForKey:@"ErrorCode"]?:nil] ;
    
    if(![error_code isEqualToString:@"1"]){
        NSString *error_description = StringFormat(@"%@",[jsonDict objectForKey:@"ErrorDescription"]); ;
        handler(nil, error_code, error_description);
        return;
    }
    NSDictionary *data = [jsonDict objectForKey:@"Data"]?:nil;
    if(data == nil) return;
    
    NSMutableArray *mArray = [NSMutableArray array];
    for(NSDictionary *d in data){
        QueueTicket *rc = [self ParseQueueTicketRecord:d];
        [mArray addObject:rc];
    }
    handler(mArray, error_code, @"");
    
}

-(QueueTicket *) ParseQueueTicketRecord:(NSDictionary *)dict{
    QueueTicket *rc = [[QueueTicket alloc]init];
    
    rc.QueueID = StringFormat(@"%@",[dict objectForKey:@"QueueID"]);
    rc.QueueName = StringFormat(@"%@",[dict objectForKey:@"QueueName"]);
    rc.New = StringFormat(@"%@",[dict objectForKey:@"New"]);
    rc.Progress = StringFormat(@"%@",[dict objectForKey:@"InProgress"]);
    rc.NotClose = StringFormat(@"%@",[dict objectForKey:@"NotClose"]);
    rc.OverTime = StringFormat(@"%@",[dict objectForKey:@"OverTime"]);
    rc.isClicked=NO;
    return rc;
}

#pragma mark - get Queue Ticket not assign

- (void)getnewTicketList:(NSString *)userID completeHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    BaseOperation * callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI, methodnewTicketList)];
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    //create json
    NSString *postString = @"";
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:userID forKey:@"UserID"];
    
    postString = [dict JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        [self endnewTicketList:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err) {
        errHandler(err);
    };
    
    [callOp start];
}

- (void)endnewTicketList:(NSData *)result response:(NSURLResponse *)response
                completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    jsonDict = [jsonDict objectForKey:@"Result"]?:nil;
    NSString *error_code = [NSString stringWithFormat:@"%@", [jsonDict objectForKey:@"ErrorCode"]?:nil] ;
    
    if(![error_code isEqualToString:@"1"]){
        NSString *error_description = StringFormat(@"%@",[jsonDict objectForKey:@"ErrorDescription"]); ;
        handler(nil, error_code, error_description);
        return;
    }
    NSDictionary *data = [jsonDict objectForKey:@"Data"]?:nil;
    if(data == nil) return;
    
    NSMutableArray *mArray = [NSMutableArray array];
    for(NSDictionary *d in data){
        MyTicket *rc = [self ParseMyTicketRecord:d];
        [mArray addObject:rc];
    }
    handler(mArray, error_code, @"");
    
}

#pragma mark - get MyTicket Status

- (void)getTicketByStatus:(NSString *)queue Status:(NSString *)status UserID:(NSString *)userId completeHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    BaseOperation * callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI, methodticketByStatus)];
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    //create json
    NSString *postString = @"";
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:queue forKey:@"Queue"];
    [dict setObject:status forKey:@"TicketStatus"];
    [dict setObject:userId forKey:@"UserID"];
    
    postString = [dict JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        [self endgetTicketByStatus:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err) {
        errHandler(err);
    };
    
    [callOp start];
}

- (void)endgetTicketByStatus:(NSData *)result response:(NSURLResponse *)response
     completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    jsonDict = [jsonDict objectForKey:@"Result"]?:nil;
    NSString *error_code = [NSString stringWithFormat:@"%@", [jsonDict objectForKey:@"ErrorCode"]?:nil] ;
    
    if(![error_code isEqualToString:@"1"]){
        NSString *error_description = StringFormat(@"%@",[jsonDict objectForKey:@"ErrorDescription"]); ;
        handler(nil, error_code, error_description);
        return;
    }
    NSDictionary *data = [jsonDict objectForKey:@"Data"]?:nil;
    if(data == nil) return;
    
    NSMutableArray *mArray = [NSMutableArray array];
    for(NSDictionary *d in data){
        MyTicket *rc = [self ParseTicketStatusRecord:d];
        [mArray addObject:rc];
    }
    handler(mArray, error_code, @"");
    
}

-(MyTicket *) ParseTicketStatusRecord:(NSDictionary *)dict{
    MyTicket *rc = [[MyTicket alloc]init];
    
    rc.Level = StringFormat(@"%@",[dict objectForKey:@"Level"]);
    rc.ExistTime = StringFormat(@"%@",[dict objectForKey:@"ExistTime"]);
    rc.IssueName = StringFormat(@"%@",[dict objectForKey:@"IssueName"]);
    rc.TicketID = StringFormat(@"%@",[dict objectForKey:@"TicketID"]);
    rc.TicketStatus = StringFormat(@"%@",[dict objectForKey:@"TicketStatus"]);
    rc.Title = StringFormat(@"%@",[dict objectForKey:@"Title"]);
    
    rc.CreateTime = StringFormat(@"%@",[dict objectForKey:@"CreatedDate"]);
    rc.TimeWorked = StringFormat(@"%@",[dict objectForKey:@"EstimatedTime"]);
    return rc;
}

#pragma mark - get MyTicket Info

- (void)getTicketInfo:(NSString *)ticketId completeHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    BaseOperation * callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI, methodTicketInfo)];
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    //create json
    NSString *postString = @"";
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    
    [dict setObject:ticketId forKey:@"TicketID"];
    
    postString = [dict JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        [self endgetTicketInfo:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err) {
        errHandler(err);
    };
    
    [callOp start];
}

- (void)endgetTicketInfo:(NSData *)result response:(NSURLResponse *)response
           completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    jsonDict = [jsonDict objectForKey:@"Result"]?:nil;
    NSString *error_code = [NSString stringWithFormat:@"%@", [jsonDict objectForKey:@"ErrorCode"]?:nil] ;
    
    if(![error_code isEqualToString:@"1"]){
        NSString *error_description = StringFormat(@"%@",[jsonDict objectForKey:@"ErrorDescription"]); ;
        handler(nil, error_code, error_description);
        return;
    }
    NSArray *data = [jsonDict objectForKey:@"Data"]?:nil;
    if((int)data.count <= 0) {
        error_code = @"0";
        handler(nil, error_code, @"");
        return;
    }
    
//    NSMutableArray *mArray = [NSMutableArray array];
//    for(NSDictionary *d in data){
//        TicketDetailRecord *rc = [self ParseTicketInfoRecord:d];
//        [mArray addObject:rc];
//    }
    TicketDetailRecord *rc = [self ParseTicketInfoRecord:[data objectAtIndex:0]];
    handler(rc, error_code, @"");
    
}

-(TicketDetailRecord *) ParseTicketInfoRecord:(NSDictionary *)dict{
    TicketDetailRecord *rc = [[TicketDetailRecord alloc]init];
    
    rc.Branch       = StringFormat(@"%@",[dict objectForKey:@"Branch"]);
    rc.BranchID     = StringFormat(@"%@",[dict objectForKey:@"BranchID"]);
    rc.CricLev      = StringFormat(@"%@",[dict objectForKey:@"CricLev"]);
    rc.CusQty       = StringFormat(@"%@",[dict objectForKey:@"CusQty"]);
    rc.CusType      = StringFormat(@"%@",[dict objectForKey:@"CusType"]);
    rc.Description  = StringFormat(@"%@",[dict objectForKey:@"Description"]);
    rc.DeviceType   = StringFormat(@"%@",[dict objectForKey:@"DeviceTypeName"]);
    rc.Effect       = StringFormat(@"%@",[dict objectForKey:@"Effect"]);
    rc.FoundIPPhone = StringFormat(@"%@",[dict objectForKey:@"FoundIPPhone"]);
    rc.FoundMobile  = StringFormat(@"%@",[dict objectForKey:@"FoundMobile"]);
    rc.FoundStaff   = StringFormat(@"%@",[dict objectForKey:@"FoundStaff"]);
    rc.Issue        = StringFormat(@"%@",[dict objectForKey:@"IssueID"]);
    rc.IssueGroup   = StringFormat(@"%@",[dict objectForKey:@"IssueGroup"]);
    rc.Level        = StringFormat(@"%@",[dict objectForKey:@"Level"]);
    rc.Location     = StringFormat(@"%@",[dict objectForKey:@"Location"]);
    rc.LocationID   = StringFormat(@"%@",[dict objectForKey:@"LocationID"]);
    rc.OperateStaff = StringFormat(@"%@",[dict objectForKey:@"OperateStaff"]);
    rc.PayTVQty     = StringFormat(@"%@",[dict objectForKey:@"PayTVQty"]);
    rc.ProcessIPPhone   = StringFormat(@"%@",[dict objectForKey:@"ProcessIPPhone"]);
    rc.ProcessMobile    = StringFormat(@"%@",[dict objectForKey:@"ProcessMobile"]);
    rc.ProcessStaff     = StringFormat(@"%@",[dict objectForKey:@"ProcessStaff"]);
    rc.Queue            = StringFormat(@"%@",[dict objectForKey:@"Queue"]);
    rc.Reason           = StringFormat(@"%@",[dict objectForKey:@"Reason"]);
    rc.RequiredDate     = StringFormat(@"%@",[dict objectForKey:@"RequiredDate"]);
    rc.ServiceType      = StringFormat(@"%@",[dict objectForKey:@"ServiceType"]);
    rc.TicketStatus     = StringFormat(@"%@",[dict objectForKey:@"TicketStatus"]);
    rc.Type             = StringFormat(@"%@",[dict objectForKey:@"DeviceType"]);
    rc.VisorStaff       = StringFormat(@"%@",[dict objectForKey:@"VisorStaff"]);
    
    rc.expectedDate     = StringFormat(@"%@",[dict objectForKey:@"ExpectedDate"]);
    
    // add by dantt 8.1.2016
    rc.ReasonDetail     = StringFormat(@"%@",[dict objectForKey:@"ReasonDetail"]);
    rc.IssueDate        = StringFormat(@"%@",[dict objectForKey:@"IssueDate"]);
    rc.CreatedDate      = StringFormat(@"%@",[dict objectForKey:@"CreatedDate"]);
    
    return rc;
}

#pragma mark - get Device Ticket

- (void)getTicketDevice:(NSString *)ticketId completeHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    BaseOperation * callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI, methodTicketDevice)];
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    //create json
    NSString *postString = @"";
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    
    [dict setObject:ticketId forKey:@"TicketID"];
    
    postString = [dict JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        [self endgetTicketDevice:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err) {
        errHandler(err);
    };
    
    [callOp start];
}

- (void)endgetTicketDevice:(NSData *)result response:(NSURLResponse *)response
       completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    jsonDict = [jsonDict objectForKey:@"Result"]?:nil;
    NSString *error_code = [NSString stringWithFormat:@"%@", [jsonDict objectForKey:@"ErrorCode"]?:nil] ;
    
    if(![error_code isEqualToString:@"1"]){
        NSString *error_description = StringFormat(@"%@",[jsonDict objectForKey:@"ErrorDescription"]); ;
        handler(nil, error_code, error_description);
        return;
    }
    NSDictionary *data = [jsonDict objectForKey:@"Data"]?:nil;
    if(data == nil) return;
    
        NSMutableArray *mArray = [NSMutableArray array];
        for(NSDictionary *d in data){
            DeviceTicketRecord *rc = [self ParseDeviceTicketRecord:d];
            [mArray addObject:rc];
        }
    handler(mArray, error_code, @"");
    
}

-(DeviceTicketRecord *) ParseDeviceTicketRecord:(NSDictionary *)dict{
    DeviceTicketRecord *rc = [[DeviceTicketRecord alloc]init];
    
    rc.DeviceName = StringFormat(@"%@",[dict objectForKey:@"DeviceName"]);
    rc.ErrorTimes = StringFormat(@"%@",[dict objectForKey:@"ErrorTimes"]);
    rc.Status = StringFormat(@"%@",[dict objectForKey:@"Status"]);
    rc.UpdatedBy = StringFormat(@"%@",[dict objectForKey:@"UpdatedBy"]);
    rc.UpdatedDate = StringFormat(@"%@",[dict objectForKey:@"UpdatedDate"]);
   
    return rc;
}

#pragma mark - get phone staff

- (void)getPhoneStaff:(NSString *)staffemail completeHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    BaseOperation * callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI, methodPhoneStaff)];
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    //create json
    NSString *postString = @"";
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:staffemail forKey:@"StaffEmail"];
    
    postString = [dict JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        [self endgetPhoneStaff:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err) {
        errHandler(err);
    };
    
    [callOp start];
}

- (void)endgetPhoneStaff:(NSData *)result response:(NSURLResponse *)response
                completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    jsonDict = [jsonDict objectForKey:@"Result"]?:nil;
    NSString *error_code = [NSString stringWithFormat:@"%@", [jsonDict objectForKey:@"ErrorCode"]?:nil] ;
    
    if(![error_code isEqualToString:@"1"]){
        NSString *error_description = StringFormat(@"%@",[jsonDict objectForKey:@"ErrorDescription"]); ;
        handler(nil, error_code, error_description);
        return;
    }
    NSArray *data = [jsonDict objectForKey:@"Data"]?:nil;
    if((int)data.count <= 0){
        error_code = @"0";
        handler(nil, error_code, @"");
        return;
    }
    
    NSDictionary *mArray = nil;
    NSString *Phone = nil, *IPPhone;
    
    Phone = [[data objectAtIndex:0] objectForKey:@"Mobile"]?:nil;
    IPPhone = [[data objectAtIndex:0] objectForKey:@"IPPhone"]?:nil;
    
    mArray = [NSDictionary dictionaryWithObjectsAndKeys:
                       Phone, @"Phone",
                       IPPhone, @"IPPhone", nil];
    
    handler(mArray, error_code, @"");
    
}

#pragma mark - Update Device Ticket

- (void)getTicketDeviceUpdate:(NSString *)devicename Status:(NSString *)status TicketId:(NSString *)ticketid UserId:(NSString *)userid Effect:(NSString *)effect CusQty:(NSString *)cusqty Keycode:(NSString *)keycode completeHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    BaseOperation * callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI, methodTicketDeviceUpdate)];
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    //create json
    NSString *postString = @"";
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    
    [dict setObject:devicename?:@"" forKey:@"DeviceName"];
    [dict setObject:status?:@"" forKey:@"Status"];
    [dict setObject:ticketid?:@"" forKey:@"TicketID"];
    [dict setObject:[NSNumber numberWithInt:[userid?:@"" intValue]] forKey:@"UserID"];
    [dict setObject:[NSNumber numberWithInt:[effect?:@"" intValue]] forKey:@"Effect"];
    [dict setValue:[NSNumber numberWithInt:[cusqty?:@"" intValue]] forKey:@"CusQty"];
    [dict setObject:keycode?:@"" forKey:@"KeyCode"];
    
    postString = [dict JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        [self endgetTicketDeviceUpdate:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err) {
        errHandler(err);
    };
    
    [callOp start];
}

- (void)endgetTicketDeviceUpdate:(NSData *)result response:(NSURLResponse *)response
         completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    jsonDict = [jsonDict objectForKey:@"Result"]?:nil;
    NSString *error_code = [NSString stringWithFormat:@"%@", [jsonDict objectForKey:@"ErrorCode"]?:nil] ;
    
    if(![error_code isEqualToString:@"1"]){
        NSString *error_description = StringFormat(@"%@",[jsonDict objectForKey:@"ErrorDescription"]); ;
        handler(nil, error_code, error_description);
        return;
    }

    handler(nil, error_code, @"");
    
}

#pragma mark - Update Device Ticket

- (void)getTicketUpdate:(NSString *)ticketid LocationID:(NSString *)locationid BrachID:(NSString *)branchid Issue:(NSString *)issue Reason:(NSString *)reason ServiceType:(NSString *)servicetype CustType:(NSString *)custype Effect:(NSString *)effect Level:(NSString *)level Description:(NSString *)description TicketStatus:(NSString *)ticketstatus Queue:(NSString *)queue QueueName:(NSString *)queueName OperateStaff:(NSString *)operatestaff VisorStaff:(NSString *)visorstaff ProcessStaff:(NSString *)processstaff ProcessMobile:(NSString *)processmobile ProcessIPPhone:(NSString *)processipphone CricLev:(NSString *)criclev CusQty:(NSString *)cusqty PayTVQty:(NSString *)paytvqty UpdatedBy:(NSString *)updatedby UpdatedEmail:(NSString *)updatedemail IssueDate:(NSString*)issueDate RequiredDate:(NSString*)requiredDate ExpectedDate:(NSString*)expectedDate ReasonDetail:(NSString*)reasonDetail KeyCode:(NSString *)keycode  completeHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    BaseOperation * callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI, methodTicketUpdate)];
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    //create json
    NSString *postString = @"";
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    
    [dict setObject:ticketid?:@"" forKey:@"TicketID"];
    [dict setObject:locationid?:@"" forKey:@"LocationID"];
    [dict setObject:branchid?:@"" forKey:@"BrachID"];
    [dict setObject:issue?:@"" forKey:@"Issue"];
    [dict setObject:reason?:@"" forKey:@"Reason"];
    [dict setObject:servicetype?:@"" forKey:@"ServiceType"];
    [dict setObject:custype?:@"" forKey:@"CusType"];
    [dict setObject:effect?:@"" forKey:@"Effect"];
    [dict setObject:level?:@"" forKey:@"Level"];
    [dict setObject:description?:@"" forKey:@"Description"];
    [dict setObject:ticketstatus?:@"" forKey:@"TicketStatus"];
    [dict setObject:queue?:@"" forKey:@"Queue"];
    [dict setObject:queueName?:@"" forKey:@"QueueName"];
    [dict setObject:operatestaff?:@"" forKey:@"OperateStaff"];
    [dict setObject:visorstaff?:@"" forKey:@"VisorStaff"];
    [dict setObject:processstaff?:@"" forKey:@"ProcessStaff"];
    [dict setObject:processmobile?:@"" forKey:@"ProcessMobile"];
    [dict setObject:processipphone?:@"" forKey:@"ProcessIPPhone"];
    [dict setObject:criclev?:@"" forKey:@"CricLev"];
    [dict setObject:cusqty?:@"" forKey:@"CusQty"];
    [dict setObject:paytvqty?:@"" forKey:@"PayTVQty"];
    [dict setObject:updatedby?:@"" forKey:@"UpdatedBy"];
    [dict setObject:updatedemail?:@"" forKey:@"UpdatedEmail"];
    [dict setObject:issueDate?:@"" forKey:@"IssueDate"];
    [dict setObject:requiredDate?:@"" forKey:@"RequiredDate"];
    [dict setObject:expectedDate?:@"" forKey:@"ExpectedDate"];
    [dict setObject:reasonDetail?:@"" forKey:@"ReasonDetail"];
    [dict setObject:keycode?:@"" forKey:@"KeyCode"];
    
    postString = [dict JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        [self endgetTicketUpdate:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err) {
        errHandler(err);
    };
    
    [callOp start];
}

- (void)endgetTicketUpdate:(NSData *)result response:(NSURLResponse *)response
               completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    jsonDict = [jsonDict objectForKey:@"Result"]?:nil;
    NSString *error_code = [NSString stringWithFormat:@"%@", [jsonDict objectForKey:@"ErrorCode"]?:nil] ;
    
    if(![error_code isEqualToString:@"1"]){
        NSString *error_description = StringFormat(@"%@",[jsonDict objectForKey:@"ErrorDescription"]); ;
        handler(nil, error_code, error_description);
        return;
    }
    
    handler(nil, error_code, @"");
    
}
// API didn't used
#pragma mark - Update Expected Date

- (void)getUpdateExpectedDate:(NSString *)ticketId expectedDate:(NSString *)expectedDate completeHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    BaseOperation * callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI, methodUpdateExpectedDate)];
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    //create json
    NSString *postString = @"";
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    
    [dict setObject:ticketId forKey:@"TicketID"];
    [dict setObject:expectedDate forKey:@"ExpectedDate"];
    
    postString = [dict JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        [self endUpdateExpectedDate:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err) {
        errHandler(err);
    };
    
    [callOp start];
}

- (void)endUpdateExpectedDate:(NSData *)result response:(NSURLResponse *)response
       completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    jsonDict = [jsonDict objectForKey:@"Result"]?:nil;
    NSString *error_code = [NSString stringWithFormat:@"%@", [jsonDict objectForKey:@"ErrorCode"]?:nil] ;
//    
//    if(![error_code isEqualToString:@"1"]){
//        NSString *error_description = StringFormat(@"%@",[jsonDict objectForKey:@"ErrorDescription"]); ;
//        handler(nil, error_code, error_description);
//        return;
//    }
    NSArray *data = [jsonDict objectForKey:@"Data"]?:nil;
//    if((int)data.count <= 0) {
//        error_code = @"0";
//        handler(nil, error_code, @"");
//        return;
//    }
    NSString *error_description = StringFormat(@"%@",[jsonDict objectForKey:@"ErrorDescription"]);
    handler(data, error_code, error_description);
    return;
    
    //TicketDetailRecord *rc = [self ParseTicketInfoRecord:[data objectAtIndex:0]];
    //handler(rc, error_code, @"");
    
}

#pragma mark - get Estimated Date

- (void)getEstimatedDate:(NSString *)cricLev ReasonDetailID:(NSString*)reasonDetailID CreatedDate:(NSString*)createdDate completeHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    BaseOperation * callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI, methodEstimatedDate)];
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    //create json
    NSString *postString = @"";
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:cricLev forKey:@"CricLev"];
    [dict setObject:reasonDetailID ?:@"" forKey:@"ReasonDetailID"];
    [dict setObject:createdDate ?:@"" forKey:@"CreatedDate"];

    postString = [dict JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        [self endGetEstimatedDate:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err) {
        errHandler(err);
    };
    
    [callOp start];
}

- (void)endGetEstimatedDate:(NSData *)result response:(NSURLResponse *)response
       completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    jsonDict = [jsonDict objectForKey:@"Result"]?:nil;
    NSString *error_code = [NSString stringWithFormat:@"%@", [jsonDict objectForKey:@"ErrorCode"]?:nil] ;
    
    if(![error_code isEqualToString:@"1"]){
        NSString *error_description = StringFormat(@"%@",[jsonDict objectForKey:@"ErrorDescription"]); ;
        handler(nil, error_code, error_description);
        return;
    }
    NSArray *data = [jsonDict objectForKey:@"Data"]?:nil;
    if((int)data.count <= 0){
        handler(nil, error_code, @"");
        return;
    }
    
    handler(data, error_code, @"");
    
}

@end
