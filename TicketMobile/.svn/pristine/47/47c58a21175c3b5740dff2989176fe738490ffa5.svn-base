//
//  FeedbackProxy.m
//  TicketMobile
//
//  Created by HIEUPC on 3/13/15.
//  Copyright (c) 2015 FPT. All rights reserved.
//

#import "FeedbackProxy.h"
#import "ListFeedbackRecord.h"
#import "FeedbackDetailRecord.h"

#define methodReponseInfo                       @"ReponseInfo"
#define methodResponseDetail                    @"responseDetail"
#define methodResponseSend                      @"ResponseSend"

@implementation FeedbackProxy

#pragma mark - Get My Ticket Feedback
- (void)getReponseInfo:(NSString *)ticketId completeHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    BaseOperation * callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI, methodReponseInfo)];
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    //create json
    NSString *postString = @"";
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:ticketId forKey:@"TicketID"];
    
    postString = [dict JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        [self endgetReponseInfo:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err) {
        errHandler(err);
    };
    
    [callOp start];
}

- (void)endgetReponseInfo:(NSData *)result response:(NSURLResponse *)response
        completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    jsonDict = [jsonDict objectForKey:@"Result"]?:nil;
    NSString *error_code = [NSString stringWithFormat:@"%@", [jsonDict objectForKey:@"ErrorCode"]?:nil] ;
    
    if(![error_code isEqualToString:@"1"]){
        NSString *error_description = StringFormat(@"%@",[jsonDict objectForKey:@"ErrorDescription"]); ;
        handler(nil, error_code, error_description);
        return;
    }
    NSDictionary *data = [jsonDict objectForKey:@"Data"]?:nil;
    if(data == nil) return;
    
    NSMutableArray *mArray = [NSMutableArray array];
    for(NSDictionary *d in data){
        ListFeedbackRecord *rc = [self ParseListFeedbackRecord:d];
        [mArray addObject:rc];
    }
    handler(mArray, error_code, @"");
    
}

-(ListFeedbackRecord *) ParseListFeedbackRecord:(NSDictionary *)dict{
    ListFeedbackRecord *rc = [[ListFeedbackRecord alloc]init];
    
    rc.CreateDate = StringFormat(@"%@",[dict objectForKey:@"CreatedDate"]);
    rc.FromAdr = StringFormat(@"%@",[dict objectForKey:@"FromAdr"]);
    rc.ID = StringFormat(@"%@",[dict objectForKey:@"ID"]);
    rc.Subject = StringFormat(@"%@",[dict objectForKey:@"Subject"]);
    return rc;
}

#pragma mark - Get detail Ticket Feedback
- (void)getResponseDetail:(NSString *)ID completeHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    BaseOperation * callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI, methodResponseDetail)];
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    //create json
    NSString *postString = @"";
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:ID forKey:@"ID"];
    
    postString = [dict JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        [self endgetResponseDetail:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err) {
        errHandler(err);
    };
    
    [callOp start];
}

- (void)endgetResponseDetail:(NSData *)result response:(NSURLResponse *)response
           completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    jsonDict = [jsonDict objectForKey:@"Result"]?:nil;
    NSString *error_code = [NSString stringWithFormat:@"%@", [jsonDict objectForKey:@"ErrorCode"]?:nil] ;
    
    if(![error_code isEqualToString:@"1"]){
        NSString *error_description = StringFormat(@"%@",[jsonDict objectForKey:@"ErrorDescription"]); ;
        handler(nil, error_code, error_description);
        return;
    }
    NSArray *data = [jsonDict objectForKey:@"Data"]?:nil;
    if(data == nil) return;
    
    //    NSMutableArray *mArray = [NSMutableArray array];
    //    for(NSDictionary *d in data){
    //        ListFeedbackRecord *rc = [self ParseListFeedbackRecord:d];
    //        [mArray addObject:rc];
    //    }
    FeedbackDetailRecord *rc = [self ParseFeedbackDetailRecord:[data objectAtIndex:0]];
    handler(rc, error_code, @"");
    
}

-(FeedbackDetailRecord *) ParseFeedbackDetailRecord:(NSDictionary *)dict{
    FeedbackDetailRecord *rc = [[FeedbackDetailRecord alloc]init];
    
    rc.CreatedDate = StringFormat(@"%@",[dict objectForKey:@"CreatedDate"]);
    rc.FromAdr = StringFormat(@"%@",[dict objectForKey:@"FromAdr"]);
    rc.ToAdr = StringFormat(@"%@",[dict objectForKey:@"ToAdr"]);
    rc.Subject = StringFormat(@"%@",[dict objectForKey:@"Subject"]);
    rc.BodyHTML = StringFormat(@"%@",[dict objectForKey:@"BodyHTML"]);
    rc.Cc = StringFormat(@"%@",[dict objectForKey:@"Cc"]);
    rc.QueueAdr = StringFormat(@"%@",[dict objectForKey:@"QueueAdr"]);
    return rc;
}

#pragma mark - Send email

- (void)getResponseSend:(NSString *)subject FromAdr:(NSString *)fromadr ToAdr:(NSString *)toadr Cc:(NSString *)cc QueueAdr:(NSString *)queueadr BodyHTML:(NSString *)bodyhtml UpdateBy:(NSString *)updateby UpdatedEmail:(NSString *)updatedemail TicketID:(NSString *)ticketid completeHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    BaseOperation * callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI, methodResponseSend)];
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    //create json
    NSString *postString = @"";
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    
    [dict setObject:subject forKey:@"Subject"];
    [dict setObject:fromadr?:@"" forKey:@"FromAdr"];
    [dict setObject:toadr forKey:@"ToAdr"];
    [dict setObject:cc?:@"" forKey:@"Cc"];
    [dict setObject:queueadr forKey:@"QueueAdr"];
    [dict setObject:bodyhtml forKey:@"BodyHTML"];
    [dict setObject:updateby forKey:@"UpdatedBy"];
    [dict setObject:updatedemail forKey:@"UpdatedEmail"];
    [dict setObject:ticketid forKey:@"TicketID"];
    
    postString = [dict JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        [self endgetResponseSend:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err) {
        errHandler(err);
    };
    
    [callOp start];
}

- (void)endgetResponseSend:(NSData *)result response:(NSURLResponse *)response
               completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    jsonDict = [jsonDict objectForKey:@"Result"]?:nil;
    NSString *error_code = [NSString stringWithFormat:@"%@", [jsonDict objectForKey:@"ErrorCode"]?:nil] ;
    
    if(![error_code isEqualToString:@"1"]){
        NSString *error_description = StringFormat(@"%@",[jsonDict objectForKey:@"ErrorDescription"]); ;
        handler(nil, error_code, error_description);
        return;
    }
    
    handler(nil, error_code, @"");
    
}


@end
